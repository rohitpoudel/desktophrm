﻿using System;
using System.ComponentModel;

namespace HRM
{
    public class Section
    {
        public Int64 SNo { get; set; }
        public int Id { get; set; }
		[DisplayName("Section Code")]
		public string SectionCode { get; set; }
		[DisplayName("Section Name")]
		public string SectionName { get; set; }
		[DisplayName("Department")]
		public int DepartmentId { get; set; }
		[DisplayName("Department")]
		public string DepartmentName { get; set; }
        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
    public class SectionViewModel
    {
        public Int64 SNo { get; set; }
        public int Id { get; set; }
		[DisplayName("Section Code")]
		public string SectionCode { get; set; }
		[DisplayName("Section Name")]
		public string SectionName { get; set; }
		[DisplayName("Department")]
		public int DepartmentId { get; set; }
		[DisplayName("Department")]
		public string DepartmentName { get; set; }
        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool DataExist { get; set; }
    }
}