﻿using System;
namespace HRM
{
    public class Designation
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		public string DesignationCode { get; set; }
		public string DesignationName { get; set; }
		public string DesignationLevel { get; set; }
		public double? MaxSalary { get; set; }
		public double? MinSalary { get; set; }
		public bool IsActive { get; set; }
		public char Event { get; set; }     
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool DataExist { get; set; }

    }
    public class DesignationViewModel
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		public string DesignationCode { get; set; }
		public string DesignationName { get; set; }
		public string DesignationLevel { get; set; }
		public double? MaxSalary { get; set; }
		public double? MinSalary { get; set; }
		public bool IsActive { get; set; }	
		public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool DataExist { get; set; }



    }


}
