﻿using System;

namespace HRM
{
    public class Branch
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		public string BranchName { get; set; }
		public string Code { get; set; }
		public string Address { get; set; }
		public char  Event { get; set; }
        public bool IsDeleted { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int CompanyId { get; set; }
    }
    public class BranchViewModel
    {
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		public string BranchName { get; set; }
		public string Code { get; set; }
		public string Address { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}