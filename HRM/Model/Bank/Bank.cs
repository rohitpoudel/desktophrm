﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace HRM
{
    public class Bank
    {
		public Int64 SNo { get; set; }
		public int Id { get; set; }
        [Required]
        [DisplayName("Bank Code")]
        public string BankCode { get; set; }
        [Required]
        [StringLength(50)]
        [DisplayName("Bank Name")]
        public string BankName { get; set; }
        [Required]
        [DisplayName("Bank Address")]
        [StringLength(50)]
        public string BankAddress { get; set; }
        [Required]
        [DisplayName("Account No.")]
        public string AccountNo { get; set; }
        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
    public class BankViewModel
    {
		public Int64 SNo { get; set; }
		public int Id { get; set; }
        [Required(ErrorMessage ="Bank Code is required")]
        [DisplayName("Bank Code")]
        public string BankCode { get; set; }
        [Required]
        [StringLength(50)]
        [DisplayName("Bank Name")]
        public string BankName { get; set; }
        [Required]
        [DisplayName("Bank Address")]
        [StringLength(50)]
        public string BankAddress { get; set; }
        [Required]
        [DisplayName("Account No.")]
        public string AccountNo { get; set; }
        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}