﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class ManualAttendance
    {
        public Int64 SNo { get; set; }
        public int DLId { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Attendance Date is required")]
        public DateTime AttendanceDate { get; set; }
        [Required(ErrorMessage = "Attendance Date is required")]
        public DateTime AttendanceDateNepali { get; set; }
        public DateTime LogInDateTime { get; set; }

        public DateTime LogOutDateTime { get; set; }

        //[Display(Name = "Device Code")]
        //public int Code { get; set; }
        [DisplayName("Section")]
        [Required(ErrorMessage = "Section is required")]
        public int SectionId { get; set; }
        [Display(Name = "Department")]
        [Required(ErrorMessage = "Department is required")]
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        [Display(Name = "Employee")]
        [Required(ErrorMessage = "Employee is required")]
        public int EmployeeAttendanceId { get; set; }
        public string Emp_Name { get; set; }
        [Display(Name = "Employee Code")]
        public string Emp_Code { get; set; }
        [Display(Name = "Login Time")]       
        public string LoginTime { get; set; }
        [Display(Name = "Logout Time")]       
        public string LogoutTime { get; set; }
       
        [Display(Name = "Logout Time")]      
      //  [Required(ErrorMessage = "Out Time is required")]
        public string OutTime { get; set; }        
        [Display(Name = "Login Time")]
       // [Required(ErrorMessage = "In Time is required")]
        public string InTime { get; set; }

        public bool IsActive { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        [DataType(DataType.DateTime)]       
        public DateTime CreateDate { get; set; }        
        public string Status { get; set; }
        public string LoginTimeLog { get; set; }
        public string Remarks { get; set; }

    }
}