﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class Devices
    {
		public Int64 SNo { get; set; }
		public int Id { get; set; }
        public int Code { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Device Name")]
        public string DeviceName { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "IP Address")]
        [RegularExpression(@"^\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "Invalid Ip Address Format")]

        public string IpAddress { get; set; }
        [StringLength(50)]
        [Display(Name = "Serial Number")]
        public string SerialNo { get; set; }
        [StringLength(50)]
        [Display(Name = "Device Type")]
        public string DeviceType { get; set; }
        [StringLength(50)]
        [Display(Name = "Device Model")]
        public string DeviceModel { get; set; }
        [Display(Name = "Device Status")]
        public bool DeviceStatus { get; set; }
        public bool  PingStatus { get; set; }
        

        public bool IsDeleted { get; set; }
        public char Event { get; set; }
		public DateTime LastDateFetched { get; set; }
        public int Port { get; set; }
        public bool ExtraProperty { get; set; }
        public bool IsTruncateTable { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int countData { get; set; }
        public int Time { get; set; }
        public bool Active { get; set; }
        public bool InActive { get; set; }
        public int BranchId { get; set; }
        public IEnumerable<SelectListItem> BranchList { get; set; }
        public string BranchName { get; set; }
        public bool IsSSREnabled { get; set; }
        public string DevicePass { get; set; }
     
        public string CompanyCode { get; set; }
        public bool IsAutoClear { get; set; }






    }
}