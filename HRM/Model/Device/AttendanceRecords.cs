﻿using System;
namespace HRM
{
    public class AttendanceRecords
    {
        public int Action { get; set; }
        public DateTime Clock { get; set; }
        public int Antipassback { get; set; }       
        public ulong DIN { get; set; }
        public int DN { get; set; }
        public int DoorStatus { get; set; }
        public int JobCode { get; set; }
        public ulong MDIN { get; set; }
        public string Remark { get; set; }
        public int Verify { get; set; }
        public string Type { get; set; }

        public string Mode { get; set; }
        public int DeviceId { get; set; }
        public string CompanyCode { get; set; }
    }
}