﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRM
{
    public class DeviceStatusReport
    {
        public int BranchId { get; set; }
        public string DeviceName { get; set; }
        public string IpAddress { get; set; }
        public string BranchName { get; set; }
        public int Code { get; set; }
        public bool DeviceStatus { get; set; }
        public string SerialNo { get; set; }
        public DateTime LastOnline { get; set; }




    }
}