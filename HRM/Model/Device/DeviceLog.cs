﻿using System;
namespace HRM
{
    public class DeviceLog
    {
		public int Id { get; set; }
        public int Code { get; set; }

        public int EmployeeAttendanceId { get; set; }
		public string DeviceIP { get; set; }
		public DateTime LogInTime { get; set; }
		public DateTime LoginOutTime { get; set; }
		public DateTime LogTime { get; set; }
        public int DeviceId { get; set; }
        public string CompanyCode { get; set; }

    }
	
}
