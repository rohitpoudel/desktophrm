﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRM
{
    public class PoliticalNonPoliticalEmployee
    {
        public string Emp_Code { get; set; }
        public string Emp_Name { get; set; }
        public string Permanet_Address { get; set; }
        public string Political { get; set; }

    }
}