using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class ShiftWO
    {
		public int Id { get; set; }

		public int ShiftId { get; set; }
		public int DualShiftId { get; set; }
		public int Emp_Id { get; set; }
		public string WeeklyOff { get; set; }

		public bool Sunday { get; set; }
		public bool Monday { get; set; }
		public bool Tuesday { get; set; }
		public bool Wednesday { get; set; }
		public bool Thursday { get; set; }
		public bool Friday { get; set; }
		public bool Saturday { get; set; }
		public DateTime Created_Date { get; set; }
		public int Created_By { get; set; }
		public char Event { get; set; }
		public bool DualShift { get; set; }
        public bool IsDualShift { get; set; }
    }
    public class ShiftWOVM
    {
        public int Id { get; set; }
        [Display(Name = "Shift")]
        [Required]
        public int ShiftId { get; set; }
        //public IEnumerable<SelectListItem> Shift { set; get; }
        [Display(Name = "Check Multiple For Dynamic Weekly Off")]
        [Required]
        public int DynamicWeeklyOff { get; set; }
        public IEnumerable<SelectListItem> CheckMultipleForDynamicWeeklyOff { set; get; }
    }
}