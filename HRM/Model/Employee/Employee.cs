﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class Employee
    {
        public bool IsSuperAdmin { get; set; }

        public Int64 SNo { get; set; }
        public int EmpId { get; set; }
        #region officeRecord
        public int Id { get; set; }
        [Display(Name = "Code")]
        public string Emp_Code { get; set; }
        [Display(Name = "Employee Name")]
        public string PhotoPath { get; set; }
        public int CountP { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_DeviceCode { get; set; }
        [DisplayName("Designation")]
        public int Designation_Id { get; set; }
        [DisplayName("Department")]
        public int Department_Id { get; set; }
        [DisplayName("Section")]
        public int Section_Id { get; set; }
        public int GradeGroup { get; set; }
        public bool IsManager { get; set; }
        public string BasicInfoView { get; set; }


        #endregion
        #region BasicInformation

        [Display(Name = "Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public string FullPath { get; set; }
        public string DOB { get; set; }
        [DisplayName("DOB Nepali")]
        public string DOBNepali { get; set; }
        [DisplayName("Marital Status")]
        public string Marital_Status { get; set; }
        public string Gender { get; set; }
        [DisplayName("Blood Group")]
        public int Blood_Group { get; set; }
        [DisplayName("Mobile No")]
        public string Mobile_No { get; set; }
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }
        public bool EmailVerified { get; set; }
        [DisplayName("PassPort No")]
        public string PassPort_No { get; set; }
        [DisplayName("CitizenShip No")]
        public string CitizenShip_No { get; set; }
        [DisplayName("Issued Date")]
        public string Issued_Date { get; set; }
        [DisplayName("Issued Date Nepali")]
        public string Issued_DateNepali { get; set; }
        [DisplayName("Issued District")]
        public string Issued_District { get; set; }
        public int Religion { get; set; }
        public string photo { get; set; }
        [DisplayName("Permanet Address")]
        public string Permanet_Address { get; set; }
        [DisplayName("Permanet Address(Nepali)")]
        public string Permanet_Nepali { get; set; }
        [DisplayName("Temporary Address")]
        public string Temp_Address { get; set; }
        [DisplayName("Temporary Address(Nepali)")]
        public string Temp_Nepali { get; set; }

        public string FileLocation { get; set; }
        public string Marital_Status_Name { get; set; }
        public string GenderName { get; set; }
        public string Blood_Group_Name { get; set; }
        public string Religion_Name { get; set; }
        public int BranchId { get; set; }
        #endregion


        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string SectionName { get; set; }
        public string BranchName { get; set; }
        public IEnumerable<SelectListItem> DesignationList { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        public IEnumerable<SelectListItem> SectionList { get; set; }
        public IEnumerable<SelectListItem> GradeGroupList { get; set; }
        public IEnumerable<SelectListItem> BranchList { get; set; }
        public IEnumerable<EmployeeTransfer> EmployeeTransferDetails { get; set; }

        public List<Allowance> Allowances { get; set; } = new List<Allowance>();
        public Allowance Allowance { get; set; }
        public DeductionRebateTax DeductionRebateTax { get; set; }
        public List<DeductionRebateTax> DeductionRebates { get; set; } = new List<DeductionRebateTax>();


        public IEnumerable<SelectListItem> AllowancesList { get; set; }
        public IEnumerable<SelectListItem> DeductionRebateList { get; set; }
        public IEnumerable<EmployeeSalaryInfo> EmployeeSalaryList { get; set; }
        public EmployeeSalaryInfo EmployeeSalaryInfo { get; set; }
        public IEnumerable<EmployeeAllowanceInfo> EmployeeAllowanceInfo { get; set; }


        public string Company_Code { get; set; }
        public char Event { get; set; }
        public bool Is_Active { get; set; }
        public DateTime Created_Date { get; set; }
        public int Created_By { get; set; }
        public bool IsPolitical { get; set; }
        public int RoleId { get; set; }


    }

    public class EmployeeSalaryInfo
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Incentive { get; set; }
        public decimal OtherIncentive { get; set; }
        public bool WorkOnRemote { get; set; }
        public decimal RemoteRebate { get; set; }
        [Display(Name = "Contract Date")]
        public string FromDate { get; set; }
        public string FromDateNepali { get; set; }
        public DateTime ToDate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ButtonName { get; set; }
    }

    public class EmployeeDeductionRebateInfo
    {
        public int Id { get; set; }
        public int SalaryId { get; set; }
        public int DeductionRebateId { get; set; }
        public string DeductionRebateName { get; set; }
        public int DeductionRebateOn { get; set; }
        public string DeductionRebateType { get; set; }
        public decimal DeductionRebate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class EmployeeAllowanceInfo
    {
        public int Id { get; set; }
        public int SalaryId { get; set; }
        public int AllowanceId { get; set; }
        public string AllowanceName { get; set; }
        public string AllowanceType { get; set; }
        public decimal Allowance { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class EmployeeAttendaceInfo
    {
        public string Date { get; set; }
        public string LoginTime { get; set; }
        public string LogOutTime { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string OfficeIn { get; set; }
        public string OfficeOut { get; set; }
    }

    public class EmployeeLeaveInfo
    {
        public string LeaveName { get; set; }
        public string Date { get; set; }
        public string LeaveDay { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OfficeIn { get; set; }
        public string Status { get; set; }
    }

    public class EmployeePayrollInfo
    {
        public string FiscalYear { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public string Created_Date { get; set; }
        public int PresentDays { get; set; }
        public int LateInDays { get; set; }
        public int EarlyOutDays { get; set; }
        public int PaidLeaveDays { get; set; }
        public int UnpaidLeaveDays { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Incentive { get; set; }
        public decimal OtherIncentive { get; set; }
        public decimal ExtraTimeWork { get; set; }
        public decimal ExtraTimeWorkIncentive { get; set; }
        public decimal OverTime { get; set; }
        public decimal OverTimeIncentive { get; set; }
        public decimal RemoteRebate { get; set; }
        public decimal TotalAllowances { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal LeaveDaysDeductByLateIn { get; set; }
        public decimal SalaryDaysDeductByLateIn { get; set; }
        public decimal LeaveDaysDeductByEarlyOut { get; set; }
        public decimal SalaryDaysDeductByEarlyOut { get; set; }
        public decimal UnpaidLeaveDeduction { get; set; }
        public decimal AdvancedPayment { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public decimal TotalTax { get; set; }
        public decimal MonthlyNetPay { get; set; }
    }

    public class EmployeeReports
    {
        public IEnumerable<EmployeeByDesignation> EmployeeByDesignation { get; set; }
        public IEnumerable<EmployeeByDepartment> EmployeeByDepartment { get; set; }
        public IEnumerable<EmployeeByMaritalStatus> EmployeeByMaritalStatus { get; set; }
        public IEnumerable<EmployeeBySection> EmployeeBySection { get; set; }
        public IEnumerable<LeaveReport> LeaveReport { get; set; }
        public IEnumerable<AttendanceReport> AttendanceReport { get; set; }
        public IEnumerable<WorkedReport> WorkedReport { get; set; }
        public IEnumerable<TotalEmployeeByDepartment> TotalEmployeeByDepartment { get; set; }

    }
    public class EmployeeByDesignation
    {
        public string Designation { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Other { get; set; }
        public int NotSpecified { get; set; }
    }
    public class EmployeeByDepartment
    {
        public string Department { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Other { get; set; }
        public int NotSpecified { get; set; }
    }
    public class TotalEmployeeByDepartment
    {
        public string Department { get; set; }
        public int Count { get; set; }
    }
    public class EmployeeByMaritalStatus
    {
        public string MaritalStatus { get; set; }
        public int Count { get; set; }
    }
    public class EmployeeBySection
    {
        public string Section { get; set; }
        public int Count { get; set; }
    }

    public class LeaveReport
    {
        public string Date { get; set; }
        public int Approve { get; set; }
        public int Pending { get; set; }
        public int Rejected { get; set; }
    }
    public class AttendanceReport
    {
        public string Date { get; set; }
        public int Absent { get; set; }
        public int EarlyOut { get; set; }
        public int LateIn { get; set; }
        public int Present { get; set; }
    }
    public class WorkedReport
    {
        public string Date { get; set; }
        public int NoOfEmployee { get; set; }
        public decimal AverageWorkedHour { get; set; }
        public decimal AverageWorkingHour { get; set; }
    }

    public class PayrollReport
    {
        public int EmployeeCount { get; set; }
        public decimal TotalBasicSalary { get; set; }
        public decimal TotalPaidLeaveDays { get; set; }
        public decimal TotalUnpaidLeaveDays { get; set; }
        public decimal TotalAllowances { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal TotalRebate { get; set; }
        public decimal TotalAdvancedPayment { get; set; }
        public decimal TotalTax { get; set; }
    }

    public class EmployeeSalaryReports
    {
        public IEnumerable<EmployeeBasicSalaryReports> EmployeeBasicSalaryReports { get; set; }
        public IEnumerable<EmployeeBasicSalaryReportsByDesignatin> EmployeeBasicSalaryReportsByDesignatin { get; set; }
        public IEnumerable<AllowanceReport> AllowanceReport { get; set; }
        public IEnumerable<DeductionRebateReport> DeductionRebateReport { get; set; }
        public PayrollReport PayrollReport { get; set; }
    }

    public class EmployeeBasicSalaryReports
    {
        public string DepartmentName { get; set; }
        public decimal MinimumBasicSalary { get; set; }
        public decimal MaximumBasicSalary { get; set; }
        public decimal AverageBasicSalary { get; set; }
        public decimal TotalIncentive { get; set; }
        public decimal TotalOtherIncentive { get; set; }
    }
    public class EmployeeBasicSalaryReportsByDesignatin
    {
        public string DesignationName { get; set; }
        public decimal MinimumBasicSalary { get; set; }
        public decimal MaximumBasicSalary { get; set; }
        public decimal AverageBasicSalary { get; set; }
        public decimal TotalIncentive { get; set; }
        public decimal TotalOtherIncentive { get; set; }
    }
    public class AllowanceReport
    {
        public string AllowanceName { get; set; }
        public decimal TotalAllowance { get; set; }
        public decimal MinimumAllowance { get; set; }
        public decimal MaximumAllowance { get; set; }
        public int EmployeeCount { get; set; }
    }
    public class DeductionRebateReport
    {
        public string DeductionRebateName { get; set; }
        public decimal TotaledDeductionRebate { get; set; }
        public decimal MinimumDeductionRebate { get; set; }
        public decimal MaximumDeductionRebate { get; set; }
        public int EmployeeCount { get; set; }
    }
}