﻿using System;


namespace HRM
{
    public class EmployeeTransfer
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int OldBranchId { get; set; }
        public string OldBranchName { get; set; }
        public string Event { get; set; }
        public int NewBranchId { get; set; }
        public string NewBranchName { get; set; }
        public int OldDepartmentId { get; set; }
        public string OldDepartmentName { get; set; }
        public int NewDepartmentId { get; set; }
        public string NewDepartmentName { get; set; }
        public int OldDesignationId { get; set; }
        public string OldDesignationName { get; set; }
        public int NewDesignationId { get; set; }
        public bool IsAproved { get; set; }
        public string NewDesignationName { get; set; }
        public int OldSectionId { get; set; }
        public string OldSectionName { get; set; }
        public int NewSectionId { get; set; }
        public string NewSectionName { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public string EffectiveFromNepali { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class EmployeeDetail
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
    }
}