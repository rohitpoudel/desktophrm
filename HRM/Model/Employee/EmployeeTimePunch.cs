﻿using System;
using System.ComponentModel.DataAnnotations;
namespace HRM
{
	public class EmployeeTimePunch
	{
		#region Punch
		public int Id { get; set; }
        public string MaxEarlyGrace { get; set; }
        public string MaxLateGrace { get; set; }
        public int Emp_Id { get; set; }
		[DisplayFormat(DataFormatString = "{0:hh}", ApplyFormatInEditMode = true)]
		[Display(Name = "Max Working Hour")]

		public string Max_WorkingHour { get; set; }
		[Display(Name = "Allowed Late In")]
		public string AllowLateIn { get; set; }
		[Display(Name = "Allowed Early Out")]
		public string AllowEarlyOut { get; set; }
		[Display(Name = "Half Day Working Hour")]
		public string HalfDay_WorkingHour { get; set; }
		[Display(Name = "Short Day Working Hour")]
		public string ShortDay_workingHour { get; set; }
		[Display(Name = "Present Marking Duration")]
		public string Present_MarkDuration { get; set; }
		[Display(Name = "Is TimeLoss Allowed")]
		public bool IsTimeLoss_Allow { get; set; }
		[Display(Name = "Half Day Marking")]
		public bool IsHalfDay_Marking { get; set; }
		[Display(Name = "Is OT Allowed")]
		public bool IsOT_Allowed { get; set; }
		public char Event { get; set; }
		public bool Is_Active { get; set; }
		public DateTime Created_Date { get; set; }
		public int Created_By { get; set; }
		public string PunchType { get; set; }
        [Display(Name = "Mark Absent When Early Out")]
        public bool MarkAbsentWhenEarlyOut { get; set; }
        [Display(Name = "Mark Absent When Late In")]
        public bool MarkAbsentWhenLateIn { get; set; }
        #endregion

    }
}


public class TimePunchVM
{
	public int Id { get; set; }
	[Display(Name = "Max Working Hour")]
	[Required]
	[Timestamp]
	public byte MaxWorkingHour { get; set; }
	[Display(Name = "Allowed Late In")]
	[Required]
	[Timestamp]
	public byte AllowedLateIn { get; set; }
	[Display(Name = "Allowed Early Out")]
	[Required]
	[Timestamp]
	public byte AllowedEarlyOut { get; set; }
	[Display(Name = "Half Day Working Hour")]
	[Required]
	[Timestamp]
	public byte HalfDayWorkingHour { get; set; }
	[Display(Name = "Short Day Working Hour")]
	[Required]
	[Timestamp]
	public byte ShortDayWorkingHour { get; set; }
	[Display(Name = "Present Marking Duration")]
	[Required]
	[Timestamp]
	public byte PresentMarkingDuration { get; set; }
	[Display(Name = "No Punch")]
	public bool NoPunch { get; set; }
	[Display(Name = "Single Punch")]
	public bool SinglePunch { get; set; }
	[Display(Name = "Multiple Punch")]
	public bool MultiplePunch { get; set; }
	[Display(Name = "Two Punch")]
	public bool TwoPunch { get; set; }
	[Display(Name = "Four Punch")]
	public bool FourPunch { get; set; }
	[Display(Name = "Consider Time Loss")]
	public bool ConsiderTimeLoss { get; set; }
	[Display(Name = "Half Day Marking")]
	public bool HalfDayMarking { get; set; }
	[Display(Name = "Is OT Allowed")]
	public bool IsOTAllowed { get; set; }
    [Display(Name = "Mark Absent When Early Out")]
    public bool MarkAbsentWhenEarlyOut { get; set; }
    [Display(Name = "Mark Absent When Late In")]
    public bool MarkAbsentWhenLateIn { get; set; }


}
