﻿

namespace HRM.MenuRole
{
    public class MenuVsTemplate
    {
        public int MenuId { get; set; }
        public int TemplateId { get; set; }
        public int MenuTemplate_MTId { get; set; }
        public char Event { get; set; }

    }
}