﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HRM
{
    public class Shift
    {
        public Int64 SNo { get; set; }

        #region Shift
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string OverTimeFrom { get; set; }
        public string OverTimeHour { get; set; }

        public string MaxEarlyGrace { get; set; }
        public string MaxLateGrace { get; set; }
        [DisplayName("Shift Type")]
        public string ShiftType { get; set; }
        [DisplayName("Shift Start")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string ShiftStart { get; set; }
        [DisplayName("Shift End")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string ShiftEnd { get; set; }
        [DisplayName("Lunch Start")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string LunchStart { get; set; }
        [DisplayName("Lunch End")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string LunchEnd { get; set; }
        [DisplayName("Early Grace")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string EarlyGrace { get; set; }
        [DisplayName("Late Grace")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string LateGrace { get; set; }
        [DisplayName("Shift Hours")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string ShiftHours { get; set; }
        [DisplayName("Half Day Hour")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string HalfDayWorkingHour { get; set; }
       
        public string HalfDayOff { get; set; }


        [DisplayName("No Of Staff")]
        public int? NoOfStaff { get; set; }
        [DisplayName("Shift Start Grace")]
        public string ShiftStartGrace { get; set; }
        [DisplayName("Start Month")]
        public string StartMonth { get; set; }
        [DisplayName("Start Grace days")]
        public double? StartGraceDays { get; set; }
        [DisplayName("Shift End Grace")]
        public string ShiftEndGrace { get; set; }
        [DisplayName("End Month")]
        public string EndMonth { get; set; }
        [DisplayName("End Grace Days")]
        public double? EndGraceDays { get; set; }
		public bool DataExist { get; set; }

		#endregion

		#region Attendance
		[DisplayName("Late In")]
        public bool LateIn { get; set; }
        [DisplayName("Early Out")]
        public bool EarlyOut { get; set; }
        [DisplayName("Early In")]
        public bool EarlyIn { get; set; }
        [DisplayName("Late Out")]
        public bool LateOut { get; set; }

        #endregion

        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        [Display(Name = "Mark Absent When Early Out")]
        public bool MarkAbsentWhenEarlyOut { get; set; }
        [Display(Name = "Mark Absent When Late In")]
        public bool MarkAbsentWhenLateIn { get; set; }
    }
    public class ShiftViewModel
    {
        public Int64 SNo { get; set; }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public TimeSpan ShiftStart { get; set; }
        public TimeSpan ShiftEnd { get; set; }
        public TimeSpan LunchStart { get; set; }
        public TimeSpan LunchEnd { get; set; }
        public string ShiftType { get; set; }
        public TimeSpan EarlyGrace { get; set; }
        public TimeSpan LateGrace { get; set; }
        public TimeSpan ShiftHours { get; set; }
        public TimeSpan HalfDayWorkingHour { get; set; }
        public double NoOfStaff { get; set; }
        public bool LateIn { get; set; }
        public bool EarlyOut { get; set; }
        public TimeSpan ShiftStartGrace { get; set; }
        public string StartMonth { get; set; }
        public int StartGracedays { get; set; }
        public TimeSpan ShiftEndGrace { get; set; }
        public string EndMonth { get; set; }
        public int EndGraceDays { get; set; }
        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
		public bool DataExist { get; set; }

	}

    public class AssignShift
    {
        public int? DepartmentId { get; set; }
        public int? DesignationId { get; set; }
        public int? SectionId { get; set; }
        public string[] EmployeeId { get; set; }
        public int ShiftId { get; set; }
        public bool? SelectAllEmp { get; set; }
        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public string WeeklyOff { get; set; }
        public DateTime Created_Date { get; set; }
        public int Created_By { get; set; }
    }

    public class DualShift
    {
        public Int64 SNo { get; set; }

        #region Shift
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string OverTimeFrom { get; set; }
        public string MaxEarlyGrace { get; set; }
        public string MaxLateGrace { get; set; }
        [DisplayName("Shift Type")]
        public string ShiftType { get; set; }


        [DisplayName("Shift Start")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string FirstShiftStart { get; set; }
        [DisplayName("Shift End")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string FirstShiftEnd { get; set; }


        [DisplayName("Shift Start")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string SecondShiftStart { get; set; }
        [DisplayName("Shift End")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string SecondShiftEnd { get; set; }


        [DisplayName("Lunch Start")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string LunchStart { get; set; }
        [DisplayName("Lunch End")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string LunchEnd { get; set; }
        [DisplayName("Early Grace")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string EarlyGrace { get; set; }
        [DisplayName("Late Grace")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string LateGrace { get; set; }
        [DisplayName("First Shift Hours")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string FirstShiftHours { get; set; }

        [DisplayName("Second Shift Hours")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string SecondShiftHours { get; set; }

        [DisplayName("Half Day Hour")]
        [RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Invalid Time Format")]
        public string HalfDayWorkingHour { get; set; }
        [DisplayName("No Of Staff")]
        public int? NoOfStaff { get; set; }
        [DisplayName("Shift Start Grace")]
        public string ShiftStartGrace { get; set; }
        [DisplayName("Start Month")]
        public string StartMonth { get; set; }
        [DisplayName("Start Grace days")]
        public double? StartGraceDays { get; set; }
        [DisplayName("Shift End Grace")]
        public string ShiftEndGrace { get; set; }
        [DisplayName("End Month")]
        public string EndMonth { get; set; }
        [DisplayName("End Grace Days")]
        public double? EndGraceDays { get; set; }
        public bool DataExist { get; set; }

        #endregion

        #region Attendance
        [DisplayName("Late In")]
        public bool LateIn { get; set; }
        [DisplayName("Early Out")]
        public bool EarlyOut { get; set; }
        [DisplayName("Early In")]
        public bool EarlyIn { get; set; }
        [DisplayName("Late Out")]
        public bool LateOut { get; set; }

        #endregion

        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public int CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [Display(Name = "Mark Absent When Early Out")]
        public bool MarkAbsentWhenEarlyOut { get; set; }
        [Display(Name = "Mark Absent When Late In")]
        public bool MarkAbsentWhenLateIn { get; set; }
    }
}
