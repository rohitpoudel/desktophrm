﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class OfficeDays
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        [Required]
        [Display(Name = "Days In Month")]
        public string DaysInMonth { get; set; }
        public string TotalDays { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public int Updated_By { get; set; }
        public DateTime Updated_Date { get; set; }
        public char Event { get; set; }
        public IEnumerable<SelectListItem> FiscalYearList { get; set; }
    }

    public class OfficeDaysViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }
        public string FiscalYearName { get; set; }
        public string FiscalYearNameNepali { get; set; }
        [Required]
        [Display(Name = "Days In Month")]
        public string DaysInMonth { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public int Updated_By { get; set; }
        public DateTime Updated_Date { get; set; }
        public char Event { get; set; }
		public bool DataExist { get; set; }

	}
}