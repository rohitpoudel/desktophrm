﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class LateRule
    {
        public int Id { get; set; }

        
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        [Required]
        [Display(Name = "Rule For")]
        public string LateInOrEarlyOut { get; set; }

        [Display(Name = "Days Allowed")]
        public int? DaysAllowed { get; set; }


        [Display(Name = "Leave On Late/Early")]
        public bool LeaveOnLate { get; set; }

        [Display(Name = "Leave Deduction on")]
        public int? LeaveLateInDays { get; set; }

        [Display(Name = "Leave Deduction")]
        public int? LeaveDeductin { get; set; }

        [Display(Name = "Salary Deduction on Late/Early")]
        public bool DeductionOnLate { get; set; }

        [Display(Name = "Salary Deduction on")]
        public int? DeductLateInDays { get; set; }

        [Display(Name = "Deduction Type")]
        public string Type { get; set; }

        [Display(Name = "Deduction")]
        public decimal? Deduction { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public int Updated_By { get; set; }
        public DateTime Updated_Date { get; set; }
        public char Event { get; set; }
        public IEnumerable<SelectListItem> FiscalYearList { get; set; }
    }

    public class LateRuleViwModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }
        public string FiscalYearName { get; set; }
        [Required]
        [Display(Name = "Rule For")]
        public string LateInOrEarlyOut { get; set; }

        [Display(Name = "Days Allowed")]
        public int DaysAllowed { get; set; }


        [Display(Name = "Leave On Late/Early")]
        public bool LeaveOnLate { get; set; }

        [Display(Name = "Leave Deduction on")]
        public int LeaveLateInDays { get; set; }

        [Display(Name = "Leave Deduction")]
        public int LeaveDeductin { get; set; }

        [Display(Name = "Salary Deduction on Late/Early")]
        public bool DeductionOnLate { get; set; }

        [Display(Name = "Salary Deduction on")]
        public int DeductLateInDays { get; set; }

        [Display(Name = "Deduction Type")]
        public string Type { get; set; }

        [Display(Name = "Deduction")]
        public decimal Deduction { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public int Updated_By { get; set; }
        public DateTime Updated_Date { get; set; }
        public char Event { get; set; }
        public string FiscalYearNameNepali { get; set; }
    }
}