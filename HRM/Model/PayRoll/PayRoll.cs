﻿
using System.Collections.Generic;


namespace HRM
{
    public class PayRoll
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Emp_Name { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public decimal BasicSalary { get; set; }
        public string Year { get; set; }
        public int Month { get; set; }
        public int FiscalYearId { get; set; }
        public int PayrollGenerated { get; set; }
        public bool Approved { get; set; }
        public string DeductionRebateName { get; set; }
        public decimal DeductionRebate { get; set; }
        public string DateType { get; set; }
    }

    public class Payslip
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Emp_Name { get; set; }
        public string FiscalYear { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Incentive { get; set; }
        public decimal OtherIncentive { get; set; }
        public decimal TotalAllowances { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal TotalRebate { get; set; }
        public bool WorkOnRemote { get; set; }
        public decimal RemoteRebate { get; set; }
        public int UnpaidLeaveDays { get; set; }
        public int LateInDays { get; set; }

        public decimal DeductionLeaveByLateIn { get; set; }
        public decimal DeductionSalaryByLateIn { get; set; }
        public int EarlyOutDays { get; set; }
        public decimal DeductionLeaveByEarlyOut { get; set; }
        public decimal DeductionSalaryByEarlyOut { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public decimal TotalTax { get; set; }
        public decimal AdvancedPayment { get; set; }
        public decimal MonthlyNetPay { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public decimal UnpaidLeaveDeduction { get; set; }
        public int MonthlySalaryId { get; set; }
        public IEnumerable<Deduction> Deductions { get; set; }
        public IEnumerable<Rebate> Rebates { get; set; }
        public IEnumerable<Allowances> Allowances { get; set; }
    }
    public class Deduction
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
    public class Rebate
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
    public class Allowances
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
    
    public class Provisional
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Emp_Name { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal RemoteRebate { get; set; }
        public int PresentDays { get; set; }
        public int LateInDays { get; set; }
        public decimal LeaveDaysDeductByLateIn { get; set; }
        public decimal DeductionSalaryByLateIn { get; set; }
        public int EarlyOutDays { get; set; }
        public decimal LeaveDaysDeductByEarlyOut { get; set; }
        public decimal DeductionSalaryByEarlyOut { get; set; }
        public int PaidLeaveDays { get; set; }
        public int UnpaidLeaveDays { get; set; }
        public decimal TotalAllowances { get; set; }
        public decimal TotalRebate { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal AdvancedPayment { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public decimal TotalTax { get; set; }
        public decimal MonthlyNetPay { get; set; }
    }
}