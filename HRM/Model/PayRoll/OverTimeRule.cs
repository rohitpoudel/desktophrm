﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class OverTimeRule
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        [Required]
        [Display(Name = "Over Time Allowed")]
        public bool OverTimeAllowed { get; set; }


        [Display(Name = "Incentive Type")]
        public string Type { get; set; }

        [Display(Name = "Incentive")]
        public decimal Incentive { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public int Updated_By { get; set; }
        public DateTime Updated_Date { get; set; }
        public char Event { get; set; }
        public IEnumerable<SelectListItem> FiscalYearList { get; set; }
    }

    public class OverTimeRuleViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        public string FiscalYearName { get; set; }
        public string FiscalYearNameNepali { get; set; }
        [Required]
        [Display(Name = "Over Time Allowed")]
        public bool OverTimeAllowed { get; set; }


        [Display(Name = "Incentive Type")]
        public string Type { get; set; }

        [Display(Name = "Incentive")]
        public decimal Incentive { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public int Updated_By { get; set; }
        public DateTime Updated_Date { get; set; }
        public char Event { get; set; }
    }
}