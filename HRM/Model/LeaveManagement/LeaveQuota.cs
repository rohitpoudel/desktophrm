﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HRM
{
    public class LeaveQuota
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        [Required]
       // [StringLength(maximumLength: 5, MinimumLength = 1)]
        public int Code { get; set; }
        [Required]
        [DisplayName("Designation Name")]
        public int DesignationId { get; set; }
        [Required]
        [DisplayName("Designation Level")]
        public int DesignationLevel { get; set; }

        [Required]
        [DisplayName("Min Salary")]
        public decimal MinSalary { get; set; }
        [Required]
        [DisplayName("Max Salary")]
        public decimal MaxSalary { get; set; }      
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }


        public bool IsDeleted { get; set; }
        public char Event { get; set; }
        public string DesignationName { get; set; }
        public List<LeaveQuota> DesignationList { get; set; }
    }
}