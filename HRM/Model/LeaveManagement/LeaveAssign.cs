﻿using System;
using System.Collections.Generic;

namespace HRM
{
    public class LeaveAssign
    {
        public bool IsChanged { get; set; }
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int FiscalYearId { get; set; }
        public string LeaveId { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public int UpdatedDate { get; set; }
        public decimal TotalLeave { get; set; }
        public string Gender { get; set; }
        public List<AssignLeave> AssignedLeave { get; set; } = new List<AssignLeave>();
    }

    public class AssignLeave
    {
        public int LeaveId { get; set; }
        public string LeaveName { get; set; }
        public decimal NoOfDays { get; set; }
        public int TotalLeave { get; set; }
        public int IsLeave { get; set; }
        public int NoOfDays1 { get; set; }

    }

    public class LeaveAssignList
    {

        public int MyProperty { get; set; }
        public List<LeaveAssign> LeaveAssigns { get; set; } = new List<LeaveAssign>();
    }
}