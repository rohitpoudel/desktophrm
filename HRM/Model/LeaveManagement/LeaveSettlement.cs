﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
	public class LeaveSettlement
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		[DisplayName("Employee")]
		public int EmployeeId { get; set; }
        public string genderId { get; set; }        
        [DisplayName("Leave")]
		public int LeaveMasterId { get; set; }
		[DisplayName("Opening Balance")]
		public double OpeningBalance { get; set; }
		[DisplayName("Leave Taken")]
		public decimal LeaveTaken { get; set; }
		[DisplayName("Remaining Leave")]
		public decimal RemainingLeave { get; set; }
		[DisplayName("Carry To Next")]
		public decimal CarryToNext { get; set; }
		public decimal Pay { get; set; }
		[DisplayName("Settling Leave")]
		public int SettlingLeave { get; set; }
		[DataType(DataType.MultilineText)]
		public string Remarks { get; set; }
		public char Event { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime CreatedDate { get; set; }
		public int CreatedBy { get; set; }
		[DisplayName("Fiscal Year")]
		public int FiscalYearId { get; set; }
		public string Emp_Name { get; set; }
		public string LeaveName { get; set; }
		public bool IsCarryable { get; set; }
		public bool IsPaid { get; set; }
		public int NoofDays { get; set; }
		public List<SelectListItem> LeaveList { get; set; }
	}
}