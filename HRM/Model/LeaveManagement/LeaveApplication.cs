﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace HRM
{ 
	public class LeaveApplication
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        public int BranchId { get; set; }

        [DisplayName("Employee")]
		public int EmployeeId { get; set; }
		[DisplayName("Leave")]

		public int LeaveId { get; set; }
		public decimal LeaveRemaining { get; set; }
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }
		public string LeaveDay { get; set; }
		public decimal TotalLeaveTaken { get; set; }
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		[DataType(DataType.Date)]
		public DateTime FromDate { get; set; }
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		[DataType(DataType.Date)]
		public DateTime? ToDate { get; set; }
		public string FromDateNepali { get; set; }
		public string ToDateNepali { get; set; }
		public string ApprovedBy { get; set; }
		public string Gender { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public char Event { get; set; }
		public bool IsDeleted { get; set; }

		public char Status { get; set; }
		[DisplayName("Employee Code")]
		public string Emp_Code { get; set; }
		public string DepartmentName { get; set; }
		public string SectionName { get; set; }
		public string DesignationName { get; set; }
		[DisplayName("Employee Name")]
		public string Emp_Name { get; set; }
		public string photo { get; set; }
		public string LeaveName { get; set; }
		
		public string ApprovedByName { get; set; }
		public int NoofDays { get; set; }

		public string  ButtonName { get; set; }
        public int? TotalLeave { get; set; }
        public decimal TotalLeaveTakenDays { get; set; }


    }

    public class EmployeeLeaveReport
    {
        public int Id { get; set; }
        public string Emp_Name { get; set; }
        public string LeaveName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal LeaveDays { get; set; }
        public string Status { get; set; }
        public string FromDateNep { get; set; }
        public string ToDateNep { get; set; }
        public decimal LeaveAssigned { get; set; }
        public decimal TotalLeaveTaken { get; set; }
        public decimal LeaveRemaining { get; set; }
        public int FiscalYearId { get; set; }
        public int EmpId { get; set; }
        public int LeaveAssignId { get; set; }
        public decimal NoOfDays { get; set; }





    }
}