﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
	public class LeaveMaster
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		[DisplayName("Leave Code")]
		public string Code { get; set; }
		[DisplayName("Leave Name")]
		public string LeaveName { get; set; }
		[DisplayName("Leave Name(Nepali)")]

		public string LeaveNameNepali { get; set; }
        public int FiscalYear { get; set; }

        [DisplayName("Number Of Days")]
		public int? NoOfDays { get; set; }
		[DisplayName("Leave Increment Period")]
		public string LeaveIncrementPeriod { get; set; }
		[DisplayName("Applicable Gender")]
		public string ApplicableGender { get; set; }
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }
		[DisplayName("Is Replacement Leave")]
		public bool IsReplacement { get; set; }
		[DisplayName("Is Paid Leave")]
		public bool IsPaid { get; set; }
		[DisplayName("Is Leave Carryable")]
		public bool IsCarryable { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		[DataType(DataType.MultilineText)]
		public string Remarks { get; set; }
		public char Event { get; set; }
		public bool IsDeleted { get; set; }
		public bool DataExist { get; set; }

	}
}