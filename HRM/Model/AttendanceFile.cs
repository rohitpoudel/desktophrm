﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRM
{
    public class AttendanceFile
    {
        public int No { get; set; }
        public int TMNo { get; set; }
        public int EnNo { get; set; }
        public int GMNo { get; set; }


        public string Name { get; set; }
        public string INOUT { get; set; }
        public string Mode { get; set; }
        public string Antipass { get; set; }
        public int ProxyWork { get; set; }


        public DateTime DateTime { get; set; }

    }
}