﻿using System;
using System.ComponentModel.DataAnnotations;


namespace HRM

{
    public class FiscalYear
    {
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        [Required]
        [Display(Name = "Fiscal Year")]
        public string YearNepali { get; set; }
        [Required]
        [Display(Name = "Fiscal Year")]
        public string Year { get; set; }
        [Display(Name = "Current Fiscal Year")]
        public bool CurrentFiscalYear { get; set; }
        [Display(Name = "Start Date")]      
        [Required]	
		public string StartDate { get; set; }
        [Display(Name = "End date")]
        [Required]	
		public string EndDate { get; set; }
        [Display(Name = "Start Date Nep")]
        [Required]
        public string StartDateNepali { get; set; }
        [Display(Name = "End date Nep")]
        [Required]
        public string EndDateNepali { get; set; }
        public bool IsActive { get; set; } 
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        


    }
    public class FiscalYearViewModel
    {
		public Int64 SNo { get; set; }
      
        public String StartDateNepali { get; set; }
       
        public String EndDateNepali { get; set; }
        public int Id { get; set; }
        public string YearNepali { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public string Year { get; set; }
        [Display(Name = "Current Fiscal Year")]
        public bool CurrentFiscalYear { get; set; }
        [Display(Name = "Start Date")]        
        [Required]		
		public DateTime StartDate { get; set; }
        [Display(Name = "End date")]
        [Required]	
		public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public char Event { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool DataExist { get; set; }
    }
}
