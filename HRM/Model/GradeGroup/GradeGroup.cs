﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace HRM

{
    public class GradeGroup
	{
		public Int64 SNo { get; set; }
		[Key]
        public int Id { get; set; }		
		[Required]
        [DisplayName("Grade Group Code")]
        public string GradeGroupCode { get; set; }
        [Required]
        [StringLength(50)]
        [DisplayName("Grade Group Name")]
        public string GroupName { get; set; }
        public string Value { get; set; }
        public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
    public class GradeGroupViewModel
	{
		public Int64 SNo { get; set; }
		[Key]
        public int Id { get; set; }	
		[Required]
        [DisplayName("Grade Group Code")]
        public string GradeGroupCode { get; set; }
        [Required]
        [StringLength(50)]
        [DisplayName("Grade Group Name")]
        public string GroupName { get; set; }
        public string Value { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
		public bool DataExist { get; set; }

	}
}
