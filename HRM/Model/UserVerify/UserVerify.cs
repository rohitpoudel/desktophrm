﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class AbsentLog
    {
        public int AId { get; set; }
        public int EmpId { get; set; }
        public DateTime AbsentDate { get; set; }
        public bool IsMailSent { get; set; }
    }
    public class UserVerify
    {

        public int VU_ID { get; set; }
        public string Code { get; set; }
        public int EmpId { get; set; }
        public bool Status { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }

    }
    public class AbsentEmpDetails
    {

        public string Emp_Name { get; set; }
        public string Email { get; set; }
        public int EmpId { get; set; }



    }
    public class Errorcheck
    {
        public bool EmailVerify { get; set; }
        public bool InvalidEmail { get; set; }
    }
    public class EmailSendToEmp
    {
        public int? DepartmentId { get; set; }
        [DisplayName("Employee")]
        public string[] EmployeeId { get; set; }
        
        public string File { get; set; }

        public bool SelectAllEmp { get; set; }
        [Required(ErrorMessage = "Message Body Is Required")]
        public string MsgBody { get; set; }
        [Required(ErrorMessage = "Message Subject Is Required")]
        public string MsgSubject { get; set; }
    }
}

public class Email
{
    public class Emails
    {

        public int EmailId { get; set; }

        public string EmailFrom { get; set; }

        public string EmailTo { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        public string EmailContent { get; set; }
        public string Attachment { get; set; }
        public string Subject { get; set; }

        public DateTime AddedOn { get; set; }

        public string AddedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime DeletedOn { get; set; }

        public string DeletedBy { get; set; }

        public bool IsActive { get; set; }
    }
}
