﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
	public class EventInformation
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		[DisplayName("Event Name")]
		public string EventName { get; set; }
		[DisplayName("Event Level")]
		public string EventLevel { get; set; }
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }
		[DisplayName("From")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		[DataType(DataType.Date)]
		public DateTime FromDate { get; set; }
		[DisplayName("To")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		[DataType(DataType.Date)]
		public DateTime ToDate { get; set; }
        [DisplayName("From")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
       // [DataType(DataType.Date)]
        public string FromDateNepali { get; set; }
        [DisplayName("To")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
       // [DataType(DataType.Date)]
        public string ToDateNepali { get; set; }
        public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		[DataType(DataType.MultilineText)]
		public string Remarks { get; set; }
		[DisplayName("Department")]
		public string[] DepartmentId { get; set; }
		[DisplayName("Branch")]
		public string[] BranchId { get; set; }
		[DisplayName("Section")]
		public string[] SectionId { get; set; }
		public string DepIdCollection { get; set; }
		public string BranIdCollection { get; set; }
		public string SectIdCollection { get; set; }
		public string XMLDepId { get; set; }
		public string DepId { get; set; }
		public string SectId { get; set; }
		public string BranId { get; set; }	


		public char Event { get; set; }
		public bool IsDeleted { get; set; }




	}
}