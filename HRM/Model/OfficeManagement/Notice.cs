﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class Notice
	{
		public Int64 SNo { get; set; }
		public int Id { get; set; }
        public string Title { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PublishOn { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpiredOn { get; set; }
        public bool IsUrgent { get; set; }
        public string NoticeLevel_Id { get; set; }   
		[DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        public char Event { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Read { get; set; }
        [DisplayName("Branch")]
        public string[] BranchId { get; set; }
        [DisplayName("Department")]
        public string[] DepartmentId { get; set; }
        [DisplayName("Section")]
        public string[] SectionId { get; set; }
        public string BranchCollection { get; set; }
        public string DepartmentCollection { get; set; }
        public string SectionCollection { get; set; }
        public string XMLBranch { get; set; }
        public string DepId { get; set; }
        public string SectId { get; set; }
        public string BranId { get; set; }
        [Display(Name = "Start Date Nep")]
        [Required]
        public string PublishOnNepali { get; set; }
        [Display(Name = "End date Nep")]
        [Required]
        public string ExpiredOnNepali { get; set; }
        public List<Notice> NoticeLevelList { get; set; }
        public List<SelectListItem> DepartmentList { get; set; }
    }
}