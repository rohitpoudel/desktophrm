﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
	public class FixedSchedule
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		[DisplayName("Department")]
		public int DepartmentId { get; set; }
		public string DepartmentName { get; set; }
		[DisplayName("Section")]
		public int SectionId { get; set; }
		public string SectionName { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public char Event { get; set; }
		public bool IsDeleted { get; set; }
		[DisplayName("Employee")]
		public string[] EmployeeId { get; set; }
		public string XMLEmpId { get; set; }
		public string EmpIdCollection { get; set; }
		public string EmpId { get; set; }

		public List<SelectListItem> EmployeeList { get; set; }
	//	public List<MultiSelectList> MEmployeeList { get; set; }
		public int[] SelectedIds { get; set; }


        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Remarks { get; set; }
        public string FromDateNepali { get; set; }
        public string ToDateNepali { get; set; }
        public IEnumerable<FixedScheduleEmployeeList> EmployeeListbyId { get; set; }
    }
	public class FixedScheduleViewModel
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
		[DisplayName("Department")]
		public int DepartmentId { get; set; }
		public string DepartmentName { get; set; }
		[DisplayName("Section")]
		public string SectionName { get; set; }
        [DisplayName("Section")]

        public int SectionId { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		[DisplayName("Employee")]
		public string[] EmployeeId { get; set; }
		public string EmpId { get; set; }

		public List<SelectListItem> EmployeeList { get; set; }
		//public List<MultiSelectList> MEmployeeList { get; set; }
		public int[] SelectedIds { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Remarks { get; set; }
        public string FromDateNepali { get; set; }
        public string ToDateNepali { get; set; }
        public List<FixedScheduleEmployeeList> EmployeeListItem { get; set; } = new List<FixedScheduleEmployeeList>();
    }

    public class FixedScheduleEmployeeList
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Emp_Name { get; set; }
    }
}