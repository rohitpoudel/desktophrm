﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HRM
{
    public class OfficeVisit
	{
		public Int64 SNo { get; set; }
        public int BranchId { get; set; }
        public int Id { get; set; }
        [DisplayName("From")]
        [Required]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime FromDate { get; set; }
        [DisplayName("To")]
        [Required]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime ToDate { get; set; }
		[DataType(DataType.MultilineText)]
		public string Remark { get; set; }
        public char Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public char Event { get; set; }
        public bool Is_Active { get; set; }
        [DisplayName("Employee Name")]
        public int Employee_Id { get; set; }

        public string Emp_Name { get; set; }
        public string FromDateNepali { get; set; }
        public string ToDateNepali { get; set; }
        public string OfficeName { get; set; }

        //public List<SelectList> EmployeeList { get; set; }
    }
}