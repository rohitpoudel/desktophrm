﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
	public class Holiday
	{
		public Int64 SNo { get; set; }
        public string[] DepartmentId { get; set; }
        public string Departments { get; set; }

        public int Id { get; set; }
		[DisplayName("Holiday Name")]
		public string HolidayName { get; set; }
		[DisplayName("Holiday Type")]
		public string HolidayType { get; set; }
		[DisplayName("Applicable Religion")]
		public string ApplicableReligion { get; set; }
		[DisplayName("Applicable Gender")]
		public string ApplicableGender { get; set; }
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }
		[DisplayName("From")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime FromDate { get; set; }
        public string FromDateNepali { get; set; }
        [DisplayName("To")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		[DataType(DataType.Date)]
		public DateTime ToDate { get; set; }
        public string ToDateNepali { get; set; }

        public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		[DataType(DataType.MultilineText)]
		public string Remarks { get; set; }
		public char Event { get; set; }
		public bool IsDeleted { get; set; }
		public bool DataExist { get; set; }	

	}
}