﻿using System;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class TimePunch
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        public string MaxWorkingHour { get; set; }
        public string AllowedLateIn { get; set; }
        public string AllowedEarlyOut { get; set; }
        public string HalfDayWorkingHour { get; set; }
        public string ShortDayWorkingHour { get; set; }
        public string PresentMarkingDuration { get; set; }
        public bool NoPunch { get; set; }
        public bool SinglePunch { get; set; }
        public bool MultiplePunch { get; set; }
        public bool TwoPunch { get; set; }
        public bool FourPunch { get; set; }
        public bool ConsiderTimeLoss { get; set; }
        public bool HalfDayMarking { get; set; }
        public bool IsOTAllowed { get; set; }

    }
    public class TimePunchVM
    {
        public int Id { get; set; }
        [Display(Name = "Max Working Hour")]
        [Required]
        [Timestamp]
        public byte MaxWorkingHour { get; set; }
        [Display(Name = "Allowed Late In")]
        [Required]
        [Timestamp]
        public byte AllowedLateIn { get; set; }
        [Display(Name = "Allowed Early Out")]
        [Required]
        [Timestamp]
        public byte AllowedEarlyOut { get; set; }
        [Display(Name = "Half Day Working Hour")]
        [Required]
        [Timestamp]
        public byte HalfDayWorkingHour { get; set; }
        [Display(Name = "Short Day Working Hour")]
        [Required]
        [Timestamp]
        public byte ShortDayWorkingHour { get; set; }
        [Display(Name = "Present Marking Duration")]
        [Required]
        [Timestamp]
        public byte PresentMarkingDuration { get; set; }
        [Display(Name = "No Punch")]
        public bool NoPunch { get; set; }
        [Display(Name = "Single Punch")]
        public bool SinglePunch { get; set; }
        [Display(Name = "Multiple Punch")]
        public bool MultiplePunch { get; set; }
        [Display(Name = "Two Punch")]
        public bool TwoPunch { get; set; }
        [Display(Name = "Four Punch")]
        public bool FourPunch { get; set; }
        [Display(Name = "Consider Time Loss")]
        public bool ConsiderTimeLoss { get; set; }
        [Display(Name = "Half Day Marking")]
        public bool HalfDayMarking { get; set; }
        [Display(Name = "Is OT Allowed")]
        public bool IsOTAllowed { get; set; }


    }

}