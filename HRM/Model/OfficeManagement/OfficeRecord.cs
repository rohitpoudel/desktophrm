﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class OfficeRecord
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string Name { get; set; }
        public DateTime DateOfJoin { get; set; }
        public string EmployeeDeviceCode { get; set; }
        public Designation Designation { get; set; }
        public int DesignationId { get; set; }
        public Department Department { get; set; }
        public int DepartmentId { get; set; }
        public Section Section { get; set; }
        public int SectionId { get; set; }
        public GradeGroup GradeGroup { get; set; }
        public int GradeId { get; set; }
    }
    public class OfficeRecordVM
    {
        public int Id { get; set; }
        [DisplayName("Employee Code")]
        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string EmployeeCode { get; set; }
        [DisplayName("Employee Name")]
        [StringLength(250, MinimumLength = 7)]
        [Required]
        public string EmployeeName { get; set; }
        [DisplayName("Name(Nepali)")]
        [StringLength(250, MinimumLength = 7)]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Start Date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime DateOfJoin { get; set; }
        [DisplayName("Device Code")]
        [StringLength(250)]
        [Required]
        public string EmployeeDeviceCode { get; set; }
        [StringLength(250)]
        [Required]
        public Designation Designation { get; set; }
        public int DesignationId { get; set; }
        [StringLength(250)]
        [Required]
        public Department Department { get; set; }
        public int DepartmentId { get; set; }
        [StringLength(250)]
        [Required]
        public Section Section { get; set; }
        public int SectionId { get; set; }
        [DisplayName("Grade Group")]
        [StringLength(250)]
        [Required]
        public GradeGroup GradeGroup { get; set; }
        public int GradeId { get; set; }
        [DisplayName("Are You a Manager?")]
        public bool IsManager { get; set; }

    }
}