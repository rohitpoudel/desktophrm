﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HRM.OfficeManagement
{
    public class KajApproval
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        [DisplayName("From")]
        [Required]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime From { get; set; }
        [Required]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime To { get; set; }
		 [DataType(DataType.MultilineText)]
        public string Remark { get; set; }
        public bool Status { get; set; }
        public char Event { get; set; }
        public bool IsActive { get; set; }
        public int Emp_Id { get; set; }
        public string EmployeeName { get; set; }
        public List<Employee> EmployeeList { get; set; }
    }
}