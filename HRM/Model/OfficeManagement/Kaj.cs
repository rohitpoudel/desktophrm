﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HRM
{
    public class Kaj
    {
        public Int64 SNo { get; set; }
        public int BranchId { get; set; }
        public int Id { get; set; }
        [DisplayName("From")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayName("To")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string Remark { get; set; }
        public char Status { get; set; }
        public int Created_By { get; set; }
        public DateTime? Created_Date { get; set; }
        public char Event { get; set; }
        public bool Is_Active { get; set; }
        [DisplayName("Employee Name")]
        public int Emp_Id { get; set; }

        public string Emp_Name { get; set; }
       // public List<SelectList> EmployeeList { get; set; }
        public string FromDateNepali { get; set; }
        public string ToDateNepali { get; set; }
        public string Title { get; set; }

    }
}