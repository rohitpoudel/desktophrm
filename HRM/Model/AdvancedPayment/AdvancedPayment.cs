﻿using System;
using System.Collections.Generic;



namespace HRM
{
	public class AdvancedPayment
	{
		public Int64 SNo { get; set; }

		public int Id { get; set; }
	
		public DateTime Date { get; set; }

		public double Amount { get; set; }
		public int Created_By { get; set; }
		public DateTime Created_Date { get; set; }
		
		public string Remarks { get; set; }
		//public IEnumerable<SelectListItem> EmployeeList { get; set; }
		public string Emp_Name { get; set; }
		public bool IsDeleted { get; set; }
		public char Event { get; set; }
        public int Emp_Id { get; set; }

    }
}