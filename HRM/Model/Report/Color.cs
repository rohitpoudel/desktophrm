﻿

namespace HRM
{
    public class Color
    {
        public int Id { get; set; }
        public string LateInColor { get; set; }
        public string EarlyOutColor { get; set; }
        public string WeekendColor { get; set; }
        public string AbsentColor { get; set; }
        public string HolidayColor { get; set; }
        public string ShiftNotAsignColor { get; set; }
        public string DidNotLogoutColor { get; set; }
        public string LeaveColor { get; set; }
        public string PresentColor { get; set; }
        public string AbsentByLE { get; set; }
    }
}