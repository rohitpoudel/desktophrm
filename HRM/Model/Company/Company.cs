﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HRM

{
    public class Company
    {
        public Int64 SNo { get; set; }
        public int CountP { get; set; }
        public string FullPath { get; set; }
        public int? Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name = "Company Code")]
       // [Remote("GetCode", "Company", HttpMethod = "POST", ErrorMessage = "This Code already exists.")]
        public string CompanyCode { get; set; }
        [Display(Name = "Company Address")]
        [Required]
        [StringLength(50)]
        public string CompanyAddress { get; set; }
        [Display(Name = "Phone No.")]
        [Required]
        public string PhoneNo { get; set; }
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
        //[Display(Name = "Web Address")]   
        public string Web { get; set; }
        public string Logo { get; set; }
        public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public string TaxationNumber { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        [DisplayName("Validate From")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        public string FromDateNepali { get; set; }
        [DisplayName("Validate To")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
        public string ToDateNepali { get; set; }
    }
    public class CompanyViewModel
    {
        public Int64 SNo { get; set; }

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name = "Company Code")]
        public string CompanyCode { get; set; }
        [Display(Name = "Company Address")]
        [Required]
        [StringLength(50)]
        public string CompanyAddress { get; set; }
        [Display(Name = "Phone No.")]
        [Required]
        public string PhoneNo { get; set; }
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
        [Display(Name = "Web Address")]
        public string Web { get; set; }
        public string Logo { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool DataExist { get; set; }
        public int TotalLeftDays { get; set; }

        public string ValidityFromNepali { get; set; }
        public string ValidityTillNepali { get; set; }
        public DateTime ValidityTill { get; set; }
        public DateTime ValidityFrom { get; set; }
    }

}