﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class Allowance
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Allowance Type")]
        public string Type { get; set; }

        [Required]
        [Display(Name = "Allowance")]
        public decimal AllowanceAmount { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }
        public IEnumerable<SelectListItem> AllowancesList { get; set; }
    }

    public class AllowanceViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Allowance Type")]
        public string Type { get; set; }

        public string strType { get; set; }

        [Required]
        [Display(Name = "Allowance")]
        public decimal AllowanceAmount { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }
    }
}