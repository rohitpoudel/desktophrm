﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class ResidentialTax
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Slab")]
        public string Slab { get; set; }

        [Required]
        [Display(Name = "Range From")]
        public decimal RangeFrom { get; set; }

        [Required]
        [Display(Name = "Range To")]
        public decimal? RangeTo { get; set; }

        [Required]
        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        [Required]
        [Display(Name = "Taxt Type")]
        public string TaxType { get; set; }

        [Required]
        [Display(Name = "Tax")]
        public decimal Tax { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }

        public IEnumerable<SelectListItem> FiscalYearList { get; set; }
    }

    public class ResidentialTaxViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Slab")]
        public string Slab { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]
        [Required]
        [Display(Name = "Range From")]
        public decimal? RangeFrom { get; set; }

        [Required]
        [Display(Name = "Range To")]
        public decimal RangeTo { get; set; }

        [Required]
        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        public string FiscalYearName { get; set; }

        [Required]
        [Display(Name = "Taxt Type")]
        public string TaxType { get; set; }

        [Required]
        [Display(Name = "Tax")]
        public decimal Tax { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }
        public string strMaritalStatus { get; set; }
        public string strGender { get; set; }
        public string YearNepali { get; set; }

    }
}