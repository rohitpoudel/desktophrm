﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class DeductionRebateTax
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        [Required]
        [Display(Name = "Deduction or Rebate")]
        public string DeductionOrRebate { get; set; }

        [Required]
        [Display(Name = "Deduction On")]
        public int DeductionOn { get; set; }

        [Required]
        [Display(Name = "Deduction Type")]
        public string Type { get; set; }

        [Required]
        [Display(Name = "Deduction")]
        public decimal Deduction { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }

        public IEnumerable<SelectListItem> FiscalYearList { get; set; }
        public IEnumerable<SelectListItem> DeductionOnList { get; set; }
        public IEnumerable<SelectListItem> DeductionRebateList { get; set; }
    }

    public class DeductionRebateTaxViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Fiscal Year")]
        public int FiscalYearId { get; set; }

        [Required]
        [Display(Name = "Deduction or Rebate")]
        public string DeductionOrRebate { get; set; }

        public string strDeductionOrRebate { get; set; }

        [Required]
        [Display(Name = "Deduction On")]
        public int DeductionOn { get; set; }

        [Required]
        [Display(Name = "Deduction Type")]
        public string Type { get; set; }

        public string strType { get; set; }

        [Required]
        [Display(Name = "Deduction")]
        public decimal Deduction { get; set; }

        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }

        public string FiscalYearName { get; set; }
        public string Slab { get; set; }
        public string YearNepali { get; set; }

    }
}