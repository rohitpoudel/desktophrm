﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class NonResidentialTax
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Non Residential Tax")]
        public decimal Tax { get; set; }
        [Required]
        [Display(Name = "Tax Type (Percent/Flat)")]
        public string TaxType { get; set; }
        [Display(Name = "Fiscal Year")]
        [Required]
        public int FiscalYearId { get; set; }
        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }

        public IEnumerable<SelectListItem> FiscalYearList { get; set; }
    }

    public class NonResidentialTaxViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Non Residential Tax")]
        public decimal Tax { get; set; }
        [Display(Name = "Tax Type")]
        public string TaxType { get; set; }
        [Display(Name = "Fiscal Year")]
        [Required]
        public int FiscalYearId { get; set; }
        public string FiscalYearName { get; set; }
        public bool Is_Active { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string YearNepali { get; set; }

    }
}