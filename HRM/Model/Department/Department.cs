﻿using System;
using System.ComponentModel.DataAnnotations;
namespace HRM
{
    public class Department
    {
		public Int64 SNo { get; set; }

		public int? Id { get; set; }
        [Required]
        [Display(Name = "Department Name")]
        [StringLength(maximumLength:50)]
        public string DepartmentName { get; set; }
        [Required]
        [Display(Name = "Department Code")]
        public string DepartmentCode { get; set; }
        [Display(Name = "Number Of Staffs")]
        public int? NoOfStaff { get; set; }
        public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
    public class DepartmentViewModel
    {
		public Int64 SNo { get; set; }

		public int Id { get; set; }
        [Required]
        [Display(Name = "Department Name")]
        [StringLength(maximumLength: 50)]
        public string DepartmentName { get; set; }
        [Required]
        [Display(Name = "Department Code")]
        public string DepartmentCode { get; set; }
        [Display(Name = "Number Of Staffs")]
        public int? NoOfStaff { get; set; }
        public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
		public bool DataExist { get; set; }
	}
}
