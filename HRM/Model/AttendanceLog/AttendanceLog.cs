﻿using System;

namespace HRM
{
    public class AttendanceLog
    {
        public string Date { get; set; }
        public int Id { get; set; }
        public string DateNepali { get; set; }

        public int EmployeeAttendanceId { get; set; }
        public int DesignationId { get; set; }
        public int DepartmentId { get; set; }
        public string DesignationName { get; set; }
        public string DepartmentName { get; set; }
        public string Emp_Name { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string CheckInTime { get; set; }
        public string Remark { get; set; }
        public string Type { get; set; }
        public string Mode { get; set; }
        public int Code { get; set; }
        public string Emp_Code { get; set; }
        public Int64 Sno { get; set; }
        public string OutTime { get; set; }
        public string InTime { get; set; }








    }
}