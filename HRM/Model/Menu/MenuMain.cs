﻿using System;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class MenuMain
    {
        public int MenuId { get; set; }
        [Required]
        [Display(Name = "Menu Caption")]
        public string MenuCaption { get; set; }
        [Required]
        [Display(Name = "Parent Menu")]
        public int PMenuId { get; set; }
        public byte[] Image { get; set; }
        [Display(Name = "Menu Order")]
        public int MenuOrder { get; set; }
        [Display(Name = "Menu Icon")]
        public string MenuIcon { get; set; }
        [Display(Name = "Is Group")]
        public bool IsGroup { get; set; }
        [Display(Name = "Is Enable")]
        public bool IsEnable { get; set; }
        [Display(Name = "Is View Menu")]
        public bool IsLayoutMenu { get; set; }
        [Display(Name = "Controller")]
        public string Controler { get; set; }
        [Display(Name = "Action")]

        public string Acton { get; set; }
        [Display(Name = "Icon Color")]
        public string IconColor { get; set; }
        [Display(Name = "Menu Color")]

        public string MenuColor { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public char Event { get; set; }

    }
}