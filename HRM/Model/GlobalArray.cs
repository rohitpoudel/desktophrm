﻿using System.Collections.Generic;
namespace HRM
{
    public  class GlobalArray
    {
        public  List<int> MenuId = new List<int>();
        public  string UserCode { get; set; }
        public  string IsSuperAdmin { get; set; }
        public  int UserId { get; set; }
        public  int AddMenuTemp { get; set; }
        public  int EditMenuTemp { get; set; }
        public  int DeleteMenuTemp { get; set; }
        public  int AddDevice { get; set; }
        public  int EditDevice { get; set; }
        public  int DeleteDevice { get; set; }
        public  int AddLateRule { get; set; }
        public  int EditLateRule { get; set; }
        public  int DeleteLateRule { get; set; }
        public  int AddFiscalYear { get; set; }
        public  int EditFiscalYear { get; set; }
        public  int DeleteFiscalYear { get; set; }
        public  int AddOverTime { get; set; }
        public  int EditOverTime { get; set; }
        public  int DeleteOverTime { get; set; }
        public  int AddOfficeDays { get; set; }
        public  int EditOfficeDays { get; set; }
        public  int DeleteOfficeDays { get; set; }
        public  int AddShift { get; set; }
        public  int EditShift { get; set; }
        public  int DeleteShift { get; set; }

        public  int AddDynamicSchedule { get; set; }
        public  int EditDynamicSchedule { get; set; }
        public  int DeleteDynamicSchedule { get; set; }
        public  int AddWeeklySchedule { get; set; }
        public  int EditWeeklySchedule { get; set; }
        public  int DeleteWeeklySchedule { get; set; }
        public  int AddFixedSchedule { get; set; }
        public  int EditFixedSchedule { get; set; }
        public  int DeleteFixedSchedule { get; set; }
        public  int AddUser { get; set; }
        public  int EditUser { get; set; }
        public  int DeleteUser { get; set; }
        public  int AddEmployee { get; set; }
        public  int EditEmployee { get; set; }
        public  int DeleteEmployee { get; set; }
        public  int AddDesignation { get; set; }
        public  int EditDesignation { get; set; }
        public  int DeleteDesignation { get; set; }
        public  int AddDepartment { get; set; }
        public  int EditDepartment { get; set; }
        public  int DeleteDepartment { get; set; }
        public  int AddCompany { get; set; }
        public  int EditCompany { get; set; }
        public  int DeleteCompany { get; set; }
        public  int AddBranch { get; set; }
        public  int EditBranch { get; set; }
        public  int DeleteBranch { get; set; }
        public  int AddCompanyDbInfo { get; set; }

        public  int AddSection { get; set; }
        public  int EditSection { get; set; }
        public  int DeleteSection { get; set; }
        public  int AddGradeGroup { get; set; }
        public  int EditGradeGroup { get; set; }
        public  int DeleteGradeGroup { get; set; }
        public  int AddBank { get; set; }
        public  int EditBank { get; set; }
        public  int DeleteBank { get; set; }
        public  int AddEvent { get; set; }
        public  int EditEvent { get; set; }
        public  int DeleteEvent { get; set; }
        public  int AddHoliday { get; set; }
        public  int EditHoliday { get; set; }
        public  int DeleteHoliday { get; set; }
        public  int AddNotice { get; set; }
        public  int EditNotice { get; set; }
        public  int DeleteNotice { get; set; }
        public  int AddOfficeVisit { get; set; }
        public  int EditOfficeVisit { get; set; }
        public  int DeleteOfficeVisit { get; set; }       
        public  int DeleteOfficeApproval { get; set; }
        public  int AddLeaveMaster { get; set; }
        public  int EditLeaveMaster { get; set; }
        public  int DeleteLeaveMaster { get; set; }
        public  int AddLeaveApplication { get; set; }
        public  int EditLeaveApplication { get; set; }
        public  int DeleteLeaveApplication { get; set; }
        public  int AddLeaveQuota { get; set; }
        public  int EditLeaveQuota { get; set; }
        public  int DeleteLeaveQuota { get; set; }
        public  int AddLeaveSettlement { get; set; }
        public  int EditLeaveSettlement { get; set; }
        public  int DeleteLeaveSettlement { get; set; }

        public  int AddAllowance { get; set; }
        public  int EditAllowance { get; set; }
        public  int DeleteAllowance { get; set; }
        public  int AddDeductionRebate { get; set; }
        public  int EditDeductionRebate { get; set; }
        public  int DeleteDeductionRebate { get; set; }
        public  int AddResidentalTax { get; set; }
        public  int EditResidentalTax { get; set; }
        public  int DeleteResidentalTax { get; set; }
        public  int AddNonResidentalTax { get; set; }
        public  int EditNonResidentalTax { get; set; }
        public  int DeleteNonResidentalTax { get; set; }
        public  int SendEmail { get; set; }

        public  int ViewAllUser { get; set; }
        public  int ViewAllEmployee { get; set; }
       // publiic int ViewAllEmpDailyRpt { get; set; }
        public  int ViewAllEmpRpt { get; set; }

        public  int CreateEmpSalary   { get; set; }
        public  int CreateBasicDetail { get; set; }
        public  int CreateEmpShift    { get; set; }
        public  int CreateEmpTimepunch { get; set; }
        public  int ViewAttendanceReport { get; set; }
        public int ViewAllEmployeeLeave { get; set; }

        public int AddEmployeeResignation    { get; set; }
        public int EditEmployeeResignation   { get; set; }
        public int ApproveEmployeeResignation { get; set; }

        public int ViewAllBranchData         { get; set; }
        public int AddEmplyeeTransfer         { get; set; }
        public int EditEmplyeeTransfer        { get; set; }
        public int ApproveEmplyeeTransfer     { get; set; }
        public int DeleteEmplyeeTransfer      { get; set; }

        public int CreateRemoteAttendance { get; set; }
        public int EditRemoteAttendance { get; set; }
        public int DeleteRemoteAttendance { get; set; }
        public int IsGovermental { get; set; }

    }
}