﻿using System;
using System.ComponentModel.DataAnnotations;
namespace HRM.MenuTemplate
{
    public class MenuTemplate
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
        public string RoleNameNepali { get; set; }
        public int DesignationId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public char Event { get; set; }
    }
}