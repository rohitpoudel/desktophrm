﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM

{
    public class ColorSetUp
    {
        public int id { get; set; }
        [Required]
        [StringLength(3)]
        [Display(Name = "Red")]
        public int Red { get; set; }
        [Required]
        [StringLength(3)]
        [Display(Name = "Green")]
        public int Green { get; set; }
        [Required]
        [StringLength(3)]
        [Display(Name = "Blue")]
        public int Blue { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64? LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        
            
            
            

    }
   
}
