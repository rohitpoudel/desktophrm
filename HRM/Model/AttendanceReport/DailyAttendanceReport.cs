﻿using System;
using System.ComponentModel.DataAnnotations;
namespace HRM
{
    public class DailyAttendanceReport
    {
        #region DailyReport
        public int TotalAbsent { get; set; }
        public int TotalPresent { get; set; }
		public int LateIn { get; set; }
		public int EarlyOut { get; set; }
		public string AllowLateIn { get; set; }
        public string AllowEarlyOut { get; set; }
        public string LoginTime { get; set; }
        public string LogOutTime { get; set; }
		public int OutTime { get; set; }
		public int InTime { get; set; }
		public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public DateTime CreateDate { get; set; }
        public int LeaveCount { get; set; }
        public int AbsWhenLIEO { get; set; }


        #endregion
    }
    public class WeeklyAttendanceReport
    {
        #region WeeklyReport
        public int LateInCount { get; set; }
        public int EarlyOutCount { get; set; }
        public int AbsentCount { get; set; }
        public int PresentCount { get; set; }
        public int LeaveCount { get; set; }
        public int AbsWhenLIEO { get; set; }


        #endregion
    }
    public class UserDetails
    {
        #region UserDetails
        public string CodeForChange { get; set; }

        public int Id { get; set; }
        public string FullPath { get; set; }
        public int Count { get; set; }
        public int EmpId { get; set; }
        public string Emp_Code { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_DeviceCode { get; set; }
        public DateTime DOB { get; set; }
        [RegularExpression(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
        public string CitizenShip_No { get; set; }
        public string photo { get; set; }
        public string Temp_Address { get; set; }
        public string Permanet_Address { get; set; }
        public string GenderName { get; set; }
        public string Marital_Status_Name { get; set; }
        public string Blood_Group_Name { get; set; }
        public string Mobile_No { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string PassWord { get; set; }
        public string ImgPath { get; set; }
        [RegularExpression(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))", ErrorMessage = "Invalid Email")]
        public string UserName { get; set; }
        public string Company_Code { get; set; }

        #endregion
    }
    public class FilteredLeave
    {
        public int EmpId { get; set; }
        public DateTime FromDate { get; set; }
        public string Status { get; set; }
        public int LeaveDay { get; set; }
        public int DeptId { get; set; }

    }
    public class EmployeeSummary
    {
        public int WorkedHours { get; set; }
        public int ExtraTime { get; set; }
        public int EmpId { get; set; }
        public string Emp_Name { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int DeptId { get; set; }
        public int DesgId { get; set; }
        public string DesignationName { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public int WorkingDays { get; set; }
        public int PresentCount { get; set; }
        public int LateInCount { get; set; }
        public int EarlyInCount { get; set; }
        public int EatlyOutCount { get; set; }

        public int LateOutCount { get; set; }

        public decimal LeaveCount { get; set; }
        public int AbsentCount { get; set; }
        public TimeSpan TotalOT { get; set; }
        public int LEA { get; set; }
        public int WeekendCount { get; set; }
        public int HolidayCount { get; set; }
        public int WorkedOnHoliday { get; set; }
        public int WorkedOnWeekend { get; set; }

    }
    public class HolidayDetails
    {
        public int EmpId { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string Status { get; set; }
        public int DeptId { get; set; }
        public string HolidayName { get; set; }

    }
    public class AbsentEOLI
    {
        public int EarlyOutAbsent { get; set; }
        public int LateInAbsent { get; set; }
        public string AllowedLateInTime { get; set; }
        public string AllowedEarlyOutTime { get; set; }
        public string EmpDeviceCode { get; set; }
        public string MaxEarlyGrace { get; set; }
        public string MaxLateGrace { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public DateTime LoginDate { get; set; }
        public string LoginDateNep { get; set; }
        public string Emp_Name { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string DesignationName { get; set; }
        public int DeptId { get; set; }
        public string DepartmentName { get; set; }
        public TimeSpan WorkedHr { get; set; }
        public int EmpId { get; set; }

    }
    public class AttendanceDetails
    {
        public string AllowedLateInTime { get; set; }
        public string AllowedEarlyOutTime { get; set; }
        public string EmpDeviceCode { get; set; }
        public string OverTime { get; set; }
        public string ExtraTime { get; set; }
        public string EarlyLateIn { get; set; }
        public string EarlyLateOut { get; set; }
        public int EarlyOutAbsent { get; set; }
        public int LateInAbsent { get; set; }
        public TimeSpan EShiftStart { get; set; }
        public TimeSpan EShiftEnd { get; set; }
        public DateTime FromDate { get; set; }
        public string WeeklyOff { get; set; }

        public string FromDateNep { get; set; }
        public DateTime ToDate { get; set; }
        public string ToDateNep { get; set; }
        public int EmpId { get; set; }
        public TimeSpan WorkedHr { get; set; }
        public int CuurentLeaveCount { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public string Emp_Name { get; set; }
        public string DesignationName { get; set; }
        public int DeptId { get; set; }

        public string DepartmentName { get; set; }
        public string Status { get; set; }

        public int Code { get; set; }
        public DateTime date { get; set; }
        public string dateNep { get; set; }
        public string DeviceIP { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string AllowedLateTime { get; set; }
        public string AllowedEarlyTime { get; set; }
        public string ShiftStart { get; set; }
        public string IpAddress { get; set; }
        public string ShiftEnd { get; set; }
        public DateTime LogInTime { get; set; }
        public DateTime LoginDate { get; set; }
        public string LoginDateNep { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
        public DateTime AttendanceDate { get; set; }

    }
    public class ValidityDetails
    {
        public DateTime ValidityTill { get; set; }
        public int TotaLdays { get; set; }
        public int Percentage { get; set; }
        public int ValidateRenaining { get; set; }
        public string ValidateDateNepali { get; set; }
    }
    public class EmployeeDualShift
    {
        public TimeSpan ShiftHours { get; set; }
        public string BranchId { get; set; }
        public string EmpDeviceCode { get; set; }
        public string FirstShiftStart { get; set; }
        public string SecondShiftStart { get; set; }
        public string FirstShiftEnd { get; set; }
        public string SecondShiftEnd { get; set; }
        public int DeptId { get; set; }
        public string DepartmentName { get; set; }
        public string Emp_Name { get; set; }
        public int EmpId { get; set; }
        public string DesignationName { get; set; }
        public string FirstShiftInTime { get; set; }
        public string SecondShiftInTime { get; set; }
        public string FirstShiftOutTime { get; set; }
        public string SecondShiftOutTime { get; set; }
        public TimeSpan FirstShiftWorkedHr { get; set; }
        public TimeSpan SecondShiftWorkedHr { get; set; }
        public DateTime AttendanceDate { get; set; }
        public int FirstShiftEarlyOut { get; set; }
        public int FirstShiftLateIn { get; set; }
        public int SecondShiftEarlyOut { get; set; }
        public int SecondShiftLateIn { get; set; }
        public string WeeklyOff { get; set; }
        public string Status { get; set; }
        public int FirstShiftEarlyOutAbsent { get; set; }
        public int FirstShiftLateInAbsent { get; set; }
        public int SecondShiftEarlyOutAbsent { get; set; }
        public int SecondShiftLateInAbsent { get; set; }
        public string ExtraTime { get; set; }
    }
}