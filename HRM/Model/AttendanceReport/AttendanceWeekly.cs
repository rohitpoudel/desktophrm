﻿using System;
namespace HRM
{
    public class AttendanceWeekly
    {
        public Int64  SNo { get; set; }
        public int Id { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public DateTime LogInTime { get; set; }
        public string LogInTimeNep { get; set; }
        public string ActualworkedHour { get; set; }
        public string Days { get; set; }
        public string Emp_Code { get; set; }
        public string Emp_Name { get; set; }
        public string DesignationName { get; set; }
        public string DepartmentName { get; set; }
        public int NepWeekNumber { get; set; }
        public int WeekNumber { get; set; }



    }
}