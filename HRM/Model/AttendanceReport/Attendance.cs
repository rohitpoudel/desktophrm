﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace HRM
{
    public class Attendance
    {
        public string AllowedLateInTime { get; set; }
        public string AllowedEarlyOutTime { get; set; }
        public string AllowedLateTime { get; set; }
        public string AllowedEarlyTime { get; set; }
        public string OverTime { get; set; }
        public string ExtraTime { get; set; }

        public string EarlyLateIn { get; set; }
        public string EarlyLateOut { get; set; }
        public int EarlyOutAbsent { get; set; }
        public int LateInAbsent { get; set; }
        public string Holiday { get; set; }
        public string Leave { get; set; }
        public string Weekend { get; set; }

        public int Id { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public string DeviceIP { get; set; }
        public DateTime LogInTime { get; set; }
        public string LogInTimeNep { get; set; }
        public DateTime LoginOutTime { get; set; }
        public DateTime LogTime { get; set; }
        public string Emp_Name { get; set; }
        public string WorkingHour { get; set; }
        public string DepartmentName { get; set; }
        public string BranchName { get; set; }
        public int BranchId { get; set; }

        public string DesignationName { get; set; }
        public string ActualworkedHour { get; set; }
        public string OutTime { get; set; }
        public string InTime { get; set; }

        public int DesignationId { get; set; }
        public int DepartmentId { get; set; }
        public string Date { get; set; }
        [DisplayName("Nepali Date")]
        public string DateNepali { get; set; }

        public string Year { get; set; }
        public string Month { get; set; }
        public int WeekNumber { get; set; }
        public string Result { get; set; }
        public string Days { get; set; }
        public int YearValue { get; set; }
        public int TotalPresent { get; set; }

        public int Code { get; set; }

        public int Jan { get; set; }
        public int Feb { get; set; }
        public int Mar { get; set; }
        public int Apr { get; set; }
        public int May { get; set; }
        public int Jun { get; set; }
        public int Jul { get; set; }
        public int Aug { get; set; }
        public int Sep { get; set; }
        public int Oct { get; set; }
        public int Nov { get; set; }
        public int Dec { get; set; }


        public string WeeklyOff { get; set; }
        public string ShiftHours { get; set; }
        public string Name { get; set; }
        public string ShiftType { get; set; }
        public TimeSpan AShiftStart { get; set; }
        public TimeSpan AShiftEnd { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string AllowLateIn { get; set; }
        public string AllowEarlyOut { get; set; }
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
        public string DiffernceHour { get; set; }
        public string Remarks { get; set; }
        public string Emp_Code { get; set; }
        public Int64 Sno { get; set; }

        public DateTime AttendanceDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string Remark { get; set; }
        public string Type { get; set; }
        public string Mode { get; set; }
        public string Status { get; set; }
        public DateTime StartDateNepali { get; set; }
        public DateTime EndDateNepali { get; set; }


        //public List<SelectListItem> Yearlist { get; set; }
        //public List<SelectListItem> Monthlist { get; set; }
        //public List<SelectListItem> EmplList { get; set; }








    }

    public class YearlyAttendance
    {
        public int WorkingDays
        {
            get; set;
        }
        public string Emp_DeviceCode { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public int YearName { get; set; }
        public int Id { get; set; }
        public string Emp_Name { get; set; }
        public int LN1 { get; set; }
        public int LN2 { get; set; }
        public int LN3 { get; set; }
        public int LN4 { get; set; }
        public int LN5 { get; set; }
        public int LN6 { get; set; }
        public int LN7 { get; set; }
        public int LN8 { get; set; }
        public int LN9 { get; set; }
        public int LN10 { get; set; }
        public int LN11 { get; set; }
        public int LN12 { get; set; }
        public int A1 { get; set; }
        public int A2 { get; set; }
        public int A3 { get; set; }
        public int A4 { get; set; }
        public int A5 { get; set; }
        public int A6 { get; set; }
        public int A7 { get; set; }
        public int A8 { get; set; }
        public int A9 { get; set; }
        public int A10 { get; set; }
        public int A11 { get; set; }
        public int A12 { get; set; }
        public decimal L1 { get; set; }
        public decimal L2 { get; set; }
        public decimal L3 { get; set; }
        public decimal L4 { get; set; }
        public decimal L5 { get; set; }
        public decimal L6 { get; set; }
        public decimal L7 { get; set; }
        public decimal L8 { get; set; }
        public decimal L9 { get; set; }
        public decimal L10 { get; set; }
        public decimal L11 { get; set; }
        public decimal L12 { get; set; }
        public int EO1 { get; set; }
        public int EO2 { get; set; }
        public int EO3 { get; set; }
        public int EO4 { get; set; }
        public int EO5 { get; set; }
        public int EO6 { get; set; }
        public int EO7 { get; set; }
        public int EO8 { get; set; }
        public int EO9 { get; set; }
        public int EO10 { get; set; }
        public int EO11 { get; set; }
        public int EO12 { get; set; }
        public int st1 { get; set; }
        public int nd2 { get; set; }
        public int rd3 { get; set; }
        public int th4 { get; set; }
        public int th5 { get; set; }
        public int th6 { get; set; }
        public int th7 { get; set; }
        public int th8 { get; set; }
        public int th9 { get; set; }
        public int th10 { get; set; }
        public int th11 { get; set; }
        public int th12 { get; set; }
        public int Total { get; set; }
        public string Year { get; set; }
        public int Code { get; set; }
        public string Month { get; set; }
        public int DesignationId { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string BranchName { get; set; }

        public int LEA1 { get; set; }
        public int LEA2 { get; set; }
        public int LEA3 { get; set; }
        public int LEA4 { get; set; }
        public int LEA5 { get; set; }
        public int LEA6 { get; set; }
        public int LEA7 { get; set; }
        public int LEA8 { get; set; }
        public int LEA9 { get; set; }
        public int LEA10 { get; set; }
        public int LEA11 { get; set; }
        public int LEA12 { get; set; }

        //public List<SelectListItem> Yearlist { get; set; }
        //public List<SelectListItem> Monthlist { get; set; }
        //public List<SelectListItem> EmplList { get; set; }
    }


    public class DynamicAttendanceReports
    {

        public DateTime FromDate { get; set; }

        public DateTime FromDateNepali { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpId { get; set; }
        public String Year { get; set; }
        public DateTime ToDateNepali { get; set; }
    }
}