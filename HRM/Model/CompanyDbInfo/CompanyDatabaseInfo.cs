﻿using System;
using System.ComponentModel;
namespace HRM
{
	public class CompanyDatabaseInfo
	{
		public Int64 SNo { get; set; }
		public int Id { get; set; }
		[DisplayName("Company Code")]
		public string Company_Code { get; set; }
		[DisplayName("Company Name")]
		public string Company_Name { get; set; }
		[DisplayName("Database UserName")]
		public string Db_UserName { get; set; }
		[DisplayName("Database Name")]
		public string DB_Name { get; set; }
		public string Password { get; set; }
        [DisplayName("Password")]
        public string AppPassword { get; set; }

        public string DataSource { get; set; }
		public string Create_By { get; set; }
		public DateTime Created_Date { get; set; }
	}
    public class CompanyDatabaseInfoViewModel
    {
        public Int64 SNo { get; set; }
        public int Id { get; set; }
        [DisplayName("Company Code")]
        public string Company_Code { get; set; }
        [DisplayName("Company Name")]
        public string Company_Name { get; set; }
        public string CompanyName { get; set; }
        [DisplayName("Database User Name")]
        public string Db_UserName { get; set; }
        [DisplayName("Database Name")]
        public string DB_Name { get; set; }
        public string Password { get; set; }
        public string DataSource { get; set; }
        public string Create_By { get; set; }
        public DateTime Created_Date { get; set; }
    }
}