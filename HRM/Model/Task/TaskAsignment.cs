﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class TaskAssignment
    {
        public int Id { get; set; }
        public string TaskName { get; set; }
        public string TaskDescription { get; set; }
        [DisplayName("Assign To")]

        public string EmployeeName { get; set; }
        public string AssignedBy { get; set; }
        [DataType(DataType.Date)]

        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]

        public DateTime EndDate { get; set; }
        public bool TaskStatus { get; set; }
		public char Event { get; set; }
        public bool IsDeleted { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }

    }
    public class TaskAssignmentViewModel
    {
        public int Id { get; set; }
        public string TaskName { get; set; }
        [DataType(DataType.MultilineText)]
        public string TaskDescription { get; set; }
        [DisplayName("Assign To")]
        public string EmployeeName { get; set; }
        public string AssignedBy { get; set; }
        [DataType(DataType.Date)]

        public DateTime StartDate { get; set; }

        public bool TaskStatus { get; set; }
        [DataType(DataType.Date)]

        public DateTime EndDate { get; set; }
        public bool IsDeleted { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }

    }
}