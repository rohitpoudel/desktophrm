﻿using System;

namespace HRM
{
    public class EmployeeResignation
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int EmployeeId { get; set; }
        public string ResignationOrTerminate { get; set; }
        public string EmpName { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int Department_Id { get; set; }
        public int Designation_Id { get; set; }
        public string Section_Id { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public DateTime Date { get; set; }
        public string DateNepali { get; set; }
        public string IssueDate { get; set; }
        public string Status { get; set; }
        public bool Is_Active { get; set; }
    }

    public class ResignationReport
    {
        public int BranchId { get; set; }

        public int EmployeeId { get; set; }
        public string Emp_Name { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string SectionName { get; set; }
        public DateTime Issued_Date { get; set; }
        public string Issued_DateNp { get; set; }
        public DateTime Date { get; set; }
        public string DateNp { get; set; }
        public string ResignationOrTerminate { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
    }

    public class ManualAttendanceReport
    {
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string AttendanceDate { get; set; }
        public string AttendanceDateNep { get; set; }
        public string LogInTime { get; set; }
        public string LoginOutTime { get; set; }
        public string AttendanceType { get; set; }
        public string EnteredDate { get; set; }
        public string EnteredDateNep { get; set; }
        public string EnteredBy { get; set; }
        public string EnteredTime { get; set; }
    }
}