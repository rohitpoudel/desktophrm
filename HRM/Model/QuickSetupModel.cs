﻿

namespace HRM
{
    public class QuickSetupModel
    {
        public Devices Device { get; set; }
        public Designation Designation { get; set; }
        public Department Department { get; set; }
        public Section Section { get; set; }
        public GradeGroup GradeGroup { get; set; }
        public Employee Employee { get; set; }
        public User User { get; set; }
        public Shift Shift { get; set; }
        public FiscalYear FiscalYear { get; set; }

    }
}