﻿

namespace HRM
{
	public class LoginCredential
	{
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }

	}
}