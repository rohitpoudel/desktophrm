﻿using System;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class UserRole
    {
		public Int64 SNo { get; set; }
		public int Id { get; set; }
		[Display(Name = "User Role")]
		public string UserName { get; set; }
		[Display(Name = "User Role(Nepali)")]
		public string UserNameNepali { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public char Event { get; set; }
    }
}