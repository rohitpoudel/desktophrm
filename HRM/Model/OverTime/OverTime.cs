﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
	public class OverTime
	{
		public Int64 SNo { get; set; }
        public int BranchId { get; set; }
        public int Id { get; set; }
		[DisplayName("Employee")]
		public int EmployeeId { get; set; }
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime Date { get; set; }
		public string StartTime { get; set; }
		public string EndTime { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public bool IsDeleted { get; set; }
		public char Event { get; set; }
		public string Emp_Name { get; set; }
		public int OverTimeValue { get; set; }
        public string  NepDate { get; set; }

    }
}