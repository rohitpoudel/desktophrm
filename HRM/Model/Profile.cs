﻿using System;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class Profile
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Shift { get; set; }
        public string LoginStatus { get; set; }
        public string OTAllow { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime EmployeeSince { get; set; }
    }
}