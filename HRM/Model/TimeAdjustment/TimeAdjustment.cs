﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace HRM
{
	public class TimeAdjustment
	{
		public Int64 SNo { get; set; }
		public int Id { get; set; }
		[DisplayName("Employee")]
		public int EmployeeId { get; set; }
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime Date { get; set; }
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm tt}", ApplyFormatInEditMode = true)]
		public DateTime StartTime { get; set; }
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm tt}", ApplyFormatInEditMode = true)]
		public DateTime EndTime { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public bool IsDeleted { get; set; }
		public char Event { get; set; }
		public string Emp_Name { get; set; }
	}
}