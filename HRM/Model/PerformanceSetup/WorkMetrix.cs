﻿using System.ComponentModel.DataAnnotations;

namespace HRM
{
    public class WorkMetrix
    {
        public int Id { get; set; }       
        public string WorkMetrixName { get; set; }
        public PerformanceMeasureMetrix PerformanceMeasureMetrix { get; set; }
        public int PerformanceMeasureMetrixId { get; set; }

    }
    public class WorkMetrixViewModels
    {
        public int Id { get; set; }
        [Display(Name = "Task")]
        public string WorkMetrixName { get; set; }
        [Display(Name = "Performance")]
        public int PerformanceMeasureMetrixId { get; set; }
        public PerformanceMeasureMetrix PerformanceMeasureMetrix { get; set; }

    }
}