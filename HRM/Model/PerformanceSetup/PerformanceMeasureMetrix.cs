﻿using System.ComponentModel.DataAnnotations;


namespace HRM
{
    public class PerformanceMeasureMetrix
    {
        public int Id { get; set; }
        public string PerformanceMetrixName { get; set; }
        [Range(0,100)]
        public double Value { get; set; }
    }
    public class PerformanceMeasureMetrixViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Performance")]
        public string PerformanceMetrixName { get; set; }
        [Range(0, 100)]
        public double Value { get; set; }
    }
}