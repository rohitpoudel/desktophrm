﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;
//System.Web.WebPages;

namespace HRM
{

    // public class CGlobalModels
    // {
    //     #region Bind ddl
    //     public static List<SelectListItem> BindDesignation(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[DesignationName] DataTextField FROM [dbo].[Designations] where IsActive = 1 ";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindDesignationWithAll(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[DesignationName] DataTextField FROM [dbo].[Designations] where IsActive = 1 ";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindActiveEmployee()
    //     {
    //         string query = @"Select e.Id DataValueField , e.[Emp_Name] DataTextField from Employee e where Is_Active= 1 and Emp_DeviceCode !=0 ";
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if(gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"Select e.Id DataValueField , e.[Emp_Name] DataTextField from Employee e where Is_Active= 1 and Emp_DeviceCode !=0 AND BranchId = "+BranchId+ " ";
    //         }
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindManualEmployee(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         //string query = @"Select e.[Emp_DeviceCode] DataValueField , e.[Emp_Name] DataTextField 
    //      //from Employee e where  Is_Active = 1 and Emp_DeviceCode !=0 ";
    //         string query = @"Select e.Emp_DeviceCode DataValueField , e.[Emp_Name] DataTextField from Employee e where Is_Active= 1 and Emp_DeviceCode !=0 ";
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"Select e.Emp_DeviceCode DataValueField , e.[Emp_Name] DataTextField from Employee e where Is_Active= 1 and Emp_DeviceCode !=0 AND BranchId = " + BranchId + " ";
    //         }

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindManualEmployeeNotRegistered(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"Select e.[Emp_DeviceCode] DataValueField , e.[Emp_Name] DataTextField 
    //      from Employee e where  e.Emp_DeviceCode not in (Select al.DIN from AttendanceLog al where al.DIN = 2) ";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindMonthNepali(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Baisakh", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Jestha", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Asadh", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Shrawan", Value = "4" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Bhadra", Value = "5" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Aswin", Value = "6" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Kartik", Value = "7" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Mangsir", Value = "8" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Poush", Value = "9" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Magh", Value = "10" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Falgun", Value = "11" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Chaitra", Value = "12" });
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindDepartmentList(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[DepartmentName] DataTextField FROM [dbo].[Departments] where IsDeleted = 0";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         //ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeWithValidEmail()
    //     {
    //         string query = @"select Id DataValueField,Emp_Name DataTextField from employee where Is_Active=1 and EmailVerified=1";
    //         // get the regions from database
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"select Id DataValueField,Emp_Name DataTextField from employee where Is_Active=1 and EmailVerified=1 AND Employee.BranchId = " + BranchId + " ";
    //         }
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeWithouShift()
    //     {
    //         string query = @"select Employee.Id DataValueField, Emp_Name DataTextField 
    //                             from employee 
    //                             where Is_Active = 1 and Employee.Id NOT IN (Select Emp_Id from EmployeeShift) 
    //                             and Emp_DeviceCode != 0";
    //         // get the regions from database
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"select Employee.Id DataValueField, Emp_Name DataTextField 
    //                             from employee 
    //                             where Is_Active = 1 and Employee.Id NOT IN (Select Emp_Id from EmployeeShift) 
    //                             and Emp_DeviceCode != 0 AND Employee.BranchId = " + BranchId + " ";
    //         }
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeWithouShiftByDepartment(int DeptId)
    //     {
    //         string query = @"select Employee.Id DataValueField, Emp_Name DataTextField 
    //                             from employee 
    //                             where Is_Active = 1 
    //                             and Employee.Id NOT IN (Select Emp_Id from EmployeeShift) 
    //                             and Emp_DeviceCode != 0
    //                             AND Employee.Department_Id =" + DeptId + "";
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"select Employee.Id DataValueField, Emp_Name DataTextField 
    //                             from employee 
    //                             where Is_Active = 1 
    //                             and Employee.Id NOT IN (Select Emp_Id from EmployeeShift) 
    //                             and Emp_DeviceCode != 0
    //                             AND Employee.Department_Id =" + DeptId + " AND Employee.BranchId = " + BranchId + " ";
    //         }
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindDepartment(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[DepartmentName] DataTextField FROM [dbo].[Departments] where IsDeleted = 0";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindDepartmentWithAll(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[DepartmentName] DataTextField FROM [dbo].[Departments] where IsDeleted = 0";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindSectionList(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[SectionName] DataTextField FROM [dbo].[Sections] where IsDeleted=0";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         //ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindSection(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[SectionName] DataTextField FROM [dbo].[Sections] where IsDeleted=0";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindBranch(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[BranchName] DataTextField FROM [dbo].[Branch] where IsDeleted=0";
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"SELECT [Id] DataValueField ,[BranchName] DataTextField FROM [dbo].[Branch] where IsDeleted = 0 AND Id = " + BranchId + " ";
    //         }
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeByBranch(int BranchId)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[EmP_Name] DataTextField FROM [dbo].[Employee] where Is_Active=1 and (BranchId = "+BranchId+ " OR "+ BranchId + " = 0) and Emp_DeviceCode != 0";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindBranchWithAll(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[BranchName] DataTextField FROM [dbo].[Branch] where IsDeleted=0";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All Branch", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindActiveEmployeeWithDualShift()
    //     {
    //         string query = @"Select e.Id DataValueField , e.[Emp_Name] DataTextField 
    //      from Employee e
    //left join EmployeeShift as es on e.Id = es.Emp_Id
    // where Is_Active= 1 and Emp_DeviceCode !=0 and IsDualShift = 1 ";
    //         // get the regions from database

    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindGroup(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[GroupName] DataTextField FROM [dbo].[GradeGroups]";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindUser(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[RoleName] DataTextField FROM [dbo].[UserRole] where IsActive = 1";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindEmployee(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[Emp_Name] DataTextField FROM [dbo].[Employee] where Is_Active = 1 and Emp_DeviceCode !=0";
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"SELECT [Id] DataValueField ,[Emp_Name] DataTextField FROM [dbo].[Employee] where Is_Active = 1 and Emp_DeviceCode !=0 AND BranchId = " + BranchId + " ";
    //         }
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeWithEmail(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[Emp_Name] DataTextField FROM [dbo].[Employee] where Is_Active = 1 AND Email IS NOT NULL  ";
    //         // get the regions from database
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"SELECT [Id] DataValueField ,[Emp_Name] DataTextField FROM [dbo].[Employee] where Is_Active = 1 AND Email IS NOT NULL AND BranchId = " + BranchId + " ";
    //         }
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeWithAll(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[Emp_Name] DataTextField FROM [dbo].[Employee] where Is_Active = 1 and Emp_DeviceCode !=0 ";
    //         // get the regions from database
    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"Select e.Id DataValueField , e.[Emp_Name] DataTextField from Employee e where Is_Active= 1 and Emp_DeviceCode !=0 AND BranchId = " + BranchId + " ";
    //         }
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindShift(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[Name] DataTextField FROM [dbo].[Shift] where IsDeleted = 0";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindDualShift(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[Name] DataTextField FROM [dbo].[DualShift] where IsActive = 1";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindLeaveMaster(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[LeaveName] DataTextField FROM [dbo].[LeaveMaster]where IsDeleted = 0 ";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> LeaveMasterList(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = @"SELECT [Id] DataValueField ,[LeaveName] DataTextField FROM [dbo].[LeaveMaster]where IsDeleted = 0 ";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindCompanyCode(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [CompanyCode] DataValueField ,[CompanyName] DataTextField FROM [dbo].[Companies] where IsDeleted=0";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEmployeeandCode(string Id, string Code1, string Code2, string Station_Id)
    //     {

    //         string query = "SELECT [Id] DataValueField ,[Emp_Name] +' (' +[Emp_Code] +')' DataTextField FROM [dbo].[Employee] where Is_Active = 1 and Emp_DeviceCode !=0";

    //         GlobalArray gs = new GlobalArray();
    //         gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
    //         if (gs.ViewAllBranchData != 1)
    //         {
    //             int BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
    //             query = @"SELECT [Id] DataValueField ,[Emp_Name] +' (' +[Emp_Code] +')' DataTextField FROM [dbo].[Employee] where Is_Active = 1 and Emp_DeviceCode !=0 AND BranchId = " + BranchId + " ";
    //         }
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindYear(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         var currentYear = DateTime.Today.Year;
    //         for (int i = 33; i >= 0; i--)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = (currentYear - i).ToString(), Value = (currentYear - i).ToString() });
    //         }
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindYearWithAll(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         var currentYear = DateTime.Today.Year;
    //         for (int i = 33; i >= 0; i--)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = (currentYear - i).ToString(), Value = (currentYear - i).ToString() });
    //         }
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindPagging(List<int> page)
    //     {
    //         int count = 0;
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         foreach (var item in page)
    //         {
    //             count = count + 1;
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = Convert.ToString(item), Value = Convert.ToString(count) });

    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindMonthNumeric(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "January", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "February", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "March", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "April", Value = "4" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "May", Value = "5" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "June", Value = "6" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "July", Value = "7" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "August", Value = "8" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "September", Value = "9" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "October", Value = "10" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "November", Value = "11" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "December", Value = "12" });
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindMonthNumericWithAll(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "January", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "February", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "March", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "April", Value = "4" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "May", Value = "5" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "June", Value = "6" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "July", Value = "7" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "August", Value = "8" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "September", Value = "9" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "October", Value = "10" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "November", Value = "11" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "December", Value = "12" });
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindWeekNumeric(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "First", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Second", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Third", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Fourth", Value = "4" });

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindWeekNumericWithAll(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "First", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Second", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Third", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Fourth", Value = "4" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Fifth", Value = "5" });

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindHolidayType(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Religious", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "NonReligious", Value = "2" });

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindReligionNumeric(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Hinduism", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Buddhism", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Islam", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Christianity", Value = "4" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Judaism", Value = "5" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "6" });


    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindGenderNumeric(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Male", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Female", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Others", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "4" });

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindEventLevel(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "1" });
    //         // ddlItems.Add(new SelectListItem() { Selected = false, Text = "Branch", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Department", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Section", Value = "4" });

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindNoticeLevel(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "1" });
    //         // ddlItems.Add(new SelectListItem() { Selected = false, Text = "Branch", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Department", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Section", Value = "4" });

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindLeaveIncrementPeriod(string Value1, string Value2)
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();

    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Yearly", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Monthly", Value = "2" });

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindDeviceCode(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Code] DataValueField ,[Code] DataTextField from [dbo].[Devices] where IsDeleted=0 order by Code";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindDeviceCodeWithAll(string Id, string Code1, string Code2, string Station_Id)

    //     {
    //         string query = @"SELECT [Code] DataValueField ,[Code] DataTextField from [dbo].[Devices] where IsDeleted=0 order by Code";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "All", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindFiscalYear(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[Year] DataTextField from [dbo].[FiscalYears] where IsActive = 1 ORDER BY CAST(SUBSTRING(Year, 1, 4) AS INT) DESC";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindNepaliFiscalYear(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[YearNepali] DataTextField from [dbo].[FiscalYears] where IsActive = 1 ORDER BY CAST(SUBSTRING(Year, 1, 4) AS INT) DESC";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = false, Text = "Select", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindTaxSlab(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[Slab] DataTextField from [dbo].[ResidentialTax] where Is_Active = 1";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select deduction On", Value = "" });
    //         ddlItems.Add(new SelectListItem() { Text = "Total Income", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindAllowances(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[Name] DataTextField from [dbo].[Allowance] where Is_Active = 1";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Allowances", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindDeductionRebate(string Id, string Code1, string Code2, string Station_Id)
    //     {
    //         string query = @"SELECT [Id] DataValueField ,[Name] DataTextField from [dbo].[DeductionRebateTax] where Is_Active = 1";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Deduction/Rebate", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }

    //         return ddlItems;
    //     }

    //     public static List<SelectListItem> BindYearsByFiscalYear(int Id, string DateType)
    //     {
    //         string query = @"SELECT Year(StartDate) DataValueField ,Year(StartDate) DataTextField from FiscalYears where IsActive = 1 and Id = " + Id + " union SELECT Year(EndDate) DataValueField ,Year(EndDate) DataTextField from FiscalYears where IsActive = 1 and Id = " + Id + "";
    //         if (DateType != "English")
    //         {
    //             query = @"SELECT Year(StartDateNepali) DataValueField ,Year(StartDateNepali) DataTextField from FiscalYears where IsActive = 1 and Id = " + Id + " union SELECT Year(EndDateNepali) DataValueField ,Year(EndDateNepali) DataTextField from FiscalYears where IsActive = 1 and Id = " + Id + "";
    //         }
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Year", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindNepaliYearsByFiscalYear(int Id)
    //     {
    //         string query = @"SELECT Year(StartDate) DataValueField ,Year(StartDate) DataTextField from FiscalYears where IsActive = 1 and Id = " + Id + " union SELECT Year(EndDate) DataValueField ,Year(EndDate) DataTextField from FiscalYears where IsActive = 1 and Id = " + Id + "";
    //         // get the regions from database
    //         Dictionary<string, string> regions = CGlobalQueryProcessor.BindDropdownItems(query);
    //         // generate the dropdown items
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Year", Value = "0" });
    //         foreach (var key in regions.Keys)
    //         {
    //             ddlItems.Add(new SelectListItem() { Selected = false, Text = regions[key], Value = key });
    //         }
    //         return ddlItems;
    //     }
    //     public static List<SelectListItem> BindNepaliYears()
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Year", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2072", Value = "2072" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2073", Value = "2073" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2074", Value = "2074" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2075", Value = "2075" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2076", Value = "2076" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2077", Value = "2077" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "2078", Value = "2078" });
    //         return ddlItems;
    //     }
    //     #endregion

    //     public static DataTable ConvertExcelToDataTable(string FileName)
    //     {
    //         DataTable dtResult = null;
    //         int totalSheet = 0; //No of sheets on excel file  
    //         using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
    //         {
    //             objConn.Open();
    //             OleDbCommand cmd = new OleDbCommand();
    //             OleDbDataAdapter oleda = new OleDbDataAdapter();
    //             DataSet ds = new DataSet();
    //             DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
    //             string sheetName = string.Empty;
    //             if (dt != null)
    //             {
    //                 var tempDataTable = (from dataRow in dt.AsEnumerable()
    //                                      where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
    //                                      select dataRow).CopyToDataTable();
    //                 dt = tempDataTable;
    //                 totalSheet = dt.Rows.Count;
    //                 sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
    //             }
    //             cmd.Connection = objConn;
    //             cmd.CommandType = CommandType.Text;
    //             cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
    //             oleda = new OleDbDataAdapter(cmd);
    //             oleda.Fill(ds, "excelData");
    //             dtResult = ds.Tables["excelData"];
    //             objConn.Close();
    //             return dtResult; //Returning Dattable  
    //         }
    //     }
    //     public static List<SelectListItem> BindDays()
    //     {
    //         List<SelectListItem> ddlItems = new List<SelectListItem>();
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Year", Value = "0" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Sunday", Value = "1" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Monday", Value = "2" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Tuesday", Value = "3" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Wenesday", Value = "4" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Thursday", Value = "5" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Friday", Value = "6" });
    //         ddlItems.Add(new SelectListItem() { Selected = true, Text = "Saturday", Value = "7" });
    //         return ddlItems;
    //     }
    // }


    //public static List<SelectListItem> BindDualShift()
    //{
    //    List<SelectListItem> ddlItems = new List<SelectListItem>();
    //    ddlItems.Add(new SelectListItem() { Selected = true, Text = " -- Select Shift--", Value = "0" });
    //    ddlItems.Add(new SelectListItem() {Text = "Dual Shift", Value = "1" });
    //    ddlItems.Add(new SelectListItem() {Text = "Morning", Value = "2" });
    //    ddlItems.Add(new SelectListItem() {Text = "Night", Value = "3" });

    //    return ddlItems;
    //}
}
