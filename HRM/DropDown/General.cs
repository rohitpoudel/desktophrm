﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Reflection;

namespace HRM

{

    public static class General
    {

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "DigitalNepal";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())

            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "DigitalNepal";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static DataTable ConvertToDataTable<T>(this IEnumerable<T> data)
        {
            List<T> list = data.Cast<T>().ToList();

            PropertyDescriptorCollection props = null;
            DataTable table = new DataTable();
            if (list != null && list.Count > 0)
            {
                props = TypeDescriptor.GetProperties(list[0]);
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            if (props != null)
            {
                object[] values = new object[props.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            return table;
        }
        public static string ConvertDateFormat(string Date)
        {
            string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
            if (Date.Length > 10)
            {
                if (Date.Contains("AM") || Date.Contains("PM"))
                {
                    Date = Date.Substring(0, Date.Length - 11);
                }
                else
                {
                    Date = Date.Substring(0, Date.Length - 8);
                }
            }
            string[] date = Date.Split('/').ToArray();


            if (sysFormat == "M/d/yyyy")
            {
                Date = date[2] + "-" + date[0] + "-" + date[1];
            }
            else
            {
                Date = date[2] + "-" + date[1] + "-" + date[0];
            }




            return Date;
        }
        public static string DifferenceInHourMins(string Time1, string Time2)

        {
            if (string.IsNullOrEmpty(Time1))
            {
                Time1 = "00:00";
            }
            if (string.IsNullOrEmpty(Time2))
            {
                Time2 = "00:00";
            }
            TimeSpan t1 = TimeSpan.Parse(Time1);
            TimeSpan t2 = TimeSpan.Parse(Time2);
            t1.Subtract(t2).ToString();

            return t1.Subtract(t2).ToString();
        }
        //public static SqlConnectionStringBuilder BuildDyanmicStringForIndCompany(string CompanyCode)
        //{
        //    ICompanyQueryProcessor companyQueryProcessor = new CompanyQueryProcessor();
        //    string code = string.Empty;
        //   // code = GetCompanyCodeByUserName(Convert.ToString(HttpContext.Current.Session["UserId"]));
        //    var data = companyQueryProcessor.GetByCode(CompanyCode);
        //    SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
        //    connectionString.ConnectTimeout = 600;
        //    connectionString.DataSource = data.DataSource;
        //    connectionString.InitialCatalog = data.DB_Name;
        //    connectionString.UserID = data.Db_UserName;
        //    connectionString.Password = Decrypt(data.Password);
        //    connectionString.IntegratedSecurity = false;
        //    return connectionString;
        //}
        //public static SqlConnectionStringBuilder BuildDyanmicString(string CompanyCode)
        //{
        //    ICompanyQueryProcessor companyQueryProcessor = new CompanyQueryProcessor();
        //    string code = string.Empty;
        //    code = GetCompanyCodeByUserName(Convert.ToString(HttpContext.Current.Session["UserId"]));
        //    var data = companyQueryProcessor.GetByCode(code);
        //    SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
        //    connectionString.ConnectTimeout = 2400;
        //    connectionString.DataSource = data.DataSource;
        //    connectionString.InitialCatalog = data.DB_Name;
        //    connectionString.UserID = data.Db_UserName;
        //    connectionString.Password = Decrypt(data.Password);
        //    connectionString.IntegratedSecurity = false;
        //    return connectionString;
        //}
        //public static SqlConnectionStringBuilder BuildAPIDyanmicString(string CompanyCode)
        //{
        //    ICompanyQueryProcessor companyQueryProcessor = new CompanyQueryProcessor();            
        //    var data = companyQueryProcessor.GetByCode(CompanyCode);
        //    SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
        //    connectionString.ConnectTimeout = 600;
        //    connectionString.DataSource = data.DataSource;
        //    connectionString.InitialCatalog = data.DB_Name;
        //    connectionString.UserID = data.Db_UserName;
        //    connectionString.Password = Decrypt(data.Password);
        //    connectionString.IntegratedSecurity = false;


        //    return connectionString;
        //}
        public static bool CheckDatabaseExists(string connectionString, string databaseName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand($"SELECT db_id('{databaseName}')", connection))
                {
                    bool result = false;
                    connection.Open();
                    result = command.ExecuteScalar() != DBNull.Value;
                    connection.Close();
                    return result;
                }
            }
        }
        //public static int DataBaseCreation(string DbName, string CompanyCode, string companyname, string password)
        //{
        //    companyname = companyname.Replace(" ", "");
        //    var filePath = HttpContext.Current.Server.MapPath("~/DBScript/db.sql");
        //    bool isFileExist = File.Exists(filePath);
        //    if (isFileExist == true)
        //    {
        //        FileInfo file = new FileInfo(filePath);
        //        string script = file.OpenText().ReadToEnd();
        //        if (script != "")
        //        {
        //            string sqlConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        //            SqlConnection conn = new SqlConnection(sqlConnectionString);
        //            Server server = new Server(new ServerConnection(conn));
        //            var dbName = DbName;
        //            var text = script.Split(' ');
        //            bool isDbExists = CheckDatabaseExists(sqlConnectionString, dbName);
        //            if (isDbExists == false)
        //            {
        //                try
        //                {
        //                    // database creation part
        //                    conn.Open();
        //                    server.ConnectionContext.ExecuteNonQuery(string.Format("CREATE DATABASE [{0}]", DbName));
        //                    conn.Close();

        //                    //run scrip part
        //                    string connection = BuildAPIDyanmicString(CompanyCode).ToString();
        //                    SqlConnection dconn = new SqlConnection(connection);
        //                    Server servers = new Server(new ServerConnection(dconn));
        //                    dconn.Open();
        //                    servers.ConnectionContext.ExecuteNonQuery(script);
        //                    dconn.Close();
        //                    //insert into company table
        //                    dconn.Open();
        //                    string compquery = @"INSERT INTO [dbo].[Companies]
        //                                   ([CompanyName],
        //                                    ValidityFrom,
        //                                    ValidityTill,
        //                                    ValidityFromNepali,
        //                                    ValidityTillNepali
        //                                   ,[CompanyCode]
        //                                   ,[PhoneNo]
        //                                   ,[Email]
        //                                   ,[Web]
        //                                   ,[Logo]
        //                                   ,[CompanyAddress]
        //                                   ,[IsDeleted]
        //                                   ,[CreatedBy]
        //                                   ,[CreatedDate])
        //                            Select CompanyName,ValidityFrom,ValidityTill, ValidityFromNepali,ValidityTillNepali,CompanyCode,PhoneNo,Email,Web,Logo,CompanyAddress,IsDeleted,CreatedBy,CreatedDate from RealTime.dbo.Companies where CompanyCode='" + CompanyCode + "'";
        //                    servers.ConnectionContext.ExecuteNonQuery(compquery);
        //                    dconn.Close();


        //                    // Employee Insert
        //                    dconn.Open();
        //                    var data = dconn.Query<Employee>("SELECT (Max(Id)+1) as Id  FROM RealTime.[dbo].[Employee]", commandType: CommandType.Text);
        //                    var EID = data.First().Id;
        //                    var cdata = dconn.Query<Employee>("SELECT (Max(Emp_Code)+1) as Id  FROM RealTime.[dbo].[Employee]", commandType: CommandType.Text);
        //                    var ECode = cdata.First().Id;
        //                    string empquery2 = @"INSERT INTO RealTime.[dbo].[Employee]
        //                               ([Id],
        //                                [Emp_Code]
        //                               ,[Emp_Name]
        //                               ,[Emp_DeviceCode]
        //                               ,[Designation_Id]
        //                               ,[Department_Id]
        //                               ,[Section_Id]
        //                               ,[GradeGroup]        
        //                               ,[Email]          
        //                               ,[Is_Active]
        //                               ,[Created_Date]
        //                               ,[Created_By]
        //                               ,[BranchId]
        //                            ,[IsPolitical])
        //                         VALUES(" + EID+ ","+ECode+",N'" + string.Format("{0}@superadmin.com", companyname) + "',0,1,1,1,1,'" + string.Format("{0}@superadmin.com", companyname) + "',1,GETDATE(),1,1,0)";
        //                    servers.ConnectionContext.ExecuteNonQuery(empquery2);
        //                    string empquery3 = @"INSERT INTO [dbo].[Employee]
        //                               ([Id],
        //                                [Emp_Code]
        //                               ,[Emp_Name]
        //                               ,[Emp_DeviceCode]
        //                               ,[Designation_Id]
        //                               ,[Department_Id]
        //                               ,[Section_Id]
        //                               ,[GradeGroup]        
        //                               ,[Email]          
        //                               ,[Is_Active]
        //                               ,[Created_Date]
        //                               ,[Created_By]
        //                               ,[BranchId]
        //                                ,[IsPolitical])
        //                         VALUES(" + EID + "," + ECode + ",N'" + string.Format("{0}@superadmin.com", companyname) + "',0,1,1,1,1,'" + string.Format("{0}@superadmin.com", companyname) + "',1,GETDATE(),1,1,0)";
        //                    servers.ConnectionContext.ExecuteNonQuery(empquery3);
        //                    dconn.Close();

        //                    string username = GetCompanyEmailByCode(CompanyCode);
        //                    var dbfactory = DbFactoryProvider.GetFactory();
        //                    dconn.Open();
        //                    var param1 = new DynamicParameters();

        //                    param1.Add("@Id", 0);
        //                    param1.Add("@Event", 'I');
        //                    param1.Add("@RoleId", 1);
        //                    param1.Add("@UserId", EID);
        //                    param1.Add("@Password", Encrypt("P@ssw0rd@001"));
        //                    param1.Add("@Company_Code", string.Format("{0}-1", CompanyCode));
        //                    //param1.Add("@UserName", companyname.Replace(" ", "") + "@superadmin.com");
        //                    param1.Add("@UserName", username.Split('@')[0].Replace(" ", "") + "@superadmin.com");
        //                    param1.Add("@BranchId", null);
        //                    param1.Add("@CreatedBy", 1);
        //                    param1.Add("@CreatedDate", DateTime.Today);
        //                    param1.Add("@IsActive", true);
        //                    param1.Add("@IsSuperAdmin", true);

        //                    param1.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
        //                    dconn.Execute("[dbo].[Usp_IUD_ApplicationUser]", param: param1, commandType: CommandType.StoredProcedure);
        //                    dconn.Close();


        //                    dconn.Close();
        //                    dconn.Open();
        //                    var data1 = dconn.Query<Employee>("SELECT (Max(Id)+1) as Id  FROM RealTime.[dbo].[Employee]", commandType: CommandType.Text);
        //                    var EID1 = data1.First().Id;
        //                    var cdata1 = dconn.Query<Employee>("SELECT (Max(Emp_Code)+1) as Id  FROM RealTIme.[dbo].[Employee]", commandType: CommandType.Text);
        //                    var ECode1 = cdata1.First().Id;
        //                    dconn.Close();
        //                    dconn.Open();
        //                    string empquery = @"INSERT INTO RealTime.[dbo].[Employee]
        //                               ([Id],
        //                                    [Emp_Code]
        //                               ,[Emp_Name]
        //                               ,[Emp_DeviceCode]
        //                               ,[Designation_Id]
        //                               ,[Department_Id]
        //                               ,[Section_Id]
        //                               ,[GradeGroup]        
        //                               ,[Email]          
        //                               ,[Is_Active]
        //                               ,[Created_Date]
        //                               ,[Created_By]
        //                               ,[BranchId]
        //                              ,[IsPolitical])
        //                         VALUES(" + EID1 + "," + ECode1 + ",N'" + string.Format("{0}@admin.com", companyname) + "',0,1,1,1,1,'" + string.Format("{0}@admin.com", companyname) + "',1,GETDATE(),1,1,0)";
        //                    servers.ConnectionContext.ExecuteNonQuery(empquery);
        //                    string empquery1 = @"INSERT INTO [dbo].[Employee]
        //                               ([Id],
        //                                   [Emp_Code]
        //                               ,[Emp_Name]
        //                               ,[Emp_DeviceCode]
        //                               ,[Designation_Id]
        //                               ,[Department_Id]
        //                               ,[Section_Id]
        //                               ,[GradeGroup]        
        //                               ,[Email]          
        //                               ,[Is_Active]
        //                               ,[Created_Date]
        //                               ,[Created_By]
        //                               ,[BranchId]
        //                               ,[IsPolitical])
        //                         VALUES(" + EID1 + "," + ECode1 + ",N'" + string.Format("{0}@admin.com", companyname) + "',0,1,1,1,1,'" + string.Format("{0}@admin.com", companyname) + "',1,GETDATE(),1,1,0)";
        //                    servers.ConnectionContext.ExecuteNonQuery(empquery1);
        //                    dconn.Close();
        //                    ///insert into application user
        //                    ///

        //                    // int i = param1.Get<Int16>("@Return_Id");

        //                    dconn.Open();
        //                    var param = new DynamicParameters();

        //                    param.Add("@Id", 0);
        //                    param.Add("@Event", 'I');
        //                    param.Add("@RoleId", 1);
        //                    param.Add("@UserId", EID1);
        //                    param.Add("@Password", Encrypt(password));
        //                    param.Add("@Company_Code", string.Format("{0}-1", CompanyCode));
        //                    param.Add("@UserName", username.Replace(" ",""));
        //                    param.Add("@BranchId", null);
        //                    param.Add("@CreatedBy", 1);
        //                    param.Add("@CreatedDate", DateTime.Today);
        //                    param.Add("@IsActive", true);
        //                    param.Add("@IsSuperAdmin", false);
        //                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
        //                    dconn.Execute("[dbo].[Usp_IUD_ApplicationUser]", param: param, commandType: CommandType.StoredProcedure);
        //                    dconn.Close();
        //                    int i = param.Get<Int16>("@Return_Id");





        //                }
        //                catch (Exception ex)
        //                {

        //                    Database db = new Database(server, dbName);
        //                    db.Refresh();
        //                    db.Drop();
        //                    throw ex;
        //                }
        //            }
        //            else
        //            {
        //                return 0;
        //            }

        //        }

        //    }
        //    return 1;
        //}
        public static string GetCompanyEmailByCode(string code)
        {
            try
            {
                string sqlConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                SqlConnection conn = new SqlConnection(sqlConnectionString);
                var param = new DynamicParameters();
                conn.Open();
                var data = conn.Query<Company>("SELECT    Email FROM [dbo].[Companies] where CompanyCode= @Code", param: new { Code = code }, commandType: CommandType.Text);
                var Email = data.Select(x => x.Email).First();
                conn.Close();
                return Email;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetCompanyCodeByUserName(string Id)
        {
            try
            {

                string sqlConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection conn = new SqlConnection(sqlConnectionString);
                var param = new DynamicParameters();
                conn.Open();
                var data = conn.Query<Company>("SELECT [Company_Code] as CompanyCode FROM [dbo].[ApplicationUser] where UserId=@id", param: new { id = Id }, commandType: CommandType.Text);
                var code = data.Select(x => x.CompanyCode).First();
                string[] values = code.Split('-');
                conn.Close();
                return values[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item) ?? DBNull.Value;
                table.Rows.Add(values);
            }
            return table;
        }
        public static int GetWeekNumber(string date)
        {
            DateTime dtime = Convert.ToDateTime(date);
            int count = 0;
            if (dtime.Day <= 7)
            {
                count = 1;
            }
            else if (dtime.Day > 7 && dtime.Day <= 14)
            {
                count = 2;
            }
            else if (dtime.Day > 14 && dtime.Day <= 21)
            {
                count = 3;
            }
            else if (dtime.Day > 21 && dtime.Day <= 28)
            {
                count = 4;
            }
            else if (dtime.Day > 28)
            {
                count = 5;
            }
            return count;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        //-----For form color setting -----
        public static int[] GetColorSetUp1()
        {
            try
            {
                int[] retarray = new int[3];
                string sqlConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection conn = new SqlConnection(sqlConnectionString);
                conn.Open();
                var data = conn.Query<ColorSetUp>("select top 1 *  from ColorSetUp", commandType: CommandType.Text);
                if (data.Any())
                {
                    foreach (var item in data)
                    {
                        retarray[0] = item.Red;
                        retarray[1] = item.Green;
                        retarray[2] = item.Blue;
                    }
                }
                else
                {
                    retarray[0] = 152;
                    retarray[1] = 252;
                    retarray[2] = 241;
                }

                conn.Close();
                return retarray;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int[] GetColorSetUp()
        {
            IColorSetUpHRM iColorSetUpHRM = null;
            iColorSetUpHRM = new ColorSetUpHRM();
            return iColorSetUpHRM.GetColorSetUp();
        }

    }
}