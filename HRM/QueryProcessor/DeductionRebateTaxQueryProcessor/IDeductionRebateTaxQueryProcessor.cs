﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IDeductionRebateTaxHRM
    {
        int Create(DeductionRebateTax deductionRebateTax);
        IEnumerable<DeductionRebateTaxViewModel> GetAllData();
        DeductionRebateTax GetById(int id);
        bool Delete(int id);
        bool chaekDuplicate(string Name, int FiscalYearId, string DeductionRebate, string Action, int id);
    }
}
