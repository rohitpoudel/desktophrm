﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM 
{
    public class DeductionRebateTaxHRM : IDeductionRebateTaxHRM
    {
        public bool chaekDuplicate(string Name, int FiscalYearId, string DeductionRebate, string Action, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@DRName", Name);
                    param.Add("@FiscalYearId", FiscalYearId);
                    param.Add("@DeductionRebate", DeductionRebate);
                    param.Add("@Action", Action);
                    param.Add("@id", id);
                    int result = db.Query<int>(sql: "[dbo].[CheckDublicateDeductionReabte]", param:param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Create(DeductionRebateTax deductionRebateTax)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", deductionRebateTax.Event);
                    param.Add("@Id", deductionRebateTax.Id);
                    param.Add("@Name", deductionRebateTax.Name);
                    param.Add("@FiscalYearId", deductionRebateTax.FiscalYearId);
                    param.Add("@DeductionOrRebate", deductionRebateTax.DeductionOrRebate);
                    param.Add("@DeductionOn", deductionRebateTax.DeductionOn);
                    param.Add("@Type", deductionRebateTax.Type);
                    param.Add("@Deduction", deductionRebateTax.Deduction);
                    param.Add("@IsActive", deductionRebateTax.Is_Active);
                    param.Add("@CreatedBy", deductionRebateTax.Created_By);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_DeductionRebateTax]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[DeductionRebateTax] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public IEnumerable<DeductionRebateTaxViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var NonResidentialTaxlist = db.Query<DeductionRebateTaxViewModel>(sql: "[dbo].[Usp_GetDeductionRebateList]", commandType: CommandType.StoredProcedure);
                    return NonResidentialTaxlist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DeductionRebateTax GetById(int id)
        {
            try
            {
                DeductionRebateTax obj = new DeductionRebateTax();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var NonResidentialTaxlist = db.Query<DeductionRebateTax>(sql: "[dbo].[Usp_GetDeductionRebateTaxById]", param: param, commandType: CommandType.StoredProcedure);
                    if (NonResidentialTaxlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(NonResidentialTaxlist.Select(x => x.Id).First());
                        obj.Name = NonResidentialTaxlist.Select(x => x.Name).First().ToString();
                        obj.FiscalYearId = NonResidentialTaxlist.Select(x => x.FiscalYearId).First();
                        obj.DeductionOrRebate = NonResidentialTaxlist.Select(x => x.DeductionOrRebate).First();
                        obj.DeductionOn = NonResidentialTaxlist.Select(x => x.DeductionOn).First();
                        obj.Type = NonResidentialTaxlist.Select(x => x.Type).First();
                        obj.Deduction = NonResidentialTaxlist.Select(x => x.Deduction).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}