﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace HRM
{
	public class ColorSetUpHRM : IColorSetUpHRM
	{
		public int Create(ColorSetUp obj)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Id", obj.id);
					param.Add("@Red", obj.Red);
					param.Add("@Green", obj.Green);
					param.Add("@Blue", obj.Blue);
					param.Add("@CreatedBy", obj.CreatedBy);
					param.Add("@CreatedOn", obj.CreatedOn);
					param.Add("@LastModifiedBy", obj.LastModifiedBy);
					param.Add("@LastModifiedOn", obj.LastModifiedOn);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_UpdateColorSetUp]", param: param, commandType: CommandType.StoredProcedure);
					db.Close();
					return param.Get<Int16>("@Return_Id");
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int[] GetColorSetUp()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					int[] retarray = new int[3];
					db.Open();
					//var param = new DynamicParameters();
					//db.Execute("[dbo].[Usp_UpdateColorSetUp]", param: param, commandType: CommandType.Text);
					var data = db.Query<ColorSetUp>("select top 1 *  from ColorSetUp", commandType: CommandType.Text);
					if (data.Any())
					{
						foreach (var item in data)
						{
							retarray[0] = item.Red;
							retarray[1] = item.Green;
							retarray[2] = item.Blue;
						}
					}
					else
					{
						retarray[0] = 152;
						retarray[1] = 252;
						retarray[2] = 241;
					}

					db.Close();
					return retarray;


				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}