﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM
{
    interface IColorSetUpHRM
    {
        int Create(ColorSetUp obj);
        int[] GetColorSetUp();
    }
}
