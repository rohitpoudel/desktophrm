﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM
{
    public class LateRuleHRM : ILateRuleHRM
    {
        public int Create(LateRule lateRule)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", lateRule.Event);
                    param.Add("@Id", lateRule.Id);
                    param.Add("@FiscalYearId", lateRule.FiscalYearId);
                    param.Add("@LateInOrEarlyOut", lateRule.LateInOrEarlyOut);
                    param.Add("@DaysAllowed", lateRule.DaysAllowed);

                    param.Add("@LeaveOnLate", lateRule.LeaveOnLate);
                    param.Add("@LeaveLateInDays", lateRule.LeaveLateInDays);
                    param.Add("@LeaveDeductin", lateRule.LeaveDeductin);

                    param.Add("@DeductionOnLate", lateRule.DeductionOnLate);
                    param.Add("@DeductLateInDays", lateRule.DeductLateInDays);
                    param.Add("@Type", lateRule.Type);
                    param.Add("@Deduction", lateRule.Deduction);

                    param.Add("@IsActive", lateRule.Is_Active);
                    param.Add("@CreatedBy", lateRule.Created_By);
                    param.Add("@UpdatedBy", lateRule.Updated_By);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_LateRule]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[LateRule] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int GetFiscalYear(string fiscalYear, string ruleFor, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var GetYear = db.Query<LateRule>("select * from LateRule where FiscalYearId = @FiscalYear and LateInOrEarlyOut = @RuleFor and Id! =  @Id and Is_Active=1", param: new { FiscalYear = fiscalYear, RuleFor = ruleFor, Id = id }, commandType: CommandType.Text);
                    var result = GetYear.Count();
                    db.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<LateRuleViwModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var LateRulelist = db.Query<LateRuleViwModel>(sql: "[dbo].[Usp_GetLateRuleList]", commandType: CommandType.StoredProcedure);
                    return LateRulelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LateRule GetById(int id)
        {
            try
            {
                LateRule obj = new LateRule();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var NonResidentialTaxlist = db.Query<LateRule>(sql: "[dbo].[Usp_GetLateRuleById]", param: param, commandType: CommandType.StoredProcedure);
                    if (NonResidentialTaxlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(NonResidentialTaxlist.Select(x => x.Id).First());
                        obj.FiscalYearId = NonResidentialTaxlist.Select(x => x.FiscalYearId).First();
                        obj.LateInOrEarlyOut = NonResidentialTaxlist.Select(x => x.LateInOrEarlyOut).First();
                        obj.DaysAllowed = NonResidentialTaxlist.Select(x => x.DaysAllowed).First();
                        obj.LeaveOnLate = NonResidentialTaxlist.Select(x => x.LeaveOnLate).First();
                        obj.LeaveLateInDays = NonResidentialTaxlist.Select(x => x.LeaveLateInDays).First();
                        obj.LeaveDeductin = NonResidentialTaxlist.Select(x => x.LeaveDeductin).First();
                        obj.DeductionOnLate = NonResidentialTaxlist.Select(x => x.DeductionOnLate).First();
                        obj.DeductLateInDays = NonResidentialTaxlist.Select(x => x.DeductLateInDays).First();
                        obj.Type = NonResidentialTaxlist.Select(x => x.Type).First();
                        obj.Deduction = NonResidentialTaxlist.Select(x => x.Deduction).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetDataForGrid()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var LateRulelist = db.Query<LateRuleViwModel>(sql: "[dbo].[Usp_GetLateRuleList]", commandType: CommandType.StoredProcedure);
                    DataTable dt = (DataTable)LateRulelist;
                    return dt;
                     
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LateRuleViwModel> GetAllDataForSearch( string searchString)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@searchString", searchString);
                    var LateRulelist = db.Query<LateRuleViwModel>(sql: "[dbo].[Usp_GetLateRuleListForSearch]", param: param, commandType: CommandType.StoredProcedure);
                    return LateRulelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}