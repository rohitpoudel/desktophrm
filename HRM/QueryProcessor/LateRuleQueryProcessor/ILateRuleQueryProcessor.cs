﻿
using System.Collections.Generic;
using System.Data;

namespace HRM
{
    public interface ILateRuleHRM
    {
        int Create(LateRule lateRule);
        IEnumerable<LateRuleViwModel> GetAllData();
        LateRule GetById(int id);
        bool Delete(int id);
        int GetFiscalYear(string fiscalYear,string ruleFor ,int id);

        IEnumerable<LateRuleViwModel> GetAllDataForSearch(string searchString);
        DataTable GetDataForGrid();
    }
}
