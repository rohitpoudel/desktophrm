﻿
using System.Collections.Generic;
namespace HRM
{
    public interface ISectionHRM
    {
        int Create(Section section);
        IEnumerable<SectionViewModel> GetAllData();
        Section GetById(int id);
        bool Delete(int id);
        int GetCode(string code, int id);

    }
}
