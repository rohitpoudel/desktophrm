﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;





namespace HRM
{
    public class SectionHRM : ISectionHRM
    {
        public int Create(Section section)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    if (section.SectionCode == null)
                    {
                      //  section.SectionCode = HttpContext.Current.Session["Code"].ToString();
                        section.SectionCode = "001";
                    }
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", section.Id);
                    param.Add("@Event", section.Event);
                    param.Add("@SectionCode", section.SectionCode);
                    param.Add("@DepartmentId", section.DepartmentId);
                    param.Add("@SectionName", section.SectionName);
                    param.Add("@IsDeleted", section.IsDeleted);
                    param.Add("@CreateBy", section.CreateBy);
                    param.Add("@CreateDate", section.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Sections]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Sections] SET [IsDeleted]=@IsDeleted Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            //throw new NotImplementedException();
        }

        public IEnumerable<SectionViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var sectionList = db.Query<SectionViewModel>(sql: "[dbo].[GetSectionList]", commandType: CommandType.StoredProcedure);
                    return sectionList.ToList();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //throw new NotImplementedException();
        }

        public Section GetById(int id)
        {
            try
            {
                var obj = new Section();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var sectionlist = db.Query<SectionViewModel>(sql: "[dbo].[GetSectionById]", param: param, commandType: CommandType.StoredProcedure);
                    if (sectionlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(sectionlist.Select(x => x.Id).First());
                        obj.SectionName = sectionlist.Select(x => x.SectionName).First().ToString();
                        obj.SectionCode = sectionlist.Select(x => x.SectionCode).First().ToString();
                        obj.DepartmentId = Convert.ToInt32(sectionlist.Select(x => x.DepartmentId).First());
                        //obj.TaskStatus = Convert.ToBoolean(tasklist.Select(x => x.TaskStatus).First());
                    }
                    db.Close();
                    return obj;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int GetCode(string code, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<Section>("SELECT SectionCode FROM[dbo].[Sections] where SectionCode = @Code and id != @id and IsDeleted = 0", param: new { Code = code, @id = id }, commandType: CommandType.Text);
                    var s = data.Count();
                    db.Close();
                    return s;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}