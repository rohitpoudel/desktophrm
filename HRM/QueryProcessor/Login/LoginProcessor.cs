﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;




namespace HRM
{
    public class LoginProcessor : ILoginProcessor
    {
      //  MenuHRM mn = new MenuHRM();
        public bool Authenticate(LoginCredential user)
        {
            try
            {
                
                //HttpContext context = System.Web.HttpContext.Current;

                var dbfactory = DbFactoryProvider.GetFactory();
                var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                //var dbfactory = DbFactoryProvider.GetFactory();
                using (db)
                {
                    db.Open();
                    string pass = General.Encrypt(user.Password);
                    var param = new DynamicParameters();
                    param.Add("@UserName", user.UserName);
                    var a = General.Encrypt(user.Password);
                    param.Add("@Password", General.Encrypt(user.Password));
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_Authenticate]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    int result = param.Get<Int16>("@Return_Id");
                   // HttpContext.Current.Session["BrowserName"] = context.Request.Url.OriginalString;

                    if (result > 0)
                    {
                        GetCompCodeByEmpId(result);
                        return true;
                    }
                    return false;

                }

            }
            catch (Exception ex)
            {
                //  Info._username = string.Empty;
                throw ex;


            }
        }
        public bool IsEmailVerified(string code, string cID)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<UserVerify>("select * from userverify where code = @Code and status= 0", param: new { Code = code }, commandType: CommandType.Text);
                    if (data.Count() > 0)
                    {
                        var empData = db.Query<Employee>("select Email from Employee where id= @Id and EmailVerified = 0 and Is_Active=1", param: new { Id = data.FirstOrDefault().EmpId }, commandType: CommandType.Text);
                        int s = data.Count();
                        if (empData.Count() > 0)
                        {
                            if (s > 0)
                            {
                                int result = db.Execute("UPDATE userverify SET [status] = 1 Where VU_ID=@Id", param: new { @Id = data.FirstOrDefault().VU_ID }, commandType: CommandType.Text);
                                int result1 = db.Execute("UPDATE Employee SET EmailVerified = 1 Where ID=@Id", param: new { @Id = data.FirstOrDefault().EmpId }, commandType: CommandType.Text);
                                int result2 = db.Execute("UPDATE [HRM].[dbo].Employee SET EmailVerified = 1 Where ID=@Id", param: new { @Id = data.FirstOrDefault().EmpId }, commandType: CommandType.Text);

                                return true;
                            }
                        }
                    }


                    db.Close();
                    return false;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<AbsentEmpDetails> GetAbsEmpList(string cId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var Employeelist = db.Query<AbsentEmpDetails>(sql: "[dbo].[GetTodaysAbsentList]", param: param, commandType: CommandType.StoredProcedure);


                    db.Close();
                    return Employeelist;


                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void GetCompCodeByEmpId(int Id)
        {

            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                var param = new DynamicParameters();
                var param1 = new DynamicParameters();
                db.Open();
                param.Add("@Id", Id);
                var Employeelist = db.Query<Employee>(sql: "[dbo].[GetCompCodeByEmpId]", param: param, commandType: CommandType.StoredProcedure);
                if (Employeelist.Count() > 0)
                {
                    string code = string.Empty;
                    code = Employeelist.Select(x => x.Company_Code).First();
                    //HttpContext.Current.Session["Code"] = code;
                    //string[] values = code.Split('-');
                    ////Info._companyCode = string.Empty;
                    //HttpContext.Current.Session["CompanyCode"] = values[0];
                    ////Info.Id = 0;
                    ////Info.Id = Convert.ToInt32(values[1]);


                    //HttpContext.Current.Session["IsSuperAdmin"] = Employeelist.Select(x => x.IsSuperAdmin).First();
                    //HttpContext.Current.Session["UserId"] = Convert.ToInt32(values[1]);
                    //HttpContext.Current.Session["EmpId"] = Employeelist.Select(x => x.Id).First();
                    //HttpContext.Current.Session["BranchId"] = Employeelist.Select(x => x.BranchId).First();
                  
                    //HttpContext.Current.Session["UserName"] = Employeelist.Select(x => x.Emp_Name).First();
                    //HttpContext.Current.Session["RoleId"] = Employeelist.Select(x => x.RoleId).First();
                    var imgpath= Employeelist.Select(x => x.PhotoPath).First();
                    if(!string.IsNullOrEmpty(imgpath))
                    {
                        //HttpContext.Current.Session["ImgPath"] = "\\" + Employeelist.Select(x => x.PhotoPath).First();

                        //HttpContext.Current.Session["Photo"] = "~/" + Employeelist.Select(x => x.PhotoPath).First().Replace(@"\", @"/");
                    }
                  
                  
                }
                db.Close();


            }
            catch (Exception ex)
            {
                // Info._companyCode = string.Empty;
                throw ex;
            }
        }
        public Errorcheck CheckEmailExist(string username)
        {
            try
            {
                var ec = new Errorcheck();
                var dbfactory = DbFactoryProvider.GetFactory();
                var db1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

                using (var db = db1)
                {
                    var param = new DynamicParameters();
                    var data = 0;
                    db.Open();
                    data = db.Query<Employee>("select Email from Employee where Email=@UserName and Is_Active =1", param: new { UserName = username }, commandType: CommandType.Text).Count();

                    if (data > 0)
                    {
                        ec.InvalidEmail = false;
                    }
                    else
                    {
                        ec.InvalidEmail = true;
                    }
                    var data1 = db.Query<Employee>("select EmailVerified from Employee where Email=@UserName and EmailVerified= 1 and Is_Active =1", param: new { UserName = username }, commandType: CommandType.Text).Count();
                    db.Close();
                    if (data1 > 0)
                    {
                        ec.EmailVerify = true;
                    }
                    else
                    {
                        ec.EmailVerify = false;
                    }
                }
                return ec;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CreateUserVerify(UserVerify uv)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            var db1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

            using (var db = db1)
            {
                try
                {

                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@VU_ID", uv.VU_ID);
                    param.Add("@Event", 'I');
                    param.Add("@Code", uv.Code);
                    param.Add("@EmpId", uv.EmpId);
                    param.Add("@Status", uv.Status);
                    param.Add("@Created_Date", uv.Created_Date);

                    db.Execute("[dbo].[Usp_IUD_UserVerify]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return true;

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
        public bool IsEmailSentToAbsEmployee(int eId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<AbsentEmpDetails>("select * from [dbo].[AbsentLog] where empId = @EID and Absentdate = cast(GETDATE() as date)", param: new { EID = eId }, commandType: CommandType.Text);
                    db.Close();

                    if (data.Count()>0)
                    {
                        return true;

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetCompanyCodebyEid(int eId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                var db1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

                using (var db = db1)
                {
                    db.Open();
                    var data = db.Query<Employee>("select Company_Code from ApplicationUser where UserId=@Mail", param: new { Mail = eId }, commandType: CommandType.Text);
                    db.Close();

                    return data.FirstOrDefault().Company_Code;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetEmpIdbymail(string mail)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                var db1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

                using (var db = db1)
                {
                    db.Open();
                    var data = db.Query<Employee>("Select Id from employee where Is_Active = 1 and Email=@Mail", param: new { Mail = mail }, commandType: CommandType.Text);
                    db.Close();
                    int Id = data.FirstOrDefault().Id;
                    return Id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdatePassword(int eId, string password, string code, string cID)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<UserVerify>("select * from [HRM].[dbo].userverify where code = @Code and status= 0", param: new { Code = code }, commandType: CommandType.Text);
                    if (data.Count() > 0)
                    {
                        int result2 = db.Execute("UPDATE [HRM].[dbo].userverify SET [status] = 1 Where VU_ID=@Id", param: new { @Id = data.FirstOrDefault().VU_ID }, commandType: CommandType.Text);

                        int result1 = db.Execute("UPDATE [HRM].[dbo].ApplicationUser set [PassWord]= @PWD where UserId=@Id", param: new { @Id = eId, PWD = password }, commandType: CommandType.Text);

                        int result = db.Execute("UPDATE ApplicationUser set [PassWord]= @PWD where UserId=@Id", param: new { @Id = eId, PWD = password }, commandType: CommandType.Text);
                        db.Close();
                        return true;
                    }
                    else
                    {
                        return false;
                    }





                }



            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return false;
            }
        }
        public bool isCodeUsed(string code, string cID)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<UserVerify>("select * from userverify where code = @Code and status= 0", param: new { Code = code }, commandType: CommandType.Text);
                    db.Close();
                    if (data.Count() > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return false;
            }
        }
        public Employee GetMailbyId(string empid)
        {
            try
            {
                Employee ee = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                var db1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

                using (var db = db1)
                {
                    db.Open();
                    var data = db.Query<Employee>("Select Id,email from employee where Is_Active = 1 and Id=@Empid", param: new { Empid = empid }, commandType: CommandType.Text);
                    db.Close();
                    if (data.Count() > 0)
                    {
                        ee.Id = data.FirstOrDefault().Id;
                        ee.Email = data.FirstOrDefault().Email;
                    }

                    return ee;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Assign view menus
        //assign all the menus which are inside the view
        //public void GetSetTemplateValue()
        //{
        //    GlobalArray gs = new GlobalArray();
        //    gs.SendEmail = 0;
        //    gs.AddMenuTemp = 0;
        //    gs.EditMenuTemp = 0;
        //    gs.DeleteMenuTemp = 0;
        //    gs.AddDevice = 0;
        //    gs.EditDevice = 0;
        //    gs.DeleteDevice = 0;
        //    gs.AddLateRule = 0;
        //    gs.EditLateRule = 0;
        //    gs.DeleteLateRule = 0;
        //    gs.AddFiscalYear = 0;
        //    gs.EditFiscalYear = 0;
        //    gs.DeleteFiscalYear = 0;
        //    gs.AddOverTime = 0;
        //    gs.EditOverTime = 0;
        //    gs.DeleteOverTime = 0;
        //    gs.AddOfficeDays = 0;
        //    gs.EditOfficeDays = 0;
        //    gs.DeleteOfficeDays = 0;
        //    gs.AddShift = 0;
        //    gs.EditShift = 0;
        //    gs.DeleteShift = 0;
        //    gs.AddDynamicSchedule = 0;
        //    gs.EditDynamicSchedule = 0;
        //    gs.DeleteDynamicSchedule = 0;
        //    gs.AddWeeklySchedule = 0;
        //    gs.EditWeeklySchedule = 0;
        //    gs.DeleteWeeklySchedule = 0;
        //    gs.AddFixedSchedule = 0;
        //    gs.EditFixedSchedule = 0;
        //    gs.DeleteFixedSchedule = 0;
        //    gs.AddUser = 0;
        //    gs.EditUser = 0;
        //    gs.DeleteUser = 0;
        //    gs.AddEmployee = 0;
        //    gs.EditEmployee = 0;
        //    gs.DeleteEmployee = 0;
        //    gs.AddDesignation = 0;
        //    gs.EditDesignation = 0;
        //    gs.DeleteDesignation = 0;
        //    gs.AddDepartment = 0;
        //    gs.EditDepartment = 0;
        //    gs.DeleteDepartment = 0;
        //    gs.AddCompany = 0;
        //    gs.EditCompany = 0;
        //    gs.DeleteCompany = 0;
        //    gs.AddBranch = 0;
        //    gs.EditBranch = 0;
        //    gs.DeleteBranch = 0;
        //    gs.AddCompanyDbInfo = 0;
        //    gs.AddSection = 0;
        //    gs.EditSection = 0;
        //    gs.DeleteSection = 0;
        //    gs.AddGradeGroup = 0;
        //    gs.EditGradeGroup = 0;
        //    gs.DeleteGradeGroup = 0;
        //    gs.AddBank = 0;
        //    gs.EditBank = 0;
        //    gs.DeleteBank = 0;
        //    gs.AddEvent = 0;
        //    gs.EditEvent = 0;
        //    gs.DeleteEvent = 0;
        //    gs.AddHoliday = 0;
        //    gs.EditHoliday = 0;
        //    gs.DeleteHoliday = 0;
        //    gs.AddNotice = 0;
        //    gs.EditNotice = 0;
        //    gs.DeleteNotice = 0;
        //    gs.AddOfficeVisit = 0;
        //    gs.EditOfficeVisit = 0;
        //    gs.DeleteOfficeVisit = 0;
        //    gs.DeleteOfficeApproval = 0;
        //    gs.AddLeaveMaster = 0;
        //    gs.EditLeaveMaster = 0;
        //    gs.DeleteLeaveMaster = 0;
        //    gs.AddLeaveApplication = 0;
        //    gs.EditLeaveApplication = 0;
        //    gs.DeleteLeaveApplication = 0;
        //    gs.AddLeaveQuota = 0;
        //    gs.EditLeaveQuota = 0;
        //    gs.DeleteLeaveQuota = 0;
        //    gs.AddLeaveSettlement = 0;
        //    gs.EditLeaveSettlement = 0;
        //    gs.DeleteLeaveSettlement = 0;

        //    gs.ViewAllUser = 0;
        //    gs.ViewAllEmployee = 0;
        //    gs.ViewAllEmpRpt = 0;
        //    gs.ViewAllEmployeeLeave = 0;

        //    gs.AddAllowance = 0;
        //    gs.EditAllowance = 0;
        //    gs.DeleteAllowance = 0;
        //    gs.AddDeductionRebate = 0;
        //    gs.EditDeductionRebate = 0;
        //    gs.DeleteDeductionRebate = 0;
        //    gs.AddResidentalTax = 0;
        //    gs.EditResidentalTax = 0;
        //    gs.DeleteResidentalTax = 0;
        //    gs.AddNonResidentalTax = 0;
        //    gs.EditNonResidentalTax = 0;
        //    gs.DeleteNonResidentalTax = 0;
        //    gs.CreateEmpSalary = 0;
        //    gs.CreateBasicDetail = 0;
        //    gs.CreateEmpShift = 0;
        //    gs.CreateEmpTimepunch = 0;
        //    gs.ViewAttendanceReport = 0;
        //    gs.ViewAllBranchData = 0;
        //    gs.AddEmployeeResignation = 0;
        //    gs.EditEmployeeResignation = 0;
        //    gs.ApproveEmployeeResignation = 0;
        //    gs.AddEmplyeeTransfer = 0;
        //    gs.EditEmplyeeTransfer = 0;
        //    gs.ApproveEmplyeeTransfer = 0;
        //    gs.DeleteEmplyeeTransfer = 0;

        //    gs.CreateRemoteAttendance = 0;
        //    gs.EditRemoteAttendance = 0;
        //    gs.DeleteRemoteAttendance = 0;
        //    gs.IsGovermental = 0;
        //    //int userId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
        //    int userId = 1;
        //   var contextList = mn.GetAllViewMenus(userId);
        //   // var isSA = HttpContext.Current.Session["IsSuperAdmin"];
        //    var isSA = true;
        //    if (isSA.ToString() == "True")
        //    {
        //        userId = 1;
        //    }
        //    //  var contextList = db.FGetPMSMenu(db.Users.Where(x => x.UserId == userId).FirstOrDefault().MTId).ToList();
        //    List<int> menuId = contextList.Select(x => x.MenuId).ToList();
        //    if (userId == 1)
        //    {
        //        gs.SendEmail = 1;
        //        gs.AddMenuTemp = 1;
        //        gs.EditMenuTemp = 1;
        //        gs.DeleteMenuTemp = 1;
        //        gs.AddDevice = 1;
        //        gs.EditDevice = 1;
        //        gs.DeleteDevice = 1;
        //        gs.AddLateRule = 1;
        //        gs.EditLateRule = 1;
        //        gs.DeleteLateRule = 1;
        //        gs.AddFiscalYear = 1;
        //        gs.EditFiscalYear = 1;
        //        gs.DeleteFiscalYear = 1;
        //        gs.AddOverTime = 1;
        //        gs.EditOverTime = 1;
        //        gs.DeleteOverTime = 1;
        //        gs.AddOfficeDays = 1;
        //        gs.EditOfficeDays = 1;
        //        gs.DeleteOfficeDays = 1;
        //        gs.AddUser = 1;
        //        gs.EditUser = 1;
        //        gs.DeleteUser = 1;
        //        gs.AddShift = 1;
        //        gs.EditShift = 1;
        //        gs.DeleteShift = 1;
        //        gs.AddDynamicSchedule = 1;
        //        gs.EditDynamicSchedule = 1;
        //        gs.DeleteDynamicSchedule = 1;
        //        gs.AddWeeklySchedule = 1;
        //        gs.EditWeeklySchedule = 1;
        //        gs.DeleteWeeklySchedule = 1;
        //        gs.AddFixedSchedule = 1;
        //        gs.EditFixedSchedule = 1;
        //        gs.DeleteFixedSchedule = 1;
        //        gs.AddEmployee = 1;
        //        gs.EditEmployee = 1;
        //        gs.DeleteEmployee = 1;
        //        gs.AddDesignation = 1;
        //        gs.EditDesignation = 1;
        //        gs.DeleteDesignation = 1;
        //        gs.AddDepartment = 1;
        //        gs.EditDepartment = 1;
        //        gs.DeleteDepartment = 1;
        //        gs.AddCompany = 1;
        //        gs.EditCompany = 1;
        //        gs.DeleteCompany = 1;
        //        gs.AddBranch = 1;
        //        gs.EditBranch = 1;
        //        gs.DeleteBranch = 1;
        //        gs.AddCompanyDbInfo = 1;
        //        gs.AddSection = 1;
        //        gs.EditSection = 1;
        //        gs.DeleteSection = 1;
        //        gs.AddGradeGroup = 1;
        //        gs.EditGradeGroup = 1;
        //        gs.DeleteGradeGroup = 1;
        //        gs.AddBank = 1;
        //        gs.EditBank = 1;
        //        gs.DeleteBank = 1;
        //        gs.AddEvent = 1;
        //        gs.EditEvent = 1;
        //        gs.DeleteEvent = 1;
        //        gs.AddHoliday = 1;
        //        gs.EditHoliday = 1;
        //        gs.DeleteHoliday = 1;
        //        gs.AddNotice = 1;
        //        gs.EditNotice = 1;
        //        gs.DeleteNotice = 1;
        //        gs.AddOfficeVisit = 1;
        //        gs.EditOfficeVisit = 1;
        //        gs.DeleteOfficeVisit = 1;
        //        gs.DeleteOfficeApproval = 1;
        //        gs.AddLeaveMaster = 1;
        //        gs.EditLeaveMaster = 1;
        //        gs.DeleteLeaveMaster = 1;
        //        gs.AddLeaveApplication = 1;
        //        gs.EditLeaveApplication = 1;
        //        gs.DeleteLeaveApplication = 1;
        //        gs.AddLeaveQuota = 1;
        //        gs.EditLeaveQuota = 1;
        //        gs.DeleteLeaveQuota = 1;
        //        gs.AddLeaveSettlement = 1;
        //        gs.EditLeaveSettlement = 1;
        //        gs.DeleteLeaveSettlement = 1;
        //        gs.ViewAllEmployeeLeave = 1;

        //        gs.ViewAllUser = 1;
        //        gs.ViewAllEmployee = 1;
        //        gs.ViewAllEmpRpt = 1;
        //        gs.AddAllowance = 1;
        //        gs.EditAllowance = 1;
        //        gs.DeleteAllowance = 1;
        //        gs.AddDeductionRebate = 1;
        //        gs.EditDeductionRebate = 1;
        //        gs.DeleteDeductionRebate = 1;
        //        gs.AddResidentalTax = 1;
        //        gs.EditResidentalTax = 1;
        //        gs.DeleteResidentalTax = 1;
        //        gs.AddNonResidentalTax = 1;
        //        gs.EditNonResidentalTax = 1;
        //        gs.DeleteNonResidentalTax = 1;
        //        gs.CreateEmpSalary = 1;
        //        gs.CreateBasicDetail = 1;
        //        gs.CreateEmpShift = 1;
        //        gs.CreateEmpTimepunch = 1;
        //        gs.ViewAttendanceReport = 1;
        //        gs.ViewAllBranchData = 1;
        //        gs.AddEmployeeResignation = 1;
        //        gs.EditEmployeeResignation = 1;
        //        gs.ApproveEmployeeResignation = 1;
        //        gs.AddEmplyeeTransfer = 1;
        //        gs.EditEmplyeeTransfer = 1;
        //        gs.ApproveEmplyeeTransfer = 1;
        //        gs.DeleteEmplyeeTransfer = 1;

        //        gs.CreateRemoteAttendance = 1;
        //        gs.EditRemoteAttendance = 1;
        //        gs.DeleteRemoteAttendance = 1;
        //        gs.IsGovermental = 1;
        //    }
        //    else
        //    {
        //        for (int i = 0; i < menuId.Count(); i++)
        //        {
        //            gs.MenuId.Add(menuId[i]);
        //            if (menuId[i] == 110)
        //            {
        //                gs.AddMenuTemp = 1;
        //            }
        //            if (menuId[i] == 119)
        //            {
        //                gs.EditMenuTemp = 1;

        //            }
        //            if (menuId[i] == 120)
        //            {
        //                gs.DeleteMenuTemp = 1;

        //            }
        //            if (menuId[i] == 153)
        //            {
        //                gs.AddDevice = 1;
        //            }
        //            if (menuId[i] == 154)
        //            {
        //                gs.EditDevice = 1;


        //            }
        //            if (menuId[i] == 155)
        //            {
        //                gs.DeleteDevice = 1;

        //            }
        //            if (menuId[i] == 156)
        //            {
        //                gs.AddLateRule = 1;

        //            }
        //            if (menuId[i] == 157)
        //            {
        //                gs.EditLateRule = 1;

        //            }
        //            if (menuId[i] == 158)
        //            {
        //                gs.DeleteLateRule = 1;

        //            }
        //            if (menuId[i] == 159)
        //            {
        //                gs.AddFiscalYear = 1;

        //            }
        //            if (menuId[i] == 160)
        //            {
        //                gs.EditFiscalYear = 1;

        //            }
        //            if (menuId[i] == 161)
        //            {
        //                gs.DeleteFiscalYear = 1;

        //            }
        //            if (menuId[i] == 162)
        //            {
        //                gs.AddOverTime = 1;

        //            }
        //            if (menuId[i] == 163)
        //            {
        //                gs.EditOverTime = 1;

        //            }
        //            if (menuId[i] == 164)
        //            {
        //                gs.DeleteOverTime = 1;

        //            }
        //            if (menuId[i] == 165)
        //            {
        //                gs.AddOfficeDays = 1;

        //            }
        //            if (menuId[i] == 166)
        //            {
        //                gs.EditOfficeDays = 1;

        //            }
        //            if (menuId[i] == 167)
        //            {
        //                gs.DeleteOfficeDays = 1;

        //            }
        //            if (menuId[i] == 168)
        //            {
        //                gs.AddShift = 1;

        //            }
        //            if (menuId[i] == 169)
        //            {
        //                gs.EditShift = 1;

        //            }
        //            if (menuId[i] == 170)
        //            {
        //                gs.DeleteShift = 1;

        //            }
        //            if (menuId[i] == 171)
        //            {
        //                gs.AddDynamicSchedule = 1;

        //            }
        //            if (menuId[i] == 172)
        //            {
        //                gs.EditDynamicSchedule = 1;

        //            }
        //            if (menuId[i] == 173)
        //            {
        //                gs.DeleteDynamicSchedule = 1;

        //            }
        //            if (menuId[i] == 174)
        //            {
        //                gs.AddWeeklySchedule = 1;

        //            }
        //            if (menuId[i] == 175)
        //            {
        //                gs.EditWeeklySchedule = 1;

        //            }
        //            if (menuId[i] == 176)
        //            {
        //                gs.DeleteWeeklySchedule = 1;

        //            }
        //            if (menuId[i] == 177)
        //            {
        //                gs.AddFixedSchedule = 1;

        //            }
        //            if (menuId[i] == 178)
        //            {
        //                gs.EditFixedSchedule = 1;

        //            }
        //            if (menuId[i] == 179)
        //            {
        //                gs.DeleteFixedSchedule = 1;

        //            }
        //            if (menuId[i] == 180)
        //            {
        //                gs.AddUser = 1;

        //            }

        //            if (menuId[i] == 181)
        //            {
        //                gs.EditUser = 1;

        //            }
        //            if (menuId[i] == 182)
        //            {
        //                gs.DeleteUser = 1;

        //            }
        //            if (menuId[i] == 184)
        //            {
        //                gs.AddEmployee = 1;

        //            }
        //            if (menuId[i] == 185)
        //            {
        //                gs.EditEmployee = 1;

        //            }
        //            if (menuId[i] == 186)
        //            {
        //                gs.DeleteEmployee = 1;

        //            }
        //            if (menuId[i] == 187)
        //            {
        //                gs.AddDesignation = 1;


        //            }
        //            if (menuId[i] == 188)
        //            {
        //                gs.EditDesignation = 1;

        //            }
        //            if (menuId[i] == 189)
        //            {
        //                gs.DeleteDesignation = 1;

        //            }
        //            if (menuId[i] == 190)
        //            {
        //                gs.AddDepartment = 1;

        //            }
        //            if (menuId[i] == 190)
        //            {
        //                gs.AddDepartment = 1;

        //            }
        //            if (menuId[i] == 191)
        //            {
        //                gs.EditDepartment = 1;


        //            }
        //            if (menuId[i] == 192)
        //            {
        //                gs.DeleteDepartment = 1;

        //            }
        //            if (menuId[i] == 193)
        //            {
        //                gs.AddCompany = 1;

        //            }
        //            if (menuId[i] == 194)
        //            {
        //                gs.EditCompany = 1;

        //            }
        //            if (menuId[i] == 195)
        //            {
        //                gs.DeleteCompany = 1;

        //            }
        //            if (menuId[i] == 196)
        //            {
        //                gs.AddBranch = 1;

        //            }
        //            if (menuId[i] == 197)
        //            {
        //                gs.EditBranch = 1;

        //            }
        //            if (menuId[i] == 198)
        //            {
        //                gs.DeleteBranch = 1;

        //            }
        //            if (menuId[i] == 199)
        //            {
        //                gs.AddCompanyDbInfo = 1;

        //            }
        //            if (menuId[i] == 200)
        //            {
        //                gs.AddEvent = 1;

        //            }
        //            if (menuId[i] == 201)
        //            {
        //                gs.EditEvent = 1;

        //            }
        //            if (menuId[i] == 202)
        //            {
        //                gs.DeleteEvent = 1;

        //            }
        //            if (menuId[i] == 203)
        //            {
        //                gs.AddSection = 1;

        //            }
        //            if (menuId[i] == 204)
        //            {
        //                gs.EditSection = 1;

        //            }
        //            if (menuId[i] == 205)
        //            {
        //                gs.DeleteSection = 1;

        //            }
        //            if (menuId[i] == 206)
        //            {
        //                gs.AddGradeGroup = 1;

        //            }
        //            if (menuId[i] == 207)
        //            {
        //                gs.EditGradeGroup = 1;

        //            }
        //            if (menuId[i] == 208)
        //            {
        //                gs.DeleteGradeGroup = 1;

        //            }
        //            if (menuId[i] == 209)
        //            {
        //                gs.AddBank = 1;

        //            }
        //            if (menuId[i] == 210)
        //            {
        //                gs.EditBank = 1;

        //            }
        //            if (menuId[i] == 211)
        //            {
        //                gs.DeleteBank = 1;


        //            }
        //            if (menuId[i] == 212)
        //            {
        //                gs.AddHoliday = 1;

        //            }
        //            if (menuId[i] == 213)
        //            {
        //                gs.EditHoliday = 1;

        //            }
        //            if (menuId[i] == 214)
        //            {
        //                gs.DeleteHoliday = 1;

        //            }
        //            if (menuId[i] == 215)
        //            {
        //                gs.AddNotice = 1;

        //            }
        //            if (menuId[i] == 216)
        //            {
        //                gs.EditNotice = 1;

        //            }
        //            if (menuId[i] == 217)
        //            {
        //                gs.DeleteNotice = 1;

        //            }
        //            if (menuId[i] == 218)
        //            {
        //                gs.AddOfficeVisit = 1;

        //            }

        //            if (menuId[i] == 219)
        //            {
        //                gs.EditOfficeVisit = 1;

        //            }
        //            if (menuId[i] == 220)
        //            {
        //                gs.DeleteOfficeVisit = 1;

        //            }
        //            if (menuId[i] == 221)
        //            {
        //                gs.DeleteOfficeApproval = 1;

        //            }
        //            if (menuId[i] == 222)
        //            {
        //                gs.AddLeaveMaster = 1;

        //            }
        //            if (menuId[i] == 223)
        //            {
        //                gs.EditLeaveMaster = 1;

        //            }
        //            if (menuId[i] == 224)
        //            {
        //                gs.DeleteLeaveMaster = 1;

        //            }
        //            if (menuId[i] == 225)
        //            {
        //                gs.AddLeaveApplication = 1;

        //            }
        //            if (menuId[i] == 226)
        //            {
        //                gs.EditLeaveApplication = 1;

        //            }
        //            if (menuId[i] == 227)
        //            {
        //                gs.DeleteLeaveApplication = 1;

        //            }
        //            if (menuId[i] == 228)
        //            {
        //                gs.AddLeaveQuota = 1;

        //            }
        //            if (menuId[i] == 229)
        //            {
        //                gs.EditLeaveQuota = 1;

        //            }
        //            if (menuId[i] == 230)
        //            {
        //                gs.DeleteLeaveQuota = 1;

        //            }
        //            if (menuId[i] == 231)
        //            {
        //                gs.AddLeaveSettlement = 1;

        //            }
        //            if (menuId[i] == 232)
        //            {
        //                gs.EditLeaveSettlement = 1;

        //            }
        //            if (menuId[i] == 233)
        //            {
        //                gs.DeleteLeaveSettlement = 1;

        //            }
        //            if (menuId[i] == 238)
        //            {
        //                gs.AddAllowance = 1;

        //            }
        //            if (menuId[i] == 239)
        //            {
        //                gs.DeleteAllowance = 1;

        //            }
        //            if (menuId[i] == 256)
        //            {
        //                gs.EditAllowance = 1;

        //            }
        //            if (menuId[i] == 240)
        //            {
        //                gs.AddDeductionRebate = 1;

        //            }
        //            if (menuId[i] == 241)
        //            {
        //                gs.EditDeductionRebate = 1;

        //            }
        //            if (menuId[i] == 242)
        //            {
        //                gs.DeleteDeductionRebate = 1;

        //            }
        //            if (menuId[i] == 243)
        //            {
        //                gs.AddNonResidentalTax = 1;

        //            }
        //            if (menuId[i] == 244)
        //            {
        //                gs.EditNonResidentalTax = 1;
        //            }
        //            if (menuId[i] == 245)
        //            {
        //                gs.DeleteNonResidentalTax = 1;

        //            }
        //            if (menuId[i] == 246)
        //            {
        //                gs.AddResidentalTax = 1;
        //            }
        //            if (menuId[i] == 247)
        //            {
        //                gs.EditResidentalTax = 1;
        //            }
        //            if (menuId[i] == 248)
        //            {
        //                gs.DeleteResidentalTax = 1;
        //            }
        //            if (menuId[i] == 2266)
        //            {
        //                gs.SendEmail = 1;
        //            }
        //            if (menuId[i] == 3269)
        //            {
        //                gs.ViewAllUser = 1;

        //            }
        //            if (menuId[i] == 3270)
        //            {
        //                gs.ViewAllEmployee = 1;
        //            }
        //            if (menuId[i] == 3271)
        //            {
        //                gs.ViewAllEmpRpt = 1;
        //            }
                   
        //            if (menuId[i] == 3275)
        //            {
        //                gs.CreateBasicDetail = 1;
        //            }
        //            if (menuId[i] == 3276)
        //            {
        //                gs.CreateEmpSalary = 1;

        //            }
        //            if (menuId[i] == 3277)
        //            {
        //                gs.CreateEmpShift = 1;
        //            }
        //            if (menuId[i] == 3278)
        //            {
        //                gs.CreateEmpTimepunch = 1;
        //            }
        //            if (menuId[i] == 3279)
        //            {
        //                gs.ViewAttendanceReport = 1;
        //            }

        //            if (menuId[i] == 4269)
        //            {
        //                gs.ViewAllEmployeeLeave = 1;
        //            }

        //            if (menuId[i] == 6274)
        //            {
        //                gs.ViewAllBranchData = 1;
        //            }
        //            if (menuId[i] == 6275)
        //            {
        //                gs.AddEmplyeeTransfer = 1;
        //            }
        //            if (menuId[i] == 6276)
        //            {
        //                gs.EditEmplyeeTransfer = 1;
        //            }
        //            if (menuId[i] == 6277)
        //            {
        //                gs.DeleteEmplyeeTransfer = 1;
        //            }
        //            if (menuId[i] == 6278)
        //            {
        //                gs.ApproveEmplyeeTransfer = 1;
        //            }
        //            if (menuId[i] == 1270)
        //            {
        //                gs.AddEmployeeResignation = 1;
        //            }
        //            if (menuId[i] == 1271)
        //            {
        //                gs.EditEmployeeResignation = 1;
        //            }
        //            if (menuId[i] == 1272)
        //            {
        //                gs.ApproveEmployeeResignation = 1;
        //            }
        //            if (menuId[i] == 7274)
        //            {
        //                gs.CreateRemoteAttendance = 1;
        //            }
        //            if (menuId[i] == 7275)
        //            {
        //                gs.EditRemoteAttendance = 1;
        //            }
        //            if (menuId[i] == 7276)
        //            {
        //                gs.DeleteRemoteAttendance = 1;
        //            }
        //            if (menuId[i] == 7278)
        //            {
        //                gs.IsGovermental = 1;
        //            }
        //        }
        //    }
        //    //HttpContext.Current.Session["GlobalArray"] = gs;
        //    //var xxx = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
           
        //}
        #endregion
    }
}