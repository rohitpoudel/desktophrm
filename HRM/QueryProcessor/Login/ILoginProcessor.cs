﻿
using System.Collections.Generic;

namespace HRM
{
public	interface ILoginProcessor
	{
		bool Authenticate(LoginCredential user);
        //void GetSetTemplateValue();
        bool IsEmailVerified(string code, string cID);
        Errorcheck CheckEmailExist(string username);
        int GetEmpIdbymail(string mail);
        bool CreateUserVerify(UserVerify uv);
        Employee GetMailbyId(string empid);
        bool isCodeUsed(string code, string cID);
        bool UpdatePassword(int eId, string password, string code, string cID);
        string GetCompanyCodebyEid(int eId);
        IEnumerable<AbsentEmpDetails> GetAbsEmpList(string cId);
        bool IsEmailSentToAbsEmployee(int eId);
    }
}
