﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM
{
    public class NonResidentialTaxHRM : INonResidentialTaxHRM
    {
        public int Create(NonResidentialTax nonResidentialTax)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", nonResidentialTax.Event);
                    param.Add("@Id", nonResidentialTax.Id);
                    param.Add("@Tax", nonResidentialTax.Tax);
                    param.Add("@TaxType", nonResidentialTax.TaxType);
                    param.Add("@FiscalYearId", nonResidentialTax.FiscalYearId);
                    param.Add("@IsActive", nonResidentialTax.Is_Active);
                    param.Add("@CreatedBy", nonResidentialTax.Created_By);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_NonResidentialTax]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[NonResidentialTax] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public IEnumerable<NonResidentialTaxViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var NonResidentialTaxlist = db.Query<NonResidentialTaxViewModel>(sql: "[dbo].[Usp_GetNonResidentialTaxList]", commandType: CommandType.StoredProcedure);
                    return NonResidentialTaxlist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NonResidentialTax GetById(int id)
        {
            try
            {
                var obj = new NonResidentialTax();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var NonResidentialTaxlist = db.Query<NonResidentialTax>(sql: "[dbo].[Usp_GetNonResidentialTaxById]", param: param, commandType: CommandType.StoredProcedure);
                    if (NonResidentialTaxlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(NonResidentialTaxlist.Select(x => x.Id).First());
                        obj.Tax = Convert.ToInt32(NonResidentialTaxlist.Select(x => x.Tax).First());
                        obj.TaxType = NonResidentialTaxlist.Select(x => x.TaxType).First();
                        obj.FiscalYearId = NonResidentialTaxlist.Select(x => x.FiscalYearId).First();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool chaekDuplicate(int FiscalYearId, string Action, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FiscalYearId", FiscalYearId);
                    param.Add("@Action", Action);
                    param.Add("@id", id);
                    int result = db.Query<int>(sql: "[dbo].[CheckDublicateNonResidentialTax]", param: param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}