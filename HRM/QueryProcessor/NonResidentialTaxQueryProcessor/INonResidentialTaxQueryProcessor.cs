﻿
using System.Collections.Generic;


namespace HRM
{
    public interface INonResidentialTaxHRM
    {
        int Create(NonResidentialTax nonResidentialTax);
        IEnumerable<NonResidentialTaxViewModel> GetAllData();
        NonResidentialTax GetById(int id);
        bool Delete(int id);
        bool chaekDuplicate(int FiscalYearId, string Action, int id);
    }
}
