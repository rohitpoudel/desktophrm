﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
    public class EmployeeResignationQueryPeocessor : IEmployeeResignationQueryPeocessor
    {
        public IEnumerable<EmployeeResignation> GetAll(string bId)
        {
            try
            {
                GlobalArray gs = new GlobalArray();
                //gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<EmployeeResignation>(sql: "[dbo].[GetResignedEmployee]", commandType: CommandType.StoredProcedure);
                   
                    if (gs.ViewAllEmployee != 1)
                    {
                       // int eId = (int)HttpContext.Current.Session["UserId"];
                        int eId = 1;



                        data = data.Where(x => x.EmployeeId == eId).ToList();



                    }
                    if (gs.ViewAllBranchData != 1)
                    {
                       // var bnId = (int)HttpContext.Current.Session["BranchId"];
                       var bnId = 1;
                        data = data.Where(x => x.BranchId == bnId).ToList();

                    }
                    if (!string.IsNullOrEmpty(bId) && Convert.ToInt32(bId) != 0)
                    {
                        data = data.Where(x => x.BranchId == Convert.ToInt32(bId)).ToList();
                    }
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Create(EmployeeResignation data, int CreatedBy, int UpdatedBy, string Event)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", Event);
                    param.Add("@Id", data.Id);
                    param.Add("@EmployeeId", data.EmployeeId);
                    param.Add("@ResignationOrTerminate", data.ResignationOrTerminate);
                    param.Add("@Date", data.Date);
                    param.Add("@Remarks", data.Remarks);
                    param.Add("@Status", data.Status);
                    param.Add("@CreatedBy", CreatedBy);
                    param.Add("@CreatedDate", DateTime.Now);
                    param.Add("@UpdatedBy", data.UpdatedBy);
                    param.Add("@UpdatedDate", data.UpdatedDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Resignation]", param: param, commandType: CommandType.StoredProcedure);

                    db.Close();
                    int ReturnParm = param.Get<Int16>("@Return_Id");

                    if (ReturnParm > 0)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeResignation Edit(int Id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", Id);
                    var data = db.Query<EmployeeResignation>(sql: "[dbo].[GetResignedEmployeeById]", param: param, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Approve(int Id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[EmployeeResignation] SET [Status] = 'A' Where Id= @Id", param: new { @Id = Id }, commandType: CommandType.Text);
                    db.Close();

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int UpdateEmployee(int EmployeeId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Employee] SET [Is_Active] = 0 Where Id = @Id", param: new { @Id = EmployeeId }, commandType: CommandType.Text);
                    db.Close();

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}