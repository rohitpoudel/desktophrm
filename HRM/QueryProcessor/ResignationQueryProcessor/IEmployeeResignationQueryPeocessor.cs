﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM
{
    interface IEmployeeResignationQueryPeocessor
    {
        IEnumerable<EmployeeResignation> GetAll(string bId);
        bool Create(EmployeeResignation data, int CreatedBy, int UpdatedBy, string Event);
        EmployeeResignation Edit(int Id);
        int Approve(int Id);
        int UpdateEmployee(int EmpId);
    }
}
