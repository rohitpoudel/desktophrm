﻿
using System.Collections.Generic;
namespace HRM
{
   public interface ITaskAssignmentHRM
    {
		/// <summary>
		/// Create Task assignment
		/// </summary>
		/// <param name="taskAssignment"></param>
		/// <returns></returns>
		int Create(TaskAssignment taskassignment);
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        IEnumerable<TaskAssignmentViewModel> GetAllData();
		/// <summary>
		/// Get all task Data
		/// </summary>
		/// <param name="id"> Task Id Parameter</param>
		/// <returns></returns>
        TaskAssignment GetById(int id);
		
		/// <summary>
		/// Delete Task
		/// </summary>
		/// <param name="id">Task Id Parameter</param>
		/// <returns></returns>
		bool delete(int id);


    }
}
