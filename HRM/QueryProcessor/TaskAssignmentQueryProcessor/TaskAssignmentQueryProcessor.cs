﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class TaskAssignmentHRM : ITaskAssignmentHRM
    {
       
		
		public  int Create(TaskAssignment taskassignment)
		{
			
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					 //db.OpenAsync();
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Id", taskassignment.Id);
					param.Add("@Event", taskassignment.Event);
					param.Add("@TaskName", taskassignment.TaskName);
					param.Add("@TaskDescription", taskassignment.TaskDescription);
					param.Add("@EmployeeName", taskassignment.EmployeeName);
					param.Add("@AssignedBy", taskassignment.AssignedBy);					
					param.Add("@StartDate", taskassignment.StartDate);
					param.Add("@EndDate", taskassignment.EndDate);
					param.Add("@TaskStatus", taskassignment.TaskStatus);
                    param.Add("@IsDeleted", taskassignment.IsDeleted);
                    param.Add("@CreateBy", taskassignment.CreateBy);
                    param.Add("@CreateDate", taskassignment.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);				
					db.Execute("[dbo].[Usp_IUD_TaskAssignments]", param: param, commandType: CommandType.StoredProcedure);
					db.Close();
					return param.Get<Int16>("@Return_Id");
					
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			
		}
		public bool delete(int id)
        {
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE [dbo].[TaskAssignments] SET [IsDeleted]=@IsDeleted Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;


				}
			}
			catch (Exception ex)
			{
				throw ex;

			}
		}

     

        public IEnumerable<TaskAssignmentViewModel> GetAllData()
        {
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{					
					db.Open();
					var tasklist =  db.Query<TaskAssignmentViewModel>(sql: "[dbo].[Usp_GetTaskList]", commandType: CommandType.StoredProcedure);
					return tasklist.ToList();


				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public TaskAssignment GetById(int id)
        {
			try
			{
				var obj = new TaskAssignment();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var tasklist = db.Query<TaskAssignmentViewModel>(sql: "[dbo].[Usp_GetTaskById]", param: param, commandType: CommandType.StoredProcedure);
					if(tasklist.Count()>0)
					{
						obj.Id = Convert.ToInt32(tasklist.Select(x=>x.Id).First());
						obj.TaskName = tasklist.Select(x => x.TaskName).First().ToString();
						obj.TaskDescription = tasklist.Select(x => x.TaskDescription).First().ToString();
						//obj.TaskStatus = Convert.ToBoolean(tasklist.Select(x => x.TaskStatus).First());
						obj.EmployeeName = tasklist.Select(x => x.EmployeeName).First().ToString();
						obj.AssignedBy = tasklist.Select(x => x.AssignedBy).First().ToString();
						obj.StartDate = Convert.ToDateTime(tasklist.Select(x => x.StartDate).First());
						obj.EndDate = Convert.ToDateTime(tasklist.Select(x => x.EndDate).First());
						
					}
					db.Close();
					return obj;


				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

       

      
    }
}