﻿
using System.Collections.Generic;


namespace HRM
{
	public interface ILeaveMasterHRM
	{
		int Create(LeaveMaster leaveMaster);
		IEnumerable<LeaveMaster> GetAllData();
		LeaveMaster GetById(int id);
		IEnumerable<LeaveMaster> GetLeaveMasterByGender(string gender);
		bool Delete(int id);
		int GetCode(string code, int id);

        LeaveAssignList GetEmployeeWithAssignLeave();

        bool AssignLeaveToEmployee(List<LeaveAssign> leaveAssigns);
        AssignLeave GetTotalAssignLeaveById(int EmployeeId, int id);

    }
}
