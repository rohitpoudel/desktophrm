﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
    public class LeaveMasterHRM : ILeaveMasterHRM
    {
        IDbTransaction sqlTran;
        public int Create(LeaveMaster leaveMaster)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    if (leaveMaster.Remarks == null)
                    {
                        leaveMaster.Remarks = "";
                    }
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", leaveMaster.Event);
                    param.Add("@Id", leaveMaster.Id);
                    param.Add("@Code", leaveMaster.Code);
                    param.Add("@LeaveName", leaveMaster.LeaveName);
                    param.Add("@LeaveNameNepali", leaveMaster.LeaveNameNepali);
                    param.Add("@NoOfDays", leaveMaster.NoOfDays);
                    param.Add("@LeaveIncrementPeriod", leaveMaster.LeaveIncrementPeriod);
                    param.Add("@ApplicableGender", leaveMaster.ApplicableGender);
                    param.Add("@IsReplacement", leaveMaster.IsReplacement);
                    param.Add("@IsCarryable", leaveMaster.IsCarryable);
                    param.Add("@IsPaid", leaveMaster.IsPaid);
                    param.Add("@CreatedBy", leaveMaster.CreatedBy);
                    param.Add("@CreatedDate", leaveMaster.CreatedDate);
                    param.Add("@Remarks", leaveMaster.Remarks);
                    param.Add("@IsDeleted", leaveMaster.IsDeleted);
                    param.Add("@FiscalYear", leaveMaster.FiscalYear);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_LeaveMaster]", param: param, commandType: CommandType.StoredProcedure);

                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE[dbo].[LeaveMaster] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LeaveMaster> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var leaveList = db.Query<LeaveMaster>(sql: "[dbo].[GetLeaveMasterList]", commandType: CommandType.StoredProcedure);
                    return leaveList.ToList();

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetCode(string code, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<LeaveMaster>("SELECT Code FROM[dbo].[LeaveMaster] where Code = @Code and Id != @id and IsDeleted = 0", param: new { Code = code, @Id = id }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    return s;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public LeaveMaster GetById(int id)
        {
            try
            {
                LeaveMaster obj = new LeaveMaster();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var leaveList = db.Query<LeaveMaster>(sql: "[dbo].[GetLeaveMasterById]", param: param, commandType: CommandType.StoredProcedure);
                    if (leaveList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(leaveList.Select(x => x.Id).First());
                        obj.Code = Convert.ToString(leaveList.Select(x => x.Code).First());
                        obj.LeaveName = Convert.ToString(leaveList.Select(x => x.LeaveName).First());
                        obj.LeaveNameNepali = Convert.ToString(leaveList.Select(x => x.LeaveNameNepali).First());
                        obj.NoOfDays = Convert.ToInt32(leaveList.Select(x => x.NoOfDays).First());
                        obj.LeaveIncrementPeriod = Convert.ToString(leaveList.Select(x => x.LeaveIncrementPeriod).First());
                        obj.ApplicableGender = Convert.ToString(leaveList.Select(x => x.ApplicableGender).First());
                        obj.IsReplacement = Convert.ToBoolean(leaveList.Select(x => x.IsReplacement).First());
                        obj.IsCarryable = Convert.ToBoolean(leaveList.Select(x => x.IsCarryable).First());
                        obj.IsPaid = Convert.ToBoolean(leaveList.Select(x => x.IsPaid).First());
                        obj.Remarks = Convert.ToString(leaveList.Select(x => x.Remarks).First());
                        obj.FiscalYear = Convert.ToInt32(leaveList.Select(x => x.FiscalYear).First());
                        obj.LeaveNameNepali = Convert.ToString(leaveList.Select(x => x.LeaveNameNepali).First());

                    }
                    db.Close();
                    return obj;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LeaveMaster> GetLeaveMasterByGender(string gender)
        {
            try
            {
                LeaveMaster obj = new LeaveMaster();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@gender", gender);
                    var leaveList = db.Query<LeaveMaster>(sql: "[dbo].[GetLeaveMasterByGender]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return leaveList;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public LeaveAssignList GetEmployeeWithAssignLeave()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();

                    LeaveAssignList data = new LeaveAssignList();
                    //var CurrentFiscalYearId = db.Query<int>(sql: "Select Id from FiscalYears Where GETDATE() Between StartDate AND EndDate", commandType: CommandType.Text).ToList();
                    var CurrentFiscalYearId = db.Query<int>(sql: "Select TOP 1 Id from FiscalYears Where CurrentFiscalYear = 1", commandType: CommandType.Text).SingleOrDefault();
                    GlobalArray gs = new GlobalArray();
                    //gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData != 1)
                    {
                       // var bId = (int)HttpContext.Current.Session["BranchId"];
                        var bId = 1;
                        data.LeaveAssigns = db.Query<LeaveAssign>(sql: "Select epm.BranchId, epm.Id EmployeeId, epm.Emp_Name EmployeeName , Gender from Employee epm where epm.Is_Active = 1 AND epm.Emp_DeviceCode != 0 and epm.BranchId = "+bId+"", commandType: CommandType.Text).ToList();

                    }
                    else
                    {
                        data.LeaveAssigns = db.Query<LeaveAssign>(sql: "Select epm.BranchId, epm.Id EmployeeId, epm.Emp_Name EmployeeName , Gender from Employee epm where epm.Is_Active = 1 AND epm.Emp_DeviceCode != 0", commandType: CommandType.Text).ToList();

                    }
                    foreach (var item in data.LeaveAssigns)
                    {
                        item.FiscalYearId = CurrentFiscalYearId;
                        item.AssignedLeave = db.Query<AssignLeave>(sql: @"select IsNull(IsLeave,0) IsLeave, LM.Id LeaveId ,cast(ISNULL(a.NoOfDays,0) as int) NoOfDays1,IsNull(a.TotalLeave,0) TotalLeave, LM.LeaveName from(Select la.Id LeaveId,  CASE WHEN a.NoOfDays IS NULL THEN la.NoOfDays ELSE a.NoOfDays END NoOfDays
                                                                            , SUM(la.NoOfDays) over () TotalLeave, 1 as IsLeave
                                                                            from LeaveMaster la
                                                                            Left JOIN (
                                                                            Select la.EmployeeId, lad.LeaveId, lad.NoOfDays from LeaveAssign la
                                                                            LEFT JOIN LeaveAssignDetail lad on la.Id = lad.LeaveAssignId
                                                                            Where EmployeeId = @EmpId And FiscalYearId = @FiscalYearId ) as a on la.Id = a.LeaveId where la.IsDeleted = 0 and la.ApplicableGender = @EGender or la.ApplicableGender = 4 ) as a
																			right join LeaveMaster as LM on a.LeaveId = LM.Id", param: new { EmpId = item.EmployeeId, FiscalYearId = CurrentFiscalYearId, EGender = item.Gender }, commandType: CommandType.Text).ToList();
                        item.TotalLeave = item.AssignedLeave.First().TotalLeave;
                    }

                 
                    //if (gs.ViewAllBranchData != 1)
                    //{
                    //    var bId = (int)HttpContext.Current.Session["BranchId"];
                    //    data = data.Where(x => x.BranchId == bId).ToList();

                    //}
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AssignLeaveToEmployee(List<LeaveAssign> leaveAssigns)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var CurrentFiscalYearId = db.Query<int>(sql: "Select TOP 1 Id from FiscalYears Where CurrentFiscalYear = 1", commandType: CommandType.Text).SingleOrDefault();
                    var count = db.Query<int>(sql: "Select Count(Id) from LeaveAssign Where FiscalYearId=@currentFY", param: new { currentFY = CurrentFiscalYearId }, commandType: CommandType.Text).SingleOrDefault();
                    //if (count > 0)
                    //{
                    //   // leaveAssigns = leaveAssigns.Where(x => x.IsChanged == true).ToList();
                    //}
                    sqlTran = db.BeginTransaction();
                    foreach (var item in leaveAssigns)
                    {
                        var FiscaYearId = item.FiscalYearId;
                        var EmployeeId = item.EmployeeId;
                        var param = new DynamicParameters();
                        param.Add("@EmployeeId", item.EmployeeId);
                        param.Add("@FiscalYearId", item.FiscalYearId);
                        param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                        db.Execute("[dbo].[DeleteAssignedLeave]", param: param, transaction: sqlTran, commandType: CommandType.StoredProcedure);
                        var RetResult = param.Get<Int16>("@Return_Id");
                        int res = db.Execute("DELETE FROM LeaveAssignDetail WHERE LeaveAssignId = @LeaveAssignId", param: new { LeaveAssignId = RetResult }, transaction: sqlTran, commandType: CommandType.Text);
                        if (RetResult >= 0)
                        {
                            var param1 = new DynamicParameters();
                            param1.Add("@EmployeeId", item.EmployeeId);
                            param1.Add("@FiscalYearId", item.FiscalYearId);
                            param1.Add("@CreatedBy", 1);
                            param1.Add("@CreatedDate", DateTime.Now);
                            param1.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                            db.Execute("[dbo].[SaveAssignedLeave]", param: param1, transaction: sqlTran, commandType: CommandType.StoredProcedure);
                            var AssignLeavetId = param.Get<Int16>("@Return_Id");

                            foreach (var assignLeave in item.AssignedLeave)
                            {
                                var AssignLeavetId1 = db.Query<int>(sql: "Select TOP 1 Id from LeaveAssign Where EmployeeId = @EmployeeId AND FiscalYearId = @FiscalYearId", param: new { EmployeeId = item.EmployeeId, FiscalYearId = item.FiscalYearId }, transaction: sqlTran, commandType: CommandType.Text).SingleOrDefault();
                                var param2 = new DynamicParameters();
                                param2.Add("@AssignLeavetId", AssignLeavetId1);
                                param2.Add("@LeaveId", assignLeave.LeaveId);
                                param2.Add("@NoOfDays", assignLeave.NoOfDays1);
                                param2.Add("@EmployeeId", item.EmployeeId);
                                param2.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                                db.Execute("[dbo].[SaveAssignedLeaveDetail]", param: param2, transaction: sqlTran, commandType: CommandType.StoredProcedure);
                                var id = param.Get<Int16>("@Return_Id");
                            }

                        }
                    }
                    sqlTran.Commit();
                    db.Close();
                    return true;

                }

            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                ///ExceptionHandler.AppendLog(ex);
                return false;
            }
        }

        public AssignLeave GetTotalAssignLeaveById(int EmployeeId, int id)
        {
            try
            {
                AssignLeave obj = new AssignLeave();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var CurrentFiscalYearId = db.Query<int>(sql: "Select TOP 1 Id from FiscalYears Where CurrentFiscalYear = 1", commandType: CommandType.Text).SingleOrDefault();
                    param.Add("@Id", id);
                    param.Add("@EmployeeId", EmployeeId);
                    var leaveList = db.Query<AssignLeave>(sql: @"Select lad.NoOfDays 
                                       from LeaveAssign la
                                       Left JOIN LeaveAssignDetail lad on la.Id = lad.LeaveAssignId
                                       Where la.EmployeeId = @EmployeeId AND lad.LeaveId = @id AND la.FiscalYearId = @FiscalYearId", param: new { EmployeeId = EmployeeId, Id = id, FiscalYearId = CurrentFiscalYearId }, commandType: CommandType.Text);
                    if (leaveList.Count() > 0)
                    {
                        obj.LeaveName = Convert.ToString(leaveList.Select(x => x.LeaveName).First());
                        obj.NoOfDays = Convert.ToInt32(leaveList.Select(x => x.NoOfDays).First());
                    }
                    db.Close();
                    return obj;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}