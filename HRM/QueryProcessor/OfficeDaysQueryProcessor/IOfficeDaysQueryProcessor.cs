﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IOfficeDaysHRM
    {
        int Create(OfficeDays residentialTax);
        IEnumerable<OfficeDaysViewModel> GetAllData();
        IEnumerable<OfficeDaysViewModel> GetAllDataForSearch(string searchString);
        OfficeDays GetById(int id);
        bool Delete(int id);
        int GetFiscalYear(string fiscalYear, int id);
    }
}
