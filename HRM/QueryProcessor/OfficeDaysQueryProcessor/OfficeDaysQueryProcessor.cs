﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM
{
    public class OfficeDaysHRM : IOfficeDaysHRM
    {
        public int Create(OfficeDays officeDays)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", officeDays.Event);
                    param.Add("@Id", officeDays.Id);
                    param.Add("@FiscalYearId", officeDays.FiscalYearId);
                    param.Add("@DaysInMonth", officeDays.DaysInMonth);
                    param.Add("@IsActive", officeDays.Is_Active);
                    param.Add("@CreatedBy", officeDays.Created_By);
                    param.Add("@UpdatedBy", officeDays.Updated_By);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_OfficeDays]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[OfficeDays] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int GetFiscalYear(string fiscalYear, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var GetYear = db.Query<OverTimeRule>("select * from OfficeDays where FiscalYearId = @FiscalYear  and Id!=@Id and Is_Active=1", param: new { FiscalYear = fiscalYear, Id = id }, commandType: CommandType.Text);
                    var result = GetYear.Count();
                    db.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<OfficeDaysViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var officeDays = db.Query<OfficeDaysViewModel>(sql: "[dbo].[Usp_GetOfficeDaysList]", commandType: CommandType.StoredProcedure);
                    return officeDays.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OfficeDays GetById(int id)
        {
            try
            {
                OfficeDays obj = new OfficeDays();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var overTimeRules = db.Query<OfficeDays>(sql: "[dbo].[Usp_GetOfficeDaysById]", param: param, commandType: CommandType.StoredProcedure);
                    if (overTimeRules.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(overTimeRules.Select(x => x.Id).First());
                        obj.FiscalYearId = overTimeRules.Select(x => x.FiscalYearId).First();
                        obj.DaysInMonth = overTimeRules.Select(x => x.DaysInMonth).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<OfficeDaysViewModel> GetAllDataForSearch(string searchString)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@searchString", searchString);
                    var officeDays = db.Query<OfficeDaysViewModel>(sql: "[dbo].[Usp_GetOfficeDaysListForSearch]", param: param, commandType: CommandType.StoredProcedure);
                    return officeDays.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}