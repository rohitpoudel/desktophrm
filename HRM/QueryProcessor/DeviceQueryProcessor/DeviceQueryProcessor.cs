﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class DeviceHRM : IDeviceHRM
    {
        public bool CheckIpAddress(int id, string iPAddress)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<Devices>("select IpAddress from Devices where IsDeleted = 0 and IpAddress = @IpAddress and id!= @Id ", param: new { IpAddress = iPAddress, Id = id }, commandType: CommandType.Text);
                    var number = data.Count();

                    db.Close();
                    if (number>0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Create(Devices device)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", device.Id);
                    param.Add("@Code", device.Code);
                    param.Add("@Event", device.Event);
                    param.Add("@DeviceName", device.DeviceName);
                    param.Add("@IpAddress", device.IpAddress);
                    param.Add("@SerialNo", device.SerialNo);
                    param.Add("@DeviceType", device.DeviceType);
                    param.Add("@DeviceModel", device.DeviceModel);
                    param.Add("@DeviceStatus", device.DeviceStatus);
                    param.Add("@Time", device.Time);
                    param.Add("@Port", device.Port);
                    param.Add("@ExtraProperty", device.ExtraProperty);
                    param.Add("@IsDeleted", device.IsDeleted);
                    param.Add("@IsTruncateTable", device.IsTruncateTable);
                    param.Add("@LastDateFetched", null);
                    param.Add("@CreateBy", device.CreateBy);
                    param.Add("@CreateDate", device.CreateDate);
                    param.Add("@BranchId", device.BranchId);
                    param.Add("@IsAutoClear", device.IsAutoClear);
                    param.Add("@IsSSREnabled", device.IsSSREnabled);
                    param.Add("@DevicePass", device.DevicePass);
                    param.Add("@CompanyCode", device.CompanyCode);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Devices]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<Devices> GetAllData()
        {
           
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var deviceList = db.Query<Devices>(sql: "[dbo].[GetDeviceList]", commandType: CommandType.StoredProcedure);
                    db.Close();
                    return deviceList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetCode(string code, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<Company>("select Code from Devices where IsDeleted = 0 and Id!= @Id and code = @Code ", param: new { Code = code, Id = id }, commandType: CommandType.Text);
                    var number = data.Count();
                    db.Close();
                    return number;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Devices GetById(int id)
        {
            try
            {
                Devices obj = new Devices();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var devicelist = db.Query<Devices>(sql: "[dbo].[GetDeviceById]", param: param, commandType: CommandType.StoredProcedure);
                    if (devicelist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(devicelist.Select(x => x.Id).First());
                        obj.Code = Convert.ToInt32(devicelist.Select(x => x.Code).First());
                        obj.DeviceName = devicelist.Select(x => x.DeviceName).First().ToString();
                        obj.IpAddress = devicelist.Select(x => x.IpAddress).First().ToString();
                        obj.SerialNo = devicelist.Select(x => x.SerialNo).First();
                        obj.DeviceType = devicelist.Select(x => x.DeviceType).First();
                        obj.DeviceModel = devicelist.Select(x => x.DeviceModel).First();
                        obj.Port = Convert.ToInt32(devicelist.Select(x => x.Port).First());
                        obj.DeviceStatus = devicelist.Select(x => x.DeviceStatus).First();
                        obj.Time = devicelist.Select(x => x.Time).First();
                        obj.BranchId = devicelist.Select(x => x.BranchId).First();
                        obj.IsSSREnabled= devicelist.Select(x => x.IsSSREnabled).First();
                        obj.DevicePass = devicelist.Select(x => x.DevicePass).First();
                        obj.CompanyCode = devicelist.Select(x => x.CompanyCode).First();
                        obj.IsAutoClear = devicelist.Select(x => x.IsAutoClear).First();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool IsDeviceExist(int Id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();

                    db.Open();
                    param.Add("@ID", Id);
                    var count = db.Query<Devices>(sql: "[dbo].[IsDeviceExist]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    if (count.FirstOrDefault().countData > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Devices] SET [IsDeleted] = 1 Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}