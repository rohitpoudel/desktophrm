﻿using System.Collections.Generic;


namespace HRM
{
    public interface IDeviceHRM
    {
        /// <summary>
        /// Ping the IP address on the LAN network
        /// </summary>
        /// <param name="id">Identity Identifier</param>
        /// <param name="iPAddress">IP Address of  the Device</param>
        /// <returns>Returns Boolean</returns>
        bool CheckIpAddress(int id, string iPAddress);
        /// <summary>
        /// Save Device data to database
        /// </summary>
        /// <param name="device">Device object Identifier</param>
        /// <returns>return integer</returns>
        int Create(Devices device);
        /// <summary>
        /// Get All the Active Device list
        /// </summary>
        /// <returns>Return Inumerable list</returns>
        IEnumerable<Devices> GetAllData();
        /// <summary>
        /// Get Device data by Id
        /// </summary>
        /// <param name="id">Identity Identfier</param>
        /// <returns>Return Device object</returns>
        Devices GetById(int id);
        /// <summary>
        /// Delete Device data 
        /// </summary>
        /// <param name="id">Identity Identfier</param>
        /// <returns>returns boolean</returns>
        bool Delete(int id);
        /// <summary>
        /// Check if Device exist on the database
        /// </summary>
        /// <param name="Id">Identity Identfier</param>
        /// <returns>Returns boolean</returns>
        bool IsDeviceExist(int Id);
        /// <summary>
        /// Check the code of active device
        /// </summary>
        /// <param name="code">Code </param>
        /// <param name="id">Identity Identifier</param>
        /// <returns></returns>
        int GetCode(string code, int id);
    }
}
