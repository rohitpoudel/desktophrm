﻿
using System.Collections.Generic;

namespace HRM
{
    public interface IOverTimeRuleHRM
    {
        int Create(OverTimeRule residentialTax);
        IEnumerable<OverTimeRuleViewModel> GetAllData();
        IEnumerable<OverTimeRuleViewModel> GetAllDataForSearch(string searchString);
        OverTimeRule GetById(int id);
        bool Delete(int id);
    }
}
