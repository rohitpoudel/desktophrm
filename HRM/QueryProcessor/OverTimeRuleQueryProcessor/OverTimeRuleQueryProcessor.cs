﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM
{
    public class OverTimeRuleHRM : IOverTimeRuleHRM
    {
        public int Create(OverTimeRule overTimeRule)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", overTimeRule.Event);
                    param.Add("@Id", overTimeRule.Id);
                    param.Add("@FiscalYearId", overTimeRule.FiscalYearId);
                    param.Add("@OverTimeAllowed", overTimeRule.OverTimeAllowed);
                    param.Add("@Type", overTimeRule.Type);
                    param.Add("@Incentive", overTimeRule.Incentive);
                    param.Add("@IsActive", overTimeRule.Is_Active);
                    param.Add("@CreatedBy", overTimeRule.Created_By);
                    param.Add("@UpdatedBy", overTimeRule.Updated_By);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_OverTimeRule]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[OverTimeRule] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<OverTimeRuleViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var overTimeRuleList = db.Query<OverTimeRuleViewModel>(sql: "[dbo].[Usp_GetOverTimeRuleList]", commandType: CommandType.StoredProcedure);
                    return overTimeRuleList.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OverTimeRule GetById(int id)
        {
            try
            {
                OverTimeRule obj = new OverTimeRule();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var overTimeRules = db.Query<OverTimeRule>(sql: "[dbo].[Usp_GetOverTimeRuleId]", param: param, commandType: CommandType.StoredProcedure);
                    if (overTimeRules.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(overTimeRules.Select(x => x.Id).First());
                        obj.FiscalYearId = overTimeRules.Select(x => x.FiscalYearId).First();
                        obj.OverTimeAllowed = overTimeRules.Select(x => x.OverTimeAllowed).First();
                        obj.Type = overTimeRules.Select(x => x.Type).First();
                        obj.Incentive = overTimeRules.Select(x => x.Incentive).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<OverTimeRuleViewModel> GetAllDataForSearch(string searchString)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@searchString", searchString);
                    var overTimeRuleList = db.Query<OverTimeRuleViewModel>(sql: "[dbo].[Usp_GetOverTimeRuleListForSearch]", commandType: CommandType.StoredProcedure);
                    return overTimeRuleList.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}