﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM
{
	public interface IOverTimeHRM
	{
		int Create(OverTime obj);
		IEnumerable<OverTime> GetAllData();
		OverTime GetById(int id);
		bool Delete(int id);
        int GetFiscalYear(string fiscalYear, int id);
		int GetOTByEmpAndDate(int empId, string date, int id);


	}
}
