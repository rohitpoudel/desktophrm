﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;




namespace HRM
{
	public class OverTimeHRM : IOverTimeHRM
	{
		public int Create(OverTime obj)
		{
			try
			{
                //var starttime = Convert.ToString(obj.Date) + ' ' + obj.StartTime;
                //var endTime = Convert.ToString(obj.Date) + ' ' + obj.EndTime;

                var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Event", obj.Event);
					param.Add("@Id", obj.Id);
					param.Add("@EmployeeId", obj.EmployeeId);
					param.Add("@Date", obj.Date);
					param.Add("@StartTime", obj.StartTime);
					param.Add("@EndTime", obj.EndTime);
					param.Add("@CreatedBy", obj.CreatedBy);
					param.Add("@CreatedDate", obj.CreatedDate);
					param.Add("@IsDeleted", obj.IsDeleted);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_OverTime]", param: param, commandType: CommandType.StoredProcedure);

					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE[dbo].[OverTime] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;

				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<OverTime> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var overTimeList = db.Query<OverTime>(sql: "[dbo].[GetOverTimeList]", commandType: CommandType.StoredProcedure);
                    foreach(var item in overTimeList)
                    {
                        item.NepDate = Convert.ToString(NepDateConverter.EngToNep(item.Date)).Replace('/','-');
                    }
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData != 1)
                    {
                       // var bnId = (int)HttpContext.Current.Session["BranchId"];
                        var bnId = 1;
                        overTimeList = overTimeList.Where(x => x.BranchId == bnId).ToList();

                    }
                    return overTimeList.ToList();

				}


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
        public int GetFiscalYear(string fiscalYear, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var GetYear = db.Query<OverTimeRule>("select * from OverTimeRule where FiscalYearId = @FiscalYear  and Id!=@Id and Is_Active=1", param: new { FiscalYear = fiscalYear,  Id = id }, commandType: CommandType.Text);
                    var result = GetYear.Count();
                    db.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OverTime GetById(int id)
		{
			try
			{
				var obj = new OverTime();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var overTimeList = db.Query<OverTime>(sql: "[dbo].[GetOverTimeById]", param: param, commandType: CommandType.StoredProcedure);
					if (overTimeList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(overTimeList.Select(x => x.Id).First());
						obj.EmployeeId = Convert.ToInt32(overTimeList.Select(x => x.EmployeeId).First());
						obj.Date = Convert.ToDateTime(overTimeList.Select(x => x.Date).First());
						obj.StartTime = Convert.ToString(overTimeList.Select(x => x.StartTime).First());
						obj.EndTime = Convert.ToString(overTimeList.Select(x => x.EndTime).First());

					}

					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int GetOTByEmpAndDate(int empId, string date, int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var list = db.Query<OverTime>("Select EmployeeId from OverTime where EmployeeId = @EmployeeId and Date=@Date and IsDeleted=0 and Id!=@Id", param: new { Employeeid = empId,Date=date, Id = id }, commandType: CommandType.Text);
					var result = list.Count();
					db.Close();
					return result;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}
	}
}