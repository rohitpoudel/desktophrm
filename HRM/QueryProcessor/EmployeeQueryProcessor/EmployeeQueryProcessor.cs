﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class EmployeeHRM : IEmployeeHRM
    {
        GlobalArray gs = null;
        public EmployeeHRM()
        {
            gs = new GlobalArray();
          //  gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
        }
        IDbTransaction sqlTran;
        public bool CreateUser(User user, DbConnection db, IDbTransaction tran)
        {
            try
            {

                //db.OpenAsync();

                var param = new DynamicParameters();
                param.Add("@Id", user.Id);
                param.Add("@Event", user.Event);
                param.Add("@RoleId", user.UserRoleId);
                param.Add("@UserId", user.EmployeeId);
                param.Add("@Password", user.Password);
                param.Add("@Company_Code", user.Company_Code);
                param.Add("@UserName", user.UserName);
                param.Add("@BranchId", null);
                param.Add("@CreatedBy", user.CreatedBy);
                param.Add("@CreatedDate",user.CreatedDate);
                param.Add("@IsActive", true);
                param.Add("@IsSuperAdmin", false);
                param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                db.Execute("[dbo].[Usp_IUD_ApplicationUser]", param: param, transaction: tran, commandType: CommandType.StoredProcedure);

                return true;

            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                throw ex;
            }
        }
        public bool CreateUserVerify(UserVerify uv)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {

                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@VU_ID", uv.VU_ID);
                    param.Add("@Event", 'I');
                    param.Add("@Code", uv.Code);
                    param.Add("@EmpId", uv.EmpId);
                    param.Add("@Status", uv.Status);
                    param.Add("@Created_Date", uv.Created_Date);

                    db.Execute("[dbo].[Usp_IUD_UserVerify]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return true;

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
        public bool CreateAbsesntLog(AbsentLog uv)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {

                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", 'I');
                    param.Add("@EmpId", uv.EmpId);
                    param.Add("@IsMailSent", uv.IsMailSent);

                    db.Execute("[dbo].[Usp_IUD_AbsentLog]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return true;

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
        public int CreateFromMain(Employee obj, User Usr)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    if (obj.Emp_Code == null)
                    {
                        //obj.Emp_Code = HttpContext.Current.Session["Code"].ToString();
                        obj.Emp_Code = "001";
                    }
                    //db.OpenAsync();
                    db.Open();
                    sqlTran = db.BeginTransaction();
                    var param = new DynamicParameters();
                    param.Add("@Id", obj.Id);
                    param.Add("@Event", obj.Event);
                    param.Add("@Emp_Code", obj.Emp_Code);
                    param.Add("@Emp_Name", obj.Emp_Name);
                    param.Add("@Emp_DeviceCode", obj.Emp_DeviceCode);
                    param.Add("@Designation_Id", obj.Designation_Id);
                    param.Add("@Department_Id", obj.Department_Id);
                    param.Add("@Section_Id", obj.Section_Id);
                    param.Add("@GradeGroup", obj.GradeGroup);
                    param.Add("@IsManager", obj.IsManager);
                    param.Add("@DOB", obj.DOB);
                    param.Add("@DOBNepali", obj.DOBNepali);
                    param.Add("@Marital_Status", obj.Marital_Status);
                    param.Add("@Gender", obj.Gender);
                    param.Add("@Blood_Group", obj.Blood_Group);
                    param.Add("@Mobile_No", obj.Mobile_No);
                    param.Add("@Email", obj.Email);
                    param.Add("@PassPort_No", obj.PassPort_No);
                    param.Add("@CitizenShip_No", obj.CitizenShip_No);
                    param.Add("@Issued_Date", obj.Issued_Date);
                    param.Add("@Issued_DateNepali", obj.Issued_DateNepali);
                    param.Add("@Issued_District", obj.Issued_District);
                    param.Add("@Religion", obj.Religion);
                    param.Add("@photo", obj.photo);
                    param.Add("@Permanet_Address", obj.Permanet_Address);
                    param.Add("@Permanet_Nepali", obj.Permanet_Nepali);
                    param.Add("@Temp_Address", obj.Temp_Address);
                    param.Add("@Temp_Nepali", obj.Temp_Nepali);
                    param.Add("@Created_Date", obj.Created_Date);
                    param.Add("@Created_By", obj.Created_By);
                    param.Add("@Is_Active", obj.Is_Active);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_Employee]", param: param, transaction: sqlTran, commandType: CommandType.StoredProcedure);

                    int empId = param.Get<Int16>("@Return_Id");
                    if (Usr.Password != null)
                    {
                        Usr.EmployeeId = empId;
                        Usr.UserName = obj.Email;
                        CreateUser(Usr, db, sqlTran);
                    }
                    sqlTran.Commit();
                    db.Close();

                    return Usr.EmployeeId;

                }
            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                throw ex;
            }

        }



        public int Create(Employee obj)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", obj.Id);
                    param.Add("@Event", obj.Event);
                    param.Add("@Emp_Code", obj.Emp_Code);
                    param.Add("@Emp_Name", obj.Emp_Name);
                    param.Add("@Emp_DeviceCode", obj.Emp_DeviceCode);
                    param.Add("@Designation_Id", obj.Designation_Id);
                    param.Add("@Department_Id", obj.Department_Id);
                    param.Add("@Section_Id", obj.Section_Id);
                    param.Add("@GradeGroup", obj.GradeGroup);
                    param.Add("@IsManager", obj.IsManager);
                    param.Add("@DOB", obj.DOB);
                    param.Add("@DOBNepali", obj.DOBNepali);
                    param.Add("@Marital_Status", obj.Marital_Status);
                    param.Add("@Gender", obj.Gender);
                    param.Add("@Blood_Group", obj.Blood_Group);
                    param.Add("@Mobile_No", obj.Mobile_No);
                    param.Add("@Email", obj.Email);
                    param.Add("@PassPort_No", obj.PassPort_No);
                    param.Add("@CitizenShip_No", obj.CitizenShip_No);
                    param.Add("@Issued_Date", obj.Issued_Date);
                    param.Add("@Issued_DateNepali", obj.Issued_DateNepali);
                    param.Add("@Issued_District", obj.Issued_District);
                    param.Add("@Religion", obj.Religion);
                    param.Add("@photo", obj.photo);
                    param.Add("@Permanet_Address", obj.Permanet_Address);
                    param.Add("@Permanet_Nepali", obj.Permanet_Nepali);
                    param.Add("@Temp_Address", obj.Temp_Address);
                    param.Add("@Temp_Nepali", obj.Temp_Nepali);
                    param.Add("@Created_Date", obj.Created_Date);
                    param.Add("@Created_By", obj.Created_By);
                    param.Add("@Is_Active", obj.Is_Active);
                    param.Add("@BranchId", obj.BranchId);
                    param.Add("@IsPolitical", obj.IsPolitical);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_Employee]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int CreateShift(ShiftWO obj)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", obj.Id);
                    param.Add("@Event", obj.Event);
                    param.Add("@ShiftId", obj.ShiftId);
                    param.Add("@Emp_Id", obj.Emp_Id);
                    param.Add("@WeeklyOff", obj.WeeklyOff);
                    param.Add("@Created_By", obj.Created_By);
                    param.Add("@Created_Date", obj.Created_Date);
                    param.Add("@DualShift", obj.DualShift);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_EmployeeShift]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Employee] SET [Is_Active] = 0 Where Id=@Id", param: new { @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Employee> GetAllData()
        {
            try
            {
                List<Employee> Employeelist = new List<Employee>();

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    //int userId = (int)HttpContext.Current.Session["UserId"];
                    //var isSA = HttpContext.Current.Session["IsSuperAdmin"];
                    int userId = 1;
                    var isSA = true;
                    if (isSA.ToString() == "True")
                    {
                        userId = 1;
                    }
                    if (userId == 1)
                    {
                        Employeelist = db.Query<Employee>(sql: "[dbo].[Usp_GetEmployeeList]", commandType: CommandType.StoredProcedure).ToList();
                    }
                    else
                    {
                        Employeelist = db.Query<Employee>(sql: "[dbo].[Usp_GetEmployeeList]", commandType: CommandType.StoredProcedure).ToList();
                        // Employeelist = Employeelist.Where(x => x.Id != ).ToList();
                        if (gs.ViewAllEmployee != 1)
                        {
                          //  int eId = (int)HttpContext.Current.Session["UserId"];
                          int  eId = 1;
                           
                            
                                Employeelist = Employeelist.Where(x => x.Id == eId).ToList();

                            

                        }
                        if (gs.ViewAllBranchData != 1)
                        {
                           // var bId = (int)HttpContext.Current.Session["BranchId"];
                            var bId = 1;
                            Employeelist = Employeelist.Where(x=>x.BranchId == bId).ToList();

                        }
                    }
                  
                    return Employeelist;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Employee GetById(int id)
        {
            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var Employeelist = db.Query<Employee>(sql: "[dbo].[Usp_GetEmployeeById]", param: param, commandType: CommandType.StoredProcedure);
                    var imgName = "";
                    var imgFullPath = "";
                    if (Employeelist.FirstOrDefault().photo != null)
                    {
                        imgFullPath = "\\" + Employeelist.FirstOrDefault().photo;
                        var imageName = Employeelist.FirstOrDefault().photo.Split('\\');
                        imgName = imageName[2];
                    }

                    if (Employeelist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(Employeelist.Select(x => x.Id).First());
                        obj.Emp_Code = Employeelist.Select(x => x.Emp_Code).First();
                        obj.Emp_Name = Employeelist.Select(x => x.Emp_Name).First();
                        obj.Emp_DeviceCode = Employeelist.Select(x => x.Emp_DeviceCode).First();
                        obj.Designation_Id = Employeelist.Select(x => x.Designation_Id).First();
                        obj.Department_Id = Employeelist.Select(x => x.Department_Id).First();
                        obj.Section_Id = Employeelist.Select(x => x.Section_Id).First();
                        obj.DesignationName = Employeelist.Select(x => x.DesignationName).First();
                        obj.DepartmentName = Employeelist.Select(x => x.DepartmentName).First();
                        obj.SectionName = Employeelist.Select(x => x.SectionName).First();
                        obj.GradeGroup = Employeelist.Select(x => x.GradeGroup).First();
                        obj.IsManager = Employeelist.Select(x => x.IsManager).First();
                        obj.DOB = Employeelist.Select(x => x.DOB).First();
                        obj.DOBNepali = Employeelist.Select(x => x.DOBNepali).First();
                        obj.Marital_Status = Employeelist.Select(x => x.Marital_Status).First();
                        obj.Gender = Employeelist.Select(x => x.Gender).First();
                        obj.Blood_Group = Employeelist.Select(x => x.Blood_Group).First();
                        obj.Mobile_No = Employeelist.Select(x => x.Mobile_No).First();
                        obj.Email = Employeelist.Select(x => x.Email).First();
                        obj.PassPort_No = Employeelist.Select(x => x.PassPort_No).First();
                        obj.CitizenShip_No = Employeelist.Select(x => x.CitizenShip_No).First();
                        obj.Issued_Date = Employeelist.Select(x => x.Issued_Date).First();
                        obj.Issued_DateNepali = Employeelist.Select(x => x.Issued_DateNepali).First();
                        obj.Issued_District = Employeelist.Select(x => x.Issued_District).First();
                        obj.Religion = Employeelist.Select(x => x.Religion).First();
                        obj.photo = imgName;
                        obj.FullPath = imgFullPath;
                        obj.Permanet_Address = Employeelist.Select(x => x.Permanet_Address).First();
                        obj.Permanet_Nepali = Employeelist.Select(x => x.Permanet_Nepali).First();
                        obj.Temp_Address = Employeelist.Select(x => x.Temp_Address).First();
                        obj.Temp_Nepali = Employeelist.Select(x => x.Temp_Nepali).First();
                        obj.Marital_Status_Name = Employeelist.Select(x => x.Marital_Status_Name).First();
                        obj.GenderName = Employeelist.Select(x => x.GenderName).First();
                        obj.Blood_Group_Name = Employeelist.Select(x => x.Blood_Group_Name).First();
                        obj.Religion_Name = Employeelist.Select(x => x.Religion_Name).First();
                        obj.BranchId = Employeelist.Select(x => x.BranchId).First();
                        obj.IsPolitical = Employeelist.Select(x => x.IsPolitical).First();
                    }
                    if (obj.Id > 0)
                    {
                        obj.EmployeeTransferDetails = db.Query<EmployeeTransfer>(sql: @"Select et.EffectiveFrom
                                                            , obr.BranchName OldBranchName
                                                            , nbr.BranchName NewBranchName
                                                            , odp.DepartmentName OldDepartmentName
                                                            , ndp.DepartmentName NewDepartmentName
                                                            , ods.DesignationName OldDesignationName
                                                            , nds.DesignationName NewDesignationName
                                                            , osc.SectionName OldSectionName
                                                            , nsc.SectionName NewSectionName
                                                            from EmployeeTransfer et
                                                            LEFT JOIN Employee ep on et.EmployeeId = ep.Id

                                                            LEFT JOIN Branch obr on et.OldBranchId = obr.Id
                                                            Left JOIN Branch nbr on et.NewBranchId = nbr.Id

                                                            LEFT JOIN Departments odp on et.OldDepartmentId = odp.Id
                                                            Left JOIN Departments ndp on et.NewDepartmentId = ndp.Id

                                                            LEFT JOIN Designations ods on et.OldDesignationId = ods.Id
                                                            Left JOIN Designations nds on et.NewDesignationId = nds.Id

                                                            LEFT JOIN Sections osc on et.OldSectionId = osc.Id
                                                            Left JOIN Sections nsc on et.NewSectionId = nsc.Id WHERE et.EmployeeId = @EmpId and et.IsAproved = 1"
                              , param: new { @EmpId = obj.Id }, commandType: CommandType.Text).ToList();
                    }
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeSalaryInfo GetEmployeeSalaryId(int id)
        {
            try
            {
                EmployeeSalaryInfo obj = new EmployeeSalaryInfo();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var EmployeeSalary = db.Query<EmployeeSalaryInfo>(sql: "[dbo].[Usp_GetEmployeeSalary]", param: param, commandType: CommandType.StoredProcedure).ToList();


                    if (EmployeeSalary.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(EmployeeSalary.Select(x => x.Id).First());
                        obj.BasicSalary = EmployeeSalary.Select(x => x.BasicSalary).First();
                        obj.FromDate = EmployeeSalary.Select(x => x.FromDate).First();
                        obj.WorkOnRemote = EmployeeSalary.Select(x => x.WorkOnRemote).First();
                        obj.RemoteRebate = EmployeeSalary.Select(x => x.RemoteRebate).First();
                        obj.Incentive = EmployeeSalary.Select(x => x.Incentive).First();
                        obj.OtherIncentive = EmployeeSalary.Select(x => x.OtherIncentive).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Shift> GetShiftDetails(int id, bool DualShift)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var data = new List<Shift>();
                    var param = new DynamicParameters();
                    db.Open();
                    if (DualShift)
                    {
                        data = db.Query<Shift>("select  EarlyGrace,LateGrace, MaxEarlyGrace , MaxLateGrace,MarkAbsentWhenEarlyOut,MarkAbsentWhenLateIn  from DualShift where IsActive= 1 and Id =@Id", param: new { Id = id }, commandType: CommandType.Text).ToList();
                    }
                    else
                    {
                        data = db.Query<Shift>("select  EarlyGrace,LateGrace, MaxEarlyGrace , MaxLateGrace,MarkAbsentWhenEarlyOut,MarkAbsentWhenLateIn  from Shift where IsDeleted=0 and Id =@Id", param: new { Id = id }, commandType: CommandType.Text).ToList();
                    }

                    db.Close();
                    return data.ToList();
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int GetCode(string code, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<Employee>("SELECT Emp_Code FROM[dbo].[Employee] where Emp_Code = @Code and Is_Active=1 and Id!=@Id", param: new { Code = code, Id = id }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    return s;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int IsPunchExists(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<EmployeeTimePunch>("select * from EmployeeTimePunch where Is_Active=1 and  Emp_Id =@Id", param: new { Id = id }, commandType: CommandType.Text);
                    int s = data.Count();

                    db.Close();
                    if (s > 0)
                    {
                        return data.FirstOrDefault().Id;
                    }
                    else
                    {
                        return 0;
                    }

                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ShiftWO GetShiftByEmpId(int id)
        {
            try
            {
                ShiftWO obj = new ShiftWO();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Emp_Id", id);
                    var Employeelist = db.Query<ShiftWO>(sql: "[dbo].[GetEmpShiftByEmpId]", param: param, commandType: CommandType.StoredProcedure);

                    if (Employeelist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(Employeelist.Select(x => x.Id).First());
                        obj.ShiftId = Convert.ToInt32(Employeelist.Select(x => x.ShiftId).First());
                        obj.Emp_Id = Convert.ToInt32(Employeelist.Select(x => x.Emp_Id).First());
                        obj.WeeklyOff = Employeelist.Select(x => x.WeeklyOff).First();
                        obj.DualShift = Employeelist.Select(x => x.IsDualShift).First();
                        string[] off = new string[] { Convert.ToString(obj.WeeklyOff) };
                        string[] offday = off[0].Split(',').ToArray();
                        int i = 0;
                        foreach (var c in offday)
                        {
                            if (c == "1" && i == 0)
                            {
                                obj.Sunday = true;
                            }
                            if (c == "2" && i == 1)
                            {
                                obj.Monday = true;
                            }
                            if (c == "3" && i == 2)
                            {
                                obj.Tuesday = true;
                            }
                            if (c == "4" && i == 3)
                            {
                                obj.Wednesday = true;

                            }
                            if (c == "5" && i == 4)
                            {
                                obj.Thursday = true;

                            }
                            if (c == "6" && i == 5)
                            {
                                obj.Friday = true;

                            }
                            if (c == "7" && i == 6)
                            {
                                obj.Saturday = true;

                            }

                            i++;
                        }
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Allowance> GetAllowances(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var Allowancelist = db.Query<Allowance>(sql: "[dbo].[Usp_GetAllowanceBySalaryId]", param: param, commandType: CommandType.StoredProcedure);
                    return Allowancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DeductionRebateTax> GetDeductionRebates(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var DeductionRebateList = db.Query<DeductionRebateTax>(sql: "[dbo].[Usp_GetDeductionRebateBySalaryId]", param: param, commandType: CommandType.StoredProcedure);
                    return DeductionRebateList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddSalary(Employee emp)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //using (TransactionScope scope = new TransactionScope())
                    //{
                    //db.OpenAsync();
                    //db.BeginTransaction();

                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", emp.EmployeeSalaryInfo.Id);
                    param.Add("@EmployeeId", emp.Id);
                    param.Add("@BasicSalary", emp.EmployeeSalaryInfo.BasicSalary);
                    param.Add("@WorkOnRemote", emp.EmployeeSalaryInfo.WorkOnRemote);
                    param.Add("@Incentive", emp.EmployeeSalaryInfo.Incentive);
                    param.Add("@OtherIncentive", emp.EmployeeSalaryInfo.OtherIncentive);
                    param.Add("@RemoteRebate", emp.EmployeeSalaryInfo.RemoteRebate);
                    param.Add("@FromDate", emp.EmployeeSalaryInfo.FromDate);
                    param.Add("@Todate", DateTime.Now);
                    param.Add("@Is_Active", true);
                    param.Add("@Created_Date",DateTime.Now);
                    param.Add("@Created_By", 1);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_EmployeeSalary]", param: param, commandType: CommandType.StoredProcedure);
                    //db.Close();
                    var SalaryId = param.Get<Int16>("@Return_Id");
                    //int abc = param.Get<int>("@Return_Id");
                    //int SalaryId = Convert.ToInt16(param.Get<int>("@Return_Id"));

                    int result = db.Execute("Delete from EmployeeAllowanceInfo Where SalaryId = @Id", param: new { @IsActive = false, @Id = SalaryId }, commandType: CommandType.Text);
                    if (emp.Allowances != null)
                    {
                        foreach (var allownace in emp.Allowances)
                        {
                            var allwanceParam = new DynamicParameters();
                            allwanceParam.Add("@Id", 0);
                            allwanceParam.Add("@SalaryId", SalaryId);
                            allwanceParam.Add("@AllowanceId", allownace.Id);
                            allwanceParam.Add("@Allowance", allownace.AllowanceAmount);
                            allwanceParam.Add("@Is_Active", true);
                            allwanceParam.Add("@Created_Date", DateTime.Now);
                            allwanceParam.Add("@Created_By", 1);
                            allwanceParam.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                            db.Execute("[dbo].[Usp_IUD_EmployeeAllownces]", param: allwanceParam, commandType: CommandType.StoredProcedure);
                            //db.Close();
                        }
                    }
                    int result1 = db.Execute("Delete from EmployeeDedRebInfo Where SalaryId = @Id", param: new { @IsActive = false, @Id = SalaryId }, commandType: CommandType.Text);
                    if (emp.DeductionRebates != null)
                    {
                        foreach (var dedReb in emp.DeductionRebates)
                        {
                            var dedParam = new DynamicParameters();
                            dedParam.Add("@Id", 0);
                            dedParam.Add("@SalaryId", SalaryId);
                            dedParam.Add("@DeductionRebateId", dedReb.Id);
                            dedParam.Add("@DeductionRebate", dedReb.Deduction);
                            dedParam.Add("@Is_Active", true);
                            dedParam.Add("@Created_By", 1);
                            dedParam.Add("@Created_Date", DateTime.Now);
                            dedParam.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                            db.Execute("[dbo].[Usp_IUD_EmployeeDeductionRebates]", param: dedParam, commandType: CommandType.StoredProcedure);
                            //db.Close();
                        }

                    }
                    //scope.Complete();
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                    //}



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Employee GetEmployeeByCode(string code)
        {
            Employee obj = new Employee();
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Emp_Code", code);
                    var data = db.Query<Employee>(sql: "[dbo].[GetEmployeeByCode]", param: param, commandType: CommandType.StoredProcedure);
                    if (data.Count() > 0)
                    {
                        obj.Id = data.Select(x => x.Id).First();
                        obj.Emp_Name = data.Select(x => x.Emp_Name).First();
                        obj.Emp_Code = data.Select(x => x.Emp_Code).First();
                        obj.Department_Id = data.Select(x => x.Department_Id).First();
                        obj.Designation_Id = data.Select(x => x.Designation_Id).First();
                        obj.Section_Id = data.Select(x => x.Section_Id).First();
                        obj.DepartmentName = data.Select(x => x.DepartmentName).First();
                        obj.DesignationName = data.Select(x => x.DesignationName).First();
                        obj.SectionName = data.Select(x => x.SectionName).First();
                        obj.Gender = data.Select(x => x.Gender).First();
                        obj.photo = data.Select(x => x.photo).First();
                    }
                    db.Close();
                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Section> GetSectionByDepartment(string depId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@DepId", depId);
                    var data = db.Query<Section>(sql: "[dbo].[GetSectionByDep]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Employee> GetEmployeeBySecAndDepAtt(string depId, string sectId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@DepId", depId);
                    param.Add("@SecId", sectId);
                    var data = db.Query<Employee>(sql: "[dbo].[GetEmpBySecAndDepAttendance]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Employee> GetEmployeeBySecDesDepAtt(int depId, int DesId, int sectId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<Employee>(sql: @"SELECT Id,Emp_Name FROM[dbo].[Employee] 
                                                        where (Department_Id = @DepId OR @DepId = 0) 
                                                        AND (Section_Id = @SecId OR @SecId = 0)
                                                        AND (Section_Id = @SecId OR @SecId = 0)
                                                        AND (Designation_Id = @DesId OR @DesId = 0)
                                                        AND Is_Active = 1 and Emp_DeviceCode !=0", param: new { @DepId = depId, @SecId = sectId, @DesId = DesId }, commandType: CommandType.Text);
                    db.Close();
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Employee> GetEmployeeBySecAndDep(string depId, string sectId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@DepId", depId);
                    param.Add("@SecId", sectId);
                    var data = db.Query<Employee>(sql: "[dbo].[GetEmpBySecAndDep]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Employee GetEmployeeInfoById(int id)
        {
            Employee obj = new Employee();
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", id);
                    var data = db.Query<Employee>(sql: "[dbo].[GetEmployeeInfoById]", param: param, commandType: CommandType.StoredProcedure);
                    if (data.Count() > 0)
                    {
                        obj.Id = data.Select(x => x.Id).First();
                        obj.Emp_Name = data.Select(x => x.Emp_Name).First();
                        obj.Emp_Code = data.Select(x => x.Emp_Code).First();
                        obj.Department_Id = data.Select(x => x.Department_Id).First();
                        obj.Designation_Id = data.Select(x => x.Designation_Id).First();
                        obj.Section_Id = data.Select(x => x.Section_Id).First();
                        obj.DepartmentName = data.Select(x => x.DepartmentName).First();
                        obj.DesignationName = data.Select(x => x.DesignationName).First();
                        obj.SectionName = data.Select(x => x.SectionName).First();
                        obj.Gender = data.Select(x => x.Gender).First();
                        obj.photo = data.Select(x => x.photo).First();
                    }
                    db.Close();
                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeeAttendaceInfo> EmployeeAttendaceInfo(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@EmployeeId", id);
                    var data = db.Query<EmployeeAttendaceInfo>(sql: "[dbo].[EmployeeAttendanceDetail]", param: param, commandType: CommandType.StoredProcedure);
                    return data;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<EmployeeLeaveInfo> EmployeeLeaveInfo(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@EmployeeId", id);
                    var data = db.Query<EmployeeLeaveInfo>(sql: "[dbo].[EmployeeLeaveDetail]", param: param, commandType: CommandType.StoredProcedure);
                    return data;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeePayrollInfo> EmployeePayrollInfo(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@EmployeeId", id);
                    var data = db.Query<EmployeePayrollInfo>(sql: "[dbo].[EmployeePayrollDetail]", param: param, commandType: CommandType.StoredProcedure);
                    return data;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetEmpDeviceCode(string code, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<Employee>("SELECT Emp_DeviceCode FROM[dbo].[Employee] where Emp_DeviceCode = @Code and Is_Active=1 and Id!=@Id ", param: new { Code = code, Id = id }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    return s;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetEmail(int id, string email)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<Employee>("Select Email from employee where Is_Active = 1 and Id !=  @Empid and Email = @Empemail", param: new { Empid = id, Empemail = email }, commandType: CommandType.Text);
                    var unique = data.Count();
                    db.Close();
                    return unique;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Employee> GetEmployeeByDepartmentDesignationSection(string departmentId, string designationId, string sectionId, string branchId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Department", departmentId);
                    param.Add("@Designation", designationId);
                    param.Add("@Section", sectionId);
                    param.Add("@BranchId", branchId);
                    var data = db.Query<Employee>(sql: "[dbo].[GetEmployeeByDDS]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeReports GetEmployeeReports()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    int BranchaID = 0;
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData == 0)
                    {
                        //BranchaID = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                        BranchaID = 1;
                    }
                    EmployeeReports EmployeeReports = new EmployeeReports();

                    var param = new DynamicParameters();
                    param.Add("@BranchId", BranchaID);

                    db.Open();
                    EmployeeReports.EmployeeByDesignation = db.Query<EmployeeByDesignation>(sql: "[dbo].[GetEmployeeCountByDesignation]", commandType: CommandType.StoredProcedure).ToList();
                    EmployeeReports.EmployeeByDepartment = db.Query<EmployeeByDepartment>(sql: "[dbo].[GetEmployeeCountByDepartment]",param:param, commandType: CommandType.StoredProcedure).ToList();
                    EmployeeReports.EmployeeByMaritalStatus = db.Query<EmployeeByMaritalStatus>(sql: "[dbo].[GetEmployeeCountByMaritalStatus]", commandType: CommandType.StoredProcedure).ToList();
                    EmployeeReports.EmployeeBySection = db.Query<EmployeeBySection>(sql: "[dbo].[GetEmployeeCountBySection]", commandType: CommandType.StoredProcedure).ToList();
                    EmployeeReports.AttendanceReport = db.Query<AttendanceReport>(sql: "[dbo].[WeeklyAttendancePeroprt]", commandType: CommandType.StoredProcedure).ToList();
                    EmployeeReports.LeaveReport = db.Query<LeaveReport>(sql: "[dbo].[WeeklyLeaveReport]", commandType: CommandType.StoredProcedure).ToList();
                    EmployeeReports.WorkedReport = db.Query<WorkedReport>(sql: "[dbo].[WeeklyWorkedReport]", commandType: CommandType.StoredProcedure).ToList();
                    List<TotalEmployeeByDepartment> a = new List<TotalEmployeeByDepartment>();
                    foreach (var item in EmployeeReports.EmployeeByDepartment)
                    {
                        var data = new TotalEmployeeByDepartment();
                        if (item.Department != null)
                        {
                            data.Department = item.Department;
                            data.Count = item.Male + item.Female + item.Other + item.NotSpecified;
                            a.Add(data);
                        }
                    }
                    EmployeeReports.TotalEmployeeByDepartment = a;
                    return EmployeeReports;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeSalaryReports GetEmployeeSalaryReports()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var EmployeeSalaryReports = new EmployeeSalaryReports();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Type", "SalaryByDepartment");
                    param.Add("@Type1", "");
                    EmployeeSalaryReports.EmployeeBasicSalaryReports = db.Query<EmployeeBasicSalaryReports>(sql: "[dbo].[GetEmployeeSalaryReport]", param: param, commandType: CommandType.StoredProcedure).ToList();

                    param.Add("@Type", "SalaryByDesignation");
                    param.Add("@Type1", "");
                    EmployeeSalaryReports.EmployeeBasicSalaryReportsByDesignatin = db.Query<EmployeeBasicSalaryReportsByDesignatin>(sql: "[dbo].[GetEmployeeSalaryReport]", param: param, commandType: CommandType.StoredProcedure).ToList();

                    param.Add("@Type", "SalarByDesignation");
                    param.Add("@Type1", "Allowance");

                    EmployeeSalaryReports.AllowanceReport = db.Query<AllowanceReport>(sql: "[dbo].[GetEmployeeSalaryReport]", param: param, commandType: CommandType.StoredProcedure).ToList();

                    param.Add("@Type", "");
                    param.Add("@Type1", "DeductionRebate");

                    EmployeeSalaryReports.DeductionRebateReport = db.Query<DeductionRebateReport>(sql: "[dbo].[GetEmployeeSalaryReport]", param: param, commandType: CommandType.StoredProcedure).ToList();

                    //EmployeeSalaryReports.PayrollReport = db.Query<PayrollReport>(sql: "[dbo].[GetEmployeeSalaryReport]", param: param, commandType: CommandType.StoredProcedure).SingleOrDefault();



                    return EmployeeSalaryReports;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int FindEmployeeOnShift(int EmpId)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                var param = new DynamicParameters();
                db.Open();
                param.Add("@Id", EmpId);
                var EmployeeCount = db.Query<int>(sql: "Select Id Count from EmployeeShift where Emp_Id = @Id", param: param, commandType: CommandType.Text).SingleOrDefault();
                db.Close();
                return EmployeeCount;
            }
        }
        public int UploadEmployee(DataTable dt)
        {

            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    sqlTran = db.BeginTransaction();
                    int ReturnVal = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToString(row["EmployeeName"]) != "")
                        {
                            var employee = new Employee();
                            employee.Event = 'I';
                            employee.Is_Active = true;
                           // employee.Created_By = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                            employee.Created_By =1;
                            employee.Created_Date = DateTime.Now;
                            employee.Emp_Code = Convert.ToString(row["Code"]);
                            employee.Emp_Name = Convert.ToString(row["EmployeeName"]);
                            employee.Emp_DeviceCode = Convert.ToString(row["EmployeeDeviceCode"]);
                            employee.Issued_Date = Convert.ToString(row["IssueDate"]);
                            employee.DOB = Convert.ToDateTime(Convert.ToString(row["DOB"])).ToString("yyyy-M-dd");
                            employee.DOBNepali = Convert.ToDateTime(Convert.ToString(row["DOBNepali"])).ToString("yyyy-MM-dd");

                            var MS = Convert.ToString(row["MaritalStatus"]);
                            employee.Marital_Status = MS == "Married" ? "0" : MS == "UnMarried" ? "1" : MS == "Devorced" ? "2" : MS == "Widow" ? "3" : null;

                            var G = Convert.ToString(row["Gender"]);
                            employee.Gender = G == "Male" ? "1" : G == "Female" ? "3" : G == "Others" ? "3" : null;

                            var BG = Convert.ToString(row["BloodGroup"]);
                            employee.Blood_Group = BG == "A+" ? 0 : BG == "A-" ? 1 : BG == "B+" ? 2 : BG == "B-" ? 3 : BG == "O+" ? 4 : BG == "O-" ? 5 : BG == "AB+" ? 6 : BG == "AB-" ? 7 : 0;

                            employee.Mobile_No = Convert.ToString(row["MobileNo"]);
                            employee.Email = Convert.ToString(row["Email"]);
                            employee.PassPort_No = Convert.ToString(row["PassportNumber"]);
                            employee.PassPort_No = Convert.ToString(row["CitizenshipNumber"]);

                            var Rl = Convert.ToString(row["Religion"]);
                            employee.Religion = Rl == "Hinduism" ? 0 : Rl == "Buddhism" ? 1 : Rl == "Islam" ? 2 : Rl == "Judaism" ? 3 : Rl == "Christianity" ? 4 : Rl == "Jainism" ? 5 : Rl == "Sikhism" ? 6 : 0;
                            employee.Permanet_Address = Convert.ToString(row["PermanentAddress"]);
                            employee.Temp_Address = Convert.ToString(row["TemporaryAddress"]);

                            var data = db.Query<Employee>("SELECT Id FROM [dbo].[Employee] where Emp_DeviceCode = @Code and Is_Active=1 and Id! = @Id ", param: new { Code = employee.Emp_DeviceCode, Id = employee.Id }, transaction: sqlTran, commandType: CommandType.Text);
                            if (data.Count() > 0)
                            {
                                sqlTran.Rollback();
                                return 0;
                            }
                            var deviceCode = db.Query<Employee>("SELECT Emp_Code FROM [dbo].[Employee] where Emp_Code = @Emp_Code and Is_Active=1 and Id! = @Id ", param: new { Emp_Code = employee.Emp_Code, Id = employee.Id }, transaction: sqlTran, commandType: CommandType.Text);
                            if (deviceCode.Count() > 0)
                            {
                                sqlTran.Rollback();
                                return 0;
                            }
                            var email = db.Query<Employee>("SELECT Email FROM [dbo].[Employee] where Email = @Email and Is_Active=1 and Id! = @Id ", param: new { Email = employee.Email, Id = employee.Id }, transaction: sqlTran, commandType: CommandType.Text);
                            if (deviceCode.Count() > 0)
                            {
                                sqlTran.Rollback();
                                return 0;
                            }

                            var param = new DynamicParameters();
                            param.Add("@Id", employee.Id);
                            param.Add("@Event", employee.Event);
                            param.Add("@Emp_Code", employee.Emp_Code);
                            param.Add("@Emp_Name", employee.Emp_Name);
                            param.Add("@Emp_DeviceCode", employee.Emp_DeviceCode);
                            param.Add("@Designation_Id", employee.Designation_Id);
                            param.Add("@Department_Id", employee.Department_Id);
                            param.Add("@Section_Id", employee.Section_Id);
                            param.Add("@GradeGroup", employee.GradeGroup);
                            param.Add("@IsManager", employee.IsManager);
                            param.Add("@DOB", employee.DOB);
                            param.Add("@DOBNepali", employee.DOBNepali);
                            param.Add("@Marital_Status", employee.Marital_Status);
                            param.Add("@Gender", employee.Gender);
                            param.Add("@Blood_Group", employee.Blood_Group);
                            param.Add("@Mobile_No", employee.Mobile_No);
                            param.Add("@Email", employee.Email);
                            param.Add("@PassPort_No", employee.PassPort_No);
                            param.Add("@CitizenShip_No", employee.CitizenShip_No);
                            param.Add("@Issued_Date", employee.Issued_Date);
                            param.Add("@Issued_DateNepali", employee.Issued_DateNepali);
                            param.Add("@Issued_District", employee.Issued_District);
                            param.Add("@Religion", employee.Religion);
                            param.Add("@photo", employee.photo);
                            param.Add("@Permanet_Address", employee.Permanet_Address);
                            param.Add("@Permanet_Nepali", employee.Permanet_Nepali);
                            param.Add("@Temp_Address", employee.Temp_Address);
                            param.Add("@Temp_Nepali", employee.Temp_Nepali);
                            param.Add("@Created_Date", employee.Created_Date);
                            param.Add("@Created_By", employee.Created_By);
                            param.Add("@Is_Active", employee.Is_Active);
                            param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                            db.Execute("[dbo].[Usp_IUD_Employee]", param: param, transaction: sqlTran, commandType: CommandType.StoredProcedure);
                            ReturnVal = param.Get<Int16>("@Return_Id");

                        }
                    }
                    sqlTran.Commit();
                    db.Close();
                    return ReturnVal;
                }
            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                //ExceptionHandler.AppendLog(ex);
                return 0;
            }

        }

        public int CreateRemoteAttendance(RemoteAttendance rt)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var data = 0;
                    db.Open();
                    if (rt.Event == "I")
                    {
                        if (CheckRemoteAttendanceInaDate(rt.EmployeeId, rt.DateEng) == 0)
                        {
                            data = db.Execute(sql: @"Insert INTO RemoteAttendance 
                                                (DateEng, DateNep, EmployeeId, Latitude,Longitude, Distance, CreatedBy, CreatedDate) values
                                                 (@DateEng, @DateNep, @EmployeeId, @Latitude, @Longitude, @Distance, @CreatedBy, @CreatedDate)",
                                                         param: new { @DateEng = rt.DateEng, @DateNep = rt.DateNep, @EmployeeId = rt.EmployeeId, @Latitude = rt.Latitude, @Longitude = rt.Longitude, @Distance = rt.Distance, @CreatedBy = rt.CreatedBy, @CreatedDate = rt.CreatedDate }, commandType: CommandType.Text);
                        }
                    }
                    else
                    {
                        data = db.Execute(sql: @"Update RemoteAttendance SET
                                                  DateEng = @DateEng, DateNep = @DateNep, Latitude = @Latitude, Longitude=@Longitude
                                                  ,Distance = @Distance, CreatedBy = @CreatedBy, CreatedDate = @CreatedDate where Id = @Id",
                                                     param: new { @DateEng = rt.DateEng, @DateNep = rt.DateNep, @Latitude = rt.Latitude, @Longitude = rt.Longitude, @Distance = rt.Distance, @CreatedBy = rt.CreatedBy, @CreatedDate = rt.CreatedDate, @Id = rt.Id }, commandType: CommandType.Text);
                    }
                    db.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public IEnumerable<RemoteAttendance> GetList()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<RemoteAttendance>(sql: @"Select ep.Emp_Name, rt.Id, rt.DateEng, rt.DateNep, rt.Latitude, rt.Longitude, rt.Distance
                                                                 from RemoteAttendance rt
                                                                 left join Employee ep on rt.EmployeeId = ep.Id", commandType: CommandType.Text);
                    db.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RemoteAttendance EditRemoteAttendance(int Id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<RemoteAttendance>(sql: @"Select * from RemoteAttendance where Id = @Id", param: new { @Id = Id }, commandType: CommandType.Text).SingleOrDefault();
                    db.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int DeleteRemoteAttendance(int Id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Execute(sql: @"Delete FROM RemoteAttendance where Id = @Id", param: new { @Id = Id }, commandType: CommandType.Text);
                    db.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int CheckRemoteAttendanceInaDate(int EmpId, DateTime date)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<int>(sql: @"Select Count(*) from RemoteAttendance where EmployeeId = @Id AND DateEng = @Date", param: new { Id = EmpId, @Date = date.Date }, commandType: CommandType.Text).SingleOrDefault();
                    db.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
    }
}