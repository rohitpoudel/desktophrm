﻿

using HRM;
using System.Collections.Generic;
using System.Data;

namespace HRM
{
  
    public	interface IEmployeeHRM
	{
		int Create(Employee obj);
		IEnumerable<Employee> GetAllData();
		Employee GetById(int id);
		Employee GetEmployeeByCode(string code);
		Employee GetEmployeeInfoById(int id);
        IEnumerable<Employee> GetEmployeeBySecAndDepAtt(string depId, string sectId);
        IEnumerable<Employee> GetEmployeeBySecDesDepAtt(int depId, int DesId, int sectId);

        IEnumerable<Employee> GetEmployeeBySecAndDep(string depId, string sectId);
        IEnumerable<Section> GetSectionByDepartment(string depId);
		IEnumerable<Employee> GetEmployeeByDepartmentDesignationSection(string departmentId, string designationId, string sectionId, string branchId);

		EmployeeSalaryInfo GetEmployeeSalaryId(int id);
        bool Delete(int id);
		int CreateShift(ShiftWO obj);
		ShiftWO GetShiftByEmpId(int id);
		int GetCode(string code,int id);
		int GetEmpDeviceCode(string code,int id);
        List<Allowance> GetAllowances(int Id);
        List<DeductionRebateTax> GetDeductionRebates(int Id);
        int AddSalary(Employee emp);
        bool CreateUserVerify(UserVerify uv);
        IEnumerable<EmployeeAttendaceInfo> EmployeeAttendaceInfo(int id);
        IEnumerable<EmployeeLeaveInfo> EmployeeLeaveInfo(int id);
        IEnumerable<EmployeePayrollInfo> EmployeePayrollInfo(int id);
        int CreateFromMain(Employee obj, User Usr);
		int GetEmail(int id, string email);

        EmployeeReports GetEmployeeReports();
        int IsPunchExists(int id);
        List<Shift> GetShiftDetails(int id, bool DualShift);
        EmployeeSalaryReports GetEmployeeSalaryReports();
        int FindEmployeeOnShift(int EmpId);
        int UploadEmployee(DataTable dt);
        bool CreateAbsesntLog(AbsentLog uv);
        int CreateRemoteAttendance(RemoteAttendance rt);
        IEnumerable<RemoteAttendance> GetList();
        RemoteAttendance EditRemoteAttendance(int Id);
        int DeleteRemoteAttendance(int Id);
    }
}
