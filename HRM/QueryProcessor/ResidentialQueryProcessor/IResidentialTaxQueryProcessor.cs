﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IResidentialTaxHRM
    {
        int Create(ResidentialTax residentialTax);
        IEnumerable<ResidentialTaxViewModel> GetAllData();
        ResidentialTax GetById(int id);
        bool Delete(int id);
        bool GetDataByFiscalYear(int FiscalYearId);
        bool checkDuplicate(int FiscalYearId, decimal ReangeFrom, decimal RangeTo, string MaritalStatus, string Gender, string Action, int id);
    }
}
