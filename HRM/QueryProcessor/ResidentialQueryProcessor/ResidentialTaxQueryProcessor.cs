﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM 
{
    public class ResidentialTaxHRM : IResidentialTaxHRM
    {
        public int Create(ResidentialTax residentialTax)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", residentialTax.Event);
                    param.Add("@Id", residentialTax.Id);
                    param.Add("@Slab", residentialTax.Slab);
                    param.Add("@RangeFrom", residentialTax.RangeFrom);
                    param.Add("@RangeTo", residentialTax.RangeTo);
                    param.Add("@MaritalStatus", residentialTax.MaritalStatus);
                    param.Add("@Gender", residentialTax.Gender);
                    param.Add("@FiscalYearId", residentialTax.FiscalYearId);
                    param.Add("@TaxType", residentialTax.TaxType);
                    param.Add("@Tax", residentialTax.Tax);
                    param.Add("@CreatedBy", residentialTax.Created_By);
                    param.Add("@IsActive", residentialTax.Is_Active);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_ResidentialTax]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[ResidentialTax] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public IEnumerable<ResidentialTaxViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var NonResidentialTaxlist = db.Query<ResidentialTaxViewModel>(sql: "[dbo].[Usp_GetResidentialTaxList]", commandType: CommandType.StoredProcedure);
                    return NonResidentialTaxlist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResidentialTax GetById(int id)
        {
            try
            {
                ResidentialTax obj = new ResidentialTax();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var NonResidentialTaxlist = db.Query<ResidentialTax>(sql: "[dbo].[Usp_GetResidentialTaxById]", param: param, commandType: CommandType.StoredProcedure);
                    if (NonResidentialTaxlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(NonResidentialTaxlist.Select(x => x.Id).First());
                        obj.Slab = NonResidentialTaxlist.Select(x => x.Slab).First().ToString();
                        obj.RangeFrom = NonResidentialTaxlist.Select(x => x.RangeFrom).First();
                        obj.RangeTo = NonResidentialTaxlist.Select(x => x.RangeTo).First();
                        obj.MaritalStatus = NonResidentialTaxlist.Select(x => x.MaritalStatus).First();
                        obj.Gender = NonResidentialTaxlist.Select(x => x.Gender).First();
                        obj.FiscalYearId = NonResidentialTaxlist.Select(x => x.FiscalYearId).First();
                        obj.TaxType = NonResidentialTaxlist.Select(x => x.TaxType).First();
                        obj.Tax = NonResidentialTaxlist.Select(x => x.Tax).First();
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool GetDataByFiscalYear(int FiscalYearId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FiscalYearId", FiscalYearId);
                    int result = db.Query<int>(sql: "[dbo].[Usp_GetResidentialTaxCountOnFiscalYear]", param:param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    if (result > 0)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool checkDuplicate(int FiscalYearId, decimal ReangeFrom, decimal RangeTo, string MaritalStatus, string Gender, string Action, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FiscalYearId", FiscalYearId);
                    param.Add("@RangeFrom", ReangeFrom);
                    param.Add("@RangeTo", RangeTo);
                    param.Add("@Gender", Gender);
                    param.Add("@MaritalStatus", MaritalStatus);
                    param.Add("@Action", Action);
                    param.Add("@id", id);
                    int result = db.Query<int>(sql: "[dbo].[CheckDuplicateResidentialTax]", param: param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}