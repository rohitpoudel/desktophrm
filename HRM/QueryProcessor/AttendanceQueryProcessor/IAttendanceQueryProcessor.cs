﻿
using System.Collections.Generic;


namespace HRM
{
	interface IAttendanceHRM
	{
		IEnumerable<Attendance> GetAllData(string Date,string EmpId, string DeptId, string DesignationId,string DeviceCode, string NepaliDate, string BranchId);
        IEnumerable<Attendance> GetAllAttendancePolicyData(string FromDate, string ToDate, string EmpId, string DeptId, string DesignationId, string DeviceCode);
        IEnumerable<AttendanceWeekly> GetAllWeeklyData(string Year, string MonthNo, string WeekNumber,string Code,string EmpId,string designationId,string departmentId,string NepYear,string NepMonth);
        IEnumerable<Attendance> GetAllMonthlyData(string Year, string MonthNo, string Code, string EmpId, string designationId, string departmentId, string NepYear, string NepMonth);
        IEnumerable<YearlyAttendance> GetAllYearlyData(string Year, string Code, string EmpId, string designationId, string departmentId, string DateType, string BranchId);
        IEnumerable<AttendanceLog> GetAttendanceLogData(string Date, string EmpId, string DesignationId, string DeptId, string DeviceCode, string NepaliDate, string DateType);

        Employee GetById(string code);

    }
}
