﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class AttendanceHRM : IAttendanceHRM
    {
        GlobalArray gs = null;
        public AttendanceHRM()
        {
            gs = new GlobalArray();
           // gs =(GlobalArray)HttpContext.Current.Session["GlobalArray"];
        }

        public IEnumerable<Attendance> GetAllData(string Date, string EmpId, string DesignationId, string DeptId, string DeviceCode, string NepaliDate, string BranchId)
        {
            try
            {
                if (gs.ViewAllEmpRpt != 1)
                {
                    //EmpId = Convert.ToString(HttpContext.Current.Session["UserId"]);
                }
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Date", Date);
                    param.Add("@EmpId", EmpId);
                    param.Add("@DeptId", DeptId);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DeviceCode", DeviceCode);
                    param.Add("@BranchId", BranchId);
                    var Attendancelist = db.Query<Attendance>(sql: "[dbo].[GetAttendanceList]", param: param, commandType: CommandType.StoredProcedure);
                    //               var Absentlist = db.Query<Attendance>(sql: "[dbo].[GetAbsentEmpList]", param: param, commandType: CommandType.StoredProcedure);
                    //               var list = Attendancelist.Union(Absentlist).OrderBy(x => x.Id).ToList();
                    //               int i = 1;
                    //               foreach (var li in list)
                    //               {
                    //                   //li.DiffernceHour=General.DifferenceInHourMins(li.ShiftHours,li.ActualworkedHour);
                    //                   if(li.OutTime=="-" || li.InTime == null)
                    //                   { li.Remarks = "A"; }                      
                    //                   else
                    //                   {
                    //                       li.Remarks = "P";                        }
                    //                   li.Sno = i;
                    //                   i++;
                    //	if (li.ShiftStart != null && li.ShiftEnd != null && li.AllowLateIn != null && li.AllowEarlyOut != null)
                    //	{
                    //		TimeSpan shiftStart = TimeSpan.Parse(li.ShiftStart);
                    //		TimeSpan inTime = TimeSpan.Parse(li.InTime);
                    //		TimeSpan shiftEnd = TimeSpan.Parse(li.ShiftEnd);
                    //		TimeSpan allowLateIn = TimeSpan.Parse(li.AllowLateIn);
                    //		TimeSpan allowEarlyOut = TimeSpan.Parse(li.AllowEarlyOut);
                    //		if (li.OutTime != "-")
                    //		{
                    //			TimeSpan outTime = TimeSpan.Parse(li.OutTime);
                    //			if ((shiftEnd - allowEarlyOut) > outTime)
                    //			{
                    //				li.EarlyOut = 1;
                    //			}
                    //			else
                    //			{
                    //				li.EarlyOut = 2;
                    //			}

                    //		}
                    //		if ((shiftStart + allowLateIn) < inTime)
                    //		{
                    //			li.LateIn = 1;
                    //		}
                    //		else
                    //		{
                    //			li.LateIn = 2;
                    //		}
                    //	}
                    //}
                    //               foreach (var item in list)
                    //               {
                    //                   var date = Convert.ToString(item.LogInTime);
                    //                   if (date!= "1/1/0001 12:00:00 AM")
                    //                   {
                    //                       item.DateNepali = NepaliDate;
                    //                   }
                    //                   else
                    //                   {
                    //                       item.DateNepali = "-";
                    //                   }


                    //                   list.ToList().Add(item);
                    //               }

                    return Attendancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // throw new NotImplementedException();
        }
        public IEnumerable<AttendanceWeekly> GetAllWeeklyData(string Year, string MonthNo, string WeekNumber, string Code, string EmpId, string DesignationId, string DepartmentId, string NepYear, string NepMonth)
        {
            //var DateType = HttpContext.Current.Session["DatePicker"].ToString();
            var DateType = "Nepali";
            if (string.IsNullOrEmpty(DesignationId))
            { DesignationId = "0"; }
            if (string.IsNullOrEmpty(DepartmentId))
            { DepartmentId = "0"; }
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Year", Convert.ToInt32(Year));
                    param.Add("@MonthNo", Convert.ToInt32(MonthNo));
                    param.Add("@WeekNumber", Convert.ToInt32(WeekNumber));
                    param.Add("@EmpId", EmpId);
                    param.Add("@DeviceCode", Code);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DepartmentId", DepartmentId);
                    var Attendancelist = db.Query<AttendanceWeekly>(sql: "[dbo].[GetWeeklyAttendanceList]", param: param, commandType: CommandType.StoredProcedure);
                    foreach (var item in Attendancelist)
                    {
                        if (item.LogInTime != null)
                        {
                            item.LogInTimeNep = NepDateConverter.EngToNep(item.LogInTime.Year, item.LogInTime.Month, item.LogInTime.Day).ToString();
                            item.LogInTimeNep = item.LogInTimeNep.Replace("/", "-");
                            item.NepWeekNumber = General.GetWeekNumber(item.LogInTimeNep);
                            item.WeekNumber = General.GetWeekNumber(Convert.ToString(item.LogInTime));
                        }
                    }
                    if (DateType == "Nepali")
                    {
                        var list2 = new List<AttendanceWeekly>();
                        var list = Attendancelist.Where(x => Convert.ToDateTime(x.LogInTimeNep).Year == Convert.ToInt32(NepYear));
                        var list1 = list.Where(x => Convert.ToDateTime(x.LogInTimeNep).Month == Convert.ToInt32(NepMonth));

                        if (WeekNumber != "0")
                        {
                            list2 = list.Where(x => x.NepWeekNumber == Convert.ToInt32(WeekNumber)).ToList();
                            Attendancelist = list1.ToList();
                        }
                        else
                        {
                            Attendancelist = list.ToList();
                        }
                    }
                    else
                    {
                        var list2 = new List<AttendanceWeekly>();
                        if (WeekNumber != "0")
                        {
                            list2 = Attendancelist.Where(x => x.WeekNumber == Convert.ToInt32(WeekNumber)).ToList();
                            Attendancelist = list2.ToList();
                        }

                    }
                    return Attendancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // throw new NotImplementedException();
        }
        public IEnumerable<Attendance> GetAllMonthlyData(string Year, string MonthNo, string Code, string EmpId, string DesignationId, string DepartmentId, string NepYear, string NepMonth)
        {
            try
            {
                //var DateType = HttpContext.Current.Session["DatePicker"].ToString();
                var DateType = "Nepali";
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Year", Convert.ToInt32(Year));
                    param.Add("@MonthNo", Convert.ToInt32(MonthNo));
                    param.Add("@EmpId", EmpId);
                    param.Add("@DeviceCode", Code);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DepartmentId", DepartmentId);
                    var Attendancelist = db.Query<Attendance>(sql: "[dbo].[GetMonthlyAttendanceList]", param: param, commandType: CommandType.StoredProcedure);
                    foreach (var item in Attendancelist)
                    {
                        if (item.LogInTime != null)
                        {
                            item.LogInTimeNep = NepDateConverter.EngToNep(item.LogInTime.Year, item.LogInTime.Month, item.LogInTime.Day).ToString();
                            item.LogInTimeNep = item.LogInTimeNep.Replace("/", "-");
                        }
                    }
                    if (DateType == "Nepali")
                    {
                        var list = Attendancelist.Where(x => Convert.ToDateTime(x.LogInTimeNep).Year == Convert.ToInt32(NepYear));
                        var list1 = list.Where(x => Convert.ToDateTime(x.LogInTimeNep).Month == Convert.ToInt32(NepMonth));
                        Attendancelist = list1.ToList();
                    }
                    return Attendancelist.ToList();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // throw new NotImplementedException();
        }
        public IEnumerable<YearlyAttendance> GetAllYearlyData(string Year, string Code, string EmpId, string DesignationId, string DepartmentId, string DateType, string BranchId)
        {
            if (DepartmentId == null)
            {
                DepartmentId = "0";
            }
            if (DesignationId == null)
            {
                DesignationId = "0";
            }
            try
            {
                DateTime StartDate1 = new DateTime(Convert.ToInt32(Year), 1, 1);
                DateTime EndDate1 = new DateTime(Convert.ToInt32(Year), 1, DateTime.DaysInMonth(Convert.ToInt32(Year), 1));
                DateTime StartDate2 = new DateTime(Convert.ToInt32(Year), 2, 1);
                DateTime EndDate2 = new DateTime(Convert.ToInt32(Year), 2, DateTime.DaysInMonth(Convert.ToInt32(Year), 2));
                DateTime StartDate3 = new DateTime(Convert.ToInt32(Year), 3, 1);
                DateTime EndDate3 = new DateTime(Convert.ToInt32(Year), 3, DateTime.DaysInMonth(Convert.ToInt32(Year), 3));
                DateTime StartDate4 = new DateTime(Convert.ToInt32(Year), 4, 1);
                DateTime EndDate4 = new DateTime(Convert.ToInt32(Year), 4, DateTime.DaysInMonth(Convert.ToInt32(Year), 4));
                DateTime StartDate5 = new DateTime(Convert.ToInt32(Year), 5, 1);
                DateTime EndDate5 = new DateTime(Convert.ToInt32(Year), 5, DateTime.DaysInMonth(Convert.ToInt32(Year), 5));
                DateTime StartDate6 = new DateTime(Convert.ToInt32(Year), 6, 1);
                DateTime EndDate6 = new DateTime(Convert.ToInt32(Year), 6, DateTime.DaysInMonth(Convert.ToInt32(Year), 6));
                DateTime StartDate7 = new DateTime(Convert.ToInt32(Year), 7, 1);
                DateTime EndDate7 = new DateTime(Convert.ToInt32(Year), 7, DateTime.DaysInMonth(Convert.ToInt32(Year), 7));
                DateTime StartDate8 = new DateTime(Convert.ToInt32(Year), 8, 1);
                DateTime EndDate8 = new DateTime(Convert.ToInt32(Year), 8, DateTime.DaysInMonth(Convert.ToInt32(Year), 8));
                DateTime StartDate9 = new DateTime(Convert.ToInt32(Year), 9, 1);
                DateTime EndDate9 = new DateTime(Convert.ToInt32(Year), 9, DateTime.DaysInMonth(Convert.ToInt32(Year), 9));
                DateTime StartDate10 = new DateTime(Convert.ToInt32(Year), 10, 1);
                DateTime EndDate10 = new DateTime(Convert.ToInt32(Year), 10, DateTime.DaysInMonth(Convert.ToInt32(Year), 10));
                DateTime StartDate11 = new DateTime(Convert.ToInt32(Year), 11, 1);
                DateTime EndDate11 = new DateTime(Convert.ToInt32(Year), 11, DateTime.DaysInMonth(Convert.ToInt32(Year), 11));
                DateTime StartDate12 = new DateTime(Convert.ToInt32(Year), 12, 1);
                DateTime EndDate12 = new DateTime(Convert.ToInt32(Year), 12, DateTime.DaysInMonth(Convert.ToInt32(Year), 12));

                if (DateType == "Nepali")
                {
                    StartDate1 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 1, 1);
                    int Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 1);
                    EndDate1 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 1, Day);

                    StartDate2 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 2, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 2);
                    EndDate2 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 2, Day);

                    StartDate3 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 3, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 3);
                    EndDate3 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 3, Day);

                    StartDate4 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 4, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 4);
                    EndDate4 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 4, Day);

                    StartDate5 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 5, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 5);
                    EndDate5 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 5, Day);

                    StartDate6 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 6, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 6);
                    EndDate6 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 6, Day);

                    StartDate7 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 7, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 7);
                    EndDate7 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 7, Day);

                    StartDate8 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 8, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 8);
                    EndDate8 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 8, Day);

                    StartDate9 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 9, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 9);
                    EndDate9 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 9, Day);

                    StartDate10 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 10, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 10);
                    EndDate10 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 10, Day);

                    StartDate11 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 11, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 11);
                    EndDate11 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 11, Day);

                    StartDate12 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 12, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 12);
                    EndDate12 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 12, Day);
                }

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    if (gs.ViewAllEmpRpt != 1)
                    {
                        //EmpId = Convert.ToString(HttpContext.Current.Session["UserId"]);
                    }
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Year", Year);
                    param.Add("@EmpId", EmpId);
                    param.Add("@StartDate1", StartDate1);
                    param.Add("@EndDate1", EndDate1);
                    param.Add("@StartDate2", StartDate2);
                    param.Add("@EndDate2", EndDate2);
                    param.Add("@StartDate3", StartDate3);
                    param.Add("@EndDate3", EndDate3);
                    param.Add("@StartDate4", StartDate4);
                    param.Add("@EndDate4", EndDate4);
                    param.Add("@StartDate5", StartDate5);
                    param.Add("@EndDate5", EndDate5);
                    param.Add("@StartDate6", StartDate6);
                    param.Add("@EndDate6", EndDate6);
                    param.Add("@StartDate7", StartDate7);
                    param.Add("@EndDate7", EndDate7);
                    param.Add("@StartDate8", StartDate8);
                    param.Add("@EndDate8", EndDate8);
                    param.Add("@StartDate9", StartDate9);
                    param.Add("@EndDate9", EndDate9);
                    param.Add("@StartDate10", StartDate10);
                    param.Add("@EndDate10", EndDate10);
                    param.Add("@StartDate11", StartDate11);
                    param.Add("@EndDate11", EndDate11);
                    param.Add("@StartDate12", StartDate12);
                    param.Add("@EndDate12", EndDate12);
                    param.Add("@DeviceCode", Code);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DepartmentId", DepartmentId);
                    param.Add("@BranchId", BranchId);
                    var Attendancelist = db.Query<YearlyAttendance>(sql: "[dbo].[GetYearlyAttendance]", param: param, commandType: CommandType.StoredProcedure);
                    //if (EmpId != "0")
                    //{
                    //    Attendancelist = Attendancelist.Where(x => x.Id == Convert.ToInt32(EmpId));
                    //}
                    foreach (var item in Attendancelist)
                    {
                        item.Year = Year;
                    }
                    Attendancelist = Attendancelist.Where(x => x.Emp_Name != null);
                    Attendancelist = Attendancelist.Where(x => x.Year == Year);
                    return Attendancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // throw new NotImplementedException();
        }
        public IEnumerable<AttendanceLog> GetAttendanceLogData(string Date, string EmpId, string DesignationId, string DeptId, string DeviceCode, string NepaliDate, string DateType)
        {

            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    //  param.Add("@Date", (Convert.ToDateTime(Date)).ToString("yyyy-MM-dd"));
                    param.Add("@Date", Date);
                    param.Add("@EmpId", EmpId);
                    param.Add("@DeptId", DeptId);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DeviceCode", DeviceCode);
                    var Attendancelist = db.Query<AttendanceLog>(sql: "[dbo].[GetAttendanceLogList]", param: param, commandType: CommandType.StoredProcedure);
                    foreach (var item in Attendancelist)
                    {
                        item.DateNepali = NepDateConverter.EngToNep(item.AttendanceDate.Year, item.AttendanceDate.Month, item.AttendanceDate.Day).ToString();
                        if (item.DateNepali != null)
                        {
                            item.DateNepali = item.DateNepali.Replace("/", "-");
                        }
                        Attendancelist.ToList().Add(item);
                    }
                    return Attendancelist.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // throw new NotImplementedException();
        }

        public Employee GetById(string code)
        {
            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", code);
                    var Employeelist = db.Query<Employee>(sql: "[dbo].[Usp_GetEmployeeById]", param: param, commandType: CommandType.StoredProcedure);

                    if (Employeelist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(Employeelist.Select(x => x.Id).First());
                        obj.Emp_Code = Employeelist.Select(x => x.Emp_Code).First();
                        obj.Emp_Name = Employeelist.Select(x => x.Emp_Name).First();
                        obj.Emp_DeviceCode = Employeelist.Select(x => x.Emp_DeviceCode).First();
                        obj.Designation_Id = Employeelist.Select(x => x.Designation_Id).First();
                        obj.Department_Id = Employeelist.Select(x => x.Department_Id).First();
                        obj.Section_Id = Employeelist.Select(x => x.Section_Id).First();
                        obj.GradeGroup = Employeelist.Select(x => x.GradeGroup).First();
                        obj.IsManager = Employeelist.Select(x => x.IsManager).First();
                        obj.DOB = Employeelist.Select(x => x.DOB).First();
                        obj.Marital_Status = Employeelist.Select(x => x.Marital_Status).First();
                        obj.Gender = Employeelist.Select(x => x.Gender).First();
                        obj.Blood_Group = Employeelist.Select(x => x.Blood_Group).First();
                        obj.Mobile_No = Employeelist.Select(x => x.Mobile_No).First();
                        obj.Email = Employeelist.Select(x => x.Email).First();
                        obj.PassPort_No = Employeelist.Select(x => x.PassPort_No).First();
                        obj.CitizenShip_No = Employeelist.Select(x => x.CitizenShip_No).First();
                        obj.Issued_Date = Employeelist.Select(x => x.Issued_Date).First();
                        obj.Issued_District = Employeelist.Select(x => x.Issued_District).First();
                        obj.Religion = Employeelist.Select(x => x.Religion).First();
                        obj.photo = Employeelist.Select(x => x.photo).First();
                        obj.Permanet_Address = Employeelist.Select(x => x.Permanet_Address).First();
                        obj.Permanet_Nepali = Employeelist.Select(x => x.Permanet_Nepali).First();
                        obj.Temp_Address = Employeelist.Select(x => x.Temp_Address).First();
                        obj.Temp_Nepali = Employeelist.Select(x => x.Temp_Nepali).First();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Attendance> GetAllAttendancePolicyData(string FromDate, string ToDate, string EmpId, string DeptId, string DesignationId, string DeviceCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FromDate", FromDate);
                    param.Add("@ToDate", ToDate);
                    param.Add("@EmpId", EmpId);
                    param.Add("@DeptId", DeptId);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DeviceCode", DeviceCode);
                    var Attendancelist = db.Query<Attendance>(sql: "[dbo].[GetAttendancePolicyList]", param: param, commandType: CommandType.StoredProcedure);

                    foreach (var li in Attendancelist)
                    {
                        if (li.AttendanceDate != null)
                        {
                            li.DateNepali = NepDateConverter.EngToNep(li.AttendanceDate.Year, li.AttendanceDate.Month, li.AttendanceDate.Day).ToString().Replace("/", "-");
                        }
                        if (li.OutTime == "-" && li.InTime == null)
                        {
                            li.Remark = "A";
                        }
                        else
                        {
                            li.Remark = "P";
                        }
                        if (li.ShiftStart != null && li.ShiftEnd != null)
                        {
                            if (string.IsNullOrEmpty(li.AllowLateIn))
                            {
                                li.AllowLateIn = "00:00";
                            }
                            if (string.IsNullOrEmpty(li.AllowEarlyOut))
                            {
                                li.AllowEarlyOut = "00:00";
                            }
                            TimeSpan ShiftStart = TimeSpan.Parse(li.ShiftStart) + TimeSpan.Parse(li.AllowLateIn);
                            TimeSpan ShiftEnd = TimeSpan.Parse(li.ShiftEnd) - TimeSpan.Parse(li.AllowEarlyOut);
                            TimeSpan intime = TimeSpan.Parse(li.InTime);
                            if (intime >= ShiftStart)
                            {
                                li.LateIn = 1;
                            }
                            else
                            {
                                li.LateIn = 2;
                            }
                            if (li.OutTime != "-")
                            {
                                TimeSpan outtime = TimeSpan.Parse(li.OutTime);
                                if (ShiftEnd >= outtime)
                                {
                                    li.EarlyOut = 1;
                                }
                                else
                                {
                                    li.EarlyOut = 2;
                                }
                            }

                        }


                    }
                    return Attendancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}