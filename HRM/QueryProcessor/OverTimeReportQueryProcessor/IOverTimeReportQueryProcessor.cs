﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM
{
	public interface IOverTimeReportHRM
	{
		IEnumerable<OverTime> GetAllData(string EmpId, string Date);

	}
}
