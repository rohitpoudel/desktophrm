﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
	public class OverTimeReportHRM : IOverTimeReportHRM
	{
		public IEnumerable<OverTime> GetAllData(string EmpId, string Date)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Date", Date);
					param.Add("@EmpId", EmpId);
					var overTimeList = db.Query<OverTime>(sql: "[dbo].[GetOverTimeReport]", param: param, commandType: CommandType.StoredProcedure);
					return overTimeList.ToList();

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}


		}
	}
}