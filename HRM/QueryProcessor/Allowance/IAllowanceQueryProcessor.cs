﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IAllowanceHRM
    {
        int Create(Allowance allowance);
        IEnumerable<AllowanceViewModel> GetAllData();
        Allowance GetById(int id);
        bool Delete(int id);
    }
}
