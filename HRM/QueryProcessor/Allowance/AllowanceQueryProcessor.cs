﻿


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM
{
    public class AllowanceHRM : IAllowanceHRM
    {
        public int Create(Allowance allowance)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", allowance.Event);
                    param.Add("@Id", allowance.Id);
                    param.Add("@Name", allowance.Name);
                    param.Add("@Type", allowance.Type);
                    param.Add("@Allowance", allowance.AllowanceAmount);
                    param.Add("@IsActive", allowance.Is_Active);
                    param.Add("@CreatedBy", allowance.Created_By);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_Allowance]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
               
                return 0;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Allowance] SET [Is_Active]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
               
                return false;
            }

        }


        public IEnumerable<AllowanceViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var allowancelist = db.Query<AllowanceViewModel>(sql: "[dbo].[Usp_GetAllowancelist]", commandType: CommandType.StoredProcedure);
                    return allowancelist.ToList();

                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
               
                return null;
            }
        }

        public Allowance GetById(int id)
        {
            try
            {
                Allowance obj = new Allowance();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var allowancelist = db.Query<Allowance>(sql: "[dbo].[Usp_GetAllowanceById]", param: param, commandType: CommandType.StoredProcedure);
                    if (allowancelist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(allowancelist.Select(x => x.Id).First());
                        obj.Name = allowancelist.Select(x => x.Name).First();
                        obj.Type = allowancelist.Select(x => x.Type).First();
                        obj.AllowanceAmount = allowancelist.Select(x => x.AllowanceAmount).First();

                    }
                    db.Close();
                    return obj;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
               
                return null;
            }

        }
    }
}