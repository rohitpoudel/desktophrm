﻿
using System.Collections.Generic;


namespace HRM
{
	public interface ITimeAdjustmentHRM
	{
		int Create(TimeAdjustment obj);
		IEnumerable<TimeAdjustment> GetAllData();
		TimeAdjustment GetById(int id);
		bool Delete(int id);
	}
}
