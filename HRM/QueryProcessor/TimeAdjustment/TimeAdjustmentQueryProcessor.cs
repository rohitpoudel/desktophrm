﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace HRM
{
	public class TimeAdjustmentHRM: ITimeAdjustmentHRM
	{
		public int Create(TimeAdjustment obj)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Event", obj.Event);
					param.Add("@Id", obj.Id);
					param.Add("@EmployeeId", obj.EmployeeId);
					param.Add("@Date", obj.Date);
					param.Add("@StartTime", obj.StartTime);
					param.Add("@EndTime", obj.EndTime);
					param.Add("@CreatedBy", obj.CreatedBy);
					param.Add("@CreatedDate", obj.CreatedDate);
					param.Add("@IsDeleted", obj.IsDeleted);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_TimeAdjustment]", param: param, commandType: CommandType.StoredProcedure);

					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE[dbo].[TimeAdjustment] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;

				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<TimeAdjustment> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var timeAdjustmentList = db.Query<TimeAdjustment>(sql: "[dbo].[GetTimeAdjustmentList]", commandType: CommandType.StoredProcedure);
					return timeAdjustmentList.ToList();

				}


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public TimeAdjustment GetById(int id)
		{
			try
			{
				var obj = new TimeAdjustment();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var timeAdjustmentList = db.Query<TimeAdjustment>(sql: "[dbo].[GetTimeAdjustmentById]", param: param, commandType: CommandType.StoredProcedure);
					if (timeAdjustmentList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(timeAdjustmentList.Select(x => x.Id).First());
						obj.EmployeeId = Convert.ToInt32(timeAdjustmentList.Select(x => x.EmployeeId).First());
						obj.Date = Convert.ToDateTime(timeAdjustmentList.Select(x => x.Date).First());
						obj.StartTime = Convert.ToDateTime(timeAdjustmentList.Select(x => x.StartTime).First());
						obj.EndTime = Convert.ToDateTime(timeAdjustmentList.Select(x => x.EndTime).First());

					}

					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}