﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
	public class EmpTimePunchHRM : IEmpTimePunchHRM
	{
		public int Create(EmployeeTimePunch obj)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					//db.OpenAsync();
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Id", obj.Id);
					param.Add("@Event", obj.Event);
					param.Add("@Event", obj.Event);
					param.Add("@Emp_Id", obj.Emp_Id);
					param.Add("@Max_WorkingHour", obj.Max_WorkingHour);
					param.Add("@AllowLateIn", obj.AllowLateIn);
					param.Add("@AllowEarlyOut", obj.AllowEarlyOut);
                    param.Add("@MaxEarlyGrace", obj.MaxEarlyGrace);
                    param.Add("@MaxLateGrace", obj.MaxLateGrace);
                    param.Add("@HalfDay_WorkingHour", obj.HalfDay_WorkingHour);
					param.Add("@ShortDay_workingHour", obj.ShortDay_workingHour);
					param.Add("@Present_MarkDuration", obj.Present_MarkDuration);
					param.Add("@IsHalfDay_Marking", obj.MarkAbsentWhenEarlyOut);
                    param.Add("@MarkAbsentWhenEarlyOut", obj.MarkAbsentWhenEarlyOut);
                    param.Add("@MarkAbsentWhenLateIn", obj.MarkAbsentWhenLateIn);
                    param.Add("@IsOT_Allowed", obj.IsOT_Allowed);
					param.Add("@IsTimeLoss_Allow", obj.IsTimeLoss_Allow);
					param.Add("@Is_Active", obj.Is_Active);
					param.Add("@Created_Date", obj.Created_Date);
					param.Add("@Created_By", obj.Created_By);
					param.Add("@PunchType", obj.PunchType);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

					db.Execute("[dbo].[Usp_IUD_EmployeeTimePunch]", param: param, commandType: CommandType.StoredProcedure);
					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE [dbo].[EmployeeTimePunch] SET [Is_Active] = 0 Where Id=@Id", param: new { @Id = id }, commandType: CommandType.Text);
					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;


				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<EmployeeTimePunch> GetAllData()
		{
			throw new NotImplementedException();
		}

		public EmployeeTimePunch GetByEmpId(int id)
		{
			try
			{
				EmployeeTimePunch obj = new EmployeeTimePunch();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Emp_Id", id);
					var timePunchList = db.Query<EmployeeTimePunch>(sql: "[dbo].[GetTimePunchByEmpId]", param: param, commandType: CommandType.StoredProcedure);
					if (timePunchList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(timePunchList.Select(x => x.Id).First());
						obj.Emp_Id = Convert.ToInt32(timePunchList.Select(x => x.Emp_Id).First());
						obj.Max_WorkingHour = Convert.ToString(timePunchList.Select(x => x.Max_WorkingHour).First());
						obj.AllowLateIn = Convert.ToString(timePunchList.Select(x => x.AllowLateIn).First());
						obj.AllowEarlyOut = Convert.ToString(timePunchList.Select(x => x.AllowEarlyOut).First());
						obj.HalfDay_WorkingHour = Convert.ToString(timePunchList.Select(x => x.HalfDay_WorkingHour).First());
						obj.ShortDay_workingHour = Convert.ToString(timePunchList.Select(x => x.ShortDay_workingHour).First());
						obj.Present_MarkDuration = Convert.ToString(timePunchList.Select(x => x.Present_MarkDuration).First());
						obj.PunchType = Convert.ToString(timePunchList.Select(x => x.PunchType).First());
						obj.IsTimeLoss_Allow = Convert.ToBoolean(timePunchList.Select(x => x.IsTimeLoss_Allow).First());
                        obj.MarkAbsentWhenEarlyOut = Convert.ToBoolean(timePunchList.Select(x => x.MarkAbsentWhenEarlyOut).First());
                        obj.MarkAbsentWhenLateIn = Convert.ToBoolean(timePunchList.Select(x => x.MarkAbsentWhenLateIn).First());
                        obj.IsHalfDay_Marking = Convert.ToBoolean(timePunchList.Select(x => x.IsHalfDay_Marking).First());
						obj.IsOT_Allowed = Convert.ToBoolean(timePunchList.Select(x => x.IsOT_Allowed).First());
                        obj.MaxEarlyGrace = Convert.ToString(timePunchList.Select(x => x.MaxEarlyGrace).First());
                        obj.MaxLateGrace = Convert.ToString(timePunchList.Select(x => x.MaxLateGrace).First());

                    }
					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public EmployeeTimePunch GetById(int id)
		{
			throw new NotImplementedException();
		}
	}
}