﻿
using System.Collections.Generic;
using System.Data;

namespace HRM
{
	public	interface IEmpTimePunchHRM
	{
		int Create(EmployeeTimePunch obj);
		IEnumerable<EmployeeTimePunch> GetAllData();
		EmployeeTimePunch GetById(int id);
		bool Delete(int id);
		EmployeeTimePunch GetByEmpId(int id);
		
	}
}
