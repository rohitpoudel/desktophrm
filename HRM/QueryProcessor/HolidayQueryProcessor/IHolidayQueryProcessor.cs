﻿
using System.Collections.Generic;
namespace HRM
{
	public interface IHolidayHRM
	{
		int Create(Holiday holiday);
		IEnumerable<Holiday> GetAllData();
		Holiday GetById(int id);
		bool Delete(int id);
        bool IsHolidayExist(string dateFrom, string dateTo, int gId, int rId, string hId);

    }
}
