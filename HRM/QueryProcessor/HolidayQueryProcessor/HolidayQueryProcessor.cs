﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class HolidayHRM : IHolidayHRM
    {
        public int Create(Holiday holiday)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", holiday.Event);
                    param.Add("@Id", holiday.Id);
                    param.Add("@HolidayName", holiday.HolidayName);
                    param.Add("@HolidayType", holiday.HolidayType);
                    param.Add("@ApplicableGender", holiday.ApplicableGender);
                    param.Add("@ApplicableReligion", holiday.ApplicableReligion);
                    param.Add("@Description", holiday.Description);
                    param.Add("@IsDeleted", holiday.IsDeleted);
                    param.Add("@FromDate", holiday.FromDate.ToString());
                    param.Add("@ToDate", holiday.ToDate.ToString());
                    param.Add("@FromDateNepali", holiday.FromDateNepali);
                    param.Add("@ToDateNepali", holiday.ToDateNepali);
                    param.Add("@CreatedBy", holiday.CreatedBy);
                    param.Add("@CreatedDate",holiday.CreatedDate);
                    param.Add("@Remarks", holiday.Remarks);
                    param.Add("@Departments", holiday.Departments);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Holiday]", param: param, commandType: CommandType.StoredProcedure);

                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool IsHolidayExist(string dateFrom, string dateTo, int gId, int rId , string hId)
        {

            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                db.Open();
                var data = db.Query<Holiday>("select * from ( select Id,HolidayName,IsDeleted,ApplicableReligion,ApplicableGender from Holiday  where @HDateFrom between FromDate and ToDate or (@HDateTo between FromDate and ToDate)) as s where IsDeleted =0 and  (ApplicableGender = @hGId or ApplicableGender= 4) and (ApplicableReligion=@hRId or ApplicableReligion=6) and Id != @hHId", param: new { HDateFrom = dateFrom, HDateTo = dateTo, hGId = gId, hRId = rId , hHId =hId}, commandType: CommandType.Text);
                int s = data.Count();
                if (s > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE[dbo].[Holiday] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Holiday> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var holidayList = db.Query<Holiday>(sql: "[dbo].[GetHolidayList]", commandType: CommandType.StoredProcedure);
                    return holidayList.ToList();

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Holiday GetById(int id)
        {
            try
            {
                Holiday obj = new Holiday();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var holidayList = db.Query<Holiday>(sql: "[dbo].[GetHolidayById]", param: param, commandType: CommandType.StoredProcedure);
                    if (holidayList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(holidayList.Select(x => x.Id).First());
                        obj.HolidayName = Convert.ToString(holidayList.Select(x => x.HolidayName).First());
                        obj.HolidayType = Convert.ToString(holidayList.Select(x => x.HolidayType).First());
                        obj.ApplicableGender = Convert.ToString(holidayList.Select(x => x.ApplicableGender).First());
                        obj.ApplicableReligion = Convert.ToString(holidayList.Select(x => x.ApplicableReligion).First());
                        obj.FromDate = Convert.ToDateTime(holidayList.Select(x => x.FromDate).First());
                        obj.ToDate = Convert.ToDateTime(holidayList.Select(x => x.ToDate).First());
                        obj.FromDateNepali = holidayList.Select(x => x.FromDateNepali).First();
                        obj.ToDateNepali = holidayList.Select(x => x.ToDateNepali).First();
                        obj.Description = Convert.ToString(holidayList.Select(x => x.Description).First());
                        obj.Remarks = Convert.ToString(holidayList.Select(x => x.Remarks).First());
                        obj.Departments = holidayList.Select(x => x.Departments).First();

                    }
                    db.Close();
                    return obj;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}