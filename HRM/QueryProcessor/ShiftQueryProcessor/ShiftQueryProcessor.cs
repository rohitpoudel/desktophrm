﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace HRM
{
    public class ShiftHRM : IShiftHRM
    {
        IDbTransaction sqlTran;
        public int Create(Shift shift)
        {
           
            try
            {
                if (string.IsNullOrEmpty(shift.EarlyGrace))
                {
                    shift.EarlyGrace = "00:00";
                }
                if (string.IsNullOrEmpty(shift.LateGrace))
                {
                    shift.LateGrace = "00:00";
                }
                //if (string.IsNullOrEmpty(shift.OverTimeFrom))
                //{
                //    shift.LateGrace = "00:00";
                //}
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    
                    //db.OpenAsync();
                    db.Open();
                    sqlTran = db.BeginTransaction();
                    var param = new DynamicParameters();
                    param.Add("@Id", shift.Id);
                    param.Add("@Event", shift.Event);
                    param.Add("@Code", shift.Code);
                    param.Add("@Name", shift.Name);
                    param.Add("@ShiftType", shift.ShiftType);
                    param.Add("@HalfDayWorking", shift.HalfDayWorkingHour);
                    param.Add("@NoOfStaff", shift.NoOfStaff);
                    param.Add("@ShiftStart", shift.ShiftStart);
                    param.Add("@ShiftEnd", shift.ShiftEnd);
                    param.Add("@LunchStart", shift.LunchStart);
                    param.Add("@LunchEnd", shift.LunchEnd);
                    param.Add("@EarlyGrace", shift.EarlyGrace);
                    param.Add("@LateGrace", shift.LateGrace);
                    param.Add("@ShiftHours", shift.ShiftHours);
                    param.Add("@ShiftStartGrace", shift.ShiftStartGrace);
                    param.Add("@StartMonth", shift.StartMonth);
                    param.Add("@StartGraceDays", shift.StartGraceDays);
                    param.Add("@ShiftEndGrace", shift.ShiftEndGrace);
                    param.Add("@EndMonth", shift.EndMonth);
                    param.Add("@EndGraceDays", shift.EndGraceDays);
                    param.Add("@LateIn", shift.LateIn);
                    param.Add("@EarlyOut", shift.EarlyOut);
                    param.Add("@MaxEarlyGrace", shift.MaxEarlyGrace);
                    param.Add("@MaxLateGrace", shift.MaxLateGrace);
                    param.Add("@OverTimeFrom", shift.OverTimeFrom);
                    param.Add("@MarkAbsentWhenEarlyOut", shift.MarkAbsentWhenEarlyOut);
                    param.Add("@MarkAbsentWhenLateIn", shift.MarkAbsentWhenLateIn);
                    param.Add("@IsDeleted", shift.IsDeleted);
                    param.Add("@CreateBy", shift.CreateBy);
                    param.Add("@CreateDate", shift.CreateDate);
                    param.Add("@OverTimeHour", shift.OverTimeHour);
                    param.Add("@HalfDayOff", shift.HalfDayOff);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_Shift]", param: param, transaction: sqlTran, commandType: CommandType.StoredProcedure);
                    var shiftID = param.Get<Int16>("@Return_Id");
                    if (shift.Event == 'U')
                    {
                        var data = db.Query<EmployeeTimePunch>("select emp_id from EmployeeShift where ShiftId = @SID", param: new { SID = shiftID }, transaction: sqlTran, commandType: CommandType.Text);
                        int s = data.Count();
                        if (s > 0)
                        {
                            foreach (var item in data)
                            {
                                int result = db.Execute("update EmployeeTimePunch set AllowLateIn = @aLIn , AllowEarlyOut= @aEo, MaxEarlyGrace = @mEG, MaxLateGrace= @mLG , MarkAbsentWhenLateIn = @mAWLI, MarkAbsentWhenEarlyOut = @mAWEO where Emp_Id = @eId", param: new { aLIn = shift.LateGrace, aEo = shift.EarlyGrace, mEG = shift.MaxEarlyGrace, mLG = shift.MaxLateGrace, mAWLI = shift.MarkAbsentWhenLateIn, mAWEO = shift.MarkAbsentWhenEarlyOut, eId = item.Emp_Id }, transaction: sqlTran, commandType: CommandType.Text);

                            }
                        }
                    }
                    sqlTran.Commit();
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                throw ex;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[DualShift] SET [IsActive]=@IsDeleted Where Id=@Id", param: new { @IsDeleted = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Shift> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var shiftList = db.Query<Shift>(sql: "[dbo].[Usp_GetShiftList]", commandType: CommandType.StoredProcedure);
                    return shiftList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Shift GetById(int id)
        {
            try
            {
                var obj = new Shift();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var shiftList = db.Query<Shift>(sql: "[dbo].[Usp_GetShiftById]", param: param, commandType: CommandType.StoredProcedure);
                    if (shiftList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(shiftList.Select(x => x.Id).First());
                        obj.Code = shiftList.Select(x => x.Code).First().ToString();
                        obj.Name = shiftList.Select(x => x.Name).First().ToString();
                        obj.ShiftType = shiftList.Select(x => x.ShiftType).First();
                        obj.ShiftStart = shiftList.Select(x => x.ShiftStart).First();
                        obj.ShiftEnd = shiftList.Select(x => x.ShiftEnd).First();
                        obj.LunchStart = shiftList.Select(x => x.LunchStart).First();
                        obj.LunchEnd = shiftList.Select(x => x.LunchEnd).First();
                        obj.EarlyGrace = shiftList.Select(x => x.EarlyGrace).First();
                        obj.LateGrace = shiftList.Select(x => x.LateGrace).First();
                        obj.ShiftHours = shiftList.Select(x => x.ShiftHours).First();
                        obj.HalfDayWorkingHour = shiftList.Select(x => x.HalfDayWorkingHour).First();
                        obj.NoOfStaff = Convert.ToInt32(shiftList.Select(x => x.NoOfStaff).First());
                        obj.LateIn = shiftList.Select(x => x.LateIn).First();
                        obj.EarlyOut = shiftList.Select(x => x.EarlyOut).First();
                        obj.ShiftStartGrace = shiftList.Select(x => x.ShiftStartGrace).First();
                        obj.ShiftEndGrace = shiftList.Select(x => x.ShiftEndGrace).First();
                        obj.StartMonth = shiftList.Select(x => x.StartMonth).First();
                        obj.EndMonth = shiftList.Select(x => x.EndMonth).First();
                        obj.StartGraceDays = shiftList.Select(x => x.StartGraceDays).First();
                        obj.EndGraceDays = shiftList.Select(x => x.EndGraceDays).First();
                        obj.MaxEarlyGrace = shiftList.Select(x => x.MaxEarlyGrace).First();
                        obj.MaxLateGrace = shiftList.Select(x => x.MaxLateGrace).First();
                        obj.MarkAbsentWhenEarlyOut = Convert.ToBoolean(shiftList.Select(x => x.MarkAbsentWhenEarlyOut).First());
                        obj.MarkAbsentWhenLateIn = Convert.ToBoolean(shiftList.Select(x => x.MarkAbsentWhenLateIn).First());
                        obj.OverTimeFrom = shiftList.Select(x => x.OverTimeFrom).First();
                        obj.OverTimeHour = shiftList.Select(x => x.OverTimeHour).First();
                        obj.HalfDayOff = shiftList.Select(x => x.HalfDayOff).First();

                    }
                    db.Close();
                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetCode(string code, int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<Shift>("SELECT Code FROM [dbo].[Shift] where substring(code, patindex('%[^0]%',Code), 10)= substring(@Code, patindex('%[^0]%',@Code), 10)   and Id!=@ID  and IsDeleted=0", param: new { Code = code, Id = id }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    return s;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Dual Shift
        public IEnumerable<DualShift> GetDualShifts()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var result = db.Query<DualShift>("Select * from DualShift Where IsActive = 1", commandType: CommandType.Text).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.AppendLog(ex);
                return null;
            }
        }
        public int CreateDualShift(DualShift dualShift)
        {
            try
            {
                if (string.IsNullOrEmpty(dualShift.EarlyGrace))
                {
                    dualShift.EarlyGrace = "00:00";
                }
                if (string.IsNullOrEmpty(dualShift.LateGrace))
                {
                    dualShift.LateGrace = "00:00";
                }
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {

                    //db.OpenAsync();
                    db.Open();
                    sqlTran = db.BeginTransaction();
                    var param = new DynamicParameters();
                    param.Add("@Id", dualShift.Id);
                    param.Add("@Code", dualShift.Code);
                    param.Add("@Name", dualShift.Name);
                    param.Add("@ShiftType", dualShift.ShiftType);

                    param.Add("@FirstShiftStart", dualShift.FirstShiftStart);
                    param.Add("@FirstShiftEnd", dualShift.FirstShiftEnd);
                    param.Add("@FirstShiftHours", dualShift.FirstShiftHours);

                    param.Add("@SecondShiftStart", dualShift.SecondShiftStart);
                    param.Add("@SecondShiftEnd", dualShift.SecondShiftEnd);
                    param.Add("@SecondShiftHours", dualShift.SecondShiftHours);

                    param.Add("@LateGrace", dualShift.LateGrace);
                    param.Add("@EarlyGrace", dualShift.EarlyGrace);

                    param.Add("@MaxEarlyGrace", dualShift.MaxEarlyGrace);
                    param.Add("@MaxLateGrace", dualShift.MaxLateGrace);

                    param.Add("@MarkAbsentWhenEarlyOut", dualShift.MarkAbsentWhenEarlyOut);
                    param.Add("@MarkAbsentWhenLateIn", dualShift.MarkAbsentWhenLateIn);
                    param.Add("@IsActive", 1);
                    param.Add("@CreatedBy", dualShift.CreatedBy);
                    param.Add("@CreatedDate", dualShift.CreatedDate);
                    param.Add("@UpdatedBy", dualShift.UpdatedBy);
                    param.Add("@UpdatedDate", dualShift.UpdatedDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[USP_UID_DualShift]", param: param, transaction: sqlTran, commandType: CommandType.StoredProcedure);
                    var shiftID = param.Get<Int16>("@Return_Id");
                    //if (shift.Event == 'U')
                    //{
                    //    var data = db.Query<EmployeeTimePunch>("select emp_id from EmployeeShift where ShiftId = @SID", param: new { SID = shiftID }, transaction: sqlTran, commandType: CommandType.Text);
                    //    int s = data.Count();
                    //    if (s > 0)
                    //    {
                    //        foreach (var item in data)
                    //        {
                    //            int result = db.Execute("update EmployeeTimePunch set AllowLateIn = @aLIn , AllowEarlyOut= @aEo, MaxEarlyGrace = @mEG, MaxLateGrace= @mLG , MarkAbsentWhenLateIn = @mAWLI, MarkAbsentWhenEarlyOut = @mAWEO where Emp_Id = @eId", param: new { aLIn = shift.LateGrace, aEo = shift.EarlyGrace, mEG = shift.MaxEarlyGrace, mLG = shift.MaxLateGrace, mAWLI = shift.MarkAbsentWhenLateIn, mAWEO = shift.MarkAbsentWhenEarlyOut, eId = item.Emp_Id }, transaction: sqlTran, commandType: CommandType.Text);

                    //        }
                    //    }
                    //}
                    sqlTran.Commit();
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                throw ex;
            }
        }
        public DualShift GetDualShiftById(int Id)
        {
            try
            {
                var obj = new DualShift();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var shiftList = db.Query<DualShift>(sql: "Select * from DualShift Where Id = @Id", param: new {Id= Id }, commandType: CommandType.Text).SingleOrDefault();
                    return shiftList;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetDualShiftCode(int Id, string Code)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<Shift>("SELECT Code FROM [dbo].[DualShift] where substring(code, patindex('%[^0]%',Code), 10)= substring(@Code, patindex('%[^0]%',@Code), 10)   and Id != @ID  and IsActive=1", param: new { Code = Code, Id = Id }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    return s;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteDualShift(int Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Shift> GetAllDataForSearch(string searchString)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@searchString", searchString);
                    var shiftList = db.Query<Shift>(sql: "[dbo].[Usp_GetShiftListForSearch]", param: param, commandType: CommandType.StoredProcedure);
                    return shiftList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<DualShift> GetDualShiftsForSearch(string searchString)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@searchString", searchString);
                    var dualShift = db.Query<DualShift>(sql: "[dbo].[Usp_GetDualShiftListForSearch]", param: param, commandType: CommandType.StoredProcedure);
                    return dualShift.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}