﻿
using System.Collections.Generic;

namespace HRM
{
    public interface IShiftHRM
    {
        Shift GetById(int id);
        int Create(Shift shift);
        IEnumerable<Shift> GetAllData();
        bool Delete(int id);
		int GetCode(string code,int id);
        IEnumerable<DualShift> GetDualShifts();
        DualShift GetDualShiftById(int Id);
        int CreateDualShift(DualShift dualShift);
        bool DeleteDualShift(int Id);
        int GetDualShiftCode(int Id, string Code);
        IEnumerable<Shift> GetAllDataForSearch(string searchString);
        IEnumerable<DualShift> GetDualShiftsForSearch(string searchString);


    }
}
