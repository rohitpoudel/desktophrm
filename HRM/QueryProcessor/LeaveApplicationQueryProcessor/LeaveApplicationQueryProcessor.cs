﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
    public class LeaveApplicationHRM : ILeaveApplicationHRM
    {
        GlobalArray gs = null;
        public LeaveApplicationHRM()
        {
            gs = new GlobalArray();
           // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
        }
        public int Create(LeaveApplication obj)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", obj.Event);
                    param.Add("@Id", obj.Id);
                    param.Add("@EmployeeId", obj.EmployeeId);
                    param.Add("@LeaveId", obj.LeaveId);
                    param.Add("@LeaveDay", obj.LeaveDay);
                    param.Add("@TotalLeaveTaken", obj.TotalLeaveTaken);
                    param.Add("@LeaveRemaining", obj.LeaveRemaining);
                    param.Add("@Description", obj.Description);
                    param.Add("@ApprovedBy", obj.ApprovedBy);
                    param.Add("@IsDeleted", obj.IsDeleted);
                    param.Add("@FromDateNepali", obj.FromDateNepali);
                    param.Add("@ToDateNepali", obj.ToDateNepali);
                    param.Add("@FromDate", obj.FromDate);
                    param.Add("@ToDate", obj.ToDate);
                    param.Add("@CreatedBy", obj.CreatedBy);
                    param.Add("@CreatedDate", obj.CreatedDate);
                    param.Add("@Status", obj.Status);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_LeaveApplication]", param: param, commandType: CommandType.StoredProcedure);

                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool IsLeaveUnApproved(int id, int Lid, int laID)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<LeaveApplication>("select EmployeeId from LeaveApplication where EmployeeId = @empId and Id!=@lId and IsDeleted=0 and LeaveId = @leaveId and Status = 'P'", param: new { empId = id, lId = laID, leaveId = Lid }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    if (s > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool IsLeaveTaken(string date, int id, string leaveId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<LeaveApplication>("select EmployeeId from LeaveApplication where @Date between FromDate and ToDate and EmployeeId= @Id and IsDeleted=0 and id!=@lId and Status!= 'R'", param: new { Date = date, Id = id, @lId = leaveId }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    if (s > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE[dbo].[LeaveApplication] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LeaveApplication> GetAllData()
        {
            try
            {
                GlobalArray gs = new GlobalArray();
                //gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();

                    var applicationList = db.Query<LeaveApplication>(sql: "[dbo].[GetLeaveApplicationList]", commandType: CommandType.StoredProcedure);
                    if (gs.ViewAllEmployeeLeave != 1)
                    {
                        //int EmpId = Convert.ToInt32(HttpContext.Current.Session["UserId"])
                            int EmpId = 1;
                        applicationList = applicationList.Where(x => x.EmployeeId == EmpId);
                    }
                    if (gs.ViewAllBranchData != 1)
                    {
                        //var bId = (int)HttpContext.Current.Session["BranchId"];
                        var bId = 1;
                        applicationList = applicationList.Where(x => x.BranchId == bId).ToList();

                    }
                    return applicationList.ToList();

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LeaveApplication> GetAllDataByStatus()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    var applicationList = db.Query<LeaveApplication>(sql: "[dbo].[GetLeaveApplicationListByStatus]", commandType: CommandType.StoredProcedure);
                    if (gs.ViewAllBranchData != 1)
                    {
                        //var bId = (int)HttpContext.Current.Session["BranchId"];
                        var bId = 1;
                        applicationList = applicationList.Where(x => x.BranchId == bId).ToList();

                    }
                    return applicationList.ToList();

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public LeaveApplication GetByEmpAndLeave(string empId, string leaveId)
        {
            try
            {
                LeaveApplication obj = new LeaveApplication();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {

                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@empId", empId);
                    param.Add("@leaveId", leaveId);
                    var applicationList = db.Query<LeaveApplication>(sql: "[dbo].[GetLeaveApplicationByEmpId]", param: param, commandType: CommandType.StoredProcedure);
                    if (applicationList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(applicationList.Select(x => x.Id).First());
                        obj.EmployeeId = Convert.ToInt32(applicationList.Select(x => x.EmployeeId).First());
                        obj.LeaveId = Convert.ToInt32(applicationList.Select(x => x.LeaveId).First());
                        obj.LeaveDay = Convert.ToString(applicationList.Select(x => x.LeaveDay).First());
                        obj.TotalLeaveTaken = Convert.ToDecimal(applicationList.Select(x => x.TotalLeaveTaken).First());
                        var RemainingresultIfReject = db.Query<LeaveApplication>(@"select NoOfDays - isnull(sum(TotalLeaveTaken),0) LeaveRemaining  FROM LeaveAssignDetail AS lad
                                                          inner join LeaveAssign as LA on la.Id = lad.LeaveAssignId
                                                          left join (select * from LeaveApplication where Status = 'A'  and LeaveId=@LeaveId) as LAP on lap.LeaveId = lad.LeaveId and lap.EmployeeId = lad.EmpID
                                                          where lad.LeaveId= @LeaveId and EmpID = @EmployeeId
                                                          group by NoOfDays
                                                          ", param: new { EmployeeId = applicationList.FirstOrDefault().EmployeeId, LeaveId = applicationList.FirstOrDefault().LeaveId }, commandType: CommandType.Text);
                        obj.LeaveRemaining = Convert.ToDecimal(RemainingresultIfReject.Select(x => x.LeaveRemaining).First());
                    }
                    

                    db.Close();
                    return obj;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public LeaveApplication GetById(int id)
        {
            try
            {
                LeaveApplication obj = new LeaveApplication();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var applicationList = db.Query<LeaveApplication>(sql: "[dbo].[GetLeaveApplicationById]", param: param, commandType: CommandType.StoredProcedure);
                    if (applicationList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(applicationList.Select(x => x.Id).First());
                        obj.EmployeeId = Convert.ToInt32(applicationList.Select(x => x.EmployeeId).First());
                        obj.LeaveId = Convert.ToInt32(applicationList.Select(x => x.LeaveId).First());
                        obj.LeaveDay = Convert.ToString(applicationList.Select(x => x.LeaveDay).First());
                        obj.Description = Convert.ToString(applicationList.Select(x => x.Description).First());
                        obj.LeaveRemaining = Convert.ToDecimal(applicationList.Select(x => x.LeaveRemaining).First());
                        obj.ApprovedBy = Convert.ToString(applicationList.Select(x => x.ApprovedBy).First());
                        obj.Status = Convert.ToChar(applicationList.Select(x => x.Status).First());
                        obj.FromDate = Convert.ToDateTime(applicationList.Select(x => x.FromDate).First());
                        obj.ToDate = Convert.ToDateTime(applicationList.Select(x => x.ToDate).First());
                        obj.Emp_Name = Convert.ToString(applicationList.Select(x => x.Emp_Name).First());
                        obj.Emp_Code = Convert.ToString(applicationList.Select(x => x.Emp_Code).First());
                        obj.DepartmentName = Convert.ToString(applicationList.Select(x => x.DepartmentName).First());
                        obj.DesignationName = Convert.ToString(applicationList.Select(x => x.DesignationName).First());
                        obj.SectionName = Convert.ToString(applicationList.Select(x => x.SectionName).First());
                        obj.Gender = Convert.ToString(applicationList.Select(x => x.Gender).First());
                        obj.photo = Convert.ToString(applicationList.Select(x => x.photo).First());
                        obj.FromDateNepali = applicationList.Select(x => x.FromDateNepali).First();
                        obj.ToDateNepali = applicationList.Select(x => x.ToDateNepali).First();
                        obj.Gender = Convert.ToString(applicationList.Select(x => x.Gender).First());
                        obj.TotalLeaveTaken = applicationList.Select(x => x.TotalLeaveTaken).First();


                    }

                    db.Close();
                    return obj;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LeaveApplication GetLeaveByEmpId(int empId, int leaveId)
        {
            try
            {
                LeaveApplication obj = new LeaveApplication();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@empId", empId);
                    param.Add("@leaveId", leaveId);
                    var applicationList = db.Query<LeaveApplication>(sql: "[dbo].[GetLeaveApplicationByEmpId]", param: param, commandType: CommandType.StoredProcedure);
                    if (applicationList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(applicationList.Select(x => x.Id).First());
                        obj.EmployeeId = Convert.ToInt32(applicationList.Select(x => x.EmployeeId).First());
                        obj.LeaveId = Convert.ToInt32(applicationList.Select(x => x.LeaveId).First());
                        obj.LeaveRemaining = Convert.ToInt32(applicationList.Select(x => x.LeaveRemaining).First());
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public decimal IsMoreLeaveAvailable(int empid, int lId, int laId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    // var result = db.Query<LeaveApplication>(@"select TOP 1 * from LeaveApplication where EmployeeId=@EmployeeId and LeaveId= @LeaveId and Id > @LID and IsDeleted = 0  ORDER BY ID DESC", param: new { EmployeeId = empid, LeaveId = lId, LID = laId }, commandType: CommandType.Text);
                    var result = db.Query<LeaveApplication>(@"select NoOfDays - isnull(sum(TotalLeaveTaken),0) LeaveRemaining  FROM LeaveAssignDetail AS lad
inner join LeaveAssign as LA on la.Id = lad.LeaveAssignId
left join (select * from LeaveApplication where Status = 'A' or Status = 'P' and LeaveId=@LeaveId) as LAP on lap.LeaveId = lad.LeaveId and lap.EmployeeId = lad.EmpID
where lad.LeaveId= @LeaveId and EmpID = @EmployeeId
group by NoOfDays
", param: new { EmployeeId = empid, LeaveId = lId, LID = laId }, commandType: CommandType.Text);

                    db.Close();
                    return result.FirstOrDefault().LeaveRemaining;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public bool LeaveApproval(int id, char approval, string approvedBy)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE[dbo].[LeaveApplication] SET[Status]=@Status,[ApprovedBy]=@ApprovedBy WHERE Id=@Id", param: new { Status = approval, Id = id, ApprovedBy = approvedBy }, commandType: CommandType.Text);

                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void IsLastLeave(int lAid, string approvedStatus)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var resultData = db.Query<LeaveApplication>("select * from LeaveApplication where Id= @LID", param: new { LID = lAid }, commandType: CommandType.Text);

                    var result = db.Query<LeaveApplication>("select TOP 1 * from LeaveApplication where EmployeeId=@EmployeeId and LeaveId= @LeaveId and Id > @LID and IsDeleted = 0  ORDER BY ID DESC", param: new { EmployeeId = resultData.FirstOrDefault().EmployeeId, LeaveId = resultData.FirstOrDefault().LeaveId, LID = resultData.FirstOrDefault().Id }, commandType: CommandType.Text);

                    var RemainingresultIfUnAPproved = db.Query<LeaveApplication>(@"select NoOfDays - isnull(sum(TotalLeaveTaken),0) LeaveRemaining  FROM LeaveAssignDetail AS lad
                                                      inner join LeaveAssign as LA on la.Id = lad.LeaveAssignId
                                                      left join (select * from LeaveApplication where Status != 'R'  and LeaveId=@LeaveId) as LAP on lap.LeaveId = lad.LeaveId and lap.EmployeeId = lad.EmpID
                                                      where lad.LeaveId= @LeaveId and EmpID = @EmployeeId
                                                      group by NoOfDays
                                                      ", param: new { EmployeeId = resultData.FirstOrDefault().EmployeeId, LeaveId = resultData.FirstOrDefault().LeaveId }, commandType: CommandType.Text);
                    if (result.Count() > 0)
                    {
                        decimal lr;
                        decimal leaveRemaining;
                        if (approvedStatus == "unApproved")
                        {
                            lr = RemainingresultIfUnAPproved.FirstOrDefault().LeaveRemaining + resultData.FirstOrDefault().TotalLeaveTaken;
                            leaveRemaining = RemainingresultIfUnAPproved.FirstOrDefault().LeaveRemaining;
                            db.Execute("update LeaveApplication set LeaveRemaining= @LR WHERE Id=@Id", param: new { LR = lr, Id = result.FirstOrDefault().Id }, commandType: CommandType.Text);


                        }
                        else if (approvedStatus == "reject")
                        {
                            var RemainingresultIfReject = db.Query<LeaveApplication>(@"select NoOfDays - isnull(sum(TotalLeaveTaken),0) LeaveRemaining  FROM LeaveAssignDetail AS lad
                                                          inner join LeaveAssign as LA on la.Id = lad.LeaveAssignId
                                                          left join (select * from LeaveApplication where Status = 'A'  and LeaveId=@LeaveId) as LAP on lap.LeaveId = lad.LeaveId and lap.EmployeeId = lad.EmpID
                                                          where lad.LeaveId= @LeaveId and EmpID = @EmployeeId
                                                          group by NoOfDays
                                                          ", param: new { EmployeeId = resultData.FirstOrDefault().EmployeeId, LeaveId = resultData.FirstOrDefault().LeaveId }, commandType: CommandType.Text);

                            leaveRemaining = RemainingresultIfReject.FirstOrDefault().LeaveRemaining;
                            db.Execute("update LeaveApplication set LeaveRemaining= @LR WHERE Id=@Id", param: new { LR = leaveRemaining, Id = result.FirstOrDefault().Id }, commandType: CommandType.Text);

                            //leaveRemaining = result.FirstOrDefault().LeaveRemaining + resultData.FirstOrDefault().TotalLeaveTaken;

                        }
                        else if (approvedStatus == "approved")
                        {

                            leaveRemaining = RemainingresultIfUnAPproved.FirstOrDefault().LeaveRemaining;
                            //leaveRemaining = result.FirstOrDefault().LeaveRemaining + resultData.FirstOrDefault().TotalLeaveTaken;
                            db.Execute("update LeaveApplication set LeaveRemaining= @LR WHERE Id=@Id", param: new { LR = leaveRemaining, Id = result.FirstOrDefault().Id }, commandType: CommandType.Text);

                        }
                        else
                        {
                            leaveRemaining = result.FirstOrDefault().LeaveRemaining - resultData.FirstOrDefault().TotalLeaveTaken;
                            // db.Execute("update LeaveApplication set LeaveRemaining= @LR WHERE Id=@Id", param: new { LR = leaveRemaining, Id = result.FirstOrDefault().Id }, commandType: CommandType.Text);
                            db.Execute("update LeaveApplication set LeaveRemaining= @LR WHERE Id=@Id", param: new { LR = leaveRemaining, Id = result.FirstOrDefault().Id }, commandType: CommandType.Text);

                        }

                        db.Execute("update LeaveApplication set LeaveRemaining= @LR WHERE Id=@Id", param: new { LR = leaveRemaining, Id = resultData.FirstOrDefault().Id }, commandType: CommandType.Text);

                    }
                    else
                    {
                        // return true;
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool LeaveRequestStatusIsPending(int empId, int leaveId, string laId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var result = db.Query<LeaveApplication>("select EmployeeId, LeaveId from LeaveApplication where Status = 'P' and EmployeeId = @EmployeeId and LeaveId = @LeaveId and Id!= @LID and IsDeleted = 0", param: new { EmployeeId = empId, LeaveId = leaveId, LID = laId }, commandType: CommandType.Text);
                    db.Close();
                    if (result.Count() > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}