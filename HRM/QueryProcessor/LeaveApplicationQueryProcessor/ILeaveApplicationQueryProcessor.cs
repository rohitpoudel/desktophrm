﻿
using System.Collections.Generic;


namespace HRM
{
	public	interface ILeaveApplicationHRM
	{
		int Create(LeaveApplication obj);
		IEnumerable<LeaveApplication> GetAllData();
		IEnumerable<LeaveApplication> GetAllDataByStatus();
		LeaveApplication GetById(int id);
		bool Delete(int id);
        bool IsLeaveTaken(string date, int id, string leaveId);

        bool LeaveApproval(int id,char approval,string approvedBy);
		bool LeaveRequestStatusIsPending(int empId, int leaveId, string laId);
		LeaveApplication GetByEmpAndLeave(string empId, string leaveId);
        bool IsLeaveUnApproved(int id, int Lid, int laID);
        void IsLastLeave(int lAid, string approvedStatus);
        decimal IsMoreLeaveAvailable(int empid, int lId, int laId);
    }
}
