﻿

using System.Collections.Generic;
namespace HRM
{
    public interface IBranchHRM
    {
        int Create(Branch branch);
        IEnumerable<BranchViewModel> GetAllData();
        Branch GetById(int id);
        bool Delete(int id);
		int GetCode(string code, int id);

	}
}
