﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class BranchHRM : IBranchHRM
    {
        public int Create(Branch branch)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                   

                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", branch.   Id);
                    param.Add("@Event", branch.Event);
                    param.Add("@BranchName", branch.BranchName);
                    param.Add("@Code", branch.Code);
                    param.Add("@Address", branch.Address);
                    param.Add("@IsDeleted", branch.IsDeleted);
                    param.Add("@CreateBy", branch.CreateBy);
                    param.Add("@CreateDate", branch.CreateDate);
                    param.Add("@CompanyId", branch.CompanyId);

                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Branch]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Branch] SET [IsDeleted]=@IsDeleted Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public IEnumerable<BranchViewModel> GetAllData()
        {
            BranchViewModel branchList = new BranchViewModel();
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                     var BranchList = db.Query<BranchViewModel>(sql: "[dbo].[Usp_GetBranchList]", commandType: CommandType.StoredProcedure);
					db.Close();
                    GlobalArray gs = new GlobalArray();
                    
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData != 1)
                    {
                        // var bId = (int)HttpContext.Current.Session["BranchId"];
                        var bId = 0;
                        BranchList = BranchList.Where(x => x.Id == bId).ToList();

                    }
                    return BranchList.ToList();
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Branch GetById(int id)
        {
            try
            {
                Branch obj = new Branch();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var branchlist = db.Query<BranchViewModel>(sql: "[dbo].[Usp_GetBranchById]", param: param, commandType: CommandType.StoredProcedure);
                    if (branchlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(branchlist.Select(x => x.Id).First());
                        obj.BranchName = branchlist.Select(x => x.BranchName).First().ToString();
                        obj.Code = branchlist.Select(x => x.Code).First().ToString();
                        obj.Address = branchlist.Select(x => x.Address).First().ToString();
                    }
                    db.Close();
                    return obj;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		public int GetCode(string code, int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();

					var data = db.Query<Branch>("SELECT Code FROM [dbo].[Branch] where BranchName = @BranchName and Id != @id and IsDeleted = 0", param: new { BranchName = code, @id = id }, commandType: CommandType.Text);
					var s = data.Count();

					db.Close();
					return s;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

	}
}