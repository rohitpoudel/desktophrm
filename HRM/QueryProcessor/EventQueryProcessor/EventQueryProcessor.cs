﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
	public class EventHRM : IEventHRM
	{
		public int Create(EventInformation evnt)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Event", evnt.Event);
					param.Add("@Id", evnt.Id);
					param.Add("@EventName", evnt.EventName);
					param.Add("@EventLevel", evnt.EventLevel);
                    param.Add("@FromDateNepali", evnt.FromDateNepali);
                    param.Add("@ToDateNepali", evnt.ToDateNepali);
                    param.Add("@FromDate", evnt.FromDate);
					param.Add("@ToDate", evnt.ToDate);
					param.Add("@Description", evnt.Description);
					param.Add("@IsDeleted", evnt.IsDeleted);
					param.Add("@Remarks", evnt.Remarks);
					param.Add("@DepartmentId", evnt.DepIdCollection);
					param.Add("@SectionId", evnt.SectIdCollection);
					param.Add("@BranchId", evnt.BranIdCollection);
					param.Add("@CreatedBy", evnt.CreatedBy);
					param.Add("@CreatedDate", evnt.CreatedDate);
					param.Add("@XMLDepId", evnt.XMLDepId);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_Events]", param: param, commandType: CommandType.StoredProcedure);
					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE [dbo].[Events] SET[IsDeleted]=@IsDeleted WHERE Id=@Id UPDATE[dbo].[EventDetail] SET[IsDeleted] = @IsDeleted WHERE EventId = @Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);

					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;

				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<EventInformation> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var eventList = db.Query<EventInformation>(sql: "[dbo].[GetEventList]", commandType: CommandType.StoredProcedure);
					return eventList.ToList();

				}


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public EventInformation GetById(int id)
		{
			try
			{
				EventInformation obj = new EventInformation();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var eventList = db.Query<EventInformation>(sql: "[dbo].[GetEventById]", param: param, commandType: CommandType.StoredProcedure);
					if (eventList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(eventList.Select(x => x.Id).First());
						obj.EventName = Convert.ToString(eventList.Select(x => x.EventName).First());
						obj.EventLevel = Convert.ToString(eventList.Select(x => x.EventLevel).First());
						obj.Description = Convert.ToString(eventList.Select(x => x.Description).First());
						obj.FromDate = Convert.ToDateTime(eventList.Select(x => x.FromDate).First());
						obj.ToDate = Convert.ToDateTime(eventList.Select(x => x.ToDate).First());
                        obj.FromDateNepali = Convert.ToString(eventList.Select(x => x.FromDateNepali).First());
                        obj.ToDateNepali = Convert.ToString(eventList.Select(x => x.ToDateNepali).First());
                        obj.Remarks = Convert.ToString(eventList.Select(x => x.Remarks).First());
						obj.DepartmentId = eventList.Select(x => x.DepId).ToArray();
						obj.SectionId = eventList.Select(x => x.SectId).ToArray();
						obj.BranchId = eventList.Select(x => x.BranId).ToArray();
						

					}
					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}