﻿
using System.Collections.Generic;


namespace HRM
{
	public interface IEventHRM
	{
		int Create(EventInformation evnt);
		IEnumerable<EventInformation> GetAllData();
		EventInformation GetById(int id);
		bool Delete(int id);
	}
}
