﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM
{
    public interface IReportHRM
    {
        IEnumerable<MonthlyReport> GetMonthlyReport(int Year, int Month, int EmpId, string DeptId, string branchId);
        IEnumerable<AttendanceDetails> GetAttendanceDetails(string empId, string deptId, DateTime stDate, DateTime endDate,string getReportFor, int BranchId);
        List<LeaveDetails> LeaveDetails();
        IEnumerable<Holiday> GetHolidays(int Year, int Month);
        Company CompanyNameById(int Id);
        List<LeaveDetails> LeaveCodeList();
        IEnumerable<HolidayDetails> GetHolidayDetails(string empId, string deptId, DateTime stDate, DateTime endDate);
        IEnumerable<FilteredLeave> GetLeaveDetails(string empId, string deptId, DateTime stDate, DateTime endDate);
        Color GetColor();
        IEnumerable<EmployeeSummary> GetEmpSummaryDetails(string empId, string deptId, DateTime stDate, DateTime endDate, int BranchId);
        IEnumerable<EmployeeLeaveReport> GetLeaveReport(int fiscalyearId, int EmpId, int DepId, char LeaveStatus);
        IEnumerable<EmployeeLeaveReport> GetDetailLeaveReport(int EmpId, int LeaveAssignId);
        IEnumerable<ResignationReport> GetResignationReport(DateTime? fromDate, DateTime? toDate, int deptId, int desgId, int secId);
        IEnumerable<EmployeeDualShift> GetEmployeeDetailsOfDualShift(string eId, string dID, string bId);
        IEnumerable<EmployeeDualShift> GetEmployeeDetailOfDualShift(string fromDate, string toDate, string eId);
        bool HasDualShift(int EId);
        IEnumerable<ManualAttendanceReport> ManualAttendanceReport(DateTime Date, int DeptId, int EmployeeId, int BranchId);
        IEnumerable<Kaj> GetKajlist(int Year, int Month);

    }
}
