﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using System.Data.Common;


namespace HRM
{
    public class ReportHRM : IReportHRM
    {
        GlobalArray gs = null;
        public ReportHRM()
        {
            gs = new GlobalArray();
           // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
        }
        public List<LeaveDetails> LeaveDetails()
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                db.Open();
                var data = db.Query<LeaveDetails>("select Code, FromDateNepali, ToDateNepali,FromDate as LeaveFromDate,ToDate as LeaveToDate,LeaveName,EmployeeId, LeaveDay from LeaveApplication as LA inner join LeaveMaster as LM on LA.LeaveId = LM.Id where LA.Status='A'", commandType: CommandType.Text);
                return data.ToList();
            }
        }
        public IEnumerable<FilteredLeave> GetLeaveDetails(string empId, string deptId, DateTime stDate, DateTime endDate)
        {
            if (empId == "0")
            {
                empId = "";
            }
            if (deptId == "0")
            {
                deptId = "";
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    var leaveReport = db.Query<FilteredLeave>(sql: "[dbo].[GetOverallLeave]", param: param, commandType: CommandType.StoredProcedure);
                    if (empId != "")
                    {
                        leaveReport = leaveReport.Where(x => x.EmpId == Convert.ToInt32(empId));
                    }
                    if (deptId != "")
                    {
                        leaveReport = leaveReport.Where(x => x.DeptId == Convert.ToInt32(deptId));
                    }
                    return leaveReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public IEnumerable<HolidayDetails> GetHolidayDetails(string empId, string deptId, DateTime stDate, DateTime endDate)
        {
            if (empId == "0")
            {
                empId = "";
            }
            if (deptId == "0")
            {
                deptId = "";
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    var WeekLyHolidayReport = db.Query<HolidayDetails>(sql: "[dbo].[GetOverallHolidayDetails]", param: param, commandType: CommandType.StoredProcedure);
                    if (empId != "")
                    {
                        WeekLyHolidayReport = WeekLyHolidayReport.Where(x => x.EmpId == Convert.ToInt32(empId));
                    }
                    if (deptId != "")
                    {
                        WeekLyHolidayReport = WeekLyHolidayReport.Where(x => x.DeptId == Convert.ToInt32(deptId));
                    }
                    return WeekLyHolidayReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public IEnumerable<EmployeeSummary> GetEmpSummaryDetails(string empId, string deptId, DateTime stDate, DateTime endDate, int BranchId)
        {
            if (gs.ViewAllEmpRpt != 1)
            {
               // empId = Convert.ToString(HttpContext.Current.Session["UserId"]);
                empId = "1";
            }
            if (empId == "0")
            {
                empId = "";
            }
            if (deptId == "0")
            {
                deptId = "";
            }
            if (gs.ViewAllBranchData != 1)
            {
               // BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                BranchId = 1;
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@StartDate", stDate);
                    param.Add("@EndDate", endDate);
                    param.Add("@empId", empId);
                    param.Add("@deptId", deptId);
                    param.Add("@branchId", BranchId);
                    var empSummary = db.Query<EmployeeSummary>(sql: "[dbo].[GetEmpSummaryReport]", param: param, commandType: CommandType.StoredProcedure);

                    return empSummary.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public IEnumerable<AttendanceDetails> GetAttendanceDetails(string empId, string deptId, DateTime stDate, DateTime endDate, string getReportFor, int BranchId)
        {

            if (gs.ViewAllEmpRpt != 1)
            {
                //empId = Convert.ToString(HttpContext.Current.Session["UserId"]);
                empId ="1";
            }
            if (string.IsNullOrEmpty(empId))
            {
                empId = "0";
            }
            if (gs.ViewAllBranchData != 1)
            {
                //BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                BranchId = 1;
            }

            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    param.Add("@empId", empId);
                    param.Add("@deptId", deptId);
                    param.Add("@branchId", BranchId);


                    var WeekLyAttendanceReport = db.Query<AttendanceDetails>(sql: "[dbo].[GetOverallAttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);
                    //WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.LateIn == 1);
                    if (getReportFor == "0")
                    {
                        WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.LateIn == 1);
                    }
                    if (getReportFor == "1")
                    {
                        WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EarlyOut == 1);
                    }
                    if (getReportFor == "2")
                    {
                        WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EarlyOutAbsent == 1 || x.LateInAbsent == 1 || x.Status == "Absent");
                    }
                    //if (empId!="")
                    //{
                    //    WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EmpId ==Convert.ToInt32(empId));
                    //}
                    //if (deptId != "")
                    //{
                    //    WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.DeptId == Convert.ToInt32(deptId));
                    //}
                    return WeekLyAttendanceReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public IEnumerable<MonthlyReport> GetMonthlyReport(int Year, int Month, int EmpId, string DeptId, string branchId)
        {
            if (gs.ViewAllEmpRpt != 1)
            {
               // EmpId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                EmpId = 1;
            }
            if (gs.ViewAllBranchData != 1)
            {
                //branchId = HttpContext.Current.Session["BranchId"].ToString();
                branchId = "1";
            }
            //var DateType = HttpContext.Current.Session["DatePicker"].ToString();
         var   DateType = "English";
            DateTime StartDate = DateTime.Now;
            DateTime EndDate = DateTime.Now;

            if (DateType == null || DateType == "English")
            {
                StartDate = new DateTime(Convert.ToInt32(Year), Month, 1);
                EndDate = new DateTime(Convert.ToInt32(Year), Month, DateTime.DaysInMonth(Convert.ToInt32(Year), Month));
            }
            else
            {
                StartDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, 1);
                int Day = NepDateConverter.DaysByYearMonth(Year, Month);
                EndDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, Day);
            }
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@StartDate", StartDate);
                    param.Add("@EndDate", EndDate);
                    List<MonthlyReport> MonthlyReport = new List<MonthlyReport>();

                    //db.Execute("[dbo].[DropProcedure]", commandType: CommandType.StoredProcedure);

                    //db.Execute("[dbo].[CreateProcedure]", commandType: CommandType.StoredProcedure);
                    if (Month == 1)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 2)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport1]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 3)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport2]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 4)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport3]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 5)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport4]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 6)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport5]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 7)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport6]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 8)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport7]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 9)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport8]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 10)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport9]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 11)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport10]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 12)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport11]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }

                    int DepId = DeptId == "" ? 0 : Convert.ToInt32(DeptId);
                    if (MonthlyReport.Count() > 0)
                    {
                        if (EmpId > 0)
                        {
                            MonthlyReport = MonthlyReport.Where(x => x.EmpId == EmpId).ToList();
                        }
                        if (DepId > 0)
                        {
                            MonthlyReport = MonthlyReport.Where(x => x.Department_Id == DepId).ToList();
                        }
                        if (!string.IsNullOrEmpty(branchId))
                        {
                            if (Convert.ToInt32(branchId) > 0)
                            {
                                int BrancId = Convert.ToInt32(branchId);
                                MonthlyReport = MonthlyReport.Where(x => x.BranchId == BrancId).ToList();
                            }
                        }
                    }

                    DateTime dateTime1 = DateTime.Now;
                    if (EndDate > dateTime1)
                    {
                        dateTime1 = DateTime.Now;
                    }
                    else
                    {
                        dateTime1 = EndDate;
                    }
                    var param1 = new DynamicParameters();
                    param1.Add("@StartDate", StartDate);
                    param1.Add("@EndDate", dateTime1);

                    var absCount = db.Query<MonthlyReport>(sql: "[dbo].[GetAbsentListCountForMonthly]", param: param1, commandType: CommandType.StoredProcedure);
                    var leaveCount = db.Query<MonthlyReport>(sql: "[dbo].[GetLeaveListCountForMonthly]", param: param, commandType: CommandType.StoredProcedure);
                    var earlyOutCount = db.Query<MonthlyReport>(sql: "[dbo].[GetEarlyOutCountForMonthly]", param: param, commandType: CommandType.StoredProcedure);
                    var LateInCount = db.Query<MonthlyReport>(sql: "[dbo].[GetLateInCountForMonthly]", param: param, commandType: CommandType.StoredProcedure);

                    var presentCount = db.Query<MonthlyReport>(sql: "[dbo].[MonthlyPresentReport]", param: param, commandType: CommandType.StoredProcedure);

                    List<MonthlyReport> mc = (from mR in MonthlyReport
                                              join pC in presentCount
                                              on mR.EmpId equals pC.EmpId into ps
                                              from p in ps.DefaultIfEmpty()
                                              join abs in absCount
                                              on mR.EmpId equals abs.EmpId into aps
                                              from a in aps.DefaultIfEmpty()
                                              join leave in leaveCount
                                              on mR.EmpId equals leave.EmpId into lv
                                              from l in lv.DefaultIfEmpty()
                                              join eO in earlyOutCount
                                              on mR.EmpId equals eO.EmpId into earlyOut
                                              from e in earlyOut.DefaultIfEmpty()
                                              join li in LateInCount
                                              on mR.EmpId equals li.EmpId into Lin
                                              from late in Lin.DefaultIfEmpty()
                                              select new MonthlyReport
                                              {

                                                  ShiftStart = mR.ShiftStart,
                                                  ShiftEnd = mR.ShiftEnd,
                                                  Emp_Name = mR.Emp_Name,
                                                  BranchName = mR.BranchName,
                                                  BranchId = mR.BranchId,
                                                  EmpId = mR.EmpId,
                                                  Religion = mR.Religion,
                                                  Department_Id = mR.Department_Id,
                                                  Gender = mR.Gender,
                                                  WeeklyOff = mR.WeeklyOff,
                                                  LogInTime_1 = mR.LogInTime_1,
                                                  LogOutTime_1 = mR.LogOutTime_1,
                                                  LogInTime_2 = mR.LogInTime_2,
                                                  LogOutTime_2 = mR.LogOutTime_2,
                                                  LogInTime_3 = mR.LogInTime_3,
                                                  LogOutTime_3 = mR.LogOutTime_3,
                                                  LogInTime_4 = mR.LogInTime_4,
                                                  LogOutTime_4 = mR.LogOutTime_4,
                                                  LogInTime_5 = mR.LogInTime_5,
                                                  LogOutTime_5 = mR.LogOutTime_5,
                                                  LogInTime_6 = mR.LogInTime_6,
                                                  LogOutTime_6 = mR.LogOutTime_6,
                                                  LogInTime_7 = mR.LogInTime_7,
                                                  LogOutTime_7 = mR.LogOutTime_7,
                                                  LogInTime_8 = mR.LogInTime_8,
                                                  LogOutTime_8 = mR.LogOutTime_8,
                                                  LogInTime_9 = mR.LogInTime_9,
                                                  LogOutTime_9 = mR.LogOutTime_9,
                                                  LogInTime_10 = mR.LogInTime_10,
                                                  LogOutTime_10 = mR.LogOutTime_10,
                                                  LogInTime_11 = mR.LogInTime_11,
                                                  LogOutTime_11 = mR.LogOutTime_11,
                                                  LogInTime_12 = mR.LogInTime_12,
                                                  LogOutTime_12 = mR.LogOutTime_12,
                                                  LogInTime_13 = mR.LogInTime_13,
                                                  LogOutTime_13 = mR.LogOutTime_13,
                                                  LogInTime_14 = mR.LogInTime_14,
                                                  LogOutTime_14 = mR.LogOutTime_14,
                                                  LogInTime_15 = mR.LogInTime_15,
                                                  LogOutTime_15 = mR.LogOutTime_15,
                                                  LogInTime_16 = mR.LogInTime_16,
                                                  LogOutTime_16 = mR.LogOutTime_16,
                                                  LogInTime_17 = mR.LogInTime_17,
                                                  LogOutTime_17 = mR.LogOutTime_17,
                                                  LogInTime_18 = mR.LogInTime_18,
                                                  LogOutTime_18 = mR.LogOutTime_18,
                                                  LogInTime_19 = mR.LogInTime_19,
                                                  LogOutTime_19 = mR.LogOutTime_19,
                                                  LogInTime_20 = mR.LogInTime_20,
                                                  LogOutTime_20 = mR.LogOutTime_20,
                                                  LogInTime_21 = mR.LogInTime_21,
                                                  LogOutTime_21 = mR.LogOutTime_21,
                                                  LogInTime_22 = mR.LogInTime_22,
                                                  LogOutTime_22 = mR.LogOutTime_22,
                                                  LogInTime_23 = mR.LogInTime_23,
                                                  LogOutTime_23 = mR.LogOutTime_23,
                                                  LogInTime_24 = mR.LogInTime_24,
                                                  LogOutTime_24 = mR.LogOutTime_24,
                                                  LogInTime_25 = mR.LogInTime_25,
                                                  LogOutTime_25 = mR.LogOutTime_25,
                                                  LogInTime_26 = mR.LogInTime_26,
                                                  LogOutTime_26 = mR.LogOutTime_26,
                                                  LogInTime_27 = mR.LogInTime_27,
                                                  LogOutTime_27 = mR.LogOutTime_27,
                                                  LogInTime_28 = mR.LogInTime_28,
                                                  LogOutTime_28 = mR.LogOutTime_28,
                                                  LogInTime_29 = mR.LogInTime_29,
                                                  LogOutTime_29 = mR.LogOutTime_29,
                                                  LogInTime_30 = mR.LogInTime_30,
                                                  LogOutTime_30 = mR.LogOutTime_30,
                                                  LogInTime_31 = mR.LogInTime_31,
                                                  LogOutTime_31 = mR.LogOutTime_31,
                                                  LogInTime_32 = mR.LogInTime_32,
                                                  LogOutTime_32 = mR.LogOutTime_32,

                                                  LateIn1 = mR.LateIn1,
                                                  EarlyOut1 = mR.EarlyOut1,
                                                  LateIn2 = mR.LateIn2,
                                                  EarlyOut2 = mR.EarlyOut2,
                                                  LateIn3 = mR.LateIn3,
                                                  EarlyOut3 = mR.EarlyOut3,
                                                  LateIn4 = mR.LateIn4,
                                                  EarlyOut4 = mR.EarlyOut4,
                                                  LateIn5 = mR.LateIn5,
                                                  EarlyOut5 = mR.EarlyOut5,
                                                  LateIn6 = mR.LateIn6,
                                                  EarlyOut6 = mR.EarlyOut6,
                                                  LateIn7 = mR.LateIn7,
                                                  EarlyOut7 = mR.EarlyOut7,
                                                  LateIn8 = mR.LateIn8,
                                                  EarlyOut8 = mR.EarlyOut8,
                                                  LateIn9 = mR.LateIn9,
                                                  EarlyOut9 = mR.EarlyOut9,
                                                  LateIn10 = mR.LateIn10,
                                                  EarlyOut10 = mR.EarlyOut10,
                                                  LateIn11 = mR.LateIn11,
                                                  EarlyOut11 = mR.EarlyOut11,
                                                  LateIn12 = mR.LateIn12,
                                                  EarlyOut12 = mR.EarlyOut12,
                                                  LateIn13 = mR.LateIn13,
                                                  EarlyOut13 = mR.EarlyOut13,
                                                  LateIn14 = mR.LateIn14,
                                                  EarlyOut14 = mR.EarlyOut14,
                                                  LateIn15 = mR.LateIn15,
                                                  EarlyOut15 = mR.EarlyOut15,
                                                  LateIn16 = mR.LateIn16,
                                                  EarlyOut16 = mR.EarlyOut16,
                                                  LateIn17 = mR.LateIn17,
                                                  EarlyOut17 = mR.EarlyOut17,
                                                  LateIn18 = mR.LateIn18,
                                                  EarlyOut18 = mR.EarlyOut18,
                                                  LateIn19 = mR.LateIn19,
                                                  EarlyOut19 = mR.EarlyOut19,
                                                  LateIn20 = mR.LateIn20,
                                                  EarlyOut20 = mR.EarlyOut20,
                                                  LateIn21 = mR.LateIn21,
                                                  EarlyOut21 = mR.EarlyOut21,
                                                  LateIn22 = mR.LateIn22,
                                                  EarlyOut22 = mR.EarlyOut22,
                                                  LateIn23 = mR.LateIn23,
                                                  EarlyOut23 = mR.EarlyOut23,
                                                  LateIn24 = mR.LateIn24,
                                                  EarlyOut24 = mR.EarlyOut24,
                                                  LateIn25 = mR.LateIn25,
                                                  EarlyOut25 = mR.EarlyOut25,
                                                  LateIn26 = mR.LateIn26,
                                                  EarlyOut26 = mR.EarlyOut26,
                                                  LateIn27 = mR.LateIn27,
                                                  EarlyOut27 = mR.EarlyOut27,
                                                  LateIn28 = mR.LateIn28,
                                                  EarlyOut28 = mR.EarlyOut28,
                                                  LateIn29 = mR.LateIn29,
                                                  EarlyOut29 = mR.EarlyOut29,
                                                  LateIn30 = mR.LateIn30,
                                                  EarlyOut30 = mR.EarlyOut30,
                                                  LateIn31 = mR.LateIn31,
                                                  EarlyOut31 = mR.EarlyOut31,
                                                  LateIn32 = mR.LateIn32,
                                                  EarlyOut32 = mR.EarlyOut32,
                                                  LateInCount = late == null ? 0 : late.LateInCount,
                                                  EarlyOutCount = e == null ? 0 : e.EarlyOutCount,
                                                  LeaveCount = l == null ? 0 : l.LeaveCount,
                                                  AbsentCount = a == null ? 0 : a.AbsentCount,
                                                  PresentCount = p == null ? 0 : p.PresentCount,
                                                  LEA = mR.LEA

                                              }).ToList();

                    db.Close();
                    return mc;
                }
            }
            catch (Exception ex)
            {

                //ExceptionHandler.AppendLog(ex);
                return null;
            }


        }

        public IEnumerable<Holiday> GetHolidays(int Year, int Month)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {

                    ///var DateType = HttpContext.Current.Session["DatePicker"].ToString();
                    var DateType = "";
                    DateTime StartDate = DateTime.Now;
                    DateTime EndDate = DateTime.Now;

                    if (DateType == null || DateType == "English")
                    {
                        StartDate = new DateTime(Convert.ToInt32(Year), Month, 1);
                        EndDate = new DateTime(Convert.ToInt32(Year), Month, DateTime.DaysInMonth(Convert.ToInt32(Year), Month));
                    }
                    else
                    {
                        StartDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, 1);
                        int Day = NepDateConverter.DaysByYearMonth(Year, Month);
                        EndDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, Day);
                    }

                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FromDate", StartDate);
                    param.Add("@EndDate", EndDate);

                    return db.Query<Holiday>(sql: "[dbo].[GetHolidayByDate]", param: param, commandType: CommandType.StoredProcedure).ToList();

                }

            }
            catch (Exception es)
            {
                //ExceptionHandler.AppendLog(es);
                return null;
            }
        }
        public IEnumerable<Kaj> GetKajlist(int Year, int Month)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {

                    //var DateType = HttpContext.Current.Session["DatePicker"].ToString();
                    var DateType = "";
                    DateTime StartDate = DateTime.Now;
                    DateTime EndDate = DateTime.Now;

                    if (DateType == null || DateType == "English")
                    {
                        StartDate = new DateTime(Convert.ToInt32(Year), Month, 1);
                        EndDate = new DateTime(Convert.ToInt32(Year), Month, DateTime.DaysInMonth(Convert.ToInt32(Year), Month));
                    }
                    else
                    {
                        StartDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, 1);
                        int Day = NepDateConverter.DaysByYearMonth(Year, Month);
                        EndDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, Day);
                    }

                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FromDate", StartDate);
                    param.Add("@EndDate", EndDate);

                    return db.Query<Kaj>(sql: "[dbo].[GetEmpKajList]", param: param, commandType: CommandType.StoredProcedure).ToList();

                }

            }
            catch (Exception es)
            {
                //ExceptionHandler.AppendLog(es);
                return null;
            }
        }

        public Company CompanyNameById(int Id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<Company>("Select Id, CompanyName, CompanyAddress,Logo,PhoneNo from Companies where CompanyCode = " + Id, commandType: CommandType.Text).SingleOrDefault();
                    return data;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeaveDetails> LeaveCodeList()
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                var str = "";
                db.Open();
                var data = db.Query<LeaveDetails>("SELECT CODE,LeaveName FROM LeaveMaster WHERE IsDeleted= 0", commandType: CommandType.Text);

                return data.ToList();
            }
        }

        public Color GetColor()
        {
            var Color = new Color();
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                var param = new DynamicParameters();
                db.Open();

                Color = db.Query<Color>("SELECT TOP 1 * from Color", commandType: CommandType.Text).SingleOrDefault();

                //db.Close();
            }
            if (Color == null)
            {
                Color = new Color();
                Color.LateInColor = "#e06666";
                Color.EarlyOutColor = "ea9999";
                Color.WeekendColor = "#98d0dd";
                Color.AbsentColor = "#8c0404";
                Color.HolidayColor = "#eeeeee";
                Color.ShiftNotAsignColor = "#ff0000";
                Color.DidNotLogoutColor = "#eeeeee";
                Color.LeaveColor = "#cc0000";
                Color.AbsentByLE = "#f50057";

                Color.PresentColor = "rgb(84, 142, 48)";
            }
            return Color;
        }
        public IEnumerable<EmployeeLeaveReport> GetLeaveReport(int fiscalyearId, int EmpId, int DepId, char LeaveStatus)
        {
            if (gs.ViewAllEmpRpt != 1)
            {
                //EmpId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                EmpId = 1;
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@fiscalyearId", fiscalyearId);
                    param.Add("@EmpId", EmpId);
                    param.Add("@LeaveSatus", LeaveStatus);
                    var leaveReport = db.Query<EmployeeLeaveReport>(sql: "[dbo].[GetLeaveReport]", param: param, commandType: CommandType.StoredProcedure);

                    return leaveReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public IEnumerable<EmployeeLeaveReport> GetDetailLeaveReport(int EmpId, int LeaveAssignId)
        {
            if (gs.ViewAllEmpRpt != 1)
            {
                //EmpId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                EmpId = 1;
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@EmpId", EmpId);
                    param.Add("@LeaveAssignId", LeaveAssignId);
                    var leaveReport = db.Query<EmployeeLeaveReport>(sql: "[dbo].[GetDetailLeaveReport]", param: param, commandType: CommandType.StoredProcedure);

                    return leaveReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public IEnumerable<ResignationReport> GetResignationReport(DateTime? fromDate, DateTime? toDate, int deptId, int desgId, int secId)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FromDate", fromDate);
                    param.Add("@ToDate", toDate);
                    param.Add("@DeptId", deptId);
                    param.Add("@DesId", desgId);
                    param.Add("@SecId", secId);
                    var leaveReport = db.Query<ResignationReport>(sql: "[dbo].[GetResignationReport]", param: param, commandType: CommandType.StoredProcedure);
                    foreach (var item in leaveReport)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(item.Issued_Date)) && Convert.ToString(item.Issued_Date) != "1/1/0001 12:00:00 AM" && Convert.ToString(item.Issued_Date) != "1/1/1900 12:00:00 AM")
                        {
                            item.Issued_DateNp = NepDateConverter.EngToNep(item.Issued_Date.Year, item.Issued_Date.Month, item.Issued_Date.Day).ToString();
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(item.Date)) && Convert.ToString(item.Date) != "1/1/0001 12:00:00 AM")
                        {
                            item.DateNp = NepDateConverter.EngToNep(item.Date.Year, item.Date.Month, item.Date.Day).ToString();
                        }
                        GlobalArray gs = new GlobalArray();
                       /// gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                        if (gs.ViewAllBranchData != 1)
                        {
                            //var bnId = (int)HttpContext.Current.Session["BranchId"];
                            var bnId = 1;
                            leaveReport = leaveReport.Where(x => x.BranchId == bnId).ToList();

                        }

                    }
                    return leaveReport.ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public IEnumerable<EmployeeDualShift> GetEmployeeDetailsOfDualShift(string eId, string dID, string bId)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();

                    param.Add("@empId", eId);
                    param.Add("@deptId", dID);
                    param.Add("@branchId", bId);
                    var data = db.Query<EmployeeDualShift>(sql: "[dbo].[GetEmployeeOfDualShift]", param: param, commandType: CommandType.StoredProcedure);

                    return data.ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public IEnumerable<EmployeeDualShift> GetEmployeeDetailOfDualShift(string fromDate, string toDate, string eId)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();

                    param.Add("@startDate", fromDate);
                    param.Add("@endDate", toDate);
                    param.Add("@empId", eId);


                    var data = db.Query<EmployeeDualShift>(sql: "[dbo].[GetOverallAttendanceDetailsOfDualShift]", param: param, commandType: CommandType.StoredProcedure);

                    return data.ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public bool HasDualShift(int EId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var data = db.Query<Company>(@"select Emp_Id from Employee as e
                               inner join EmployeeShift as es on e.Id = es.Emp_Id
                               inner join DualShift as ds on ds.Id= es.ShiftId where Emp_Id= 
                               " + EId, commandType: CommandType.Text);
                    if (data.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ManualAttendanceReport> ManualAttendanceReport(DateTime Date, int DeptId, int EmployeeId, int BranchId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@EmpId", EmployeeId);
                    param.Add("@DeptId", DeptId);
                    param.Add("@Date", Date);
                    param.Add("@BranchId", BranchId);
                    var data = db.Query<ManualAttendanceReport>(sql: "ManualAttendanceReport", param: param, commandType: CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                List<ManualAttendanceReport> a = new List<ManualAttendanceReport>();
                return a;
            }
        }
    }
}