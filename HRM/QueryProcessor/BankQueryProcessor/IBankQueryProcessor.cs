﻿

using System.Collections.Generic;


namespace HRM
{
   public interface IBankHRM
    {
		int Create(Bank bank);
		IEnumerable<BankViewModel> GetAllData();
		Bank GetById(int id);
		bool Delete(int id);
		int GetCode(string code, int id);

	}
}
