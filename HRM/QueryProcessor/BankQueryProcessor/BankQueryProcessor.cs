﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Common;
using System.Data;


namespace HRM
{

		public class BankHRM : IBankHRM
		{
			public int Create(Bank bank)
			{
				try
				{
					var dbfactory = DbFactoryProvider.GetFactory();
					using (var db = (DbConnection)dbfactory.GetConnection())
					{
						//db.OpenAsync();
						db.Open();
						var param = new DynamicParameters();
						param.Add("@Id", bank.Id);
						param.Add("@Event", bank.Event);
						param.Add("@BankName", bank.BankName);
						param.Add("@BankAddress", bank.BankAddress);
						param.Add("@BankCode", bank.BankCode);
						param.Add("@AccountNo", bank.AccountNo);
						param.Add("@IsDeleted", bank.IsDeleted);
                        param.Add("@CreateBy", bank.CreateBy);
                        param.Add("@CreateDate", bank.CreateDate);
                        param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
						db.Execute("[dbo].[Usp_IUD_Banks]", param: param, commandType: CommandType.StoredProcedure);
						db.Close();
						return param.Get<Int16>("@Return_Id");

					}
				}
				catch (Exception ex)
				{
					throw ex;
				}

				//throw new NotImplementedException();
			}

			public bool Delete(int id)
			{
				try
				{
					var dbfactory = DbFactoryProvider.GetFactory();
					using (var db = (DbConnection)dbfactory.GetConnection())
					{
						db.Open();
						int result = db.Execute("UPDATE [dbo].[Banks] SET [IsDeleted]=@IsDeleted Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
						db.Close();
						if (result > 0)
						{
							return true;
						}
						return false;


					}
				}
				catch (Exception ex)
				{
					throw ex;

				}

				//throw new NotImplementedException();
			}

			public IEnumerable<BankViewModel> GetAllData()
			{
				try
				{
					var dbfactory = DbFactoryProvider.GetFactory();
					using (var db = (DbConnection)dbfactory.GetConnection())
					{
						db.Open();
						var bankList = db.Query<BankViewModel>(sql: "[dbo].[GetBankList]", commandType: CommandType.StoredProcedure);
						return bankList.ToList();
					}
				}
				catch (Exception ex)
				{
					throw ex;
				}

				// throw new NotImplementedException();
			}

			public Bank GetById(int id)
			{
				try
				{
					Bank obj = new Bank();
					var dbfactory = DbFactoryProvider.GetFactory();
					using (var db = (DbConnection)dbfactory.GetConnection())
					{
						var param = new DynamicParameters();
						db.Open();
						param.Add("@Id", id);
						var banklist = db.Query<BankViewModel>(sql: "[dbo].[GetBankById]", param: param, commandType: CommandType.StoredProcedure);
						if (banklist.Count() > 0)
						{
							obj.Id = Convert.ToInt32(banklist.Select(x => x.Id).First());
							obj.BankName = banklist.Select(x => x.BankName).First().ToString();
							obj.BankCode = banklist.Select(x => x.BankCode).First().ToString();
							obj.BankAddress = banklist.Select(x => x.BankAddress).First().ToString();
							obj.AccountNo = banklist.Select(x => x.AccountNo).First();
						}
						db.Close();
						return obj;


					}
				}
				catch (Exception ex)
				{
					throw ex;
				}

				// throw new NotImplementedException();
			}
		public int GetCode(string code, int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();

					var data = db.Query<Bank>("SELECT BankCode FROM[dbo].[Banks] where BankCode = @Code and IsDeleted=0 and Id != @id", param: new { @Code = code , @Id = id}, commandType: CommandType.Text);
					var s = data.Count();

					db.Close();
					return s;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

	}


}
