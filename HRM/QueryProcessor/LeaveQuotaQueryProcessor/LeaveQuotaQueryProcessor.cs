﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
    public class LeaveQuotaHRM : ILeaveQuotaHRM
    {
        public int Create(LeaveQuota leaveQuota)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", leaveQuota.Id);
                    param.Add("@Event", leaveQuota.Event);
                    param.Add("@Code", leaveQuota.Code);
                    param.Add("@DesignationId", leaveQuota.DesignationId);
                    param.Add("@DesignationLevel", leaveQuota.DesignationLevel);
                    param.Add("@MaxSalary", leaveQuota.MaxSalary);
                    param.Add("@MinSalary", leaveQuota.MinSalary);
                    param.Add("@CreatedBy", leaveQuota.CreatedBy);
                    param.Add("@CreatedDate", leaveQuota.CreatedDate);
                    param.Add("@IsDeleted", leaveQuota.IsDeleted);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_LeaveQuota]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[LeaveQuota] SET [IsDeleted] = @IsDeleted Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public IEnumerable<LeaveQuota> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var leaveQuotaList = db.Query<LeaveQuota>(sql: "[dbo].[GetLeaveQuotaList]", commandType: CommandType.StoredProcedure);
                    return leaveQuotaList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LeaveQuota GetById(int id)
        {
            try
            {
                LeaveQuota obj = new LeaveQuota();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var leaveQuotalist = db.Query<LeaveQuota>(sql: "[dbo].[GetLeaveQuotaById]", param: param, commandType: CommandType.StoredProcedure);
                    if (leaveQuotalist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(leaveQuotalist.Select(x => x.Id).First());
                        obj.Code = Convert.ToInt32(leaveQuotalist.Select(x => x.Code).First());
                        obj.DesignationId = Convert.ToInt32(leaveQuotalist.Select(x => x.DesignationId).First());
                        obj.DesignationLevel = Convert.ToInt32(leaveQuotalist.Select(x => x.DesignationLevel).First());
                        obj.MaxSalary = Convert.ToDecimal(leaveQuotalist.Select(x => x.MaxSalary).First().ToString());
                        obj.MinSalary = Convert.ToDecimal(leaveQuotalist.Select(x => x.MinSalary).First().ToString());

                    }
                    db.Close();
                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}