﻿
using System.Collections.Generic;


namespace HRM
{
    public interface ILeaveQuotaHRM
    {
        int Create(LeaveQuota leaveQuota);
        IEnumerable<LeaveQuota> GetAllData();
        LeaveQuota GetById(int id);
        bool Delete(int id);
    }
}
