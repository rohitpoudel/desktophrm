﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;




namespace HRM
{
	public class WeeklyScheduleHRM : IWeeklyScheduleHRM
	{
		public int Create(WeeklySchedule weeklySchedule)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Event", weeklySchedule.Event);
					param.Add("@Id", weeklySchedule.Id);
					param.Add("@DepartmentId", weeklySchedule.DepartmentId);
					param.Add("@SectionId", weeklySchedule.SectionId);
					param.Add("@CreatedBy", weeklySchedule.CreatedBy);
					param.Add("@CreatedDate", weeklySchedule.CreatedDate);
					param.Add("@IsDeleted", weeklySchedule.IsDeleted);
					param.Add("@XMLEmpId", weeklySchedule.XMLEmpId);
					param.Add("@EmpIdCollection", weeklySchedule.EmpIdCollection);
                    param.Add("@FromDate", weeklySchedule.FromDate);
                    param.Add("@FromDateNepali", weeklySchedule.FromDateNepali);
                    param.Add("@ToDate", weeklySchedule.ToDate);
                    param.Add("@ToDateNepali", weeklySchedule.ToDateNepali);
                    param.Add("@Remarks", weeklySchedule.Remarks);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_WeeklySchedule]", param: param, commandType: CommandType.StoredProcedure);

					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE[dbo].[WeeklySchedule] SET[IsDeleted]=@IsDeleted WHERE Id=@Id UPDATE[dbo].[WeeklyScheduleDetail] SET[IsDeleted]=@IsDeleted WHERE WeeklyScheduleId=@Id ", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);

					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<WeeklyScheduleViewModel> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
                    DynamicParameters param = new DynamicParameters();
					var weeklySccheduleList = db.Query<WeeklyScheduleViewModel>(sql: "[dbo].[GetWeeklyScheduleList]", commandType: CommandType.StoredProcedure);
                    foreach (var item in weeklySccheduleList)
                    {
                        param.Add("@Id", item.Id);
                        item.EmployeeListItem = db.Query<WeeklyScheduleEmployeeList>(sql: "[dbo].[GetWeeklyScheduleEmployeeList]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    db.Close();
					return weeklySccheduleList.ToList();
				}

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public WeeklySchedule GetById(int id)
		{
			try
			{
				var obj = new WeeklySchedule();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var weeklyScheduleList = db.Query<WeeklyScheduleViewModel>(sql: "[dbo].[GetWeeklyScheduleById]", param: param, commandType: CommandType.StoredProcedure);
					if (weeklyScheduleList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(weeklyScheduleList.Select(x => x.Id).First());
						obj.DepartmentId = Convert.ToInt32(weeklyScheduleList.Select(x => x.DepartmentId).First());
						obj.SectionId = Convert.ToInt32(weeklyScheduleList.Select(x => x.SectionId).First());
						obj.CreatedBy = Convert.ToInt32(weeklyScheduleList.Select(x => x.CreatedBy).First());
						obj.CreatedDate = Convert.ToDateTime(weeklyScheduleList.Select(x => x.CreatedDate).First());
						obj.EmployeeId = weeklyScheduleList.Select(x => x.EmpId).ToArray();
                        obj.FromDate = weeklyScheduleList.Select(x => x.FromDate).First();
                        obj.FromDateNepali = weeklyScheduleList.Select(x => x.FromDateNepali).First();
                        obj.ToDate = weeklyScheduleList.Select(x => x.ToDate).First();
                        obj.ToDateNepali = weeklyScheduleList.Select(x => x.ToDateNepali).First();
                        obj.Remarks = weeklyScheduleList.Select(x => x.Remarks).First();
                        obj.DepartmentName = weeklyScheduleList.Select(x => x.DepartmentName).First();
                        obj.SectionName = weeklyScheduleList.Select(x => x.SectionName).First();
                    }
					db.Close();
					return obj;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public IEnumerable<WeeklyScheduleEmployeeList> GetEmployeeListonly(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())

                {
                    db.Open();
                    DynamicParameters param = new DynamicParameters();
                    var Employeelist = db.Query<WeeklyScheduleEmployeeList>(sql: "[dbo].[GetWeeklyScheduleEmployeeList]", param: new { @id = id }, commandType: CommandType.StoredProcedure);
                    return Employeelist.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}