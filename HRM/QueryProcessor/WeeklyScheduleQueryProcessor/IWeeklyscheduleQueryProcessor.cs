﻿
using System.Collections.Generic;


namespace HRM
{
	public interface IWeeklyScheduleHRM
	{
		int Create(WeeklySchedule weeklyschedule);
		bool Delete(int id);
		WeeklySchedule GetById(int id);
		IEnumerable<WeeklyScheduleViewModel> GetAllData();
        IEnumerable<WeeklyScheduleEmployeeList> GetEmployeeListonly(int id);
    }
}
