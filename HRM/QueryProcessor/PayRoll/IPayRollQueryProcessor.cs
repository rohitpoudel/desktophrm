﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
	public interface IPayRollHRM
	{
		IEnumerable<PayRoll> GetAllData( int FiscalYearId, int Year, int Month, int DesignsnationId, int DepartmentId, int EmployeeId, string DateType);
		IEnumerable<PayRoll> GetBySalaryId(string SalId);
		Allowance GetAllowanceBySalaryId(string SalId);
        List<SelectListItem> BindMonthByYear(string Year, int Id, string DateType);
        bool GeneratePayroll(int FiscalYearId, string Year, int Month, int DepartmentId, int DesignationId, string EmployeeIds, string MonthStartDate, string MonthEndDate);
        Payslip GetPayslip(int id, int FiscalYearId,string Year, string Month, string DateType);
        List<Provisional> Provisional(int FiscalYearId, string Year, int Month, int DepartmentId, int DesignationId);
    }
}
