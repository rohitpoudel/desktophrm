﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace HRM
{
    public class PayRollHRM : IPayRollHRM
    {
        public IEnumerable<PayRoll> GetAllData( int FiscalYearId, int Year, int Month, int DesignsnationId, int DepartmentId, int EmployeeId, string DateType)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@EmpId", EmployeeId);
                    param.Add("@DepId", DepartmentId);
                    param.Add("@DegId", DesignsnationId);
                    param.Add("@FiscalYear", FiscalYearId);
                    param.Add("@Year", Year);
                    param.Add("@Month", Month);
                    var payRollList = db.Query<PayRoll>(sql: "[dbo].[GetPayRollList]", param: param, commandType: CommandType.StoredProcedure);
                    foreach (var item in payRollList)
                    {
                        item.FiscalYearId = FiscalYearId;
                        item.Year = Year.ToString();
                        item.Month = Month;
                        item.DateType = DateType;
                    }
                    return payRollList.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Allowance GetAllowanceBySalaryId(string SalId)
        {
            Allowance obj = new Allowance();
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", SalId);
                    var list = db.Query<Allowance>(sql: "[dbo].[Usp_GetAllowanceBySalaryId]", param: param, commandType: CommandType.StoredProcedure);
                    if (list.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(list.Select(x => x.Id).First());
                        obj.Name = list.Select(x => x.Name).First();
                        obj.Type = list.Select(x => x.Type).First();
                        obj.AllowanceAmount = list.Select(x => x.AllowanceAmount).First();
                    }

                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public IEnumerable<PayRoll> GetBySalaryId(string SalId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@EmpId", SalId);
                    var list = db.Query<PayRoll>(sql: "[dbo].[GetPayRollBySalaryId]", param: param, commandType: CommandType.StoredProcedure);
                    return list.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<SelectListItem> BindMonthByYear(string Year, int Id, string DateType)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                db.Open();
                var param = new DynamicParameters();
                param.Add("@FiscalYearId", Id);
                param.Add("@Year", Year);
                param.Add("@DateType", DateType);
                var list = db.Query<dynamic>(sql: "[dbo].[GetMonthByYear]", param: param, commandType: CommandType.StoredProcedure).ToList();
                List<SelectListItem> ddlItems = new List<SelectListItem>();
                if(DateType == "English")
                    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Month", Value = "0" });
                else
                    ddlItems.Add(new SelectListItem() { Selected = true, Text = "महिना छानुहोस", Value = "0" });
                foreach (var key in list)
                {
                    ddlItems.Add(new SelectListItem() { Selected = false, Text = key.Text.ToString(), Value = key.Value.ToString() });
                }
                return ddlItems;
            }
        }
        public bool GeneratePayroll(int FiscalYearId, string Year, int Month, int DepartmentId, int DesignationId, string EmployeeIds, string MonthStartDate, string MonthEndDate)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    //var startDate = new DateTime(Convert.ToInt32(Year), Month, 1);
                    //var endDate = startDate.AddMonths(1).AddDays(-1);
                    var param = new DynamicParameters();
                    param.Add("@S", EmployeeIds);
                    param.Add("@MonthStartDate", MonthStartDate);
                    param.Add("@MonthEndDate", MonthEndDate);
                    param.Add("@FiscalYearId", FiscalYearId);
                    param.Add("@Month", Month);
                    param.Add("@Year", Year);
                    param.Add("@CreatedBy", 1);
                    param.Add("@UpdatedBy", 1);
                    int res = db.Execute(sql: "[dbo].[GeneratePayroll]", param: param, commandType: CommandType.StoredProcedure);
                    //int result = db.Query<PayRoll>(sql: "[dbo].[GetPayRollList]", param: param, commandType: CommandType.StoredProcedure);
                    if (res > 1)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Payslip GetPayslip(int id, int FiscalYearId, string Year, string Month, string DateType)
        {
            Payslip obj = new Payslip();
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", id);
                    param.Add("@FiscalYearId", FiscalYearId);
                    param.Add("@Year", Year);
                    param.Add("@Month", Month);
                    param.Add("@DateType", DateType);
                    obj = db.Query<Payslip>(sql: "[dbo].[GetPayslip]", param: param, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Deductions = db.Query<Deduction>(sql: "Select DedRebName Name, Deduction Amount from EmpMonthlyDedRebInfo where MonthlySalaryId = @MonthlySalaryId and DeductionOrRebate = 'D'", param: new { @MonthlySalaryId = obj.MonthlySalaryId }, commandType: CommandType.Text);
                        obj.Rebates = db.Query<Rebate>(sql: "Select DedRebName Name, Deduction Amount from EmpMonthlyDedRebInfo where MonthlySalaryId = @MonthlySalaryId and DeductionOrRebate = 'R'", param: new { @MonthlySalaryId = obj.MonthlySalaryId }, commandType: CommandType.Text);
                        obj.Allowances = db.Query<Allowances>(sql: "SELECT AllowanceName Name, Allowance Amount from EmpMonthlyAllowanceInfo where MonthlySalaryId = @MonthlySalaryId", param: new { @MonthlySalaryId = obj.MonthlySalaryId }, commandType: CommandType.Text);
                    }
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Provisional> Provisional(int FiscalYearId, string Year, int Month, int DepartmentId, int DesignationId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FiscalYearId", FiscalYearId);
                    param.Add("@Year", Year);
                    param.Add("@Month", Month);
                    param.Add("@DepartmentId", DepartmentId);
                    param.Add("@DesignationId", DesignationId);
                    var list = db.Query<Provisional>(sql: "[dbo].[GetProvisional]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    return list;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}