﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;




namespace HRM
{
    public class DesignationHRM : IDesignationHRM
    {

        public int Create(Designation designation)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    if (designation.DesignationCode==null)
                    {
                        //designation.DesignationCode = HttpContext.Current.Session["Code"].ToString();
                        designation.DesignationCode = "001";
                    }
                    var param = new DynamicParameters();
                    param.Add("@Id", designation.Id);
                    param.Add("@Event", designation.Event);
                    param.Add("@DesignationCode", designation.DesignationCode);
                    param.Add("@DesignationName", designation.DesignationName);
                    param.Add("@DesignationLevel", designation.DesignationLevel);
                    param.Add("@MaxSalary", designation.MaxSalary);
                    param.Add("@MinSalary", designation.MinSalary);
                    param.Add("@IsActive", designation.IsActive);
                    param.Add("@CreateBy", designation.CreateBy);
                    param.Add("@CreateDate", designation.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Designations]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = 0;
                    int DesignationOnEmp = db.Execute("SELECT COUNT(*) DesignationOnEmp FROM Employee e WHERE e.Is_Active = 1 AND e.Designation_Id = "+ id+";", commandType: CommandType.Text);
                    if(DesignationOnEmp < 1)
                    {
                        result = db.Execute("UPDATE [dbo].[Designations] SET [IsActive] = @IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    }
                    
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<DesignationViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var designationLists = db.Query<DesignationViewModel>(sql: "[dbo].[Usp_GetDesignationList]", commandType: CommandType.StoredProcedure);
					db.Close();
                    return designationLists.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Designation GetById(int id)
        {
            try
            {
                Designation obj = new Designation();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var designationList = db.Query<DesignationViewModel>(sql: "[dbo].[Usp_GetDesignationById]", param: param, commandType: CommandType.StoredProcedure);
                    if (designationList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(designationList.Select(x => x.Id).First());
                        obj.DesignationCode = designationList.Select(x => x.DesignationCode).First().ToString();
                        obj.DesignationName = designationList.Select(x => x.DesignationName).First().ToString();
                        obj.DesignationLevel = designationList.Select(x => x.DesignationLevel).First();
                        obj.MaxSalary = designationList.Select(x => x.MaxSalary).First();
                        obj.MinSalary = designationList.Select(x => x.MinSalary).First();
                   
                    }
                    db.Close();
                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

		public int GetCode(string code, int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
                    var data = 0;
                    var param = new DynamicParameters();
					db.Open();
                    if (id == 0)
                    {
                        data = db.Query<Designation>("SELECT DesignationCode FROM[dbo].[Designations] where DesignationCode = @Code and IsActive=1", param: new { Code = code }, commandType: CommandType.Text).Count();
                    }
                    else
                    {
                        data = db.Query<Designation>("SELECT DesignationCode FROM[dbo].[Designations] where DesignationCode = @Code AND Id != @Id", param: new { Code = code, id=id }, commandType: CommandType.Text).Count();
                    }
					db.Close();
					return data;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}