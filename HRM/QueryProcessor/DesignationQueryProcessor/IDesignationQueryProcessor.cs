﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IDesignationHRM
    {
        Designation GetById(int id);
        int Create(Designation designation);
        IEnumerable<DesignationViewModel> GetAllData();
        bool Delete(int id);
		int GetCode(string code, int id);
    }
}
