﻿
using System.Collections.Generic;
namespace HRM.PerformanceMetrixHRM
{
    public interface IPerformanceMetrixHRM
    {
        PerformanceMeasureMetrix Create(PerformanceMeasureMetrix performance);
        List<PerformanceMeasureMetrix> GetAll();
        List<PerformanceMeasureMetrixViewModel> GetAllData();
        void Delete(int id);
        PerformanceMeasureMetrix Edit(PerformanceMeasureMetrix performance);
        PerformanceMeasureMetrix GetById(int id);
    }
}