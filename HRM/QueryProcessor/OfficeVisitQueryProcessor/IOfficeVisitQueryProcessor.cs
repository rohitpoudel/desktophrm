﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IOfficeVisitHRM
    {
        int Create(OfficeVisit officeVisit);
        IEnumerable<OfficeVisit> GetAllData();
        OfficeVisit GetById(int id);
        bool Delete(int id);

    }
}
