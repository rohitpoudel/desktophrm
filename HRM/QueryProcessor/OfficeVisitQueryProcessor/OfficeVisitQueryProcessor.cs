﻿


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace HRM
{

    public class OfficeVisitHRM : IOfficeVisitHRM
    {
        public int Create(OfficeVisit officeVisit)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    officeVisit.CreatedBy =1;
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", officeVisit.Id);
                    param.Add("@Event", officeVisit.Event);
                    param.Add("@FromDate", officeVisit.FromDate.ToString("yyyy-MM-dd"));
                    param.Add("@ToDate", officeVisit.ToDate.ToString("yyyy-MM-dd"));
                    param.Add("@FromDateNepali", officeVisit.FromDateNepali);
                    param.Add("@ToDateNepali", officeVisit.ToDateNepali);
                    param.Add("@Remark", officeVisit.Remark);
                    param.Add("@Emp_Id", officeVisit.Employee_Id);
                    param.Add("@Status", officeVisit.Status);
                    param.Add("@Created_By", officeVisit.CreatedBy);
                    param.Add("@Created_Date", DateTime.Now.ToString("yyyy-MM-dd"));
                    param.Add("@Is_Active", officeVisit.Is_Active);
                    param.Add("@OfficeName", officeVisit.OfficeName);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_OfficeVisit]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<OfficeVisit> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var officeList = db.Query<OfficeVisit>(sql: "[dbo].[GetOfficeVisitList]", commandType: CommandType.StoredProcedure);
                    GlobalArray gs = new GlobalArray();
                  //  gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData != 1)
                    {
                      //  var bnId = (int)HttpContext.Current.Session["BranchId"];
                        var bnId = 1;
                        officeList = officeList.Where(x => x.BranchId == bnId).ToList();

                    }
                    //var employeeNameInOfficeVisit = db.Query<OfficeVisit>(sql: "Select E.Emp_Name From Employee E Inner Join OfficeVisit O ON O.Employee_Id = E.Id;", commandType: CommandType.Text);
                    return officeList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OfficeVisit GetById(int id)
        {
            try
            {
                OfficeVisit obj = new OfficeVisit();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var officeList = db.Query<OfficeVisit>(sql: "[dbo].[GetOfficeVisitById]", param: param, commandType: CommandType.StoredProcedure);
                    if (officeList.Count() > 0)
                    {

                        obj.Id = Convert.ToInt32(officeList.Select(x => x.Id).First());
                        obj.FromDate = officeList.Select(x => x.FromDate).First();
                        obj.FromDateNepali = officeList.Select(x => x.FromDateNepali).First();
                        obj.ToDate = officeList.Select(x => x.ToDate).First();
                        obj.ToDateNepali = officeList.Select(x => x.ToDateNepali).First();
                        obj.Remark = officeList.Select(x => x.Remark).First().ToString();
                        obj.Employee_Id = Convert.ToInt32(officeList.Select(x => x.Employee_Id).First());
						obj.Emp_Name = officeList.Select(x => x.Emp_Name).First().ToString();
						obj.Status = officeList.Select(x => x.Status).First();
                        obj.CreatedBy = Convert.ToInt32(officeList.Select(x => x.CreatedBy).First());
                        obj.CreatedDate = Convert.ToDateTime(officeList.Select(x => x.CreatedDate).First());
                        obj.OfficeName = officeList.Select(x => x.OfficeName).First().ToString();


                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[OfficeVisit] SET [Is_Active] = 0 Where Id=@Id", param: new { @Is_Active = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}