﻿
using System.Collections.Generic;

namespace HRM.Device
{
    interface IDeviceHRM
	{
        /// <summary>
        ///  List the Active Device
        /// </summary>
        /// <param name="list">Inumerable List</param>
        /// <returns>Returns string</returns>
		string Create(List<DeviceLog> list);
        /// <summary>
        /// List all the Active Device
        /// </summary>
        /// <param name="companyCode">Company Code Identifier</param>
        /// <returns>Inumerable Device List</returns>
		IEnumerable<Devices> GetAllDeviceData(string companyCode);
        /// <summary>
        /// Save Attendance data to AttendanceLog table
        /// </summary>
        /// <param name="list">Inumerable list of Attendance data</param>
        /// <returns>Retrun string</returns>
        string CreateAttendanceLog(List<AttendanceRecords> list);
        /// <summary>
        /// Update the machine status if cannot connect to device
        /// </summary>
        /// <param name="companycode">Comopany Identifier</param>
        /// <param name="machineId">Machine Id identifier</param>
        string UpdateMachineStatus(string companycode, string machineId, bool deviceStatus);
        IEnumerable<Devices> GetCompanyCodeBySerial(string Sn);

    }
}
