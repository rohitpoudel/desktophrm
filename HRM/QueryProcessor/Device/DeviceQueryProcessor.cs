﻿

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace HRM.Device
{
	public class DeviceHRM: IDeviceHRM
	{
      private  object deviceid;
       // private bool _extraproperty;
       private static string _companycode="";

        public string Create(List<DeviceLog> list)
		{
			string result = "";

			try
			{
                              
               // string connection = General.BuildAPIDyanmicString(list[0].CompanyCode).ToString();
                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                _companycode = string.Empty;
                _companycode = list[0].CompanyCode;
                SqlConnection _conn = new SqlConnection(connection);
                DataTable table = new DataTable();
					table = General.ConvertToDataTable<DeviceLog>(list);
                    deviceid = table.Rows[0]["DeviceId"];
                    table.Columns.RemoveAt(7);
                ///table.Columns.RemoveAt(6);
                _conn.Open();
                    if (IsTruncateTable(deviceid))
                    {
                        SqlCommand truncate = new SqlCommand("TRUNCATE TABLE DeviceLog", _conn);
                        truncate.ExecuteNonQuery();
                    }
                    using (SqlBulkCopy _copy = new SqlBulkCopy(_conn))
						{
							_copy.DestinationTableName = "DeviceLog";
							_copy.BatchSize = 1000;
							_copy.WriteToServer(table);
						}
                    _conn.Close();
                    result ="Success";
                    if (result == "Success")
                    {
                        UpdateExtraproperty(deviceid);
                    }
                    if (result=="Success")
					{
						try
						{
							
							DateTime dt =list[0].LogTime;
							string IpAddress = list[0].DeviceIP;
                        _conn.Open();
							var param = new DynamicParameters();
							param.Add("@LogTime", dt);
							param.Add("@IpAddress", IpAddress);
							param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                        _conn.Execute("[dbo].[Usp_UpdateLogtime]", param: param, commandType: CommandType.StoredProcedure);
                        _conn.Close();
							 param.Get<Int16>("@Return_Id");




						}
						catch (Exception ex)
						{
							throw ex;

						}
					}
					 
				//}
			}
			catch (Exception ex)
			{
				result = ex.ToString();
				
			}
           
            return result;
		}
        public string CreateAttendanceLog(List<AttendanceRecords> list)
        {
            string result = "";

            try
            {
               

                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                _companycode = string.Empty;
                _companycode = list[0].CompanyCode;
                SqlConnection _conn = new SqlConnection(connection);

                DataTable table = new DataTable();
                    table = General.ConvertToDataTable<AttendanceRecords>(list);
                    deviceid = table.Rows[0]["DeviceId"];
                    table.Columns.RemoveAt(12);
                table.Columns.RemoveAt(11);
                table.Columns.RemoveAt(10);

                _conn.Open();
                if (IsTruncateTable(deviceid))
                    {
                        SqlCommand truncate = new SqlCommand("TRUNCATE TABLE AttendanceLog", _conn);
                        truncate.ExecuteNonQuery();
                    }

                    using (SqlBulkCopy _copy = new SqlBulkCopy(_conn))
                    {
                        _copy.DestinationTableName = "AttendanceLog";
                        _copy.BatchSize = 1000;
                        _copy.WriteToServer(table);
                    }
                    _conn.Close();
                    result = "Success";
                  
                   

                
            }
            catch (Exception ex)
            {
                result = ex.ToString();

            }
            return result;
        }
        public void UpdateExtraproperty(object DeviceId)
        {
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection _conn = new SqlConnection(connection);

                _conn.Open();
                    int result = _conn.Execute("UPDATE[dbo].[Devices]  SET[ExtraProperty] = 1 Where Id=@Id", param: new {  @Id = Convert.ToInt32(DeviceId) }, commandType: CommandType.Text);
                _conn.Close();                


               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsTruncateTable(object deviceId)
        {
            bool result = false;
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection _conn = new SqlConnection(connection);
                _conn.Open();
                   var List = _conn.Query("SELECT    [IsTruncateTable] FROM [dbo].[Devices] Where Id=@Id", param: new { @Id = Convert.ToInt32(deviceId) }, commandType: CommandType.Text);
                _conn.Close();
                    foreach(var item in List)
                    {
                        if(item.IsTruncateTable)
                        { result= true; }
                       
                    }
                    return result;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Devices> GetAllDeviceData(string companycode)
		{
			try
			{
                _companycode = companycode;
                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection _conn = new SqlConnection(connection);
                _conn.Open();
				var deviceList = _conn.Query<Devices>(sql: "[dbo].[GetDeviceList]", commandType: CommandType.StoredProcedure);
                _conn.Close();
                return deviceList.ToList();
                

            }
			catch (Exception ex)
			{
				throw ex;
			}
		}
        public string UpdateMachineStatus(string companycode, string machineId,bool deviceStatus)
        {
            try
            {
                _companycode = companycode;
                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection _conn = new SqlConnection(connection);
                _conn.Open();
                int result = _conn.Execute("UPDATE[dbo].[Devices]  SET[DeviceStatus] =@DeviceStatus Where Id=@Id", param: new { @Id = Convert.ToInt32(machineId),@DeviceStatus=deviceStatus }, commandType: CommandType.Text);
                _conn.Close();

                return "Status Updated Successfully";

            }
            catch (Exception ex)
            {
                return "Status cannot be  Updated ";
                throw ex;
            }
        }
        public IEnumerable<Devices> GetCompanyCodeBySerial(string Sn)
        {
            try
            {

               
              


                string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection _conn = new SqlConnection(connection);
                _conn.Open();
                var param = new DynamicParameters();
                param.Add("@Sn", Sn);
                var deviceList = _conn.Query<Devices>(sql: "[dbo].[GetDeviceBySn]", param: param, commandType: CommandType.StoredProcedure);
                _conn.Close();

                return deviceList;

            }
            catch (Exception ex)
            {
               /// return "Status cannot be  Updated ";
                throw ex;
            }
        }


    }
}