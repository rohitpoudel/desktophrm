﻿

using System.Collections.Generic;


namespace HRM
{
    interface IAttendanceReportHRM
    {
        IEnumerable<DailyAttendanceReport> GetAllData();
        IEnumerable<AttendanceDetails> GetAttendanceDetails(string dataFor, string reportFor);
        IEnumerable<WeeklyAttendanceReport> GetWeeklyData(int numberOfDays);
        UserDetails GetUserDetails();
        UserDetails GetIndUserDetails();
        bool UpdatePassword(UserDetails ud);
        bool UpdateUserInfo(UserDetails ud);
        IEnumerable<AttendanceDetails> GetAbsent(string dataFor);
        IEnumerable<AttendanceDetails> GetLeave(string dataFor);
        void GetImagePath(int EmployeeId);
        string Email(int id);
        bool ChangeEmailVerifyStatus(int id);
        IEnumerable<Employee> GetEmpList(int id);
        IEnumerable<Employee> GetEmpListById(EmailSendToEmp model);
        IEnumerable<Employee> GetEmpListForDept(int id);
        IEnumerable<AbsentEOLI> GetAbsentLIEO(string dataFor);
        ValidityDetails GetValidityDetails(string DateType,string companycode);
        IEnumerable<Employee> GetEmpListForDeptByShift(int id, bool isDualShift);
         IEnumerable<DeviceStatusReport> GetDeviceStatusList();
        IEnumerable<PoliticalNonPoliticalEmployee> GetPoliticalNonPoliticalEmployee();
        void EmployeeDetailsInfo();
    }
}
