﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;






namespace HRM
{
    public class AttendanceReportHRM : IAttendanceReportHRM
    {

        IEnumerable<DailyAttendanceReport> IAttendanceReportHRM.GetAllData()
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();

                    int numberOfDays = 0;
                    int BranchaID = 0;
                    GlobalArray gs = new GlobalArray();
                    //gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    //if(gs.ViewAllBranchData == 0)
                    //{
                    //    BranchaID = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                    //}


                    var param = new DynamicParameters();
                    param.Add("@NumberOfDays", numberOfDays);
                    param.Add("@BranchId", BranchaID);

                    var param1 = new DynamicParameters();
                    param1.Add("@BranchId", BranchaID);


                    var dailyAttendanceReport = db.Query<DailyAttendanceReport>(sql: "[dbo].[DailyAttendanceReport]", param: param1, commandType: CommandType.StoredProcedure);
                    var count = db.Query<WeeklyAttendanceReport>(sql: "[dbo].[GetAbsentListCount]", param: param, commandType: CommandType.StoredProcedure);
                    var countAbs = db.Query<Attendance>(sql: "[dbo].[GetEarlyOutLateInAbsent]", param: param, commandType: CommandType.StoredProcedure);
                    dailyAttendanceReport.FirstOrDefault().AbsWhenLIEO = countAbs.ToList().Count();
                    dailyAttendanceReport.FirstOrDefault().TotalAbsent = count.FirstOrDefault().AbsentCount;
                    return dailyAttendanceReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }


        }
        public string Email(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    var data = db.Query<Employee>("SELECT Email FROM[dbo].[Employee] where  Is_Active=1 and Id=@Id", param: new { Id = id }, commandType: CommandType.Text);
                    var s = data.Count();

                    db.Close();
                    return data.First().Email;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public IEnumerable<Employee> GetEmpListById(EmailSendToEmp model)
        {
            List<Employee> empList = new List<Employee>();
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                db.Open();
                int count = 0;
                foreach (var item in model.EmployeeId)
                {
                    Employee emp = new Employee();
                    
                    var param = new DynamicParameters();
                    var id = item[count];

                    var data = db.Query<Employee>("select Id,Emp_Name,Email from employee where Id=@Id", param: new { @Id = item }, commandType: CommandType.Text);
                    emp.Email = data.FirstOrDefault().Email;
                    emp.Id = data.FirstOrDefault().Id;
                    emp.Emp_Name = data.FirstOrDefault().Emp_Name;
                    empList.Add(emp);
                    count++;
                }
                db.Close();
                return empList;
            }
        }
        public IEnumerable<Employee> GetEmpListForDept(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    if (id == 0)
                    {
                        var data = db.Query<Employee>("select Id,Emp_Name,Email from Employee where  Is_Active=1 and Emp_DeviceCode!=0 ", param: new { @Id = id }, commandType: CommandType.Text);
                        db.Close();

                        return data.ToList();
                    }
                    else
                    {
                        var data = db.Query<Employee>("select Id,Emp_Name,Email from Employee where Department_Id = @Id and  Is_Active=1 and Emp_DeviceCode!=0 ", param: new { @Id = id }, commandType: CommandType.Text);
                        db.Close();

                        return data.ToList();
                    }


                    // var result = db.Execute("select Id,Emp_Name from Employee where Department_Id = @Id and  Is_Active=1 and EmailVerified=1", param: new { @Id = id }, commandType: CommandType.Text);





                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public IEnumerable<Employee> GetEmpList(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();

                    if (id == 0)
                    {
                        var data = db.Query<Employee>("select Id,Emp_Name,Email from Employee where  Is_Active=1 and EmailVerified=1", param: new { @Id = id }, commandType: CommandType.Text);
                        db.Close();

                        return data.ToList();
                    }
                    else
                    {
                        var data = db.Query<Employee>("select Id,Emp_Name,Email from Employee where Department_Id = @Id and  Is_Active=1 and EmailVerified=1", param: new { @Id = id }, commandType: CommandType.Text);
                        db.Close();

                        return data.ToList();
                    }


                    // var result = db.Execute("select Id,Emp_Name from Employee where Department_Id = @Id and  Is_Active=1 and EmailVerified=1", param: new { @Id = id }, commandType: CommandType.Text);





                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool ChangeEmailVerifyStatus(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();



                    int result1 = db.Execute("UPDATE [HRM].[dbo].Employee SET EmailVerified = 0 Where ID=@Id", param: new { @Id = id }, commandType: CommandType.Text);

                    int result = db.Execute("UPDATE Employee SET EmailVerified = 0 Where ID=@Id", param: new { @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    return true;





                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool UpdateUserInfo(UserDetails ud)
        {
            try
            {

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@email", ud.Email);

                    param.Add("@empName", ud.Emp_Name);
                    param.Add("@userName", ud.UserName);
                    param.Add("@photo", ud.photo);
                 //   param.Add("@userId", (int)HttpContext.Current.Session["UserId"]);

                    db.Execute("[dbo].[UpdateUserInfo]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return true;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return false;
            }
        }

        public bool UpdatePassword(UserDetails ud)
        {
            try
            {
                ud.PassWord = General.Encrypt(ud.PassWord);
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@password", ud.PassWord);

                    param.Add("@id", ud.Id);
                    db.Execute("[dbo].[UpdatePassword]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return true;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return false;
            }
        }
        public UserDetails GetIndUserDetails()
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {




                    var obj = new UserDetails();
                    db.Open();
                    var param = new DynamicParameters();
                    //param.Add("@Id", HttpContext.Current.Session["UserId"]);
                    param.Add("@Id", 1);
                    var UserDetails = db.Query<UserDetails>(sql: "[dbo].[GetSpecificUserDetails]", param: param, commandType: CommandType.StoredProcedure);
                    if (UserDetails.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(UserDetails.Select(x => x.Id).First());


                        obj.EmpId = Convert.ToInt32(UserDetails.Select(x => x.UserId).First());
                        obj.RoleId = UserDetails.Select(x => x.RoleId).First();
                        obj.Company_Code = UserDetails.Select(x => x.Company_Code).First();
                        obj.UserName = UserDetails.Select(x => x.UserName).First();
                        obj.UserId = UserDetails.Select(x => x.UserId).First();

                        obj.PassWord = General.Decrypt(UserDetails.Select(x => x.PassWord).First());



                    }
                    return obj;

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public UserDetails GetUserDetails()
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {




                    var obj = new UserDetails();
                    db.Open();
                    var param = new DynamicParameters();
                    //var usrId = HttpContext.Current.Session["UserId"];
                    var usrId = 1;
                    param.Add("@Id",usrId);
                    var UserDetails = db.Query<UserDetails>(sql: "[dbo].[GetOverAllEMployeeDetails]", param: param, commandType: CommandType.StoredProcedure);
                    var imgName = "";
                    var imgFullPath = "";
                    if (UserDetails.FirstOrDefault().photo != null)
                    {
                        imgFullPath = "\\" + UserDetails.FirstOrDefault().photo;
                        var imageName = UserDetails.FirstOrDefault().photo.Split('\\');
                        imgName = imageName[2];
                    }
                    if (UserDetails.Count() > 0)
                    {

                        obj.UserName = UserDetails.Select(x => x.UserName).First();

                        obj.EmpId = Convert.ToInt32(UserDetails.Select(x => x.EmpId).First());
                        obj.Emp_Code = UserDetails.Select(x => x.Emp_Code).First();
                        obj.Emp_Name = UserDetails.Select(x => x.Emp_Name).First();
                        obj.Emp_DeviceCode = UserDetails.Select(x => x.Emp_DeviceCode).First();
                        obj.DOB = UserDetails.Select(x => x.DOB).First();
                        obj.Email = UserDetails.Select(x => x.Email).First();
                        obj.CitizenShip_No = UserDetails.Select(x => x.CitizenShip_No).First();
                        // obj.photo = UserDetails.Select(x => x.photo).First();
                        //  obj.ImgPath = UserDetails.Select(x => x.photo).First();

                        obj.DOB = UserDetails.Select(x => x.DOB).First();
                        obj.Temp_Address = UserDetails.Select(x => x.Temp_Address).First();
                        obj.Permanet_Address = UserDetails.Select(x => x.Permanet_Address).First();
                        obj.Mobile_No = UserDetails.Select(x => x.Mobile_No).First();
                        obj.Email = UserDetails.Select(x => x.Email).First();
                        obj.GenderName = UserDetails.Select(x => x.GenderName).First();
                        obj.CitizenShip_No = UserDetails.Select(x => x.CitizenShip_No).First();
                        obj.Marital_Status_Name = UserDetails.Select(x => x.Marital_Status_Name).First();
                        obj.Blood_Group_Name = UserDetails.Select(x => x.Blood_Group_Name).First();
                        obj.Mobile_No = UserDetails.Select(x => x.Mobile_No).First();

                        obj.UserId = UserDetails.Select(x => x.UserId).First();
                        obj.RoleId = UserDetails.Select(x => x.RoleId).First();
                        obj.Temp_Address = UserDetails.Select(x => x.Temp_Address).First();
                        obj.PassWord = UserDetails.Select(x => x.PassWord).First();
                        obj.photo = imgName;
                        obj.FullPath = imgFullPath;

                    }
                    return obj;

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public IEnumerable<WeeklyAttendanceReport> GetWeeklyData(int numberOfDays)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    int BranchaID = 0;
                    GlobalArray gs = new GlobalArray();
                    //gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData == 0)
                    {
                        //BranchaID = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                        BranchaID = 1;
                    }
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@NumberOfDays", numberOfDays);
                    param.Add("@BranchId", BranchaID);
                    var WeekLyAttendanceReport = db.Query<WeeklyAttendanceReport>(sql: "[dbo].[WeeklyAttendanceReport]", param: param, commandType: CommandType.StoredProcedure);
                    var count = db.Query<WeeklyAttendanceReport>(sql: "[dbo].[GetAbsentListCount]", param: param, commandType: CommandType.StoredProcedure);
                    WeekLyAttendanceReport.FirstOrDefault().AbsentCount = count.FirstOrDefault().AbsentCount;
                    //  WeekLyAttendanceReport.FirstOrDefault().AbsentCount = WeekLyAttendanceReport.FirstOrDefault().AbsentCount - WeekLyAttendanceReport.FirstOrDefault().LeaveCount;
                    return WeekLyAttendanceReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        //procedure to get attendance details
        public IEnumerable<AttendanceDetails> GetAttendanceDetails(string dataFor, string reportFor)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    int numberOfDays = 0;
                    if (dataFor == "monthly")
                    {
                        numberOfDays = 30;
                    }
                    if (dataFor == "Daily")
                    {
                        numberOfDays = 0;

                    }
                    if (dataFor == "weekly")
                    {
                        numberOfDays = 7;
                    }
                    int BranchId = 0;
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData == 0)
                    {
                        //BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                        BranchId = 1;
                    }

                    var param = new DynamicParameters();
                    param.Add("@NumberOfDays", numberOfDays);
                    param.Add("@BranchId", BranchId);
                    var WeekLyAttendanceReport = db.Query<AttendanceDetails>(sql: "[dbo].[AttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);
                    if (reportFor == "lateIn")
                    {
                        WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.LateIn == 1);
                    }
                    if (reportFor == "earlyOut")
                    {
                        WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EarlyOut == 1 && x.OutTime != null);

                        //WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EarlyOut == 1 || (x.OutTime == null && x.AllowedEarlyTime != null));
                    }
                    return WeekLyAttendanceReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public IEnumerable<AbsentEOLI> GetAbsentLIEO(string dataFor)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    int numberOfDays = 0;
                    if (dataFor == "monthly")
                    {
                        numberOfDays = 30;
                    }
                    //if (dataFor == "Daily")
                    //{
                    //    numberOfDays = 0;

                    //}
                    if (dataFor == "weekly")
                    {
                        numberOfDays = 7;
                    }

                    var param = new DynamicParameters();
                    param.Add("@NumberOfDays", numberOfDays);
                    param.Add("@BranchId", 0);
                    var countAbs = db.Query<AbsentEOLI>(sql: "[dbo].[GetEarlyOutLateInAbsent]", param: param, commandType: CommandType.StoredProcedure);
                    return countAbs.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public IEnumerable<AttendanceDetails> GetAbsent(string dataFor)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    int numberOfDays = 0;
                    if (dataFor == "monthly")
                    {
                        numberOfDays = 30;
                    }
                    //if (dataFor == "Daily")
                    //{
                    //    numberOfDays = 0;

                    //}
                    if (dataFor == "weekly")
                    {
                        numberOfDays = 7;
                    }
                    int BranchId = 0;
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData == 0)
                    {
                       // BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                        BranchId = 1;
                    }

                    var param = new DynamicParameters();
                    param.Add("@day", numberOfDays);
                    param.Add("@BranchId", BranchId);
                    var WeekLyAttendanceReport = db.Query<AttendanceDetails>(sql: "[dbo].[GetAbsentList]", param: param, commandType: CommandType.StoredProcedure);
                    return WeekLyAttendanceReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public IEnumerable<AttendanceDetails> GetLeave(string dataFor)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection())
            {
                try
                {
                    db.Open();
                    int numberOfDays = 0;
                    if (dataFor == "monthly")
                    {
                        numberOfDays = 30;
                    }
                    if (dataFor == "Daily")
                    {
                        numberOfDays = 0;

                    }
                    if (dataFor == "weekly")
                    {
                        numberOfDays = 7;
                    }

                    int BranchId = 0;
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData == 0)
                    {
                        //BranchId = Convert.ToInt32(HttpContext.Current.Session["BranchId"]);
                        BranchId = 1;
                    }


                    var param = new DynamicParameters();
                    param.Add("@NumberOfDays", numberOfDays);
                    param.Add("@BranchId", BranchId);
                    var WeekLyAttendanceReport = db.Query<AttendanceDetails>(sql: "[dbo].[LeaveDetails]", param: param, commandType: CommandType.StoredProcedure);

                    return WeekLyAttendanceReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public ValidityDetails GetValidityDetails(string DateType,string companycode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<ValidityDetails>("select cast(DATEDIFF(day, GETDATE(), ValidityTill) as int) AS ValidateRenaining, cast(DATEDIFF(day,ValidityFrom, ValidityTill) as int)  as TotaLdays ,ValidityTill from [RealTime].[dbo].Companies where CompanyCode="+companycode+"",  commandType: CommandType.Text);
                    db.Close();
                    var b = Convert.ToString(data.FirstOrDefault().ValidityTill);
                    if (Convert.ToString(data.FirstOrDefault().ValidityTill)!= "1/1/0001 12:00:00 AM")
                    {
                        //int percentRemain = (data.FirstOrDefault().ValidateRenaining / data.FirstOrDefault().TotaLdays) * 100;
                        //double num3 = (double)data.FirstOrDefault().ValidateRenaining / (double)data.FirstOrDefault().TotaLdays;
                        //num3 = num3 * 100;
                        //data.FirstOrDefault().Percentage = Convert.ToInt32(num3);

                        if (DateType == "Nepali")
                        {

                            var StartDate = NepDateConverter.EngToNep(Convert.ToInt32(data.FirstOrDefault().ValidityTill.Year), Convert.ToInt32(data.FirstOrDefault().ValidityTill.Month), Convert.ToInt32(data.FirstOrDefault().ValidityTill.Day));
                            data.FirstOrDefault().ValidateDateNepali = StartDate.ToString();
                        }
                    }
                    
                   
                    return data.FirstOrDefault();




                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void GetImagePath(int EmployeeId)
        {
            try
            {
                Employee obj = new Employee();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", EmployeeId);
                    var Employeelist = db.Query<Employee>(sql: "[dbo].[GetImageByEmpId]", param: param, commandType: CommandType.StoredProcedure);
                    if (Employeelist.Count() > 0)
                    {
                        //HttpContext.Current.Session["ImgPath"] = "\\" + Employeelist.Select(x => x.PhotoPath).First();
                    }
                    db.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public IEnumerable<Employee> GetEmpListForDeptByShift(int id, bool isDualShift)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    if (isDualShift == false)
                    {

                        if (id == 0)
                        {
                            var data = db.Query<Employee>(@"select E.Id,Emp_Name,Email from Employee e
		                             left join EmployeeShift as es on e.Id = es.Emp_Id
		                              where Is_Active= 1 and Emp_DeviceCode !=0 and (IsDualShift = 0 or IsDualShift Is  Null)", param: new { @Id = id }, commandType: CommandType.Text);
                            db.Close();

                            return data.ToList();
                        }
                        else
                        {
                            var data = db.Query<Employee>(@"select E.Id,Emp_Name,Email from Employee e
                                      left join EmployeeShift as es on e.Id = es.Emp_Id
                                       where Is_Active = 1 and Emp_DeviceCode != 0 and(IsDualShift = 0 or IsDualShift Is  Null) and Department_Id = @Id and  Is_Active=1 and Emp_DeviceCode!=0 ", param: new { @Id = id }, commandType: CommandType.Text);
                            db.Close();

                            return data.ToList();
                        }

                    }
                    else
                    {
                        if (id == 0)
                        {
                            var data = db.Query<Employee>(@"select E.Id,Emp_Name,Email from Employee e
		                             left join EmployeeShift as es on e.Id = es.Emp_Id
		                              where Is_Active= 1 and Emp_DeviceCode !=0 and (IsDualShift = 1)", param: new { @Id = id }, commandType: CommandType.Text);
                            db.Close();

                            return data.ToList();
                        }
                        else
                        {
                            var data = db.Query<Employee>(@"select E.Id,Emp_Name,Email from Employee e
                                      left join EmployeeShift as es on e.Id = es.Emp_Id
                                       where Is_Active = 1 and Emp_DeviceCode != 0 and(IsDualShift = 1) and Department_Id = @Id and  Is_Active=1 and Emp_DeviceCode!=0 ", param: new { @Id = id }, commandType: CommandType.Text);
                            db.Close();

                            return data.ToList();
                        }
                    }

                    // var result = db.Execute("select Id,Emp_Name from Employee where Department_Id = @Id and  Is_Active=1 and EmailVerified=1", param: new { @Id = id }, commandType: CommandType.Text);





                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public IEnumerable<DeviceStatusReport> GetDeviceStatusList()
        {

            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var manualAttendanceList = db.Query<DeviceStatusReport>(sql: "[dbo].[GetDeviceStatusList]", commandType: CommandType.StoredProcedure);
                    db.Close();

                    return manualAttendanceList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<PoliticalNonPoliticalEmployee> GetPoliticalNonPoliticalEmployee()
        {

            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var manualAttendanceList = db.Query<PoliticalNonPoliticalEmployee>(sql: "[dbo].[GetPoliticalNonPoliticalEmployee]", commandType: CommandType.StoredProcedure);
                    db.Close();

                    return manualAttendanceList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void EmployeeDetailsInfo()
            {


                try
                {
                   // var  empid=  HttpContext.Current.Session["UserId"];
                var empid = 1;
                    var dbfactory = DbFactoryProvider.GetFactory();
                    using (var db = (DbConnection)dbfactory.GetConnection())
                    {
                        db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", empid);
                    var manualAttendanceList = db.Query<Employee>(sql: "[dbo].[GetEmployeeDetailByEmpId]", param: param, commandType: CommandType.StoredProcedure);
                   
                      var data=   manualAttendanceList.ToList();
                  if(  data.Count>0)
                    {
                        //HttpContext.Current.Session["Department"] = data.Select(x => x.DepartmentName).First();
                        //HttpContext.Current.Session["Designation"] = data.Select(x => x.DesignationName).First();
                        //HttpContext.Current.Session["Address"] = data.Select(x => x.Permanet_Address).First();
                        //HttpContext.Current.Session["Email"] = data.Select(x => x.Email).First();
                        //HttpContext.Current.Session["Mobile_No"] =data.Select(x => x.Mobile_No).First();
                    }
                     
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


        }
}