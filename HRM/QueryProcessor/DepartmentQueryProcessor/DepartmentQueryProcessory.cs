﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;




namespace HRM
{
    public class DepartmentHRMy : IDepartmentHRM
    {
        public int Create(Department department)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    if (department.DepartmentCode == null)
                    {
                       // department.DepartmentCode = HttpContext.Current.Session["Code"].ToString();
                        department.DepartmentCode = "001";
                    }
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", department.Id);

					param.Add("@Event", department.Event);
                    param.Add("@DepartmentCode", department.DepartmentCode);
                    param.Add("@DepartmentName", department.DepartmentName);
                    param.Add("@NoOfStaff", department.NoOfStaff);
                    param.Add("@IsDeleted", department.IsDeleted);
                    param.Add("@CreateBy", department.CreateBy);
                    param.Add("@CreateDate", department.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Departments]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Departments] SET [IsDeleted] = 1 Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            //throw new NotImplementedException();
        }

        public IEnumerable<DepartmentViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var tasklist = db.Query<DepartmentViewModel>(sql: "[dbo].[GetDepartmentList]", commandType: CommandType.StoredProcedure);
                    return tasklist.ToList();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // throw new NotImplementedException();
        }

        public Department GetById(int id)
        {
            try
            {
                Department obj = new Department();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var departmentlist = db.Query<DepartmentViewModel>(sql: "[dbo].[GetDepartmentById]", param: param, commandType: CommandType.StoredProcedure);
                    if (departmentlist.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(departmentlist.Select(x => x.Id).First());
                        obj.DepartmentName = departmentlist.Select(x => x.DepartmentName).First().ToString();
                        obj.DepartmentCode = departmentlist.Select(x => x.DepartmentCode).First().ToString();
                        obj.NoOfStaff = departmentlist.Select(x => x.NoOfStaff).First();
                    }
                    db.Close();
                    return obj;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //throw new NotImplementedException();
        }

		public int GetCode(string code, int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
                    var data = 0;
					db.Open();
                    if (id == 0)
                    {
                        data = db.Query<Department>("SELECT DepartmentCode FROM[dbo].[Departments] where DepartmentCode = @Code and IsDeleted=0", param: new { Code = code }, commandType: CommandType.Text).Count();
                    }
                    else
                    {
                        data = db.Query<Department>("SELECT DepartmentCode FROM[dbo].[Departments] where DepartmentCode = @Code AND Id != @id", param: new { Code = code, Id= id }, commandType: CommandType.Text).Count();
                    }
					db.Close();
					return data;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}