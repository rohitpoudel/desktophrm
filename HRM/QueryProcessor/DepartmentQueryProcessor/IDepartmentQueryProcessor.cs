﻿
using System.Collections.Generic;

namespace HRM
{
    public interface IDepartmentHRM
    {

        int Create(Department department);
        IEnumerable<DepartmentViewModel> GetAllData();
        Department GetById(int id);
        bool Delete(int id);
		int GetCode(string code, int id);

    }
}
