﻿
using System.Collections.Generic;


namespace HRM.GradeGroupHRM
{
   public interface IGradeGroupHRM
    {
        int Create(GradeGroup gradeGroup);
        IEnumerable<GradeGroupViewModel> GetAllData();
        GradeGroup GetById(int id);
        bool Delete(int id);
		int GetCode(string code, int id);
	}
}
