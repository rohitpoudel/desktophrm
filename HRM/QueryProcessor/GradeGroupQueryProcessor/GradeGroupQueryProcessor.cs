﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;




namespace HRM.GradeGroupHRM
{
    public class GradeGroupHRM : IGradeGroupHRM
    {
        public int Create(GradeGroup gradeGroup)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    if (gradeGroup.GradeGroupCode == null)
                    {
                        // gradeGroup.GradeGroupCode = HttpContext.Current.Session["Code"].ToString();
                        gradeGroup.GradeGroupCode = "001";
                    }
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", gradeGroup.Id);
                    param.Add("@Event", gradeGroup.Event);
                    param.Add("@GradeGroupCode", gradeGroup.GradeGroupCode);
                    param.Add("@GroupName", gradeGroup.GroupName);
                    param.Add("@Value", gradeGroup.Value);
                    param.Add("@IsDeleted", gradeGroup.IsDeleted);
                    param.Add("@CreateBy", gradeGroup.CreateBy);
                    param.Add("@CreateDate", gradeGroup.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_GradeGroups]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int GradeGruopCount = db.Query<int>("Select Count(*) GradeGruopCount from Employee where GradeGroup = @GradeId", param: new { GradeId = id }, commandType: CommandType.Text).SingleOrDefault();
                    if(GradeGruopCount < 1)
                    {
                        int result = db.Execute("UPDATE [dbo].[GradeGroups] SET [IsDeleted] = 1 Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                        if (result > 0)
                        {
                            return true;
                        }
                    }
                    db.Close();
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        public IEnumerable<GradeGroupViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var gradeGrouplist = db.Query<GradeGroupViewModel>(sql: "[dbo].[GetGradeGroupList]", commandType: CommandType.StoredProcedure);
					db.Close();
                    return gradeGrouplist.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GradeGroup GetById(int id)
        {
            try
            {
                GradeGroup obj = new GradeGroup();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var gradeGroupList = db.Query<GradeGroupViewModel>(sql: "[dbo].[GetGradeGroupById]", param: param, commandType: CommandType.StoredProcedure);
                    if (gradeGroupList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(gradeGroupList.Select(x => x.Id).First());
                        obj.GroupName = gradeGroupList.Select(x => x.GroupName).First().ToString();
                        obj.GradeGroupCode = gradeGroupList.Select(x => x.GradeGroupCode).First().ToString();
                        obj.Value = gradeGroupList.Select(x => x.Value).First();                    
                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }        
        }
		public int GetCode(string code, int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();

					var data = db.Query<GradeGroup>("SELECT GradeGroupCode FROM[dbo].[GradeGroups] where GradeGroupCode = @Code and id!=@id AND IsDeleted = 0", param: new { @Code = code, @id = id }, commandType: CommandType.Text);
					var s = data.Count();

					db.Close();
					return s;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

	}
}