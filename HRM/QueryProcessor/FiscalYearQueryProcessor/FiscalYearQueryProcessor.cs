﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using System.Data;


using System.Web;

namespace HRM
{
    public class FiscalYearQueryProcessor : IFiscalYearQueryProcessor
    {

        public int Create(FiscalYear fiscalYear)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Event", fiscalYear.Event);
                    param.Add("@Id", fiscalYear.Id);
                    param.Add("@YearNepali", fiscalYear.YearNepali);

                    param.Add("@Year", fiscalYear.Year);
                    param.Add("@CurrentFiscalYear", fiscalYear.CurrentFiscalYear);
                    param.Add("@StartDate", fiscalYear.StartDate);
                    param.Add("@EndDate", fiscalYear.EndDate);
                    param.Add("@StartDateNepali", fiscalYear.StartDateNepali);
                    param.Add("@EndDateNepali", fiscalYear.EndDateNepali);
                    param.Add("@IsActive", fiscalYear.IsActive);
                    param.Add("@CreateBy", fiscalYear.CreateBy);
                    param.Add("@CreateDate", fiscalYear.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_FiscalYears]", param: param, commandType: CommandType.StoredProcedure);

                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[FiscalYears] SET [IsActive]=@IsActive Where Id=@Id", param: new { @IsActive = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<FiscalYearViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var fiscalYearlist = db.Query<FiscalYearViewModel>(sql: "[dbo].[Usp_GetFiscalYearList]", commandType: CommandType.StoredProcedure);
                    return fiscalYearlist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public FiscalYear GetById(int id)
        {
            try
            {
                FiscalYear obj = new FiscalYear();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var fiscalYearlist = db.Query<FiscalYearViewModel>(sql: "[dbo].[Usp_GetFiscalYearById]", param: param, commandType: CommandType.StoredProcedure);
                    if (fiscalYearlist.Count() > 0)
                    {
                        //var dateFormat = (string)HttpContext.Current.Session["DatePicker"];
                        var dateFormat = "";
                        obj.Id = Convert.ToInt32(fiscalYearlist.Select(x => x.Id).First());
                        obj.YearNepali = fiscalYearlist.Select(x => x.YearNepali).First().ToString();

                        obj.Year = fiscalYearlist.Select(x => x.Year).First().ToString();
                        obj.CurrentFiscalYear = fiscalYearlist.Select(x => x.CurrentFiscalYear).First();
                        obj.StartDate = Convert.ToDateTime(fiscalYearlist.Select(x => x.StartDate).First()).ToString("yyyy-MM-dd");
                        obj.EndDate = Convert.ToDateTime(fiscalYearlist.Select(x => x.EndDate).First()).ToString("yyyy-MM-dd");
                        obj.EndDateNepali = fiscalYearlist.Select(x => x.EndDateNepali).First();
                        obj.StartDateNepali = fiscalYearlist.Select(x => x.StartDateNepali).First();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int CurrentFiscalExist()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = 0;

                    var GetYear = db.Query<FiscalYear>("select Year from FiscalYears where CurrentFiscalYear=1 and  IsActive=1", commandType: CommandType.Text);
                    result = GetYear.Count();

                    db.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetYear(string data, int id, string dateFormat)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = 0;
                    if (dateFormat == "True")
                    {

                        var GetYear = db.Query<FiscalYear>("Select Year from FiscalYears where IsActive = 1 and YearNepali = @Year and Id!=@id", param: new { Year = data, Id = id }, commandType: CommandType.Text);
                        result = GetYear.Count();
                    }
                    else
                    {
                        var GetYear = db.Query<FiscalYear>("Select Year from FiscalYears where IsActive = 1 and Year = @Year and Id!=@id", param: new { Year = data, Id = id }, commandType: CommandType.Text);
                        result = GetYear.Count();

                    }
                    db.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<FiscalYearViewModel> GetAllDataForSearch(string searchString)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@searchString", searchString);
                    var fiscalYearlist = db.Query<FiscalYearViewModel>(sql: "[dbo].[Usp_GetFiscalYearListForSearch]", param: param, commandType: CommandType.StoredProcedure);
                    return fiscalYearlist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}