﻿
using System.Collections.Generic;
namespace HRM
{
   public interface IFiscalYearQueryProcessor
    {


        /// <summary>
        /// Create the fiscal Year
        /// </summary>
        /// <param name="fiscalYear">Fiscal Year Object</param>
        /// <returns>return integer</returns>
        int Create(FiscalYear fiscalYear);
        /// <summary>
        /// list All the Fiscal Year
        /// </summary>
        /// <returns>Inumerable Identifier</returns>
        IEnumerable<FiscalYearViewModel> GetAllData();
        /// <summary>
        /// Get Fiscal Year List by Id
        /// </summary>
        /// <param name="id">Fiscal Year Id Identifier</param>
        /// <returns>Inumerable Identfier </returns>
        FiscalYear GetById(int id);
        /// <summary>
        /// Delete the fiscal year by id
        /// </summary>
        /// <param name="id">FiscaL Year Id Identifier</param>
        /// <returns>rerturns identifier</returns>
        bool Delete(int id);
        /// <summary>
        /// Get Fiscal Year Used on the other module
        /// </summary>
        /// <param name="data"> List of Year</param>
        /// <param name="id">Fiscal Year Id Identifier</param>
        /// <param name="dateFormat">Date format date</param>
        /// <returns>Return Integer</returns>
        int GetYear(string data, int id,string dateFormat);
        /// <summary>
        /// Check the Existance of the current Year
        /// </summary>
        /// <returns>Return Integer</returns>
        int CurrentFiscalExist();

        IEnumerable<FiscalYearViewModel> GetAllDataForSearch(string searchString );
        
    }
}
