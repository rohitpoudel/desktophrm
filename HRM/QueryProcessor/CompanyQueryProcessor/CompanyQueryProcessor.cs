﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace HRM
{
    public class CompanyHRM : ICompanyHRM
    {

        int ICompanyHRM.Create(Company company)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", company.Id);
                    param.Add("@Event", company.Event);
                    param.Add("@CompanyName", company.CompanyName);
                    param.Add("@CompanyCode", company.CompanyCode);
                    param.Add("@CompanyAddress", company.CompanyAddress);
                    param.Add("@Email", company.Email);
                    param.Add("@PhoneNo", company.PhoneNo);
                    param.Add("@ValidityFromNepali", company.FromDateNepali);
                    param.Add("@ValidityTillNepali", company.ToDateNepali);
                    param.Add("@ValidityFrom", company.FromDate);
                    param.Add("@ValidityTill", company.ToDate);
                    param.Add("@Web", company.Web);
                    param.Add("@Logo", company.Logo);
                    param.Add("@IsDeleted", company.IsDeleted);
                    param.Add("@CreateBy", company.CreateBy);
                    param.Add("@CreateDate", company.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Companies]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CompanyDatabaseInfo GetCompanyByCode(string code)
        {
            try
            {
                CompanyDatabaseInfo obj = new CompanyDatabaseInfo();
                var dbfactory = DbFactoryProvider.GetFactory();
                // using (var db = (DbConnection)dbfactory.GetConnection())
                // {
                var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                var param = new DynamicParameters();
                db.Open();
                param.Add("@C_Code", code);
                var companyDbDetails = db.Query<CompanyDatabaseInfoViewModel>(sql: "[dbo].[GetCompaniesInfoByCode]", param: param, commandType: CommandType.StoredProcedure);
                if (companyDbDetails.Count() > 0)
                {
                    obj.Company_Name = companyDbDetails.Select(x => x.CompanyName).First().ToString();
                  
                    

                }
                db.Close();
                return obj;

                //   }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public CompanyDatabaseInfo GetByCode(string code)
        {
            try
            {
                CompanyDatabaseInfo obj = new CompanyDatabaseInfo();
                var dbfactory = DbFactoryProvider.GetFactory();
                // using (var db = (DbConnection)dbfactory.GetConnection())
                // {
                var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                var param = new DynamicParameters();
                db.Open();
                param.Add("@C_Code", code);
                var companyDbDetails = db.Query<CompanyDatabaseInfoViewModel>(sql: "[dbo].[GetCompaniesDBInfoByCode]", param: param, commandType: CommandType.StoredProcedure);
                if (companyDbDetails.Count() > 0)
                {
                    obj.Company_Name = companyDbDetails.Select(x => x.Company_Name).First().ToString();
                    obj.DataSource = companyDbDetails.Select(x => x.DataSource).First().ToString();
                    obj.DB_Name = companyDbDetails.Select(x => x.DB_Name).First().ToString();
                    obj.Password = companyDbDetails.Select(x => x.Password).First().ToString();
                    obj.Db_UserName = companyDbDetails.Select(x => x.Db_UserName).First().ToString();

                }
                db.Close();
                return obj;

                //   }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Company GetById(int id)
        {
            try
            {
                Company obj = new Company();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var companyList = db.Query<CompanyViewModel>(sql: "[dbo].[GetCompaniesById]", param: param, commandType: CommandType.StoredProcedure);
                    var imgName = "";
                    var imgFullPath = "";
                    if ( companyList.FirstOrDefault().Logo != null)
                    {
                        imgFullPath = "\\" + companyList.FirstOrDefault().Logo;
                        var imageName = companyList.FirstOrDefault().Logo.Split('\\');
                        imgName = imageName[2];
                    }
                    if (companyList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(companyList.Select(x => x.Id).First());
                        obj.CompanyName = companyList.Select(x => x.CompanyName).First().ToString();
                        obj.CompanyCode = companyList.Select(x => x.CompanyCode).First().ToString();
                        obj.CompanyAddress = companyList.Select(x => x.CompanyAddress).First().ToString();
                        obj.Email = companyList.Select(x => x.Email).First().ToString();
                        obj.PhoneNo = companyList.Select(x => x.PhoneNo).First().ToString();                      
                        obj.Web = companyList.Select(x => x.Web).FirstOrDefault();
                        obj.FromDateNepali = companyList.Select(x => x.ValidityFromNepali).First().Substring(0,10);
                        obj.ToDateNepali = companyList.Select(x => x.ValidityTillNepali ).First().Substring(0, 10);
                        obj.FromDate = companyList.Select(x => x.ValidityFrom).First();

                        obj.ToDate = companyList.Select(x => x.ValidityTill).First();

                        // obj.Logo = companyList.Select(x => x.Logo).First().ToString();
                        obj.Logo = imgName;
                        obj.FullPath = imgFullPath;

                    }
                    db.Close();
                    return obj;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CompanyViewModel> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var companyList = db.Query<CompanyViewModel>(sql: "[dbo].[Usp_GetCompaniesList]", commandType: CommandType.StoredProcedure);
                    return companyList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Companies] SET [IsDeleted] = 1 Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetCode(string code,int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                  
                    var data = db.Query<Company>("SELECT CompanyCode FROM[dbo].[Companies] where CompanyCode = @Code and Id!=@id and IsDeleted = 0", param: new { Code = code,id=id }, commandType: CommandType.Text);
                    var number = data.Count();

                    db.Close();
                    return number;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
    
}