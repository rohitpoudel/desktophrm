﻿
using System.Collections.Generic;

namespace HRM
{
  public  interface ICompanyHRM
    {
        int Create(Company company);
        IEnumerable<CompanyViewModel> GetAllData();
        Company GetById(int id);
        bool Delete(int id);
        int GetCode(string code,int id);
        CompanyDatabaseInfo GetByCode(string code);
        CompanyDatabaseInfo GetCompanyByCode(string code);
       
    }
}
