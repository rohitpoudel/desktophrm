﻿using HRM.Menu;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Windows.Forms;

namespace HRM
{
    //public class MenuTemplateHRM : IMenuTemplateHRM
    //{
    //    IDbTransaction sqlTran;

    //    public TreeView GetMenuTemplateGroupTree(int? templateId, string filter = "")
    //    {
    //        var treelist = GetAllData().Where(x => x.IsEnable == true);
    //        List<MenuMain> list = treelist.ToList();

    //        if (filter.Trim() != "")
    //        {
    //            list = FilterTree(list, filter);
    //        }
    //        var list123 = GetAllMenuVsTemp();
    //        List<int> data = new List<int>();
    //        data.Add(193);
    //        data.Add(194);
    //        data.Add(195);
    //        data.Add(196);
    //        data.Add(197);
    //        data.Add(198);
    //        data.Add(199);
         
    //        var tree = this.GenerateTree(list, 0, templateId, list123.ToList(), data);
    //        return tree;
    //    }

    //    private List<MenuMain> FilterTree(List<MenuMain> list, string filter)
    //    {
    //        bool lLoop = true;

    //        var filteredList = list.Where(x => x.MenuCaption.ToLower().Contains(filter.ToLower())).ToList();

    //        while (lLoop)
    //        {
    //            //select all parents of filtered list
    //            var allParent = (from mainList in list
    //                             join selList in filteredList on mainList.MenuId equals selList.PMenuId
    //                             select mainList).ToList();

    //            //Select unique parent only
    //            var parentList = (from p in allParent
    //                              join c in filteredList on p.MenuId equals c.MenuId into gj
    //                              from uniqueParent in gj.DefaultIfEmpty()
    //                              where uniqueParent == null
    //                              select p).ToList();

    //            if (parentList.Count() == 0)
    //            {
    //                lLoop = false;
    //            }

    //            filteredList = filteredList.Union(parentList).OrderBy(x => x.MenuId).ToList();
    //        }
    //        list = filteredList;
    //        return list;

    //    }
    //    public IEnumerable<MenuVsTemplate> GetAllMenuVsTemp()
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                db.Open();
    //                var MenuVsTempList = db.Query<MenuVsTemplate>(sql: "[dbo].[Usp_GetMenuVsTemplateList]", commandType: CommandType.StoredProcedure);
    //                db.Close();
    //                return MenuVsTempList;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }
    //    List<int> templateList = new List<int>();

    //    public List<int> CheckedMenuTemplate(TreeView data)
    //    {

    //        foreach (var item in data.TreeData)
    //        {
    //            if (item.IsChecked == true)
    //            {
    //                templateList.Add(item.Id);
    //            }
    //            if (item.Children != null)
    //            {
    //                TemplateChild(item);
    //            }

    //        }
    //        return templateList;

    //    }
    //    private void TemplateChild(TreeDTO data)
    //    {
    //        foreach (var item in data.Children)
    //        {
    //            if (item.IsChecked == true)
    //            {
    //                templateList.Add(item.Id);

    //            }
    //            if (item.Children != null)
    //            {
    //                data.Children = item.Children;
    //                TemplateChild(data);
    //            }
    //        }

    //    }
    //    public int Create(MenuTemplate menuTemplate, DbConnection db, IDbTransaction tran)
    //    {
    //        try
    //        {



    //            var param = new DynamicParameters();
    //            param.Add("@Event", menuTemplate.Event);

    //            param.Add("@Id", menuTemplate.Id);
    //            param.Add("@RoleName", menuTemplate.RoleName);
    //            param.Add("@RoleNameNepali", null);
    //            param.Add("@DesignationId", null);
    //            param.Add("@IsActive", '1');

    //            param.Add("@CreatedBy", menuTemplate.CreatedBy);
    //            param.Add("@CreatedDate", menuTemplate.CreatedDate);

    //            param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
    //            db.Execute("[dbo].[Usp_IUD_UserRoleCreate]", param: param, transaction: tran, commandType: CommandType.StoredProcedure);
    //            return param.Get<Int16>("@Return_Id");


    //        }
    //        catch (Exception ex)
    //        {
    //            sqlTran.Rollback();
    //            throw ex;
    //        }
    //    }

    //    public void CreateMenuVsTemp(MenuVsTemplate menuVsTemplate, DbConnection db, IDbTransaction tran)
    //    {
    //        try
    //        {

    //            var param = new DynamicParameters();
    //            param.Add("@Event", menuVsTemplate.Event);

    //            param.Add("@MenuId", menuVsTemplate.MenuId);
    //            param.Add("@TemplateId", menuVsTemplate.TemplateId);
    //            param.Add("@MenuTemplate_MTId", null);



    //            db.Execute("[dbo].[Usp_IUD_MenuVsTemplate]", param: param, transaction: tran, commandType: CommandType.StoredProcedure);
               

    //        }
    //        catch (Exception ex)
    //        {
    //            sqlTran.Rollback();
    //            throw ex;
    //        }
    //    }

    //    public void Save(MenuTemplate TemplateData, TreeView data)
    //    {
    //        int checkExists = GetAllMenuTemp().Where(x => x.Id != TemplateData.Id && x.RoleName == TemplateData.RoleName).Count();
    //        if (checkExists > 0)
    //        {
    //            throw new Exception("Duplicate MenuTemplate Found. MenuTemplate Caption Not Valid");
    //        }
    //        var templateList = CheckedMenuTemplate(data);
    //        if (templateList.Count == 0)
    //        {

    //            throw new Exception("Please Assign Menu");
    //        }

    //        try
    //        {
    //            try
    //            {
    //                var dbfactory = DbFactoryProvider.GetFactory();
    //                using (var db = (DbConnection)dbfactory.GetConnection())
    //                {
    //                    db.Open();
    //                    TemplateData.CreatedDate = DateTime.Now;
    //                    TemplateData.CreatedBy = 1;
    //                    sqlTran = db.BeginTransaction();

    //                    if (TemplateData.Id == 0)
    //                    {
    //                        TemplateData.Event = 'I';
                          
    //                        int roleId = Create(TemplateData, db, sqlTran);
    //                        foreach (var item in templateList)
    //                        {


    //                            MenuVsTemplate menuvsTemplate = new MenuVsTemplate();
    //                            menuvsTemplate.Event = 'I';
    //                            menuvsTemplate.MenuId = item;
    //                            menuvsTemplate.TemplateId = roleId;
    //                            CreateMenuVsTemp(menuvsTemplate, db, sqlTran);
    //                        }
    //                    }
    //                    else
    //                    {
    //                        TemplateData.Event = 'U';

    //                        int roleId = Create(TemplateData, db, sqlTran);
    //                        foreach (var item in templateList)
    //                        {
    //                            if (item==3273)
    //                            {

    //                            }
    //                            var menuvsTemplate = new MenuVsTemplate();
    //                            menuvsTemplate.Event = 'I';
    //                            menuvsTemplate.MenuId = item;
    //                            menuvsTemplate.TemplateId = roleId;
    //                            CreateMenuVsTemp(menuvsTemplate, db, sqlTran);
    //                        }
    //                    }

    //                    sqlTran.Commit();
    //                    db.Close();

    //                }
    //            }
    //            catch (Exception )
    //            {
    //                sqlTran.Rollback();
    //            }
    //        }



    //        catch (Exception )
    //        {

    //        }

    //    }
    //    public int GetRoleName(string roleName, int id)
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                var param = new DynamicParameters();
    //                db.Open();

    //                var data = db.Query<MenuTemplate>("select RoleName from UserRole where RoleName =@roleName  and Id!= @id", param: new { roleName = roleName, id = id }, commandType: CommandType.Text);
    //                var number = data.Count();

    //                db.Close();
    //                return number;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }
    //    public bool Delete(int MTId)
    //    {
    //        var MenuTemplate = this.GetAllMenuTemp().Where(x=>x.Id==MTId).Single();

    //        if (MenuTemplate != null)
    //        {
    //            try
    //            {
    //                var dbfactory = DbFactoryProvider.GetFactory();
    //                using (var db = (DbConnection)dbfactory.GetConnection())
    //                {

    //                    db.Open();
    //                    var param = new DynamicParameters();
    //                    param.Add("@id", MTId);

    //                    db.Execute("[dbo].[Usp_DeleteMenuTemplateById]", param: param, commandType: CommandType.StoredProcedure);

    //                    db.Close();

    //                }
    //            }
    //            catch (Exception ex)
    //            {

    //                throw ex;

    //            }
    //            //uow.Repository<MenuTemplate>().Delete(MenuTemplate);
    //            //uow.Commit();
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    public IEnumerable<MenuTemplate> GetAllMenuTemp()
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                db.Open();
    //                var MenuTempList = db.Query<MenuTemplate>(sql: "[dbo].[Usp_GetMenuTemplateList]", commandType: CommandType.StoredProcedure);
    //                db.Close();
    //                return MenuTempList;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }

    //    public IEnumerable<MenuMain> GetAllData()
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                db.Open();
    //                var MenuList = db.Query<MenuMain>(sql: "[dbo].[Usp_GetMenuList]", commandType: CommandType.StoredProcedure);
    //                db.Close();
    //                DataTable dt = new DataTable();
    //                // dt = ToDataTable(MenuList.ToList());
    //                return MenuList.ToList();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }
    //    private TreeView GenerateTree(List<MenuMain> list, int? parentMTId, int? templateId, List<MenuVsTemplate> list123,List<int> data)
    //    {
    //        int? menuTemplateId = templateId;
    //        var parent = list.Where(x => x.PMenuId == parentMTId);
    //        var tree = new TreeView();
    //        tree.Title = "MenuTemplate";
            
    //        var pr = parent.ToArray();
    //        for (int i = 0; i < parent.Count(); i++)
    //        {
    //            if (pr[i].MenuId == 62 || data.Contains(pr[i].MenuId) )
    //            {

    //                //int userId = (int)HttpContext.Current.Session["UserId"];
    //                //var isSA = HttpContext.Current.Session["IsSuperAdmin"];

    //                int userId = 1;
    //                var isSA = true;
    //                if (isSA.ToString() == "True")
    //                {
    //                    userId = 1;
    //                }
    //                if (userId == 1)
    //                {
    //                    tree.TreeData.Add(new TreeDTO
    //                    {
    //                        Id = pr[i].MenuId,
    //                        PId = pr[i].PMenuId,
    //                        Text = pr[i].MenuCaption,
    //                        IsGroup = pr[i].IsGroup,
    //                        IsChecked = IsChecked(pr[i], templateId, list123),
    //                        Image = pr[i].Image,

    //                    });
    //                }

    //            }
    //            else
    //            {
    //                tree.TreeData.Add(new TreeDTO
    //                {
    //                    Id = pr[i].MenuId,
    //                    PId = pr[i].PMenuId,
    //                    Text = pr[i].MenuCaption,
    //                    IsGroup = pr[i].IsGroup,
    //                    IsChecked = IsChecked(pr[i], templateId, list123.ToList()),
    //                    Image = pr[i].Image,

    //                });
    //            }
    //        }
    //        //foreach (var itm in parent)
    //        //{
    //        //    if (itm.MenuId == 62)
    //        //    {
    //        //        int userId = (int)HttpContext.Current.Session["UserId"];
    //        //        if (userId == 1)
    //        //        {
    //        //            tree.TreeData.Add(new TreeDTO
    //        //            {
    //        //                Id = itm.MenuId,
    //        //                PId = itm.PMenuId,
    //        //                Text = itm.MenuCaption,
    //        //                IsGroup = itm.IsGroup,
    //        //                IsChecked = IsChecked(itm, templateId, list123.ToList()),
    //        //                Image = itm.Image,

    //        //            });
    //        //        }

    //        //    }
    //        //    else
    //        //    {
    //        //        tree.TreeData.Add(new TreeDTO
    //        //        {
    //        //            Id = itm.MenuId,
    //        //            PId = itm.PMenuId,
    //        //            Text = itm.MenuCaption,
    //        //            IsGroup = itm.IsGroup,
    //        //            IsChecked = IsChecked(itm, templateId, list123.ToList()),
    //        //            Image = itm.Image,

    //        //        });
    //        //    }

    //        //}
    //        for (int i = 0; i < tree.TreeData.Count(); i++)
    //        {
    //            tree.TreeData.ToArray()[i].Children = GenerateTree(list, tree.TreeData.ToArray()[i].Id, templateId,list123,data).TreeData.ToList();

    //        }
    //        //foreach (var itm in tree.TreeData)
    //        //{
    //        //    itm.Children = GenerateTree(list, itm.Id, templateId).TreeData.ToList();
    //        //}
    //        return tree;
    //    }

    //    private bool IsChecked(MenuMain data, int? templateId,List<MenuVsTemplate> list)
    //    {
    //        var checkedList = list.Where(x => x.TemplateId == templateId);
    //        var returnVal = false;
    //        var chk = checkedList.ToArray();
    //        for (int i = 0; i < chk.Count(); i++)
    //        {
    //            if (chk[i].MenuId == data.MenuId)
    //            {
    //                returnVal = true;
    //            }
    //        }
    //        //foreach (var item in checkedList)
    //        //{
    //        //    if (item.MenuId == data.MenuId)
    //        //    {
    //        //        returnVal = true;
    //        //    }

    //        //}
    //        return returnVal;
    //    }
    //}
}