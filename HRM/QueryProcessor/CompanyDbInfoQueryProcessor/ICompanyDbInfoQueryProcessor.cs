﻿
using System.Collections.Generic;


namespace HRM
{
	public interface ICompanyDbInfoHRM
	{
		int Create(CompanyDatabaseInfo obj);
		IEnumerable<CompanyDatabaseInfo> GetAllData();
		CompanyDatabaseInfo GetById(int id);
        int CheckCompanyCode(int code);
        int CheckCompanyName(string name);
    }
}
