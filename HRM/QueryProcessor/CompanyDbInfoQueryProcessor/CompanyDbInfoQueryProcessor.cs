﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
	public class CompanyDbInfoHRM : ICompanyDbInfoHRM
	{
		public int Create(CompanyDatabaseInfo obj)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					//db.OpenAsync();
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Id", obj.Id);
					//param.Add("@Event", obj.Event);
					param.Add("@Company_Code", obj.Company_Code);
					param.Add("@Company_Name", obj.Company_Name);
					param.Add("@DB_Name", obj.DB_Name);
					param.Add("@Db_UserName", obj.Db_UserName);
					param.Add("@Password", obj.Password);
					param.Add("@DataSource", obj.DataSource);
					param.Add("@Create_By", obj.Create_By);
					param.Add("@Created_Date", obj.Created_Date);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_Companies_DBInfo]", param: param, commandType: CommandType.StoredProcedure);
					db.Close();
					return param.Get<Int16>("@Return_Id");
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<CompanyDatabaseInfo> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var companyDbList = db.Query<CompanyDatabaseInfo>(sql: "[dbo].[GetCompanyDbList]", commandType: CommandType.StoredProcedure);
					return companyDbList.ToList();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public CompanyDatabaseInfo GetById(int id)
		{
			try
			{
				CompanyDatabaseInfo obj = new CompanyDatabaseInfo();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var companyList = db.Query<CompanyDatabaseInfo>(sql: "[dbo].[GetCompanyDbById]", param: param, commandType: CommandType.StoredProcedure);
					if (companyList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(companyList.Select(x => x.Id).First());
						obj.Company_Name = companyList.Select(x => x.Company_Name).First().ToString();
						obj.Company_Code = companyList.Select(x => x.Company_Code).First().ToString();
						obj.DB_Name = companyList.Select(x => x.DB_Name).First().ToString();
						obj.Db_UserName = companyList.Select(x => x.Db_UserName).First().ToString();
						obj.Password = General.Encrypt(companyList.Select(x => x.Password).First().ToString());
						obj.DataSource = companyList.Select(x => x.DataSource).First().ToString();


					}
					db.Close();
					return obj;

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public int CheckCompanyCode(int code)
        {
                try
                {
                    var dbfactory = DbFactoryProvider.GetFactory();
                    using (var db = (DbConnection)dbfactory.GetConnection())
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        var data = db.Query<CompanyDatabaseInfo>("SELECT Company_Code FROM[dbo].[Companies_DBInfo] where Company_Code = @Code ", param: new { Code = code}, commandType: CommandType.Text);
                        var s = data.Count();
                        db.Close();
                        return s;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }


        public int CheckCompanyName(string name)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    var data = db.Query<CompanyDatabaseInfo>("SELECT DB_Name FROM[dbo].[Companies_DBInfo] where DB_Name = @name", param: new { Name = name}, commandType: CommandType.Text);                   
                    var s = data.Count();                   
                    db.Close();
                    return s;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}