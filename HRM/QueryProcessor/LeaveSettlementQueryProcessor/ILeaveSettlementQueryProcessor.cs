﻿
using System.Collections.Generic;


namespace HRM
{
	public interface ILeaveSettlementHRM
	{
		int Create(LeaveSettlement obj);
		IEnumerable<LeaveSettlement> GetAllData();
		LeaveSettlement GetById(int id);
		bool Delete(int id);
		LeaveSettlement GetByEmpAndLeave(string empId, string leaveId);
		int GetSettlementByEmpIdAndLeaveId(int empId, int leaveId,int id);
		IEnumerable<LeaveSettlement> GetLeaveByFiscal(string fiscalYearId);
	}
}
