﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
	public class LeaveSettlementHRM : ILeaveSettlementHRM
	{
		public int Create(LeaveSettlement obj)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Event", obj.Event);
					param.Add("@Id", obj.Id);
					param.Add("@EmployeeId", obj.EmployeeId);
					param.Add("@LeaveMasterId", obj.LeaveMasterId);
					param.Add("@OpeningBalance", obj.OpeningBalance);
					param.Add("@LeaveTaken", obj.LeaveTaken);
					param.Add("@RemainingLeave", obj.RemainingLeave);
					param.Add("@CarryToNext", obj.CarryToNext);
					param.Add("@Pay", obj.Pay);
					param.Add("@SettlingLeave", obj.SettlingLeave);
					param.Add("@FiscalYearId", obj.FiscalYearId);
					
					param.Add("@CreatedBy", obj.CreatedBy);
					param.Add("@CreatedDate",obj.CreatedDate);
					param.Add("@Remarks", obj.Remarks);
					param.Add("@IsDeleted", obj.IsDeleted);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_LeaveSettlement]", param: param, commandType: CommandType.StoredProcedure);

					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE[dbo].[LeaveSettlement] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;

				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<LeaveSettlement> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var leaveSettlementList = db.Query<LeaveSettlement>(sql: "[dbo].[GetLeaveSettlementList]", commandType: CommandType.StoredProcedure);
					return leaveSettlementList.ToList();

				}


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public LeaveSettlement GetByEmpAndLeave(string empId, string leaveId)
		{
			try
			{
				LeaveSettlement obj = new LeaveSettlement();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@empId", empId);
					param.Add("@leaveId", leaveId);
					var infoList = db.Query<LeaveSettlement>(sql: "[dbo].[GetLeaveSettlementInfo]", param: param, commandType: CommandType.StoredProcedure);
					if (infoList.Count() > 0)
					{
						obj.RemainingLeave = infoList.Select(x => x.RemainingLeave).First();

						foreach (var info in infoList)
						{
							obj.LeaveTaken = obj.LeaveTaken + info.LeaveTaken;
							if (info.IsCarryable == true)
							{
								obj.CarryToNext = obj.RemainingLeave;
							}
							if (info.IsPaid == true)
							{
								obj.Pay = obj.RemainingLeave;
							}
						}
						
						

					}
					else
					{
						infoList = db.Query<LeaveSettlement>(sql: "Select IsCarryable,IsPaid,NoOfDays from [dbo].[LeaveMaster] where Id=@id", param: new {Id= leaveId }, commandType: CommandType.Text);
						foreach (var info in infoList)
						{
							obj.LeaveTaken = 0;
							obj.RemainingLeave = info.NoofDays;
							if (info.IsCarryable == true)
							{
								obj.CarryToNext = info.RemainingLeave;
							}
							if (info.IsPaid == true)
							{
								obj.Pay = obj.RemainingLeave;
							}
						}

					}
					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public LeaveSettlement GetById(int id)
		{
			try
			{
				LeaveSettlement obj = new LeaveSettlement();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var leaveSettlementList = db.Query<LeaveSettlement>(sql: "[dbo].[GetLeaveSettlementById]", param: param, commandType: CommandType.StoredProcedure);
					if (leaveSettlementList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(leaveSettlementList.Select(x => x.Id).First());
						obj.EmployeeId = Convert.ToInt32(leaveSettlementList.Select(x => x.EmployeeId).First());
						obj.LeaveMasterId = Convert.ToInt32(leaveSettlementList.Select(x => x.LeaveMasterId).First());
						obj.OpeningBalance = Convert.ToDouble(leaveSettlementList.Select(x => x.OpeningBalance).First());
						obj.LeaveTaken = Convert.ToDecimal(leaveSettlementList.Select(x => x.LeaveTaken).First());
						obj.RemainingLeave = Convert.ToDecimal(leaveSettlementList.Select(x => x.RemainingLeave).First());
						obj.CarryToNext = Convert.ToDecimal(leaveSettlementList.Select(x => x.CarryToNext).First());
						obj.Pay = Convert.ToDecimal(leaveSettlementList.Select(x => x.Pay).First());
						obj.SettlingLeave = Convert.ToInt32(leaveSettlementList.Select(x => x.SettlingLeave).First());
						obj.Remarks = Convert.ToString(leaveSettlementList.Select(x => x.Remarks).First());
						obj.FiscalYearId = Convert.ToInt32(leaveSettlementList.Select(x => x.FiscalYearId).First());
					}
					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int GetSettlementByEmpIdAndLeaveId(int empId, int leaveId,int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var leaveSettlementList = db.Query<LeaveSettlement>(sql: "Select Id from [dbo].[LeaveSettlement] where EmployeeId=@EmpId and LeaveMasterId=@LeaveId and Id!=@Id", param: new { LeaveId = leaveId,EmpId= empId,Id=id }, commandType: CommandType.Text);
					return leaveSettlementList.Count();

				}


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		public IEnumerable<LeaveSettlement> GetLeaveByFiscal(string fiscalYearId)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var data = db.Query<LeaveSettlement>("Select Id, LeaveName from LeaveMaster where FiscalYear = @Id", new { Id = fiscalYearId }, commandType: CommandType.Text);
					db.Close();
					return data.ToList();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}