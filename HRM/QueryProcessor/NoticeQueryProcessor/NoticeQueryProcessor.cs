﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;




namespace HRM
{
    public class NoticeHRM : INoticeHRM
    {
        public int Create(Notice notice)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", notice.Id);
                    param.Add("@Event", notice.Event);                   
                    param.Add("@Title", notice.Title);
                    param.Add("@NoticeLevel_Id", notice.NoticeLevel_Id);
                    param.Add("@BranchId", notice.BranchCollection);
                    param.Add("@DepartmentId", notice.DepartmentCollection);
                    param.Add("@SectionId", notice.SectionCollection);
                    param.Add("@PublishOn", notice.PublishOn);
                    param.Add("@ExpiredOn", notice.ExpiredOn);
                    param.Add("@PublishOnNepali", notice.PublishOnNepali);
                    param.Add("@ExpiredOnNepali", notice.ExpiredOnNepali);
                    param.Add("@IsUrgent", notice.IsUrgent);                   
                    param.Add("@Remarks", notice.Remarks);
                    param.Add("@Is_Active", notice.Is_Active);
                    param.Add("@Is_Read", notice.Is_Read);
                    param.Add("@Created_By", notice.CreatedBy);
					param.Add("@Created_Date", notice.CreatedDate);
					param.Add("@XMLBranch", notice.XMLBranch);                 
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Notice]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                }
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
        
        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var result = db.Execute("Update [dbo].[Notice] SET [Is_Active] = 0 Where Id=@Id Update [dbo].[NoticeDetails] SET [Is_Active] = 0 Where Notice_Id=@Id",  param: new { @IsActive = false , @Id = id}, commandType : CommandType.Text );
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Notice> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var noticeList = db.Query<Notice>(sql : "[dbo].[GetNoticeList]", commandType: CommandType.StoredProcedure);
                    return noticeList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Notice GetById(int id)
        {
            try
            {
                Notice obj = new Notice();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var noticeList = db.Query<Notice>("[dbo].[GetNoticeById]", param: param, commandType : CommandType.StoredProcedure);
                    if (noticeList.Count() > 0)
                    {
                        var remarks = noticeList.Select(x => x.Remarks).First();

                        obj.Id = Convert.ToInt32(noticeList.Select(x => x.Id).First());
                        obj.Title = noticeList.Select(x => x.Title).First().ToString();                       
                        obj.NoticeLevel_Id = Convert.ToString(noticeList.Select(x => x.NoticeLevel_Id).First());
                        obj.PublishOn = Convert.ToDateTime(noticeList.Select(x => x.PublishOn).First());
                        obj.ExpiredOn = Convert.ToDateTime(noticeList.Select(x => x.ExpiredOn).First());
                        obj.PublishOnNepali = noticeList.Select(x => x.PublishOnNepali).First();
                        obj.ExpiredOnNepali = noticeList.Select(x => x.ExpiredOnNepali).First();
                        obj.BranchId = noticeList.Select(x => x.BranId).ToArray();
                        obj.DepartmentId = noticeList.Select(x => x.DepId).ToArray();
                        obj.SectionId = noticeList.Select(x => x.SectId).ToArray();
                        if (remarks== null)
                        {
                            obj.Remarks = "";
                        }
                        else
                        {
                            obj.Remarks = noticeList.Select(x => x.Remarks).First().ToString();
                        }
                        
                        obj.IsUrgent = noticeList.Select(x => x.IsUrgent).First();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}