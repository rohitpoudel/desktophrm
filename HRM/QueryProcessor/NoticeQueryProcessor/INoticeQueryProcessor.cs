﻿
using System.Collections.Generic;


namespace HRM
{
    public interface INoticeHRM
    {
        int Create(Notice notice);
        Notice GetById(int id);
        IEnumerable<Notice> GetAllData();
        bool Delete(int id);
    }
}
