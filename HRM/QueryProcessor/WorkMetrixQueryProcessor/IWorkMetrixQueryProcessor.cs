﻿
using System.Collections.Generic;


namespace HRM.WorkMetrixHRM
{
    public interface IWorkMetrixHRM
    {
        WorkMetrix Create(WorkMetrix workMetrix);
        List<WorkMetrix> GetAll();
        List<WorkMetrixViewModels> GetAllData();
        WorkMetrix GetById(int id);
        WorkMetrix Edit(WorkMetrix workMetrix);
        bool Delete(int id);
    }
}
