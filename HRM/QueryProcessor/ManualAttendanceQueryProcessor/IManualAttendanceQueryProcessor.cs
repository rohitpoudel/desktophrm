﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IManualAttendanceHRM
    {
        ManualAttendance GetLoginDetails(string empId, string date);
        int Create(ManualAttendance manualAttendance);
        IEnumerable<ManualAttendance> GetAllData();
        ManualAttendance GetById(int id);
        int GetManualAttendanceCode(string code);
        bool CreateMA(ManualAttendance mA);
    }
}
