﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
    public class ManualAttendanceHRM : IManualAttendanceHRM
    {
        public int Create(ManualAttendance manualAttendance)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", manualAttendance.Id);
                    param.Add("@Event", manualAttendance.Event);
                    //  param.Add("@Code", manualAttendance.Code);
                    param.Add("@DepartmentId", manualAttendance.DepartmentId);
                    param.Add("@EmployeeAttendanceId", manualAttendance.EmployeeAttendanceId);
                    param.Add("@LoginTime", manualAttendance.LoginTime);
                    param.Add("@LogoutTime", manualAttendance.LogoutTime);
                    param.Add("@IsActive", manualAttendance.IsActive);
                    param.Add("@CreateBy", manualAttendance.CreateBy);                    
                    param.Add("@CreateDate", manualAttendance.CreateDate);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_ManualDeviceLog]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ManualAttendance> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var manualAttendanceList = db.Query<ManualAttendance>(sql: "[dbo].[GetManualAttendanceList]", commandType: CommandType.StoredProcedure);
                    db.Close();

                    return manualAttendanceList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ManualAttendance GetById(int id)
        {
            try
            {
                ManualAttendance obj = new ManualAttendance();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var manualAttendanceList = db.Query<ManualAttendance>(sql: "[dbo].[GetManualAttendanceListById]", param: param, commandType: CommandType.StoredProcedure);
                    if (manualAttendanceList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(manualAttendanceList.Select(x => x.Id).First());
                        obj.DepartmentId = Convert.ToInt32(manualAttendanceList.Select(x => x.DepartmentId).First());
                        obj.EmployeeAttendanceId = Convert.ToInt32(manualAttendanceList.Select(x => x.EmployeeAttendanceId).First());
                        //obj.Code = Convert.ToInt32(manualAttendanceList.Select(x => x.Code).First());
                        obj.LoginTime = manualAttendanceList.Select(x => x.LoginTime).First();
                        obj.LogoutTime = manualAttendanceList.Select(x => x.LogoutTime).First();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CreateMA(ManualAttendance mA)
        {
            try
            {
                if (mA.DLId > 0)
                {
                    mA.Event = 'U';
                }
                else
                {
                    mA.Event = 'I';
                }
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    //db.OpenAsync();
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", mA.Id);
                    param.Add("@DlId", mA.DLId);
                    param.Add("@Event", mA.Event);
                    param.Add("@LogInTime", mA.LogInDateTime);
                    param.Add("@logoutTime", mA.LogOutDateTime);
                    param.Add("@CreatedDate", DateTime.Now);
                    param.Add("@EmpAId", mA.EmployeeAttendanceId);
                    param.Add("@CreatedBy",1);
                    param.Add("@AttendanceType", 'M');
                    param.Add("@Remarks", mA.Remarks);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_ManualAttendanceLog]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public ManualAttendance GetLoginDetails(string empId, string date)
        {
            try
            {
                ManualAttendance obj = new ManualAttendance();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@empId", empId);
                    param.Add("@date", date);
                    var manualAttendanceList = db.Query<ManualAttendance>(sql: "[dbo].[GetEmpAttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);
                    if (manualAttendanceList.Count() > 0)
                    {
                        obj.Id = Convert.ToInt32(manualAttendanceList.Select(x => x.Id).First());

                        obj.DLId = Convert.ToInt32(manualAttendanceList.Select(x => x.DLId).LastOrDefault());
                        obj.InTime = manualAttendanceList.Select(x => x.InTime).LastOrDefault();
                        obj.OutTime = manualAttendanceList.Select(x => x.OutTime).LastOrDefault();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetManualAttendanceCode(string code)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {

                    db.Open();
                    var ManualAttendaceCode = db.Query<ManualAttendance>("Select [ManualAttendanceCode] from [dbo].[ManualDeviceLog] where IsActive = 1 and [ManualAttendanceCode] = @ManualAttendaceCode", param: new { ManualAttendaceCode = code }, commandType: CommandType.Text);
                    var codeResult = ManualAttendaceCode.Count();
                    db.Close();
                    return codeResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}