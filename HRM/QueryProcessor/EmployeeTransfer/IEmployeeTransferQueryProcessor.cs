﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRM
{
    public interface IEmployeeTransferHRM
    {
        IEnumerable<EmployeeTransfer> GetAllData(string nbranchId, string ObranchId);
        EmployeeDetail GetEmployeeDetail(int Id);
        int CreateEmployeeTransfer(EmployeeTransfer employeeTransfer);
        EmployeeTransfer Edit(int Id);
        int Approve(int id, int EmployeeId);
        bool UpdateEmployee(EmployeeTransfer eT);
        bool Delete(int eT);
    }
}
