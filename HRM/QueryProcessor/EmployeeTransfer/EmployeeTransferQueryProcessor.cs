﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
    public class EmployeeTransferHRM : IEmployeeTransferHRM
    {
        GlobalArray gs = null;
        public EmployeeTransferHRM()
        {
            gs = new GlobalArray();
           // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
        }
        public IEnumerable<EmployeeTransfer> GetAllData(string nbranchId, string ObranchId)
        {
            try
            {
                List<EmployeeTransfer> Employeelist = new List<EmployeeTransfer>();

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@NBId", nbranchId);
                    param.Add("@OBId", ObranchId);
                    //int userId = (int)HttpContext.Current.Session["UserId"];
                    //var isSA = HttpContext.Current.Session["IsSuperAdmin"];

                    int userId = 1;
                    var isSA = true;
                    Employeelist = db.Query<EmployeeTransfer>(sql: "[dbo].[Usp_GetEmployeeTransferList]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    if (isSA.ToString() != "True")
                    {
                        if (gs.ViewAllEmployee != 1)
                        {
                            Employeelist = Employeelist.Where(x => x.Id == userId).ToList();
                        }
                        //if (gs.ViewAllBranchData !=1)
                        //{
                        //    var bId = (int)HttpContext.Current.Session["BranchId"];

                        //    Employeelist = Employeelist.Where(x => x.OldBranchId == userId).ToList();
                        //}
                    }
                    return Employeelist;
                }
            }
            catch (Exception ex)
            {
               // ExceptionHandler.AppendLog(ex);
                List<EmployeeTransfer> Employeelist = new List<EmployeeTransfer>();
                return Employeelist;
            }
        }
        public EmployeeDetail GetEmployeeDetail(int Id)
        {
            try
            {
                EmployeeDetail employeeDetail = new EmployeeDetail();

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    employeeDetail = db.Query<EmployeeDetail>(sql: @"Select ep.Id, ISNULL(ep.BranchId,0) BranchId, br.BranchName, ep.Department_Id DepartmentId, dp.DepartmentName
                                                                 , ep.Designation_Id DesignationId, ds.DesignationName, ep.Section_Id SectionId, sc.SectionName
                                                                 from Employee ep
                                                                 LEft join Branch br on ep.BranchId = br.Id
                                                                 LEFT JOIN Departments dp on ep.Department_Id = dp.Id
                                                                 LEFT JOIN Designations ds on ep.Designation_Id = ds.Id
                                                                 LEFT JOIN Sections sc on ep.Section_Id = sc.Id
                                                                 Where ep.Emp_DeviceCode != 0 and ep.Is_Active = 1 and ep.Id = @EmpId", param: new { @EmpId = Id }, commandType: CommandType.Text).SingleOrDefault();
                    return employeeDetail;
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.AppendLog(ex);
                EmployeeDetail employeeDetail = new EmployeeDetail();
                return employeeDetail;
            }
        }
        public int CreateEmployeeTransfer(EmployeeTransfer obj)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", obj.Id);
                    param.Add("@Event", obj.Event);
                    param.Add("@EmployeeId", obj.EmployeeId);
                    param.Add("@EffectiveFrom", obj.EffectiveFrom);
                    param.Add("@OldBranchId", obj.OldBranchId);
                    param.Add("@NewBranchId", obj.NewBranchId);
                    param.Add("@NewDesignationId", obj.NewDesignationId);
                    param.Add("@OldDesignationId", obj.OldDesignationId);
                    param.Add("@NewDepartmentId", obj.NewDepartmentId);
                    param.Add("@OldDepartmentId", obj.OldDepartmentId);
                    param.Add("@NewSectionId", obj.NewSectionId);
                    param.Add("@OldSectionId", obj.OldSectionId);
                    param.Add("@CreatedBy", obj.CreatedBy);
                    param.Add("@CreatedDate", obj.CreatedDate);
                    param.Add("@UpdatedBy", obj.UpdatedBy);
                    param.Add("@UpdatedDate", obj.UpdatedDate);
                    param.Add("@IsAproved", obj.IsAproved);
                    param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                    db.Execute("[dbo].[Usp_IUD_EmployeeTransfer]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public EmployeeTransfer Edit(int Id)
        {
            try
            {
                EmployeeTransfer employeeDetail = new EmployeeTransfer();

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    employeeDetail = db.Query<EmployeeTransfer>(sql: @"Select et.Id, et.EmployeeId, ep.Emp_Name EmployeeName, et.EffectiveFrom, et.CreatedBy, et.CreatedDate
                                    , et.OldBranchId, obr.BranchName OldBranchName
                                    , et.NewBranchId, nbr.BranchName NewBranchName
                                    , et.OldDepartmentId, odp.DepartmentName OldDepartmentName
                                    , et.NewDepartmentId, ndp.DepartmentName NewDepartmentName
                                    , et.OldDesignationId, ods.DesignationName OldDesignationName
                                    , et.NewDesignationId, nds.DesignationName NewDesignationName
                                    , et.OldSectionId, osc.SectionName OldSectionName, et.NewSectionId, nsc.SectionName NewSectionName
                                    from EmployeeTransfer et
                                    LEFT JOIN Employee ep on et.EmployeeId = ep.Id
                                    
                                    LEFT JOIN Branch obr on et.OldBranchId = obr.Id
                                    Left JOIN Branch nbr on et.NewBranchId = nbr.Id
                                    
                                    LEFT JOIN Departments odp on et.OldDepartmentId = odp.Id
                                    Left JOIN Departments ndp on et.NewDepartmentId = ndp.Id

                                    LEFT JOIN Designations ods on et.OldDesignationId = ods.Id
                                    Left JOIN Designations nds on et.NewDesignationId = nds.Id
                                    
                                    LEFT JOIN Sections osc on et.OldSectionId = osc.Id
                                    Left JOIN Sections nsc on et.NewSectionId = nsc.Id WHERE et.Id = @Id", param: new { @Id = Id }, commandType: CommandType.Text).SingleOrDefault();
                    return employeeDetail;
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.AppendLog(ex);
                EmployeeTransfer employeeDetail = new EmployeeTransfer();
                return employeeDetail;
            }
        }
        public bool UpdateEmployee(EmployeeTransfer eT)
        {
            try
            {

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("update Employee set BranchId = @NewBranchId, Department_Id = @NewDepartmentId, Designation_Id = @NewDesignationId, Section_Id = @NewSectionId where Id = @EmployeeId", param: new { NewBranchId= eT.NewBranchId, NewDepartmentId = eT.NewDepartmentId, NewDesignationId= eT.NewDesignationId, NewSectionId = eT.NewSectionId, EmployeeId = eT.EmployeeId }, commandType: CommandType.Text);
                    int result1 = db.Execute("update [RealTime].dbo.Employee set BranchId = @NewBranchId, Department_Id = @NewDepartmentId, Designation_Id = @NewDesignationId, Section_Id = @NewSectionId where Id = @EmployeeId", param: new { NewBranchId = eT.NewBranchId, NewDepartmentId = eT.NewDepartmentId, NewDesignationId = eT.NewDesignationId, NewSectionId = eT.NewSectionId, EmployeeId = eT.EmployeeId }, commandType: CommandType.Text);

                    db.Close();

                    return true;




                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool Delete(int eT)
        {
            try
            {

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("Delete EmployeeTransfer where id=@Id", param: new { Id = eT }, commandType: CommandType.Text);

                    db.Close();

                    return true;




                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Approve(int id, int EmployeeId)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", id);
                    param.Add("@EmployeeId", EmployeeId);
                    param.Add("@ReturnId", dbType: DbType.Int16, direction: ParameterDirection.Output);
                    db.Execute("[dbo].[ApproveEmployeeTransfer]", param: param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@ReturnId");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}