﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;



namespace HRM
{
	public class AdvancedPaymentHRM : IAdvancedPaymentHRM
	{
		public int Create(AdvancedPayment obj)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var param = new DynamicParameters();
					param.Add("@Event", obj.Event);
					param.Add("@Id", obj.Id);
					param.Add("@Emp_Id", obj.Emp_Id);
					param.Add("@Date", obj.Date);
					param.Add("@Amount", obj.Amount);
					param.Add("@Created_By", obj.Created_By);
					param.Add("@Created_Date", obj.Created_Date);
					param.Add("@Remarks", obj.Remarks);
					param.Add("@IsDeleted", obj.IsDeleted);
					param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
					db.Execute("[dbo].[Usp_IUD_AdvancedPayment]", param: param, commandType: CommandType.StoredProcedure);

					db.Close();
					return param.Get<Int16>("@Return_Id");

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		public bool Delete(int id)
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					int result = db.Execute("UPDATE[dbo].[AdvancedPayment] SET[IsDeleted]=@IsDeleted WHERE Id=@Id", param: new { IsDeleted = true, Id = id }, commandType: CommandType.Text);
					db.Close();
					if (result > 0)
					{
						return true;
					}
					return false;

				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IEnumerable<AdvancedPayment> GetAllData()
		{
			try
			{
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					db.Open();
					var advancedPaymentList = db.Query<AdvancedPayment>(sql: "[dbo].[GetAdvancedPaymentList]", commandType: CommandType.StoredProcedure);
					return advancedPaymentList.ToList();

				}


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public AdvancedPayment GetById(int id)
		{
			try
			{
				AdvancedPayment obj = new AdvancedPayment();
				var dbfactory = DbFactoryProvider.GetFactory();
				using (var db = (DbConnection)dbfactory.GetConnection())
				{
					var param = new DynamicParameters();
					db.Open();
					param.Add("@Id", id);
					var advancedPaymentList = db.Query<AdvancedPayment>(sql: "[dbo].[GetAdvancedPaymentById]", param: param, commandType: CommandType.StoredProcedure);
					if (advancedPaymentList.Count() > 0)
					{
						obj.Id = Convert.ToInt32(advancedPaymentList.Select(x => x.Id).First());
						obj.Emp_Id = Convert.ToInt32(advancedPaymentList.Select(x => x.Emp_Id).First());
						obj.Date = Convert.ToDateTime(advancedPaymentList.Select(x => x.Date).First());
						obj.Amount = Convert.ToDouble(advancedPaymentList.Select(x => x.Amount).First());
						obj.Emp_Name = Convert.ToString(advancedPaymentList.Select(x => x.Emp_Name).First());

					}
					db.Close();
					return obj;
				}



			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}