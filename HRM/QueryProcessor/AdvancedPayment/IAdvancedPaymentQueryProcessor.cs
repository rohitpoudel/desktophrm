﻿
using System.Collections.Generic;


namespace HRM
{
	public interface IAdvancedPaymentHRM
	{
		int Create(AdvancedPayment obj);
		IEnumerable<AdvancedPayment> GetAllData();
		AdvancedPayment GetById(int id);
		bool Delete(int id);
	}
}
