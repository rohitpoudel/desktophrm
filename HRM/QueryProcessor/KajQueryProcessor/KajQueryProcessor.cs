﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace HRM
{
    public class KajHRM : IKajHRM
    {
        public int Create(Kaj kaj)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Id", kaj.Id);
                    param.Add("@Event", kaj.Event);
                    param.Add("@FromDate", kaj.FromDate);
                    param.Add("@ToDate", kaj.ToDate);
                    param.Add("@FromDateNepali", kaj.FromDateNepali);
                    param.Add("@ToDateNepali", kaj.ToDateNepali);
                    param.Add("@Remark", kaj.Remark);
                    param.Add("@Emp_Id", kaj.Emp_Id);
                    param.Add("@Status", kaj.Status);
                    param.Add("@Created_By", kaj.Created_By);
					param.Add("@Created_Date", kaj.Created_Date);
                    param.Add("@Is_Active", kaj.Is_Active);
                    param.Add("@Title", kaj.Title);
                    param.Add("@Return_Id", dbType : DbType.Int16, direction : ParameterDirection.Output);
                    db.Execute("[dbo].[Usp_IUD_Kaj]", param : param, commandType: CommandType.StoredProcedure);
                    db.Close();
                    return param.Get<Int16>("@Return_Id");
                } 
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                    int result = db.Execute("UPDATE [dbo].[Kaj] SET [Is_Active] = 0 Where Id=@Id", param: new { @Is_Active = false, @Id = id }, commandType: CommandType.Text);
                    db.Close();
                    if (result > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Kaj> GetAllData()
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    db.Open();
                   
                    var kajList = db.Query<Kaj>(sql: "[dbo].[GetKajList]", commandType: CommandType.StoredProcedure);
                    GlobalArray gs = new GlobalArray();
                   // gs = (GlobalArray)HttpContext.Current.Session["GlobalArray"];
                    if (gs.ViewAllBranchData != 1)
                    {
                        // var bnId = (int)HttpContext.Current.Session["BranchId"];
                        var bnId = 1;
                        kajList = kajList.Where(x => x.BranchId == bnId).ToList();

                    }
                    return kajList.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Kaj GetById(int id)
        {
            try
            {
                Kaj obj = new Kaj();
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection())
                {
                    var param = new DynamicParameters();
                    db.Open();
                    param.Add("@Id", id);
                    var kajList = db.Query<Kaj>(sql: "[dbo].[GetKajById]", param: param, commandType: CommandType.StoredProcedure);
                    if (kajList.Count() > 0)
                    {

                        obj.Id = Convert.ToInt32(kajList.Select(x => x.Id).First());
                        obj.FromDate = kajList.Select(x => x.FromDate).First();
                        obj.ToDate = kajList.Select(x => x.ToDate).First();
                        obj.FromDateNepali = kajList.Select(x => x.FromDateNepali).First();
                        obj.ToDateNepali = kajList.Select(x => x.ToDateNepali).First();
                        obj.Remark = kajList.Select(x => x.Remark).First().ToString();
                        obj.Emp_Id = Convert.ToInt32(kajList.Select(x => x.Emp_Id).First());
                        obj.Status = kajList.Select(x => x.Status).First();
                        obj.Created_By = Convert.ToInt32(kajList.Select(x => x.Created_By).First());
                        obj.Created_Date = Convert.ToDateTime(kajList.Select(x => x.Created_Date).First());
						obj.Emp_Name = kajList.Select(x => x.Emp_Name).First().ToString();
                        obj.Title = kajList.Select(x => x.Title).First().ToString();

                    }
                    db.Close();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}