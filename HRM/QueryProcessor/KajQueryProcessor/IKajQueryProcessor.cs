﻿
using System.Collections.Generic;


namespace HRM
{
    public interface IKajHRM
    {
        int Create(Kaj kaj);
        IEnumerable<Kaj> GetAllData();
        Kaj GetById(int id);
        bool Delete(int id);
    }
}
