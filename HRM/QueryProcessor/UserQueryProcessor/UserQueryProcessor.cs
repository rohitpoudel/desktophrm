﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;




namespace HRM
{
    //public class UserHRM : IUserHRM
    //{
    //    public int Create(UserRole userRole)
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                //db.OpenAsync();
    //                db.Open();
    //                var param = new DynamicParameters();
    //                param.Add("@Id", userRole.Id);
    //                param.Add("@Event", userRole.Event);
    //                param.Add("@UserName", userRole.UserName);
    //                param.Add("@UserNameNepali", userRole.UserNameNepali);
				//	param.Add("@CreatedDate", userRole.CreatedDate);
				//	param.Add("@CreatedBy", userRole.CreatedBy);
				//	param.Add("@IsActive", userRole.IsActive);
				//	param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
    //                db.Execute("[dbo].[Usp_IUD_UserRole]", param: param, commandType: CommandType.StoredProcedure);
    //                db.Close();
    //                return param.Get<Int16>("@Return_Id");

    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }

    //    public bool Delete(int id)
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                db.Open();
    //                int result = db.Execute("UPDATE [dbo].[UserRole] SET [IsDeleted] = 1 Where Id=@Id", param: new { @IsDeleted = true, @Id = id }, commandType: CommandType.Text);
    //                db.Close();
    //                if (result > 0)
    //                {
    //                    return true;
    //                }
    //                return false;


    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;

    //        }
    //    }

    //    public IEnumerable<UserRole> GetAllData()
    //    {
    //        try
    //        {
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                db.Open();
    //                var userRoleList = db.Query<UserRole>(sql: "[dbo].[Usp_GetUserRoleList]", commandType: CommandType.StoredProcedure);
    //                db.Close();
    //                return userRoleList.ToList();

    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }

    //    public UserRole GetById(int id)
    //    {
    //        try
    //        {
    //            var obj = new UserRole();
    //            var dbfactory = DbFactoryProvider.GetFactory();
    //            using (var db = (DbConnection)dbfactory.GetConnection())
    //            {
    //                var param = new DynamicParameters();
    //                db.Open();
    //                param.Add("@Id", id);
    //                var userRoleList = db.Query<UserRole>(sql: "[dbo].[Usp_GetUserRoleById]", param: param, commandType: CommandType.StoredProcedure);
    //                if (userRoleList.Count() > 0)
    //                {
    //                    obj.Id = Convert.ToInt32(userRoleList.Select(x => x.Id).First());
    //                    obj.UserName = userRoleList.Select(x => x.UserName).First().ToString();
    //                    obj.UserNameNepali = userRoleList.Select(x => x.UserNameNepali).First().ToString();
    //                    obj.CreatedBy = Convert.ToInt32(userRoleList.Select(x => x.CreatedBy).First());
    //                    //obj.CreatedDate = Convert.ToDateTime(userRoleList.Select(x => x.CreatedDate).First()).ToShortDateString(); convert into current date 
    //                    obj.CreatedDate = Convert.ToDateTime(userRoleList.Select(x => x.CreatedDate).First());



    //                }
    //                db.Close();
    //                return obj;


    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }
    //}
}