﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRM
{
  public  class Dropdown
    {
        //public static List<SelectListItem> BindDays()
        //{
        //    List<SelectListItem> ddlItems = new List<SelectListItem>();
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Select Day", Value = "0" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Sunday", Value = "1" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Monday", Value = "2" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Tuesday", Value = "3" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Wenesday", Value = "4" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Thursday", Value = "5" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Friday", Value = "6" });
        //    ddlItems.Add(new SelectListItem() { Selected = true, Text = "Saturday", Value = "7" });
        //    return ddlItems;
        //}
        public static List<Dropdowns> BindYears()
        {

            List<Dropdowns> ddlItems = new List<Dropdowns>();
            ddlItems.Add(new Dropdowns() {  Text = "Select Year", Value = "0" });
            ddlItems.Add(new Dropdowns() {  Text = "2075", Value = "2075" });
            ddlItems.Add(new Dropdowns() {  Text = "2076", Value = "2076" });
            ddlItems.Add(new Dropdowns() {  Text = "2077", Value = "2077" });
            ddlItems.Add(new Dropdowns() {  Text = "2078", Value = "2078" });
            ddlItems.Add(new Dropdowns() {  Text = "2079", Value = "2079" });
            ddlItems.Add(new Dropdowns() {  Text = "2080", Value = "2080" });
            ddlItems.Add(new Dropdowns() {  Text = "2081", Value = "2081" });

            return ddlItems;
        }
    }
}
