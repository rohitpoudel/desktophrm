﻿namespace HRM.Forms.SetUp
{
    partial class frmFiscalYear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtStartDate = new System.Windows.Forms.TextBox();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.chkCurrentFiscalYear = new System.Windows.Forms.CheckBox();
            this.cmbFisicalYear = new System.Windows.Forms.ComboBox();
            this.lblLeaveOnLate = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblFiscalYear = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_close = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.lblheader = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(300, 117);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(247, 22);
            this.txtStartDate.TabIndex = 59;
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(434, 271);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(113, 36);
            this.btn_Reset.TabIndex = 57;
            this.btn_Reset.Text = "Cancel";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(300, 271);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(113, 36);
            this.btnCreate.TabIndex = 58;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(-56, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(971, 1);
            this.label1.TabIndex = 56;
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(-115, 55);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1028, 1);
            this.lblhorizentalline.TabIndex = 55;
            // 
            // chkCurrentFiscalYear
            // 
            this.chkCurrentFiscalYear.AutoSize = true;
            this.chkCurrentFiscalYear.Location = new System.Drawing.Point(300, 191);
            this.chkCurrentFiscalYear.Name = "chkCurrentFiscalYear";
            this.chkCurrentFiscalYear.Size = new System.Drawing.Size(18, 17);
            this.chkCurrentFiscalYear.TabIndex = 53;
            this.chkCurrentFiscalYear.UseVisualStyleBackColor = true;
            // 
            // cmbFisicalYear
            // 
            this.cmbFisicalYear.FormattingEnabled = true;
            this.cmbFisicalYear.Location = new System.Drawing.Point(300, 78);
            this.cmbFisicalYear.Name = "cmbFisicalYear";
            this.cmbFisicalYear.Size = new System.Drawing.Size(247, 24);
            this.cmbFisicalYear.TabIndex = 51;
            // 
            // lblLeaveOnLate
            // 
            this.lblLeaveOnLate.AutoSize = true;
            this.lblLeaveOnLate.Location = new System.Drawing.Point(75, 191);
            this.lblLeaveOnLate.Name = "lblLeaveOnLate";
            this.lblLeaveOnLate.Size = new System.Drawing.Size(129, 17);
            this.lblLeaveOnLate.TabIndex = 46;
            this.lblLeaveOnLate.Text = "Current Fiscal Year";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(75, 120);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(72, 17);
            this.lblStartDate.TabIndex = 47;
            this.lblStartDate.Text = "Start Date";
            // 
            // lblFiscalYear
            // 
            this.lblFiscalYear.AutoSize = true;
            this.lblFiscalYear.Location = new System.Drawing.Point(75, 87);
            this.lblFiscalYear.Name = "lblFiscalYear";
            this.lblFiscalYear.Size = new System.Drawing.Size(78, 17);
            this.lblFiscalYear.TabIndex = 49;
            this.lblFiscalYear.Text = "Fiscal Year";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(75, 157);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(67, 17);
            this.lblEndDate.TabIndex = 47;
            this.lblEndDate.Text = "End Date";
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(300, 154);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(247, 22);
            this.txtEndDate.TabIndex = 59;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_close);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.lblheader);
            this.panel1.Location = new System.Drawing.Point(2, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 38);
            this.panel1.TabIndex = 60;
            // 
            // button_close
            // 
            this.button_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_close.ForeColor = System.Drawing.Color.Red;
            this.button_close.Location = new System.Drawing.Point(882, 3);
            this.button_close.Name = "button_close";
            this.button_close.Size = new System.Drawing.Size(26, 32);
            this.button_close.TabIndex = 47;
            this.button_close.Text = "X";
            this.button_close.UseVisualStyleBackColor = true;
            this.button_close.Click += new System.EventHandler(this.button_close_Click);
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.Red;
            this.btn_close.Location = new System.Drawing.Point(999, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 32);
            this.btn_close.TabIndex = 46;
            this.btn_close.Text = "X";
            this.btn_close.UseVisualStyleBackColor = true;
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(3, 5);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(221, 29);
            this.lblheader.TabIndex = 41;
            this.lblheader.Text = "Create Fiscal Year";
            // 
            // frmFiscalYear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 360);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtEndDate);
            this.Controls.Add(this.txtStartDate);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.chkCurrentFiscalYear);
            this.Controls.Add(this.cmbFisicalYear);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.lblLeaveOnLate);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.lblFiscalYear);
            this.Name = "frmFiscalYear";
            this.Load += new System.EventHandler(this.frmFiscalYear_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtStartDate;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.CheckBox chkCurrentFiscalYear;
        private System.Windows.Forms.ComboBox cmbFisicalYear;
        private System.Windows.Forms.Label lblLeaveOnLate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblFiscalYear;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.TextBox txtEndDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Button button_close;
    }
}