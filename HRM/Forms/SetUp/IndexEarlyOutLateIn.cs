﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class IndexEarlyOutLateIn : Form
    {
        IOverTimeRuleHRM iOverTimeRuleHRM = null;

        public IndexEarlyOutLateIn()
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
        }

        private void IndexOverTimeSetUp_Load(object sender, EventArgs e)
        {
            string searchString = string.Empty;
            BindGrid(searchString);
            dataGridView1.CellValueChanged += new DataGridViewCellEventHandler(dataGridView1_CellValueChanged);
            dataGridView1.CurrentCellDirtyStateChanged += new EventHandler(dataGridView1_CurrentCellDirtyStateChanged);

        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView1.IsCurrentCellDirty)
            {
                // This fires the cell value changed handler below
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewComboBoxCell cb = (DataGridViewComboBoxCell)dataGridView1.Rows[e.RowIndex].Cells["Action"];
            if (cb.Value != null)
            {
                string selectId = dataGridView1.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                string selectEvent = cb.Value.ToString();

                var frm = new frmOfficeDays(selectId, selectEvent);
                frm.ShowDialog();

                dataGridView1.DataSource = null;
                string searchString = string.Empty;
                BindGrid(searchString);
            }
        }

        private void BindGrid(string searchString)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.ColumnCount = 7;

            dataGridView1.Columns[0].Name = "SNO";
            dataGridView1.Columns[0].HeaderText = "SNo";

            dataGridView1.Columns[1].Name = "Id";
            dataGridView1.Columns[1].HeaderText = "Id";
            dataGridView1.Columns[1].DataPropertyName = "Id";

            dataGridView1.Columns[2].Name = "FiscalYearName";
            dataGridView1.Columns[2].HeaderText = "Fiscal Year";
            dataGridView1.Columns[2].DataPropertyName = "FiscalYearName";

            dataGridView1.Columns[3].Name = "FiscalYearNameNepali";
            dataGridView1.Columns[3].HeaderText = "Fiscal Year Name Nepali";
            dataGridView1.Columns[3].DataPropertyName = "FiscalYearNameNepali";

            dataGridView1.Columns[4].Name = "OverTimeAllowed";
            dataGridView1.Columns[4].HeaderText = "Over Time Allowed";
            dataGridView1.Columns[4].DataPropertyName = "OverTimeAllowed";

            dataGridView1.Columns[5].Name = "Type";
            dataGridView1.Columns[5].HeaderText = "Incentive Type";
            dataGridView1.Columns[5].DataPropertyName = "Type";

            dataGridView1.Columns[6].Name = "Incentive";
            dataGridView1.Columns[6].HeaderText = "Incentive";
            dataGridView1.Columns[6].DataPropertyName = "Incentive";

            //Fetch the data from Database.
            iOverTimeRuleHRM = new OverTimeRuleHRM();
            if (string.IsNullOrEmpty(searchString))
            {
                var getAllData = iOverTimeRuleHRM.GetAllData();
                dataGridView1.DataSource = getAllData;
            }
            else
            {
                var getAllData = iOverTimeRuleHRM.GetAllDataForSearch(searchString);
                dataGridView1.DataSource = getAllData;
            }

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Columns[1].Visible = false;

            #region ---- Add Action field ---
            //Add a ComboBox Column to the DataGridView.
            DataGridViewComboBoxColumn comboBoxColumn = new DataGridViewComboBoxColumn();
            comboBoxColumn.HeaderText = "Action";
            comboBoxColumn.Width = 100;
            comboBoxColumn.Name = "Action";
            comboBoxColumn.DefaultCellStyle.NullValue = "--Select--";
            comboBoxColumn.DisplayIndex = 9;
            dataGridView1.Columns.Add(comboBoxColumn);
            ////Loop through the DataGridView Rows.
            DataGridViewComboBoxColumn comboBoxCell = dataGridView1.Columns["Action"] as DataGridViewComboBoxColumn;
            comboBoxCell.DataSource = BindActionComboBoxInGrid();
            comboBoxCell.DisplayMember = "Text";
            comboBoxCell.ValueMember = "Value";

            #endregion

            #region ----- Add  SNO ---------
            int i = 1;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.Cells["SNO"].Value = i;
                i++;
            }
            #endregion

            #region ------Add Header Design--------

            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("SansSerif", 8, FontStyle.Bold);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;

            #endregion

        }

        public class BindActionForGrid
        {
            public string Text { get; set; }
            public string Value { get; set; }

        }

        public List<BindActionForGrid> BindActionComboBoxInGrid()
        {
            List<BindActionForGrid> lst = new List<BindActionForGrid>();
            lst.Add(new BindActionForGrid { Text = "--Select--", Value = "-1" });
            lst.Add(new BindActionForGrid { Text = "View", Value = "V" });
            lst.Add(new BindActionForGrid { Text = "Modify", Value = "U" });
            return lst;
        }

        private void btn_HideGridPanel_Click(object sender, EventArgs e)
        {
            if (btn_HideGridPanel.Text == "-")
            {
                MainPanel.Hide();
                btn_HideGridPanel.Text = "+";
            }
            else if (btn_HideGridPanel.Text == "+")
            {
                btn_HideGridPanel.Text = "-";
                MainPanel.Show();
            }
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            string searchString = string.Empty;
            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                searchString = txtSearch.Text.Trim();
                BindGrid(searchString);
            }
            else
            {
                BindGrid(searchString);
            }
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            string selectId = "-1";
            string selectEvent = "I";
            var frm = new frmOverTimeSetUp(selectId, selectEvent);
            frm.ShowDialog();
            BindGrid(string.Empty);
        
        }


    }
}
