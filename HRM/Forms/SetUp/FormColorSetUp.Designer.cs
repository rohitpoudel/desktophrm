﻿namespace HRM.Forms.SetUp
{
    partial class FormColorSetUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.B = new System.Windows.Forms.Label();
            this.txtR = new System.Windows.Forms.TextBox();
            this.txtG = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.lblheader = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlBackGround = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.comShiftType = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkMarkAbsentWhenLateIn = new System.Windows.Forms.CheckBox();
            this.chkMarkAbsentWhenEarlyOut = new System.Windows.Forms.CheckBox();
            this.lblMarkAbsentWhenLateIn = new System.Windows.Forms.Label();
            this.lblMarkAbsentWhenEarlyOut = new System.Windows.Forms.Label();
            this.txtMaxLateInGrace = new System.Windows.Forms.TextBox();
            this.txtLateInGrace = new System.Windows.Forms.TextBox();
            this.txtSecondShiftStart = new System.Windows.Forms.TextBox();
            this.txtFirstShiftStart = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMaxEarlyOutGrace = new System.Windows.Forms.TextBox();
            this.txtEarlyOutGrace = new System.Windows.Forms.TextBox();
            this.txtSecondShiftEnd = new System.Windows.Forms.TextBox();
            this.txtFirstShiftEnd = new System.Windows.Forms.TextBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblMaxLateInGrace = new System.Windows.Forms.Label();
            this.lblLateInGrace = new System.Windows.Forms.Label();
            this.lblSecondShiftStart = new System.Windows.Forms.Label();
            this.lblShiftStart = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMaxEarlyOutGrace = new System.Windows.Forms.Label();
            this.lblEarlyOutGrace = new System.Windows.Forms.Label();
            this.lblSecondShiftEnd = new System.Windows.Forms.Label();
            this.lblShiftEnd = new System.Windows.Forms.Label();
            this.lblShiftType = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.pnlBackGround.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.62712F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.37288F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.Controls.Add(this.trackBar1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.trackBar2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.trackBar3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.B, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtR, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtG, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtB, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 89);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(267, 100);
            this.tableLayoutPanel1.TabIndex = 53;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(3, 3);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Minimum = 10;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(160, 27);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickFrequency = 10;
            this.trackBar1.Value = 10;
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(3, 36);
            this.trackBar2.Maximum = 255;
            this.trackBar2.Minimum = 10;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(160, 27);
            this.trackBar2.TabIndex = 1;
            this.trackBar2.TickFrequency = 10;
            this.trackBar2.Value = 10;
            // 
            // trackBar3
            // 
            this.trackBar3.Location = new System.Drawing.Point(3, 69);
            this.trackBar3.Maximum = 255;
            this.trackBar3.Minimum = 10;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(160, 28);
            this.trackBar3.TabIndex = 2;
            this.trackBar3.TickFrequency = 10;
            this.trackBar3.Value = 10;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "R";
            // 
            // B
            // 
            this.B.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.B.AutoSize = true;
            this.B.Location = new System.Drawing.Point(245, 74);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(17, 17);
            this.B.TabIndex = 3;
            this.B.Text = "B";
            // 
            // txtR
            // 
            this.txtR.Location = new System.Drawing.Point(190, 3);
            this.txtR.Name = "txtR";
            this.txtR.Size = new System.Drawing.Size(47, 22);
            this.txtR.TabIndex = 4;
            // 
            // txtG
            // 
            this.txtG.Location = new System.Drawing.Point(190, 36);
            this.txtG.Name = "txtG";
            this.txtG.Size = new System.Drawing.Size(47, 22);
            this.txtG.TabIndex = 4;
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(190, 69);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(47, 22);
            this.txtB.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(244, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "G";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(4, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1028, 1);
            this.label1.TabIndex = 52;
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(7, 68);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1028, 2);
            this.lblhorizentalline.TabIndex = 49;
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(333, 14);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(343, 46);
            this.lblheader.TabIndex = 48;
            this.lblheader.Text = "Form Color SetUp";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(711, 119);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(118, 39);
            this.btn_Save.TabIndex = 50;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(673, 384);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 17);
            this.label4.TabIndex = 54;
            this.label4.Text = "Message shows";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlBackGround);
            this.groupBox1.Location = new System.Drawing.Point(19, 217);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1007, 475);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Form Layout Example";
            // 
            // pnlBackGround
            // 
            this.pnlBackGround.BackColor = System.Drawing.SystemColors.Control;
            this.pnlBackGround.Controls.Add(this.label5);
            this.pnlBackGround.Controls.Add(this.label4);
            this.pnlBackGround.Controls.Add(this.comShiftType);
            this.pnlBackGround.Controls.Add(this.button1);
            this.pnlBackGround.Controls.Add(this.button2);
            this.pnlBackGround.Controls.Add(this.label6);
            this.pnlBackGround.Controls.Add(this.label7);
            this.pnlBackGround.Controls.Add(this.chkMarkAbsentWhenLateIn);
            this.pnlBackGround.Controls.Add(this.chkMarkAbsentWhenEarlyOut);
            this.pnlBackGround.Controls.Add(this.lblMarkAbsentWhenLateIn);
            this.pnlBackGround.Controls.Add(this.lblMarkAbsentWhenEarlyOut);
            this.pnlBackGround.Controls.Add(this.txtMaxLateInGrace);
            this.pnlBackGround.Controls.Add(this.txtLateInGrace);
            this.pnlBackGround.Controls.Add(this.txtSecondShiftStart);
            this.pnlBackGround.Controls.Add(this.txtFirstShiftStart);
            this.pnlBackGround.Controls.Add(this.txtName);
            this.pnlBackGround.Controls.Add(this.txtMaxEarlyOutGrace);
            this.pnlBackGround.Controls.Add(this.txtEarlyOutGrace);
            this.pnlBackGround.Controls.Add(this.txtSecondShiftEnd);
            this.pnlBackGround.Controls.Add(this.txtFirstShiftEnd);
            this.pnlBackGround.Controls.Add(this.txtCode);
            this.pnlBackGround.Controls.Add(this.lblMaxLateInGrace);
            this.pnlBackGround.Controls.Add(this.lblLateInGrace);
            this.pnlBackGround.Controls.Add(this.lblSecondShiftStart);
            this.pnlBackGround.Controls.Add(this.lblShiftStart);
            this.pnlBackGround.Controls.Add(this.lblName);
            this.pnlBackGround.Controls.Add(this.lblMaxEarlyOutGrace);
            this.pnlBackGround.Controls.Add(this.lblEarlyOutGrace);
            this.pnlBackGround.Controls.Add(this.lblSecondShiftEnd);
            this.pnlBackGround.Controls.Add(this.lblShiftEnd);
            this.pnlBackGround.Controls.Add(this.lblShiftType);
            this.pnlBackGround.Controls.Add(this.lblCode);
            this.pnlBackGround.Location = new System.Drawing.Point(6, 38);
            this.pnlBackGround.Name = "pnlBackGround";
            this.pnlBackGround.Size = new System.Drawing.Size(995, 431);
            this.pnlBackGround.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(-4, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(1028, 2);
            this.label5.TabIndex = 46;
            // 
            // comShiftType
            // 
            this.comShiftType.FormattingEnabled = true;
            this.comShiftType.Items.AddRange(new object[] {
            "-- Select Shift--",
            "Dual Shift",
            "Morning",
            "Day",
            "Night"});
            this.comShiftType.Location = new System.Drawing.Point(207, 131);
            this.comShiftType.Name = "comShiftType";
            this.comShiftType.Size = new System.Drawing.Size(245, 24);
            this.comShiftType.TabIndex = 44;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(480, 373);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 39);
            this.button1.TabIndex = 42;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(337, 372);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 39);
            this.button2.TabIndex = 40;
            this.button2.Text = "Create";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(-1, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1028, 2);
            this.label6.TabIndex = 39;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(325, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(323, 46);
            this.label7.TabIndex = 38;
            this.label7.Text = "Create Dual Shift";
            // 
            // chkMarkAbsentWhenLateIn
            // 
            this.chkMarkAbsentWhenLateIn.AutoSize = true;
            this.chkMarkAbsentWhenLateIn.Location = new System.Drawing.Point(763, 275);
            this.chkMarkAbsentWhenLateIn.Name = "chkMarkAbsentWhenLateIn";
            this.chkMarkAbsentWhenLateIn.Size = new System.Drawing.Size(18, 17);
            this.chkMarkAbsentWhenLateIn.TabIndex = 37;
            this.chkMarkAbsentWhenLateIn.UseVisualStyleBackColor = true;
            // 
            // chkMarkAbsentWhenEarlyOut
            // 
            this.chkMarkAbsentWhenEarlyOut.AutoSize = true;
            this.chkMarkAbsentWhenEarlyOut.Location = new System.Drawing.Point(241, 315);
            this.chkMarkAbsentWhenEarlyOut.Name = "chkMarkAbsentWhenEarlyOut";
            this.chkMarkAbsentWhenEarlyOut.Size = new System.Drawing.Size(18, 17);
            this.chkMarkAbsentWhenEarlyOut.TabIndex = 36;
            this.chkMarkAbsentWhenEarlyOut.UseVisualStyleBackColor = true;
            // 
            // lblMarkAbsentWhenLateIn
            // 
            this.lblMarkAbsentWhenLateIn.AutoSize = true;
            this.lblMarkAbsentWhenLateIn.Location = new System.Drawing.Point(555, 276);
            this.lblMarkAbsentWhenLateIn.Name = "lblMarkAbsentWhenLateIn";
            this.lblMarkAbsentWhenLateIn.Size = new System.Drawing.Size(171, 17);
            this.lblMarkAbsentWhenLateIn.TabIndex = 35;
            this.lblMarkAbsentWhenLateIn.Text = "Mark Absent When LateIn";
            // 
            // lblMarkAbsentWhenEarlyOut
            // 
            this.lblMarkAbsentWhenEarlyOut.AutoSize = true;
            this.lblMarkAbsentWhenEarlyOut.Location = new System.Drawing.Point(48, 314);
            this.lblMarkAbsentWhenEarlyOut.Name = "lblMarkAbsentWhenEarlyOut";
            this.lblMarkAbsentWhenEarlyOut.Size = new System.Drawing.Size(187, 17);
            this.lblMarkAbsentWhenEarlyOut.TabIndex = 34;
            this.lblMarkAbsentWhenEarlyOut.Text = "Mark Absent When EarlyOut";
            // 
            // txtMaxLateInGrace
            // 
            this.txtMaxLateInGrace.Location = new System.Drawing.Point(699, 233);
            this.txtMaxLateInGrace.Name = "txtMaxLateInGrace";
            this.txtMaxLateInGrace.Size = new System.Drawing.Size(245, 22);
            this.txtMaxLateInGrace.TabIndex = 30;
            // 
            // txtLateInGrace
            // 
            this.txtLateInGrace.Location = new System.Drawing.Point(699, 198);
            this.txtLateInGrace.Name = "txtLateInGrace";
            this.txtLateInGrace.Size = new System.Drawing.Size(245, 22);
            this.txtLateInGrace.TabIndex = 29;
            // 
            // txtSecondShiftStart
            // 
            this.txtSecondShiftStart.Location = new System.Drawing.Point(699, 162);
            this.txtSecondShiftStart.Name = "txtSecondShiftStart";
            this.txtSecondShiftStart.Size = new System.Drawing.Size(245, 22);
            this.txtSecondShiftStart.TabIndex = 26;
            // 
            // txtFirstShiftStart
            // 
            this.txtFirstShiftStart.Location = new System.Drawing.Point(699, 126);
            this.txtFirstShiftStart.Name = "txtFirstShiftStart";
            this.txtFirstShiftStart.Size = new System.Drawing.Size(245, 22);
            this.txtFirstShiftStart.TabIndex = 25;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(699, 90);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(245, 22);
            this.txtName.TabIndex = 24;
            // 
            // txtMaxEarlyOutGrace
            // 
            this.txtMaxEarlyOutGrace.Location = new System.Drawing.Point(207, 276);
            this.txtMaxEarlyOutGrace.Name = "txtMaxEarlyOutGrace";
            this.txtMaxEarlyOutGrace.Size = new System.Drawing.Size(245, 22);
            this.txtMaxEarlyOutGrace.TabIndex = 23;
            // 
            // txtEarlyOutGrace
            // 
            this.txtEarlyOutGrace.Location = new System.Drawing.Point(207, 238);
            this.txtEarlyOutGrace.Name = "txtEarlyOutGrace";
            this.txtEarlyOutGrace.Size = new System.Drawing.Size(245, 22);
            this.txtEarlyOutGrace.TabIndex = 22;
            // 
            // txtSecondShiftEnd
            // 
            this.txtSecondShiftEnd.Location = new System.Drawing.Point(207, 201);
            this.txtSecondShiftEnd.Name = "txtSecondShiftEnd";
            this.txtSecondShiftEnd.Size = new System.Drawing.Size(245, 22);
            this.txtSecondShiftEnd.TabIndex = 20;
            // 
            // txtFirstShiftEnd
            // 
            this.txtFirstShiftEnd.Location = new System.Drawing.Point(207, 165);
            this.txtFirstShiftEnd.Name = "txtFirstShiftEnd";
            this.txtFirstShiftEnd.Size = new System.Drawing.Size(245, 22);
            this.txtFirstShiftEnd.TabIndex = 19;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(207, 90);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(245, 22);
            this.txtCode.TabIndex = 16;
            // 
            // lblMaxLateInGrace
            // 
            this.lblMaxLateInGrace.AutoSize = true;
            this.lblMaxLateInGrace.Location = new System.Drawing.Point(561, 233);
            this.lblMaxLateInGrace.Name = "lblMaxLateInGrace";
            this.lblMaxLateInGrace.Size = new System.Drawing.Size(123, 17);
            this.lblMaxLateInGrace.TabIndex = 14;
            this.lblMaxLateInGrace.Text = "Max Late In Grace";
            // 
            // lblLateInGrace
            // 
            this.lblLateInGrace.AutoSize = true;
            this.lblLateInGrace.Location = new System.Drawing.Point(561, 200);
            this.lblLateInGrace.Name = "lblLateInGrace";
            this.lblLateInGrace.Size = new System.Drawing.Size(94, 17);
            this.lblLateInGrace.TabIndex = 13;
            this.lblLateInGrace.Text = "Late In Grace";
            // 
            // lblSecondShiftStart
            // 
            this.lblSecondShiftStart.AutoSize = true;
            this.lblSecondShiftStart.Location = new System.Drawing.Point(561, 165);
            this.lblSecondShiftStart.Name = "lblSecondShiftStart";
            this.lblSecondShiftStart.Size = new System.Drawing.Size(122, 17);
            this.lblSecondShiftStart.TabIndex = 10;
            this.lblSecondShiftStart.Text = "Second Shift Start";
            // 
            // lblShiftStart
            // 
            this.lblShiftStart.AutoSize = true;
            this.lblShiftStart.Location = new System.Drawing.Point(561, 131);
            this.lblShiftStart.Name = "lblShiftStart";
            this.lblShiftStart.Size = new System.Drawing.Size(101, 17);
            this.lblShiftStart.TabIndex = 9;
            this.lblShiftStart.Text = "First Shift Start";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(561, 93);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Name";
            // 
            // lblMaxEarlyOutGrace
            // 
            this.lblMaxEarlyOutGrace.AutoSize = true;
            this.lblMaxEarlyOutGrace.Location = new System.Drawing.Point(48, 278);
            this.lblMaxEarlyOutGrace.Name = "lblMaxEarlyOutGrace";
            this.lblMaxEarlyOutGrace.Size = new System.Drawing.Size(139, 17);
            this.lblMaxEarlyOutGrace.TabIndex = 7;
            this.lblMaxEarlyOutGrace.Text = "Max Early Out Grace";
            // 
            // lblEarlyOutGrace
            // 
            this.lblEarlyOutGrace.AutoSize = true;
            this.lblEarlyOutGrace.Location = new System.Drawing.Point(48, 238);
            this.lblEarlyOutGrace.Name = "lblEarlyOutGrace";
            this.lblEarlyOutGrace.Size = new System.Drawing.Size(110, 17);
            this.lblEarlyOutGrace.TabIndex = 6;
            this.lblEarlyOutGrace.Text = "Early Out Grace";
            // 
            // lblSecondShiftEnd
            // 
            this.lblSecondShiftEnd.AutoSize = true;
            this.lblSecondShiftEnd.Location = new System.Drawing.Point(48, 201);
            this.lblSecondShiftEnd.Name = "lblSecondShiftEnd";
            this.lblSecondShiftEnd.Size = new System.Drawing.Size(117, 17);
            this.lblSecondShiftEnd.TabIndex = 4;
            this.lblSecondShiftEnd.Text = "Second Shift End";
            // 
            // lblShiftEnd
            // 
            this.lblShiftEnd.AutoSize = true;
            this.lblShiftEnd.Location = new System.Drawing.Point(48, 170);
            this.lblShiftEnd.Name = "lblShiftEnd";
            this.lblShiftEnd.Size = new System.Drawing.Size(96, 17);
            this.lblShiftEnd.TabIndex = 3;
            this.lblShiftEnd.Text = "First Shift End";
            // 
            // lblShiftType
            // 
            this.lblShiftType.AutoSize = true;
            this.lblShiftType.Location = new System.Drawing.Point(48, 131);
            this.lblShiftType.Name = "lblShiftType";
            this.lblShiftType.Size = new System.Drawing.Size(72, 17);
            this.lblShiftType.TabIndex = 1;
            this.lblShiftType.Text = "Shift Type";
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(48, 93);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(41, 17);
            this.lblCode.TabIndex = 0;
            this.lblCode.Text = "Code";
            // 
            // FormColorSetUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 704);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.lblheader);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormColorSetUp";
            this.Text = "FormColorSetUp";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.pnlBackGround.ResumeLayout(false);
            this.pnlBackGround.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label B;
        private System.Windows.Forms.TextBox txtR;
        private System.Windows.Forms.TextBox txtG;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel pnlBackGround;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comShiftType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkMarkAbsentWhenLateIn;
        private System.Windows.Forms.CheckBox chkMarkAbsentWhenEarlyOut;
        private System.Windows.Forms.Label lblMarkAbsentWhenLateIn;
        private System.Windows.Forms.Label lblMarkAbsentWhenEarlyOut;
        private System.Windows.Forms.TextBox txtMaxLateInGrace;
        private System.Windows.Forms.TextBox txtLateInGrace;
        private System.Windows.Forms.TextBox txtSecondShiftStart;
        private System.Windows.Forms.TextBox txtFirstShiftStart;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMaxEarlyOutGrace;
        private System.Windows.Forms.TextBox txtEarlyOutGrace;
        private System.Windows.Forms.TextBox txtSecondShiftEnd;
        private System.Windows.Forms.TextBox txtFirstShiftEnd;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label lblMaxLateInGrace;
        private System.Windows.Forms.Label lblLateInGrace;
        private System.Windows.Forms.Label lblSecondShiftStart;
        private System.Windows.Forms.Label lblShiftStart;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMaxEarlyOutGrace;
        private System.Windows.Forms.Label lblEarlyOutGrace;
        private System.Windows.Forms.Label lblSecondShiftEnd;
        private System.Windows.Forms.Label lblShiftEnd;
        private System.Windows.Forms.Label lblShiftType;
        private System.Windows.Forms.Label lblCode;
    }
}