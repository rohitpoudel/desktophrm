﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRM;

namespace HRM.Forms.SetUp
{
    public partial class frmDualShift : Form
    {
        IShiftHRM iShiftHRM = null;
        private string selectedEvent = "-1";
        private string selectedId = "-1";

        public frmDualShift()
        {
            InitializeComponent();
            comShiftType.SelectedIndex = 0;
            //comHalfDay.SelectedIndex = 0;
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
        }
        public frmDualShift(string selectId, string selectEvent)
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            comShiftType.SelectedIndex = 0;
           // comHalfDay.SelectedIndex = 0;
            selectedId = selectId;
            selectedEvent = selectEvent;
        }

        private void frmShift_Load(object sender, EventArgs e)
        {
            if (selectedEvent == "I")
            {
                btn_Create.Text = "Create";
                lblheader.Text = "Create Office Days";
            }
            else if (selectedId != "-1")
            {
                iShiftHRM = new ShiftHRM();
                DualShift dualShift = null;
                dualShift = iShiftHRM.GetDualShiftById(Convert.ToInt32(selectedId));
                if (dualShift != null)
                {
                    SetDualShift(ref dualShift);
                    if (selectedEvent == "V")
                    {
                        lblheader.Text = "View Office Days";
                        btn_Create.Visible = false;
                        ReadOnlyGetDualShift();
                    }
                    else if (selectedEvent == "U")
                    {
                        lblheader.Text = "Modify Office Days";
                        btn_Create.Text = "Save";
                    }
                }
            }
        }

        private void ReadOnlyGetDualShift()
        {
            txtCode.ReadOnly = true;
            txtName.ReadOnly = true;
            comShiftType.Enabled = false;
            txtFirstShiftStart.ReadOnly = true;
            txtFirstShiftEnd.ReadOnly = true;
            //txtFirstShiftHours.ReadOnly = true;
            txtSecondShiftStart.ReadOnly = true;
            txtSecondShiftEnd.ReadOnly = true;
            // txtSecondShiftHours.ReadOnly = true;
            txtLateInGrace.ReadOnly = true;
            txtEarlyOutGrace.ReadOnly = true;
            txtMaxLateInGrace.ReadOnly = true;
            txtMaxEarlyOutGrace.ReadOnly = true;
            chkMarkAbsentWhenLateIn.Enabled = false;
            chkMarkAbsentWhenEarlyOut.Enabled = false;

        }

        public void GetDualShift(ref DualShift dualShift)
        {
            dualShift.Code = txtCode.Text.Trim();
            dualShift.Name = txtName.Text.Trim();
            dualShift.ShiftType = comShiftType.SelectedText;
            dualShift.FirstShiftStart = txtFirstShiftStart.Text.Trim();
            dualShift.FirstShiftEnd = txtFirstShiftEnd.Text.Trim();
            dualShift.FirstShiftHours = "8"; ; //txtFirstShiftHours.Text.Trim();
            dualShift.SecondShiftStart = txtSecondShiftStart.Text.Trim();
            dualShift.SecondShiftEnd = txtSecondShiftEnd.Text.Trim();
            dualShift.SecondShiftHours = "6"; //txtSecondShiftHours.Text.Trim();
            dualShift.LateGrace = txtLateInGrace.Text.Trim();
            dualShift.EarlyGrace = txtEarlyOutGrace.Text.Trim();
            dualShift.MaxLateGrace = txtMaxLateInGrace.Text.Trim();
            dualShift.MaxEarlyGrace = txtMaxEarlyOutGrace.Text.Trim();
            dualShift.MarkAbsentWhenLateIn = chkMarkAbsentWhenLateIn.Checked;
            dualShift.MarkAbsentWhenEarlyOut = chkMarkAbsentWhenEarlyOut.Checked;

            //dualShift.OverTimeFrom = txtOverTimeFrom.Text.Trim();
        }
        private void SetDualShift(ref DualShift dualShift)
        {
            txtCode.Text = dualShift.Code ;
             txtName.Text = dualShift.Name ;
            comShiftType.SelectedText =dualShift.ShiftType  ;
            txtFirstShiftStart.Text= dualShift.FirstShiftStart;
            txtFirstShiftEnd.Text = dualShift.FirstShiftEnd ;
            //dualShift.FirstShiftHours = "8"; ; //txtFirstShiftHours.Text.Trim();
            txtSecondShiftStart.Text = dualShift.SecondShiftStart;
            txtSecondShiftEnd.Text = dualShift.SecondShiftEnd ;
            //dualShift.SecondShiftHours = "6"; //txtSecondShiftHours.Text.Trim();
            txtLateInGrace.Text = dualShift.LateGrace ;
            txtEarlyOutGrace.Text = dualShift.EarlyGrace ;
            txtMaxLateInGrace.Text = dualShift.MaxLateGrace ;
            txtMaxEarlyOutGrace.Text= dualShift.MaxEarlyGrace;
            chkMarkAbsentWhenLateIn.Checked= dualShift.MarkAbsentWhenLateIn  ;
            chkMarkAbsentWhenEarlyOut.Checked = dualShift.MarkAbsentWhenEarlyOut;

        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            iShiftHRM = new ShiftHRM();
            if (btn_Create.Text == "Create" && selectedEvent == "I")
            {
                DualShift dualShift = new DualShift();
                GetDualShift(ref dualShift);
                dualShift.CreatedBy = 1;
                dualShift.CreatedDate = System.DateTime.Now;
                dualShift.IsDeleted = false;
                dualShift.Event = 'I';
                dualShift.Id = 0;
                int a = iShiftHRM.CreateDualShift(dualShift);
                if (a > 0)
                {
                    message = "Dual Shift Created Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot create Dual shift This Time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            else if (btn_Create.Text == "Save" && selectedEvent == "U")
            {
                DualShift dualShift = null;
                dualShift = iShiftHRM.GetDualShiftById(Convert.ToInt32(selectedId));
                GetDualShift(ref dualShift);
                dualShift.UpdatedBy = 1;
                dualShift.UpdatedDate = System.DateTime.Now;
                dualShift.IsDeleted = false;
                dualShift.Event = 'U';
                dualShift.Id = Convert.ToInt32(selectedId);
                int a = iShiftHRM.CreateDualShift(dualShift);
                if (a > 0)
                {
                    message = "Update Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot Update this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }
                                                                       


    }
}
