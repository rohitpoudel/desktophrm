﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM
{
    public partial class CreateFiscalYear : Form
    {
        IFiscalYearQueryProcessor iFiscalYearQueryProcessor = null;
        public CreateFiscalYear()
        {
            InitializeComponent();
            var data = Dropdown.BindYears();
            var dt = General.ToDataTable(data);
            cboFiscalYear.DataSource = dt;
            cboFiscalYear.DisplayMember = "Text";
            cboFiscalYear.ValueMember = "Value";
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            iFiscalYearQueryProcessor = new FiscalYearQueryProcessor();
            FiscalYear fiscalYear= new FiscalYear();
            GetFiscalYear(ref fiscalYear);
            int a = iFiscalYearQueryProcessor.Create(fiscalYear);
            if (true)
            {

            }

        }

        private void GetFiscalYear(ref FiscalYear fiscalYear)
        {
            fiscalYear.StartDate = txtStartDate.Text.Trim();
            fiscalYear.EndDate = txtEndDate.Text.Trim();
            //fiscalYear.StartDateNepali = NepDateConverter.EngToNep(Convert.ToDateTime(fiscalYear.StartDate));
            //fiscalYear.EndDateNepali = NepDateConverter.EngToNep(Convert.ToDateTime(fiscalYear.EndDate));
        }

        private void cboFiscalYear_SelectedValueChanged(object sender, EventArgs e)
        {
            var val = cboFiscalYear.SelectedItem;
           
            //if(val!=0)
            //{
            //    txtStartDate.Text = string.Format("{0}-{1}-{2}", Year, 04, 01);
            //    txtEndDate.Text = string.Format("{0}-{1}-{2}", Convert.ToInt32(Year) + 1, 03, 30);
            //}
           
        }

        private void CreateFiscalYear_Load(object sender, EventArgs e)
        {

        }


    }
}
