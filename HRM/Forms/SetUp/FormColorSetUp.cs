﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class FormColorSetUp : Form
    {
        public FormColorSetUp()
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            //this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            txtR.Text = aa[0].ToString();
            txtG.Text = aa[1].ToString();
            txtB.Text = aa[2].ToString();
            trackBar1.Value = aa[0];
            trackBar2.Value = aa[1];
            trackBar3.Value = aa[2];


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            txtR.Text = trackBar1.Value.ToString();
            txtG.Text = trackBar2.Value.ToString();
            txtB.Text = trackBar3.Value.ToString();

            int r = Convert.ToInt32(txtR.Text);
            int g = Convert.ToInt32(txtG.Text);
            int b = Convert.ToInt32(txtB.Text);
            //this.BackColor = Color.FromArgb(r, g, b);
            pnlBackGround.BackColor= System.Drawing.Color.FromArgb(r, g, b);
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            ColorSetUp colorSetUp = new ColorSetUp();
            colorSetUp.id = -1;
            colorSetUp.Red = Convert.ToInt32(txtR.Text.ToString().Trim());
            colorSetUp.Green = Convert.ToInt32(txtG.Text.ToString().Trim());
            colorSetUp.Blue = Convert.ToInt32(txtB.Text.ToString().Trim());
            colorSetUp.CreatedBy = 0;
            colorSetUp.CreatedOn = DateTime.Now;
            colorSetUp.LastModifiedBy = 0;
            colorSetUp.LastModifiedOn = DateTime.Now;

            IColorSetUpHRM iColorSetUpHRM = null;
            iColorSetUpHRM = new ColorSetUpHRM();
            int i = iColorSetUpHRM.Create(colorSetUp);
            if (i>0)
            {
                MessageBox.Show("Update Success", "update Conformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Update failed", "update Conformation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
