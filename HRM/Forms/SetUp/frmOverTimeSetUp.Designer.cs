﻿namespace HRM.Forms.SetUp
{
    partial class frmOverTimeSetUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.chkOverTimeAllowed = new System.Windows.Forms.CheckBox();
            this.cmbFicsalYear = new System.Windows.Forms.ComboBox();
            this.lblSalaryDeductionOnLate = new System.Windows.Forms.Label();
            this.lblLateInOrEarlyOut = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.lblheader = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(496, 224);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(113, 36);
            this.btn_Reset.TabIndex = 56;
            this.btn_Reset.Text = "Cancel";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(362, 224);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(113, 36);
            this.btnCreate.TabIndex = 57;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(1, 211);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1038, 1);
            this.label1.TabIndex = 55;
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(4, 49);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1035, 1);
            this.lblhorizentalline.TabIndex = 54;
            // 
            // chkOverTimeAllowed
            // 
            this.chkOverTimeAllowed.AutoSize = true;
            this.chkOverTimeAllowed.Location = new System.Drawing.Point(362, 128);
            this.chkOverTimeAllowed.Name = "chkOverTimeAllowed";
            this.chkOverTimeAllowed.Size = new System.Drawing.Size(18, 17);
            this.chkOverTimeAllowed.TabIndex = 51;
            this.chkOverTimeAllowed.UseVisualStyleBackColor = true;
            // 
            // cmbFicsalYear
            // 
            this.cmbFicsalYear.FormattingEnabled = true;
            this.cmbFicsalYear.Location = new System.Drawing.Point(362, 82);
            this.cmbFicsalYear.Name = "cmbFicsalYear";
            this.cmbFicsalYear.Size = new System.Drawing.Size(247, 24);
            this.cmbFicsalYear.TabIndex = 49;
            // 
            // lblSalaryDeductionOnLate
            // 
            this.lblSalaryDeductionOnLate.AutoSize = true;
            this.lblSalaryDeductionOnLate.Location = new System.Drawing.Point(201, 128);
            this.lblSalaryDeductionOnLate.Name = "lblSalaryDeductionOnLate";
            this.lblSalaryDeductionOnLate.Size = new System.Drawing.Size(126, 17);
            this.lblSalaryDeductionOnLate.TabIndex = 45;
            this.lblSalaryDeductionOnLate.Text = "Over Time Allowed";
            // 
            // lblLateInOrEarlyOut
            // 
            this.lblLateInOrEarlyOut.AutoSize = true;
            this.lblLateInOrEarlyOut.Location = new System.Drawing.Point(201, 89);
            this.lblLateInOrEarlyOut.Name = "lblLateInOrEarlyOut";
            this.lblLateInOrEarlyOut.Size = new System.Drawing.Size(78, 17);
            this.lblLateInOrEarlyOut.TabIndex = 48;
            this.lblLateInOrEarlyOut.Text = "Fiscal Year";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.lblheader);
            this.panel1.Location = new System.Drawing.Point(1, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 38);
            this.panel1.TabIndex = 68;
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.Red;
            this.btn_close.Location = new System.Drawing.Point(999, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 32);
            this.btn_close.TabIndex = 46;
            this.btn_close.Text = "X";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(3, 5);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(272, 29);
            this.lblheader.TabIndex = 41;
            this.lblheader.Text = "Create Over Time Rule";
            // 
            // frmOverTimeSetUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 344);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.chkOverTimeAllowed);
            this.Controls.Add(this.cmbFicsalYear);
            this.Controls.Add(this.lblSalaryDeductionOnLate);
            this.Controls.Add(this.lblLateInOrEarlyOut);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmOverTimeSetUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmOverTimeSetUp_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.CheckBox chkOverTimeAllowed;
        private System.Windows.Forms.ComboBox cmbFicsalYear;
        private System.Windows.Forms.Label lblSalaryDeductionOnLate;
        private System.Windows.Forms.Label lblLateInOrEarlyOut;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lblheader;
    }
}