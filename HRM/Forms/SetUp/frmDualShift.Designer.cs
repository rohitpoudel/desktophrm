﻿namespace HRM.Forms.SetUp
{
    partial class frmDualShift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comShiftType = new System.Windows.Forms.ComboBox();
            this.btn_Create = new System.Windows.Forms.Button();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.chkMarkAbsentWhenLateIn = new System.Windows.Forms.CheckBox();
            this.chkMarkAbsentWhenEarlyOut = new System.Windows.Forms.CheckBox();
            this.lblMarkAbsentWhenLateIn = new System.Windows.Forms.Label();
            this.lblMarkAbsentWhenEarlyOut = new System.Windows.Forms.Label();
            this.txtMaxLateInGrace = new System.Windows.Forms.TextBox();
            this.txtLateInGrace = new System.Windows.Forms.TextBox();
            this.txtSecondShiftStart = new System.Windows.Forms.TextBox();
            this.txtFirstShiftStart = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMaxEarlyOutGrace = new System.Windows.Forms.TextBox();
            this.txtEarlyOutGrace = new System.Windows.Forms.TextBox();
            this.txtSecondShiftEnd = new System.Windows.Forms.TextBox();
            this.txtFirstShiftEnd = new System.Windows.Forms.TextBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblMaxLateInGrace = new System.Windows.Forms.Label();
            this.lblLateInGrace = new System.Windows.Forms.Label();
            this.lblSecondShiftStart = new System.Windows.Forms.Label();
            this.lblShiftStart = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMaxEarlyOutGrace = new System.Windows.Forms.Label();
            this.lblEarlyOutGrace = new System.Windows.Forms.Label();
            this.lblSecondShiftEnd = new System.Windows.Forms.Label();
            this.lblShiftEnd = new System.Windows.Forms.Label();
            this.lblShiftType = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.lblheader = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(2, 365);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1028, 2);
            this.label1.TabIndex = 46;
            // 
            // comShiftType
            // 
            this.comShiftType.FormattingEnabled = true;
            this.comShiftType.Items.AddRange(new object[] {
            "-- Select Shift--",
            "Dual Shift",
            "Morning",
            "Day",
            "Night"});
            this.comShiftType.Location = new System.Drawing.Point(213, 129);
            this.comShiftType.Name = "comShiftType";
            this.comShiftType.Size = new System.Drawing.Size(245, 24);
            this.comShiftType.TabIndex = 44;
            // 
            // btn_Create
            // 
            this.btn_Create.Location = new System.Drawing.Point(343, 391);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(118, 39);
            this.btn_Create.TabIndex = 40;
            this.btn_Create.Text = "Create";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(5, 63);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1028, 2);
            this.lblhorizentalline.TabIndex = 39;
            // 
            // chkMarkAbsentWhenLateIn
            // 
            this.chkMarkAbsentWhenLateIn.AutoSize = true;
            this.chkMarkAbsentWhenLateIn.Location = new System.Drawing.Point(769, 273);
            this.chkMarkAbsentWhenLateIn.Name = "chkMarkAbsentWhenLateIn";
            this.chkMarkAbsentWhenLateIn.Size = new System.Drawing.Size(18, 17);
            this.chkMarkAbsentWhenLateIn.TabIndex = 37;
            this.chkMarkAbsentWhenLateIn.UseVisualStyleBackColor = true;
            // 
            // chkMarkAbsentWhenEarlyOut
            // 
            this.chkMarkAbsentWhenEarlyOut.AutoSize = true;
            this.chkMarkAbsentWhenEarlyOut.Location = new System.Drawing.Point(247, 313);
            this.chkMarkAbsentWhenEarlyOut.Name = "chkMarkAbsentWhenEarlyOut";
            this.chkMarkAbsentWhenEarlyOut.Size = new System.Drawing.Size(18, 17);
            this.chkMarkAbsentWhenEarlyOut.TabIndex = 36;
            this.chkMarkAbsentWhenEarlyOut.UseVisualStyleBackColor = true;
            // 
            // lblMarkAbsentWhenLateIn
            // 
            this.lblMarkAbsentWhenLateIn.AutoSize = true;
            this.lblMarkAbsentWhenLateIn.Location = new System.Drawing.Point(561, 274);
            this.lblMarkAbsentWhenLateIn.Name = "lblMarkAbsentWhenLateIn";
            this.lblMarkAbsentWhenLateIn.Size = new System.Drawing.Size(171, 17);
            this.lblMarkAbsentWhenLateIn.TabIndex = 35;
            this.lblMarkAbsentWhenLateIn.Text = "Mark Absent When LateIn";
            // 
            // lblMarkAbsentWhenEarlyOut
            // 
            this.lblMarkAbsentWhenEarlyOut.AutoSize = true;
            this.lblMarkAbsentWhenEarlyOut.Location = new System.Drawing.Point(54, 312);
            this.lblMarkAbsentWhenEarlyOut.Name = "lblMarkAbsentWhenEarlyOut";
            this.lblMarkAbsentWhenEarlyOut.Size = new System.Drawing.Size(187, 17);
            this.lblMarkAbsentWhenEarlyOut.TabIndex = 34;
            this.lblMarkAbsentWhenEarlyOut.Text = "Mark Absent When EarlyOut";
            // 
            // txtMaxLateInGrace
            // 
            this.txtMaxLateInGrace.Location = new System.Drawing.Point(705, 231);
            this.txtMaxLateInGrace.Name = "txtMaxLateInGrace";
            this.txtMaxLateInGrace.Size = new System.Drawing.Size(245, 22);
            this.txtMaxLateInGrace.TabIndex = 30;
            // 
            // txtLateInGrace
            // 
            this.txtLateInGrace.Location = new System.Drawing.Point(705, 196);
            this.txtLateInGrace.Name = "txtLateInGrace";
            this.txtLateInGrace.Size = new System.Drawing.Size(245, 22);
            this.txtLateInGrace.TabIndex = 29;
            // 
            // txtSecondShiftStart
            // 
            this.txtSecondShiftStart.Location = new System.Drawing.Point(705, 160);
            this.txtSecondShiftStart.Name = "txtSecondShiftStart";
            this.txtSecondShiftStart.Size = new System.Drawing.Size(245, 22);
            this.txtSecondShiftStart.TabIndex = 26;
            // 
            // txtFirstShiftStart
            // 
            this.txtFirstShiftStart.Location = new System.Drawing.Point(705, 124);
            this.txtFirstShiftStart.Name = "txtFirstShiftStart";
            this.txtFirstShiftStart.Size = new System.Drawing.Size(245, 22);
            this.txtFirstShiftStart.TabIndex = 25;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(705, 88);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(245, 22);
            this.txtName.TabIndex = 24;
            // 
            // txtMaxEarlyOutGrace
            // 
            this.txtMaxEarlyOutGrace.Location = new System.Drawing.Point(213, 274);
            this.txtMaxEarlyOutGrace.Name = "txtMaxEarlyOutGrace";
            this.txtMaxEarlyOutGrace.Size = new System.Drawing.Size(245, 22);
            this.txtMaxEarlyOutGrace.TabIndex = 23;
            // 
            // txtEarlyOutGrace
            // 
            this.txtEarlyOutGrace.Location = new System.Drawing.Point(213, 236);
            this.txtEarlyOutGrace.Name = "txtEarlyOutGrace";
            this.txtEarlyOutGrace.Size = new System.Drawing.Size(245, 22);
            this.txtEarlyOutGrace.TabIndex = 22;
            // 
            // txtSecondShiftEnd
            // 
            this.txtSecondShiftEnd.Location = new System.Drawing.Point(213, 199);
            this.txtSecondShiftEnd.Name = "txtSecondShiftEnd";
            this.txtSecondShiftEnd.Size = new System.Drawing.Size(245, 22);
            this.txtSecondShiftEnd.TabIndex = 20;
            // 
            // txtFirstShiftEnd
            // 
            this.txtFirstShiftEnd.Location = new System.Drawing.Point(213, 163);
            this.txtFirstShiftEnd.Name = "txtFirstShiftEnd";
            this.txtFirstShiftEnd.Size = new System.Drawing.Size(245, 22);
            this.txtFirstShiftEnd.TabIndex = 19;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(213, 88);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(245, 22);
            this.txtCode.TabIndex = 16;
            // 
            // lblMaxLateInGrace
            // 
            this.lblMaxLateInGrace.AutoSize = true;
            this.lblMaxLateInGrace.Location = new System.Drawing.Point(567, 231);
            this.lblMaxLateInGrace.Name = "lblMaxLateInGrace";
            this.lblMaxLateInGrace.Size = new System.Drawing.Size(123, 17);
            this.lblMaxLateInGrace.TabIndex = 14;
            this.lblMaxLateInGrace.Text = "Max Late In Grace";
            // 
            // lblLateInGrace
            // 
            this.lblLateInGrace.AutoSize = true;
            this.lblLateInGrace.Location = new System.Drawing.Point(567, 198);
            this.lblLateInGrace.Name = "lblLateInGrace";
            this.lblLateInGrace.Size = new System.Drawing.Size(94, 17);
            this.lblLateInGrace.TabIndex = 13;
            this.lblLateInGrace.Text = "Late In Grace";
            // 
            // lblSecondShiftStart
            // 
            this.lblSecondShiftStart.AutoSize = true;
            this.lblSecondShiftStart.Location = new System.Drawing.Point(567, 163);
            this.lblSecondShiftStart.Name = "lblSecondShiftStart";
            this.lblSecondShiftStart.Size = new System.Drawing.Size(122, 17);
            this.lblSecondShiftStart.TabIndex = 10;
            this.lblSecondShiftStart.Text = "Second Shift Start";
            // 
            // lblShiftStart
            // 
            this.lblShiftStart.AutoSize = true;
            this.lblShiftStart.Location = new System.Drawing.Point(567, 129);
            this.lblShiftStart.Name = "lblShiftStart";
            this.lblShiftStart.Size = new System.Drawing.Size(101, 17);
            this.lblShiftStart.TabIndex = 9;
            this.lblShiftStart.Text = "First Shift Start";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(567, 91);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Name";
            // 
            // lblMaxEarlyOutGrace
            // 
            this.lblMaxEarlyOutGrace.AutoSize = true;
            this.lblMaxEarlyOutGrace.Location = new System.Drawing.Point(54, 276);
            this.lblMaxEarlyOutGrace.Name = "lblMaxEarlyOutGrace";
            this.lblMaxEarlyOutGrace.Size = new System.Drawing.Size(139, 17);
            this.lblMaxEarlyOutGrace.TabIndex = 7;
            this.lblMaxEarlyOutGrace.Text = "Max Early Out Grace";
            // 
            // lblEarlyOutGrace
            // 
            this.lblEarlyOutGrace.AutoSize = true;
            this.lblEarlyOutGrace.Location = new System.Drawing.Point(54, 236);
            this.lblEarlyOutGrace.Name = "lblEarlyOutGrace";
            this.lblEarlyOutGrace.Size = new System.Drawing.Size(110, 17);
            this.lblEarlyOutGrace.TabIndex = 6;
            this.lblEarlyOutGrace.Text = "Early Out Grace";
            // 
            // lblSecondShiftEnd
            // 
            this.lblSecondShiftEnd.AutoSize = true;
            this.lblSecondShiftEnd.Location = new System.Drawing.Point(54, 199);
            this.lblSecondShiftEnd.Name = "lblSecondShiftEnd";
            this.lblSecondShiftEnd.Size = new System.Drawing.Size(117, 17);
            this.lblSecondShiftEnd.TabIndex = 4;
            this.lblSecondShiftEnd.Text = "Second Shift End";
            // 
            // lblShiftEnd
            // 
            this.lblShiftEnd.AutoSize = true;
            this.lblShiftEnd.Location = new System.Drawing.Point(54, 168);
            this.lblShiftEnd.Name = "lblShiftEnd";
            this.lblShiftEnd.Size = new System.Drawing.Size(96, 17);
            this.lblShiftEnd.TabIndex = 3;
            this.lblShiftEnd.Text = "First Shift End";
            // 
            // lblShiftType
            // 
            this.lblShiftType.AutoSize = true;
            this.lblShiftType.Location = new System.Drawing.Point(54, 129);
            this.lblShiftType.Name = "lblShiftType";
            this.lblShiftType.Size = new System.Drawing.Size(72, 17);
            this.lblShiftType.TabIndex = 1;
            this.lblShiftType.Text = "Shift Type";
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(54, 91);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(41, 17);
            this.lblCode.TabIndex = 0;
            this.lblCode.Text = "Code";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(486, 391);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 39);
            this.button1.TabIndex = 42;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.lblheader);
            this.panel1.Location = new System.Drawing.Point(2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 38);
            this.panel1.TabIndex = 47;
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.Red;
            this.btn_close.Location = new System.Drawing.Point(999, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 32);
            this.btn_close.TabIndex = 46;
            this.btn_close.Text = "X";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(3, 5);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(205, 29);
            this.lblheader.TabIndex = 41;
            this.lblheader.Text = "Create Dual Shift";
            // 
            // frmDualShift
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 458);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comShiftType);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.btn_Create);
            this.Controls.Add(this.lblShiftType);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.lblShiftEnd);
            this.Controls.Add(this.lblSecondShiftEnd);
            this.Controls.Add(this.chkMarkAbsentWhenLateIn);
            this.Controls.Add(this.lblEarlyOutGrace);
            this.Controls.Add(this.chkMarkAbsentWhenEarlyOut);
            this.Controls.Add(this.lblMaxEarlyOutGrace);
            this.Controls.Add(this.lblMarkAbsentWhenLateIn);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblMarkAbsentWhenEarlyOut);
            this.Controls.Add(this.lblShiftStart);
            this.Controls.Add(this.txtMaxLateInGrace);
            this.Controls.Add(this.lblSecondShiftStart);
            this.Controls.Add(this.txtLateInGrace);
            this.Controls.Add(this.lblLateInGrace);
            this.Controls.Add(this.txtSecondShiftStart);
            this.Controls.Add(this.lblMaxLateInGrace);
            this.Controls.Add(this.txtFirstShiftStart);
            this.Controls.Add(this.txtFirstShiftEnd);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtSecondShiftEnd);
            this.Controls.Add(this.txtMaxEarlyOutGrace);
            this.Controls.Add(this.txtEarlyOutGrace);
            this.Name = "frmDualShift";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblMaxLateInGrace;
        private System.Windows.Forms.Label lblLateInGrace;
        private System.Windows.Forms.Label lblSecondShiftStart;
        private System.Windows.Forms.Label lblShiftStart;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMaxEarlyOutGrace;
        private System.Windows.Forms.Label lblEarlyOutGrace;
        private System.Windows.Forms.Label lblSecondShiftEnd;
        private System.Windows.Forms.Label lblShiftEnd;
        private System.Windows.Forms.Label lblShiftType;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox txtMaxLateInGrace;
        private System.Windows.Forms.TextBox txtLateInGrace;
        private System.Windows.Forms.TextBox txtSecondShiftStart;
        private System.Windows.Forms.TextBox txtFirstShiftStart;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMaxEarlyOutGrace;
        private System.Windows.Forms.TextBox txtEarlyOutGrace;
        private System.Windows.Forms.TextBox txtSecondShiftEnd;
        private System.Windows.Forms.TextBox txtFirstShiftEnd;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label lblMarkAbsentWhenLateIn;
        private System.Windows.Forms.Label lblMarkAbsentWhenEarlyOut;
        private System.Windows.Forms.CheckBox chkMarkAbsentWhenLateIn;
        private System.Windows.Forms.CheckBox chkMarkAbsentWhenEarlyOut;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.ComboBox comShiftType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Button btn_close;
    }
}