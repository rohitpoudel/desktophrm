﻿namespace HRM.Forms.SetUp
{
    partial class frmLateRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFiscalYear = new System.Windows.Forms.Label();
            this.lblLateInOrEarlyOut = new System.Windows.Forms.Label();
            this.lblDaysAllowed = new System.Windows.Forms.Label();
            this.lblLeaveOnLate = new System.Windows.Forms.Label();
            this.lblSalaryDeductionOnLate = new System.Windows.Forms.Label();
            this.cmbFisicalYear = new System.Windows.Forms.ComboBox();
            this.cmbLateinOnEarlyOut = new System.Windows.Forms.ComboBox();
            this.chkLeaveOnLate = new System.Windows.Forms.CheckBox();
            this.chkSalaryDecuctionOnLate = new System.Windows.Forms.CheckBox();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.lblheader = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.txtDaysAllowed = new System.Windows.Forms.TextBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lblFiscalYear
            // 
            this.lblFiscalYear.AutoSize = true;
            this.lblFiscalYear.Location = new System.Drawing.Point(74, 77);
            this.lblFiscalYear.Name = "lblFiscalYear";
            this.lblFiscalYear.Size = new System.Drawing.Size(78, 17);
            this.lblFiscalYear.TabIndex = 0;
            this.lblFiscalYear.Text = "Fiscal Year";
            // 
            // lblLateInOrEarlyOut
            // 
            this.lblLateInOrEarlyOut.AutoSize = true;
            this.lblLateInOrEarlyOut.Location = new System.Drawing.Point(74, 111);
            this.lblLateInOrEarlyOut.Name = "lblLateInOrEarlyOut";
            this.lblLateInOrEarlyOut.Size = new System.Drawing.Size(130, 17);
            this.lblLateInOrEarlyOut.TabIndex = 0;
            this.lblLateInOrEarlyOut.Text = "LateIn Or Early Out";
            // 
            // lblDaysAllowed
            // 
            this.lblDaysAllowed.AutoSize = true;
            this.lblDaysAllowed.Location = new System.Drawing.Point(74, 145);
            this.lblDaysAllowed.Name = "lblDaysAllowed";
            this.lblDaysAllowed.Size = new System.Drawing.Size(92, 17);
            this.lblDaysAllowed.TabIndex = 0;
            this.lblDaysAllowed.Text = "Days Allowed";
            // 
            // lblLeaveOnLate
            // 
            this.lblLeaveOnLate.AutoSize = true;
            this.lblLeaveOnLate.Location = new System.Drawing.Point(74, 181);
            this.lblLeaveOnLate.Name = "lblLeaveOnLate";
            this.lblLeaveOnLate.Size = new System.Drawing.Size(102, 17);
            this.lblLeaveOnLate.TabIndex = 0;
            this.lblLeaveOnLate.Text = "Leave On Late";
            // 
            // lblSalaryDeductionOnLate
            // 
            this.lblSalaryDeductionOnLate.AutoSize = true;
            this.lblSalaryDeductionOnLate.Location = new System.Drawing.Point(74, 213);
            this.lblSalaryDeductionOnLate.Name = "lblSalaryDeductionOnLate";
            this.lblSalaryDeductionOnLate.Size = new System.Drawing.Size(171, 17);
            this.lblSalaryDeductionOnLate.TabIndex = 0;
            this.lblSalaryDeductionOnLate.Text = "Salary Deduction On Late";
            // 
            // cmbFisicalYear
            // 
            this.cmbFisicalYear.FormattingEnabled = true;
            this.cmbFisicalYear.Location = new System.Drawing.Point(299, 68);
            this.cmbFisicalYear.Name = "cmbFisicalYear";
            this.cmbFisicalYear.Size = new System.Drawing.Size(247, 24);
            this.cmbFisicalYear.TabIndex = 1;
            // 
            // cmbLateinOnEarlyOut
            // 
            this.cmbLateinOnEarlyOut.FormattingEnabled = true;
            this.cmbLateinOnEarlyOut.Location = new System.Drawing.Point(299, 104);
            this.cmbLateinOnEarlyOut.Name = "cmbLateinOnEarlyOut";
            this.cmbLateinOnEarlyOut.Size = new System.Drawing.Size(247, 24);
            this.cmbLateinOnEarlyOut.TabIndex = 1;
            // 
            // chkLeaveOnLate
            // 
            this.chkLeaveOnLate.AutoSize = true;
            this.chkLeaveOnLate.Location = new System.Drawing.Point(299, 181);
            this.chkLeaveOnLate.Name = "chkLeaveOnLate";
            this.chkLeaveOnLate.Size = new System.Drawing.Size(18, 17);
            this.chkLeaveOnLate.TabIndex = 2;
            this.chkLeaveOnLate.UseVisualStyleBackColor = true;
            // 
            // chkSalaryDecuctionOnLate
            // 
            this.chkSalaryDecuctionOnLate.AutoSize = true;
            this.chkSalaryDecuctionOnLate.Location = new System.Drawing.Point(299, 213);
            this.chkSalaryDecuctionOnLate.Name = "chkSalaryDecuctionOnLate";
            this.chkSalaryDecuctionOnLate.Size = new System.Drawing.Size(18, 17);
            this.chkSalaryDecuctionOnLate.TabIndex = 2;
            this.chkSalaryDecuctionOnLate.UseVisualStyleBackColor = true;
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(-116, 45);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1028, 1);
            this.lblhorizentalline.TabIndex = 41;
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(4, 5);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(203, 29);
            this.lblheader.TabIndex = 40;
            this.lblheader.Text = "Create Late Rule";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(-57, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(971, 1);
            this.label1.TabIndex = 42;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(299, 275);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(113, 36);
            this.btnCreate.TabIndex = 43;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(433, 275);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(113, 36);
            this.btn_Reset.TabIndex = 43;
            this.btn_Reset.Text = "Cancel";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // txtDaysAllowed
            // 
            this.txtDaysAllowed.Location = new System.Drawing.Point(299, 142);
            this.txtDaysAllowed.Name = "txtDaysAllowed";
            this.txtDaysAllowed.Size = new System.Drawing.Size(247, 22);
            this.txtDaysAllowed.TabIndex = 44;
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.Red;
            this.btn_close.Location = new System.Drawing.Point(885, 4);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 32);
            this.btn_close.TabIndex = 45;
            this.btn_close.Text = "X";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(-9, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 38);
            this.panel1.TabIndex = 46;
            // 
            // frmLateRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 439);
            this.ControlBox = false;
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.txtDaysAllowed);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.lblheader);
            this.Controls.Add(this.chkSalaryDecuctionOnLate);
            this.Controls.Add(this.chkLeaveOnLate);
            this.Controls.Add(this.cmbLateinOnEarlyOut);
            this.Controls.Add(this.cmbFisicalYear);
            this.Controls.Add(this.lblSalaryDeductionOnLate);
            this.Controls.Add(this.lblLeaveOnLate);
            this.Controls.Add(this.lblDaysAllowed);
            this.Controls.Add(this.lblLateInOrEarlyOut);
            this.Controls.Add(this.lblFiscalYear);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLateRule";
            this.Load += new System.EventHandler(this.frmLateRule_Load);
            this.Move += new System.EventHandler(this.frmLateRule_Move);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFiscalYear;
        private System.Windows.Forms.Label lblLateInOrEarlyOut;
        private System.Windows.Forms.Label lblDaysAllowed;
        private System.Windows.Forms.Label lblLeaveOnLate;
        private System.Windows.Forms.Label lblSalaryDeductionOnLate;
        private System.Windows.Forms.ComboBox cmbFisicalYear;
        private System.Windows.Forms.ComboBox cmbLateinOnEarlyOut;
        private System.Windows.Forms.CheckBox chkLeaveOnLate;
        private System.Windows.Forms.CheckBox chkSalaryDecuctionOnLate;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.TextBox txtDaysAllowed;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Panel panel1;
    }
}