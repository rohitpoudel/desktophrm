﻿namespace HRM.Forms.SetUp
{
    partial class IndexOfficeDays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.lblheader = new System.Windows.Forms.Label();
            this.btn_Create = new System.Windows.Forms.Button();
            this.btn_HideGridPanel = new System.Windows.Forms.Button();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(0, 63);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1356, 2);
            this.lblhorizentalline.TabIndex = 41;
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(12, 23);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(155, 29);
            this.lblheader.TabIndex = 40;
            this.lblheader.Text = "Office Days";
            // 
            // btn_Create
            // 
            this.btn_Create.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Create.ForeColor = System.Drawing.Color.Green;
            this.btn_Create.Location = new System.Drawing.Point(1172, 24);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(119, 34);
            this.btn_Create.TabIndex = 42;
            this.btn_Create.Text = "+Create";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // btn_HideGridPanel
            // 
            this.btn_HideGridPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_HideGridPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btn_HideGridPanel.ForeColor = System.Drawing.Color.Red;
            this.btn_HideGridPanel.Location = new System.Drawing.Point(1297, 24);
            this.btn_HideGridPanel.Name = "btn_HideGridPanel";
            this.btn_HideGridPanel.Size = new System.Drawing.Size(46, 34);
            this.btn_HideGridPanel.TabIndex = 42;
            this.btn_HideGridPanel.Text = "-";
            this.btn_HideGridPanel.UseVisualStyleBackColor = true;
            this.btn_HideGridPanel.Click += new System.EventHandler(this.btn_HideGridPanel_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(1171, 12);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(119, 25);
            this.btn_Search.TabIndex = 43;
            this.btn_Search.Text = "Search";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click_1);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(926, 13);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(239, 22);
            this.txtSearch.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(-1, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1356, 2);
            this.label1.TabIndex = 45;
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.dataGridView1);
            this.MainPanel.Controls.Add(this.btn_Search);
            this.MainPanel.Controls.Add(this.label1);
            this.MainPanel.Controls.Add(this.txtSearch);
            this.MainPanel.Location = new System.Drawing.Point(5, 69);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1351, 449);
            this.MainPanel.TabIndex = 47;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 41);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1345, 384);
            this.dataGridView1.TabIndex = 74;
            // 
            // IndexOfficeDays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1355, 642);
            this.ControlBox = false;
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.btn_HideGridPanel);
            this.Controls.Add(this.btn_Create);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.lblheader);
            this.Name = "IndexOfficeDays";
            this.Load += new System.EventHandler(this.IndexOfficeDays_Load);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.Button btn_HideGridPanel;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}