﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class frmFiscalYear : Form
    {
        IFiscalYearQueryProcessor iFiscalYearQueryProcessor = null;
        private string selectedEvent = "-1";
        private string selectedId = "-1";
       
        public frmFiscalYear()
        {
            InitializeComponent();

            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            BindCmbFicsalYear();
        }

        public frmFiscalYear(string selectId, string selectEvent)
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            BindCmbFicsalYear();
            selectedId = selectId;
            selectedEvent = selectEvent;
        }

        public void BindCmbFicsalYear()
        {
            var data = Dropdown.BindYears();
            var dt = General.ToDataTable(data);
            cmbFisicalYear.DataSource = dt;
            cmbFisicalYear.DisplayMember = "Text";
            cmbFisicalYear.ValueMember = "Value";
        }

        private void frmFiscalYear_Load(object sender, EventArgs e)
        {
            iFiscalYearQueryProcessor = new FiscalYearQueryProcessor();
            if (selectedEvent == "I")
            {
                btnCreate.Text = "Create";
                lblheader.Text = "Create Fiscal Year";
            }
            else if (selectedEvent == "V" && selectedId != "-1")
            {
                lblheader.Text = "View Fiscal Year";
                btnCreate.Visible = false;
                FiscalYear fiscalYear = null;
                fiscalYear = iFiscalYearQueryProcessor.GetById(Convert.ToInt32(selectedId));
                if (fiscalYear != null)
                {
                    SetFiscalYear(ref fiscalYear);
                    ReadOnlyFiscalYear();
                }

            }
            else if (selectedEvent == "U" && selectedId != "-1")
            {
                lblheader.Text = "Modify Fiscal Year";
                btnCreate.Text = "Save";
                FiscalYear fiscalYear = null;
                fiscalYear = iFiscalYearQueryProcessor.GetById(Convert.ToInt32(selectedId));
                if (fiscalYear != null)
                {
                    SetFiscalYear(ref fiscalYear);
                }
            }

        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GetFiscalYear(ref FiscalYear fiscalYear)
        {
            fiscalYear.StartDate = txtStartDate.Text.Trim();
            fiscalYear.EndDate = txtEndDate.Text.Trim();
            //fiscalYear.StartDateNepali = NepDateConverter.EngToNep(Convert.ToDateTime(fiscalYear.StartDate));
            //fiscalYear.EndDateNepali = NepDateConverter.EngToNep(Convert.ToDateTime(fiscalYear.EndDate));
        }

        private void ReadOnlyFiscalYear()
        {
            txtStartDate.ReadOnly=true;
            txtEndDate.ReadOnly = true;
        }

        private void SetFiscalYear(ref FiscalYear fiscalYear)
        {
            txtStartDate.Text = fiscalYear.StartDate;
            txtEndDate.Text = fiscalYear.EndDate;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            iFiscalYearQueryProcessor = new FiscalYearQueryProcessor();
            if (btnCreate.Text == "Create" && selectedEvent == "I")
            {
                FiscalYear fiscalYear = new FiscalYear();
                GetFiscalYear(ref fiscalYear);
                fiscalYear.CreateBy = 1;
                fiscalYear.CreateDate = System.DateTime.Now;
                fiscalYear.IsActive = true;
                fiscalYear.Event = 'I';
                fiscalYear.Id = 0;
                int a = iFiscalYearQueryProcessor.Create(fiscalYear);
                if (a > 0)
                {
                    message = "Fiscal Year Created Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot create Fiscal Year This Time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            else if (btnCreate.Text == "Save" && selectedEvent == "U")
            {
                FiscalYear fiscalYear = null;
                fiscalYear = iFiscalYearQueryProcessor.GetById(Convert.ToInt32(selectedId));
                GetFiscalYear(ref fiscalYear);
                fiscalYear.Event = 'U';
                fiscalYear.Id = Convert.ToInt32(selectedId);
                int a = iFiscalYearQueryProcessor.Create(fiscalYear);
                if (a > 0)
                {
                    message = "Update Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot Update this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

            }
        }

       
    }
}
