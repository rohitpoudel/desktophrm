﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class frmLateRule : Form
    {
        ILateRuleHRM iLateRuleHRM = null;

        private string selectedEvent = "-1";
        private string selectedId = "-1";
        public frmLateRule()
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
           
            BindLateinOnEarlyOut();
            BindFiscalYeal();

        }

        public frmLateRule(string selectId ,string selectEvent)
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);

            BindLateinOnEarlyOut();
            BindFiscalYeal();

            selectedId = selectId;
            selectedEvent = selectEvent;
        }

        #region ------ Bind dropdown --------

        public void BindFiscalYeal()
        {
            IFiscalYearQueryProcessor iFiscalYearQueryProcessor = null;
            iFiscalYearQueryProcessor = new FiscalYearQueryProcessor();
            var lstFiscalYeal = iFiscalYearQueryProcessor.GetAllData();
            cmbFisicalYear.DataSource = lstFiscalYeal;
            cmbFisicalYear.DisplayMember = "YearNepali";
            cmbFisicalYear.ValueMember = "id";
        }

        public void BindLateinOnEarlyOut()
        {
            cmbLateinOnEarlyOut.DataSource = BindLateRule();
            cmbLateinOnEarlyOut.DisplayMember = "Text";
            cmbLateinOnEarlyOut.ValueMember = "Value";
        }

        public class BindlateRule
        {
            public string Text { get; set; }
            public string Value { get; set; }

        }

        public List<BindlateRule> BindLateRule()
        {
            List<BindlateRule> lst = new List<BindlateRule>();
            lst.Add(new BindlateRule { Text = "--Select--", Value = "0" });
            lst.Add(new BindlateRule { Text = "Late In", Value = "LATEIN" });
            lst.Add(new BindlateRule { Text = "Early Out", Value = "EARLYOUT" });
            return lst;
        }

        #endregion

        private void frmLateRule_Load(object sender, EventArgs e)
        {
            iLateRuleHRM = new LateRuleHRM();
            if (selectedEvent == "I")
            {
                btnCreate.Text = "Create";
                lblheader.Text = "Create Late Rule";
            }
            else if (selectedEvent == "V" && selectedId != "-1")
            {
                lblheader.Text = "View Late Rule";
                btnCreate.Visible = false;
                LateRule lateRule = null;
                lateRule = iLateRuleHRM.GetById(Convert.ToInt32(selectedId));
                if (lateRule != null)
                {
                    SetLateRule(ref lateRule);
                    ReadOnlyLateRule();
                }

            }
            else if (selectedEvent == "U" && selectedId != "-1")
            {
                lblheader.Text = "Modify Late Rule";
                btnCreate.Text = "Save";
                LateRule lateRule = null;
                lateRule = iLateRuleHRM.GetById(Convert.ToInt32(selectedId));
                if (lateRule != null)
                {
                    SetLateRule(ref lateRule);
                }
            }

        }
        private void frmLateRule_Move(object sender, EventArgs e)
        {
            //var frm = new IndexLateRule();
            //this.Location = new Point(((frm.ClientSize.Width / 2) - (this.Width / 2)),
            //    (((frm.Height - 100) / 2) - (this.Height / 2)));
        }

        public void GetLateRule( ref LateRule lateRule)
        {
            lateRule.FiscalYearId =Convert.ToInt32( cmbFisicalYear.SelectedValue.ToString());
            lateRule.LateInOrEarlyOut = cmbLateinOnEarlyOut.SelectedValue.ToString();
            lateRule.DaysAllowed =Convert.ToInt32( txtDaysAllowed.Text.Trim());
            lateRule.LeaveOnLate = chkLeaveOnLate.Checked;
            lateRule.DeductionOnLate = chkSalaryDecuctionOnLate.Checked;

        }
        private void SetLateRule(ref LateRule lateRule)
        {
           // cmbFisicalYear.SelectedValue = 1;
            Int32 selectedFisicalYear = Convert.ToInt32( lateRule.FiscalYearId.ToString());
            cmbFisicalYear.SelectedValue = selectedFisicalYear;
            cmbLateinOnEarlyOut.SelectedValue = lateRule.LateInOrEarlyOut.ToString();
            txtDaysAllowed.Text = (lateRule.DaysAllowed ?? 0).ToString();
            chkLeaveOnLate.Checked = lateRule.LeaveOnLate;
            chkSalaryDecuctionOnLate.Checked = lateRule.DeductionOnLate;
        }
        public void ReadOnlyLateRule()
        {
            cmbFisicalYear.Enabled = false;
            cmbLateinOnEarlyOut.Enabled = false;
            txtDaysAllowed.ReadOnly = true;
            chkLeaveOnLate.Enabled = false;
            chkSalaryDecuctionOnLate.Enabled = false;
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
          
        }
        private void btn_Reset_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            iLateRuleHRM = new LateRuleHRM();
            if (btnCreate.Text == "Create" && selectedEvent == "I")
            {
                LateRule lateRule = new LateRule();
                GetLateRule(ref lateRule);
                lateRule.Event = 'I';
                lateRule.Created_By = 1;
                lateRule.Created_Date = DateTime.Now;
                int a = iLateRuleHRM.Create(lateRule);
                if (a > 0)
                {
                    message = "Created Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot create this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            else if (btnCreate.Text == "Save" && selectedEvent == "U")
            {
                LateRule lateRule = null;
                lateRule = iLateRuleHRM.GetById(Convert.ToInt32(selectedId));
                GetLateRule(ref lateRule);
                lateRule.Updated_By = 1;
                lateRule.Updated_Date = DateTime.Now;
                lateRule.Event = 'U';
                lateRule.Id = Convert.ToInt32(selectedId);
                int a = iLateRuleHRM.Create(lateRule);
                if (a > 0)
                {
                    message = "Update Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    message = "Cannot Update this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }


    }
}
