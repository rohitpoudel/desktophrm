﻿namespace HRM.Forms.SetUp
{
    partial class frmShift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comHalfDay = new System.Windows.Forms.ComboBox();
            this.comShiftType = new System.Windows.Forms.ComboBox();
            this.btn_Back = new System.Windows.Forms.Button();
            this.btn_Create = new System.Windows.Forms.Button();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.chkMarkAbsentWhenLateIn = new System.Windows.Forms.CheckBox();
            this.chkMarkAbsentWhenEarlyOut = new System.Windows.Forms.CheckBox();
            this.lblMarkAbsentWhenLateIn = new System.Windows.Forms.Label();
            this.lblMarkAbsentWhenEarlyOut = new System.Windows.Forms.Label();
            this.txtOverTimeFrom = new System.Windows.Forms.TextBox();
            this.lblOverTimeFrom = new System.Windows.Forms.Label();
            this.txtOverTimeHour = new System.Windows.Forms.TextBox();
            this.txtMaxLateInGrace = new System.Windows.Forms.TextBox();
            this.txtLateInGrace = new System.Windows.Forms.TextBox();
            this.txtLunchEnd = new System.Windows.Forms.TextBox();
            this.txtShiftHours = new System.Windows.Forms.TextBox();
            this.txtShiftStart = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMaxEarlyOutGrace = new System.Windows.Forms.TextBox();
            this.txtEarlyOutGrace = new System.Windows.Forms.TextBox();
            this.txtLunchStart = new System.Windows.Forms.TextBox();
            this.txtHalfDayHour = new System.Windows.Forms.TextBox();
            this.txtShiftEnd = new System.Windows.Forms.TextBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblOverTimeHour = new System.Windows.Forms.Label();
            this.lblMaxLateInGrace = new System.Windows.Forms.Label();
            this.lblLateInGrace = new System.Windows.Forms.Label();
            this.lblLunchEnd = new System.Windows.Forms.Label();
            this.lblHalfDay = new System.Windows.Forms.Label();
            this.lblShiftHours = new System.Windows.Forms.Label();
            this.lblShiftStart = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMaxEarlyOutGrace = new System.Windows.Forms.Label();
            this.lblEarlyOutGrace = new System.Windows.Forms.Label();
            this.lblLunchStart = new System.Windows.Forms.Label();
            this.lblHalfDayHour = new System.Windows.Forms.Label();
            this.lblShiftEnd = new System.Windows.Forms.Label();
            this.lblShiftType = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.lblheader = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(-10, 435);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1039, 2);
            this.label1.TabIndex = 46;
            // 
            // comHalfDay
            // 
            this.comHalfDay.FormattingEnabled = true;
            this.comHalfDay.Items.AddRange(new object[] {
            "-- Select --",
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thrusday",
            "Friday",
            "Saturday"});
            this.comHalfDay.Location = new System.Drawing.Point(693, 207);
            this.comHalfDay.Name = "comHalfDay";
            this.comHalfDay.Size = new System.Drawing.Size(245, 24);
            this.comHalfDay.TabIndex = 45;
            // 
            // comShiftType
            // 
            this.comShiftType.FormattingEnabled = true;
            this.comShiftType.Items.AddRange(new object[] {
            "-- Select Shift--",
            "Morning",
            "Day",
            "Night"});
            this.comShiftType.Location = new System.Drawing.Point(201, 140);
            this.comShiftType.Name = "comShiftType";
            this.comShiftType.Size = new System.Drawing.Size(245, 24);
            this.comShiftType.TabIndex = 44;
            // 
            // btn_Back
            // 
            this.btn_Back.Location = new System.Drawing.Point(443, 454);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(126, 39);
            this.btn_Back.TabIndex = 42;
            this.btn_Back.Text = "Cancel";
            this.btn_Back.UseVisualStyleBackColor = true;
            this.btn_Back.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // btn_Create
            // 
            this.btn_Create.Location = new System.Drawing.Point(310, 454);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(118, 39);
            this.btn_Create.TabIndex = 40;
            this.btn_Create.Text = "Create";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(-7, 74);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1036, 2);
            this.lblhorizentalline.TabIndex = 39;
            // 
            // chkMarkAbsentWhenLateIn
            // 
            this.chkMarkAbsentWhenLateIn.AutoSize = true;
            this.chkMarkAbsentWhenLateIn.Location = new System.Drawing.Point(763, 382);
            this.chkMarkAbsentWhenLateIn.Name = "chkMarkAbsentWhenLateIn";
            this.chkMarkAbsentWhenLateIn.Size = new System.Drawing.Size(18, 17);
            this.chkMarkAbsentWhenLateIn.TabIndex = 37;
            this.chkMarkAbsentWhenLateIn.UseVisualStyleBackColor = true;
            // 
            // chkMarkAbsentWhenEarlyOut
            // 
            this.chkMarkAbsentWhenEarlyOut.AutoSize = true;
            this.chkMarkAbsentWhenEarlyOut.Location = new System.Drawing.Point(235, 394);
            this.chkMarkAbsentWhenEarlyOut.Name = "chkMarkAbsentWhenEarlyOut";
            this.chkMarkAbsentWhenEarlyOut.Size = new System.Drawing.Size(18, 17);
            this.chkMarkAbsentWhenEarlyOut.TabIndex = 36;
            this.chkMarkAbsentWhenEarlyOut.UseVisualStyleBackColor = true;
            // 
            // lblMarkAbsentWhenLateIn
            // 
            this.lblMarkAbsentWhenLateIn.AutoSize = true;
            this.lblMarkAbsentWhenLateIn.Location = new System.Drawing.Point(555, 383);
            this.lblMarkAbsentWhenLateIn.Name = "lblMarkAbsentWhenLateIn";
            this.lblMarkAbsentWhenLateIn.Size = new System.Drawing.Size(171, 17);
            this.lblMarkAbsentWhenLateIn.TabIndex = 35;
            this.lblMarkAbsentWhenLateIn.Text = "Mark Absent When LateIn";
            // 
            // lblMarkAbsentWhenEarlyOut
            // 
            this.lblMarkAbsentWhenEarlyOut.AutoSize = true;
            this.lblMarkAbsentWhenEarlyOut.Location = new System.Drawing.Point(42, 396);
            this.lblMarkAbsentWhenEarlyOut.Name = "lblMarkAbsentWhenEarlyOut";
            this.lblMarkAbsentWhenEarlyOut.Size = new System.Drawing.Size(187, 17);
            this.lblMarkAbsentWhenEarlyOut.TabIndex = 34;
            this.lblMarkAbsentWhenEarlyOut.Text = "Mark Absent When EarlyOut";
            // 
            // txtOverTimeFrom
            // 
            this.txtOverTimeFrom.Location = new System.Drawing.Point(201, 359);
            this.txtOverTimeFrom.Name = "txtOverTimeFrom";
            this.txtOverTimeFrom.Size = new System.Drawing.Size(245, 22);
            this.txtOverTimeFrom.TabIndex = 33;
            // 
            // lblOverTimeFrom
            // 
            this.lblOverTimeFrom.AutoSize = true;
            this.lblOverTimeFrom.Location = new System.Drawing.Point(42, 361);
            this.lblOverTimeFrom.Name = "lblOverTimeFrom";
            this.lblOverTimeFrom.Size = new System.Drawing.Size(110, 17);
            this.lblOverTimeFrom.TabIndex = 32;
            this.lblOverTimeFrom.Text = "Over Time From";
            // 
            // txtOverTimeHour
            // 
            this.txtOverTimeHour.Location = new System.Drawing.Point(693, 351);
            this.txtOverTimeHour.Name = "txtOverTimeHour";
            this.txtOverTimeHour.Size = new System.Drawing.Size(245, 22);
            this.txtOverTimeHour.TabIndex = 31;
            // 
            // txtMaxLateInGrace
            // 
            this.txtMaxLateInGrace.Location = new System.Drawing.Point(693, 315);
            this.txtMaxLateInGrace.Name = "txtMaxLateInGrace";
            this.txtMaxLateInGrace.Size = new System.Drawing.Size(245, 22);
            this.txtMaxLateInGrace.TabIndex = 30;
            // 
            // txtLateInGrace
            // 
            this.txtLateInGrace.Location = new System.Drawing.Point(693, 279);
            this.txtLateInGrace.Name = "txtLateInGrace";
            this.txtLateInGrace.Size = new System.Drawing.Size(245, 22);
            this.txtLateInGrace.TabIndex = 29;
            // 
            // txtLunchEnd
            // 
            this.txtLunchEnd.Location = new System.Drawing.Point(693, 243);
            this.txtLunchEnd.Name = "txtLunchEnd";
            this.txtLunchEnd.Size = new System.Drawing.Size(245, 22);
            this.txtLunchEnd.TabIndex = 28;
            // 
            // txtShiftHours
            // 
            this.txtShiftHours.Location = new System.Drawing.Point(693, 171);
            this.txtShiftHours.Name = "txtShiftHours";
            this.txtShiftHours.Size = new System.Drawing.Size(245, 22);
            this.txtShiftHours.TabIndex = 26;
            // 
            // txtShiftStart
            // 
            this.txtShiftStart.Location = new System.Drawing.Point(693, 135);
            this.txtShiftStart.Name = "txtShiftStart";
            this.txtShiftStart.Size = new System.Drawing.Size(245, 22);
            this.txtShiftStart.TabIndex = 25;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(693, 99);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(245, 22);
            this.txtName.TabIndex = 24;
            // 
            // txtMaxEarlyOutGrace
            // 
            this.txtMaxEarlyOutGrace.Location = new System.Drawing.Point(201, 318);
            this.txtMaxEarlyOutGrace.Name = "txtMaxEarlyOutGrace";
            this.txtMaxEarlyOutGrace.Size = new System.Drawing.Size(245, 22);
            this.txtMaxEarlyOutGrace.TabIndex = 23;
            // 
            // txtEarlyOutGrace
            // 
            this.txtEarlyOutGrace.Location = new System.Drawing.Point(201, 282);
            this.txtEarlyOutGrace.Name = "txtEarlyOutGrace";
            this.txtEarlyOutGrace.Size = new System.Drawing.Size(245, 22);
            this.txtEarlyOutGrace.TabIndex = 22;
            // 
            // txtLunchStart
            // 
            this.txtLunchStart.Location = new System.Drawing.Point(201, 246);
            this.txtLunchStart.Name = "txtLunchStart";
            this.txtLunchStart.Size = new System.Drawing.Size(245, 22);
            this.txtLunchStart.TabIndex = 21;
            // 
            // txtHalfDayHour
            // 
            this.txtHalfDayHour.Location = new System.Drawing.Point(201, 210);
            this.txtHalfDayHour.Name = "txtHalfDayHour";
            this.txtHalfDayHour.Size = new System.Drawing.Size(245, 22);
            this.txtHalfDayHour.TabIndex = 20;
            // 
            // txtShiftEnd
            // 
            this.txtShiftEnd.Location = new System.Drawing.Point(201, 174);
            this.txtShiftEnd.Name = "txtShiftEnd";
            this.txtShiftEnd.Size = new System.Drawing.Size(245, 22);
            this.txtShiftEnd.TabIndex = 19;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(201, 99);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(245, 22);
            this.txtCode.TabIndex = 16;
            // 
            // lblOverTimeHour
            // 
            this.lblOverTimeHour.AutoSize = true;
            this.lblOverTimeHour.Location = new System.Drawing.Point(555, 353);
            this.lblOverTimeHour.Name = "lblOverTimeHour";
            this.lblOverTimeHour.Size = new System.Drawing.Size(109, 17);
            this.lblOverTimeHour.TabIndex = 15;
            this.lblOverTimeHour.Text = "Over Time Hour";
            // 
            // lblMaxLateInGrace
            // 
            this.lblMaxLateInGrace.AutoSize = true;
            this.lblMaxLateInGrace.Location = new System.Drawing.Point(555, 315);
            this.lblMaxLateInGrace.Name = "lblMaxLateInGrace";
            this.lblMaxLateInGrace.Size = new System.Drawing.Size(123, 17);
            this.lblMaxLateInGrace.TabIndex = 14;
            this.lblMaxLateInGrace.Text = "Max Late In Grace";
            // 
            // lblLateInGrace
            // 
            this.lblLateInGrace.AutoSize = true;
            this.lblLateInGrace.Location = new System.Drawing.Point(555, 281);
            this.lblLateInGrace.Name = "lblLateInGrace";
            this.lblLateInGrace.Size = new System.Drawing.Size(94, 17);
            this.lblLateInGrace.TabIndex = 13;
            this.lblLateInGrace.Text = "Late In Grace";
            // 
            // lblLunchEnd
            // 
            this.lblLunchEnd.AutoSize = true;
            this.lblLunchEnd.Location = new System.Drawing.Point(555, 243);
            this.lblLunchEnd.Name = "lblLunchEnd";
            this.lblLunchEnd.Size = new System.Drawing.Size(76, 17);
            this.lblLunchEnd.TabIndex = 12;
            this.lblLunchEnd.Text = "Lunch End";
            // 
            // lblHalfDay
            // 
            this.lblHalfDay.AutoSize = true;
            this.lblHalfDay.Location = new System.Drawing.Point(555, 212);
            this.lblHalfDay.Name = "lblHalfDay";
            this.lblHalfDay.Size = new System.Drawing.Size(62, 17);
            this.lblHalfDay.TabIndex = 11;
            this.lblHalfDay.Text = "Half Day";
            // 
            // lblShiftHours
            // 
            this.lblShiftHours.AutoSize = true;
            this.lblShiftHours.Location = new System.Drawing.Point(555, 174);
            this.lblShiftHours.Name = "lblShiftHours";
            this.lblShiftHours.Size = new System.Drawing.Size(78, 17);
            this.lblShiftHours.TabIndex = 10;
            this.lblShiftHours.Text = "Shift Hours";
            // 
            // lblShiftStart
            // 
            this.lblShiftStart.AutoSize = true;
            this.lblShiftStart.Location = new System.Drawing.Point(555, 140);
            this.lblShiftStart.Name = "lblShiftStart";
            this.lblShiftStart.Size = new System.Drawing.Size(70, 17);
            this.lblShiftStart.TabIndex = 9;
            this.lblShiftStart.Text = "Shift Start";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(555, 102);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Name";
            // 
            // lblMaxEarlyOutGrace
            // 
            this.lblMaxEarlyOutGrace.AutoSize = true;
            this.lblMaxEarlyOutGrace.Location = new System.Drawing.Point(42, 320);
            this.lblMaxEarlyOutGrace.Name = "lblMaxEarlyOutGrace";
            this.lblMaxEarlyOutGrace.Size = new System.Drawing.Size(139, 17);
            this.lblMaxEarlyOutGrace.TabIndex = 7;
            this.lblMaxEarlyOutGrace.Text = "Max Early Out Grace";
            // 
            // lblEarlyOutGrace
            // 
            this.lblEarlyOutGrace.AutoSize = true;
            this.lblEarlyOutGrace.Location = new System.Drawing.Point(42, 282);
            this.lblEarlyOutGrace.Name = "lblEarlyOutGrace";
            this.lblEarlyOutGrace.Size = new System.Drawing.Size(110, 17);
            this.lblEarlyOutGrace.TabIndex = 6;
            this.lblEarlyOutGrace.Text = "Early Out Grace";
            // 
            // lblLunchStart
            // 
            this.lblLunchStart.AutoSize = true;
            this.lblLunchStart.Location = new System.Drawing.Point(42, 248);
            this.lblLunchStart.Name = "lblLunchStart";
            this.lblLunchStart.Size = new System.Drawing.Size(81, 17);
            this.lblLunchStart.TabIndex = 5;
            this.lblLunchStart.Text = "Lunch Start";
            // 
            // lblHalfDayHour
            // 
            this.lblHalfDayHour.AutoSize = true;
            this.lblHalfDayHour.Location = new System.Drawing.Point(42, 210);
            this.lblHalfDayHour.Name = "lblHalfDayHour";
            this.lblHalfDayHour.Size = new System.Drawing.Size(97, 17);
            this.lblHalfDayHour.TabIndex = 4;
            this.lblHalfDayHour.Text = "Half Day Hour";
            // 
            // lblShiftEnd
            // 
            this.lblShiftEnd.AutoSize = true;
            this.lblShiftEnd.Location = new System.Drawing.Point(42, 179);
            this.lblShiftEnd.Name = "lblShiftEnd";
            this.lblShiftEnd.Size = new System.Drawing.Size(65, 17);
            this.lblShiftEnd.TabIndex = 3;
            this.lblShiftEnd.Text = "Shift End";
            // 
            // lblShiftType
            // 
            this.lblShiftType.AutoSize = true;
            this.lblShiftType.Location = new System.Drawing.Point(42, 140);
            this.lblShiftType.Name = "lblShiftType";
            this.lblShiftType.Size = new System.Drawing.Size(72, 17);
            this.lblShiftType.TabIndex = 1;
            this.lblShiftType.Text = "Shift Type";
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(42, 102);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(41, 17);
            this.lblCode.TabIndex = 0;
            this.lblCode.Text = "Code";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.lblheader);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 38);
            this.panel1.TabIndex = 69;
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.Red;
            this.btn_close.Location = new System.Drawing.Point(999, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 32);
            this.btn_close.TabIndex = 46;
            this.btn_close.Text = "X";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(3, 5);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(146, 29);
            this.lblheader.TabIndex = 41;
            this.lblheader.Text = "Create Shift";
            // 
            // frmShift
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 541);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comHalfDay);
            this.Controls.Add(this.comShiftType);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.btn_Back);
            this.Controls.Add(this.lblShiftType);
            this.Controls.Add(this.lblShiftEnd);
            this.Controls.Add(this.btn_Create);
            this.Controls.Add(this.lblHalfDayHour);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.lblLunchStart);
            this.Controls.Add(this.lblEarlyOutGrace);
            this.Controls.Add(this.chkMarkAbsentWhenLateIn);
            this.Controls.Add(this.lblMaxEarlyOutGrace);
            this.Controls.Add(this.chkMarkAbsentWhenEarlyOut);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblMarkAbsentWhenLateIn);
            this.Controls.Add(this.lblShiftStart);
            this.Controls.Add(this.lblMarkAbsentWhenEarlyOut);
            this.Controls.Add(this.lblShiftHours);
            this.Controls.Add(this.txtOverTimeFrom);
            this.Controls.Add(this.lblHalfDay);
            this.Controls.Add(this.lblOverTimeFrom);
            this.Controls.Add(this.lblLunchEnd);
            this.Controls.Add(this.txtOverTimeHour);
            this.Controls.Add(this.lblLateInGrace);
            this.Controls.Add(this.txtMaxLateInGrace);
            this.Controls.Add(this.lblMaxLateInGrace);
            this.Controls.Add(this.txtLateInGrace);
            this.Controls.Add(this.lblOverTimeHour);
            this.Controls.Add(this.txtLunchEnd);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.txtShiftHours);
            this.Controls.Add(this.txtShiftEnd);
            this.Controls.Add(this.txtShiftStart);
            this.Controls.Add(this.txtHalfDayHour);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtLunchStart);
            this.Controls.Add(this.txtMaxEarlyOutGrace);
            this.Controls.Add(this.txtEarlyOutGrace);
            this.Name = "frmShift";
            this.Load += new System.EventHandler(this.frmShift_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblOverTimeHour;
        private System.Windows.Forms.Label lblMaxLateInGrace;
        private System.Windows.Forms.Label lblLateInGrace;
        private System.Windows.Forms.Label lblLunchEnd;
        private System.Windows.Forms.Label lblHalfDay;
        private System.Windows.Forms.Label lblShiftHours;
        private System.Windows.Forms.Label lblShiftStart;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMaxEarlyOutGrace;
        private System.Windows.Forms.Label lblEarlyOutGrace;
        private System.Windows.Forms.Label lblLunchStart;
        private System.Windows.Forms.Label lblHalfDayHour;
        private System.Windows.Forms.Label lblShiftEnd;
        private System.Windows.Forms.Label lblShiftType;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox txtOverTimeHour;
        private System.Windows.Forms.TextBox txtMaxLateInGrace;
        private System.Windows.Forms.TextBox txtLateInGrace;
        private System.Windows.Forms.TextBox txtLunchEnd;
        private System.Windows.Forms.TextBox txtShiftHours;
        private System.Windows.Forms.TextBox txtShiftStart;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMaxEarlyOutGrace;
        private System.Windows.Forms.TextBox txtEarlyOutGrace;
        private System.Windows.Forms.TextBox txtLunchStart;
        private System.Windows.Forms.TextBox txtHalfDayHour;
        private System.Windows.Forms.TextBox txtShiftEnd;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.TextBox txtOverTimeFrom;
        private System.Windows.Forms.Label lblOverTimeFrom;
        private System.Windows.Forms.Label lblMarkAbsentWhenLateIn;
        private System.Windows.Forms.Label lblMarkAbsentWhenEarlyOut;
        private System.Windows.Forms.CheckBox chkMarkAbsentWhenLateIn;
        private System.Windows.Forms.CheckBox chkMarkAbsentWhenEarlyOut;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.Button btn_Back;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.ComboBox comShiftType;
        private System.Windows.Forms.ComboBox comHalfDay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lblheader;
    }
}