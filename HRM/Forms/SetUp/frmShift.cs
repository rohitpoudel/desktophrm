﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class frmShift : Form
    {
        IShiftHRM iShiftHRM = null;
        private string selectedEvent = "-1";
        private string selectedId = "-1";

        public frmShift()
        {
            InitializeComponent();
            comShiftType.SelectedIndex = 0;
            comHalfDay.SelectedIndex = 0;
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
        }

        public frmShift(string selectId, string selectEvent)
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            comShiftType.SelectedIndex = 0;
            comHalfDay.SelectedIndex = 0;
            selectedId = selectId;
            selectedEvent = selectEvent;
        }

        private void frmShift_Load(object sender, EventArgs e)
        {
            if (selectedEvent == "I")
            {
                btn_Create.Text = "Create";
                lblheader.Text = "Create Shift";
            }
            else if (selectedId != "-1")
            {
                iShiftHRM = new ShiftHRM();
                Shift shift = null;
                shift = iShiftHRM.GetById(Convert.ToInt32(selectedId));
                if (shift != null)
                {
                    SetShift(ref shift);
                    if (selectedEvent == "V")
                    {
                        lblheader.Text = "View Shift";
                        btn_Create.Visible = false;
                        ReadOnlyshift();
                    }
                    else if (selectedEvent == "U")
                    {
                        lblheader.Text = "Modify Shift";
                        btn_Create.Text = "Save";
                    }
                }
            }
        }

        private void ReadOnlyshift()
        {
            txtCode.ReadOnly = true;
            txtLateInGrace.ReadOnly = true;
            txtLunchEnd.ReadOnly = true;
            txtLunchStart.ReadOnly = true;
            txtMaxEarlyOutGrace.ReadOnly = true;
            txtMaxLateInGrace.ReadOnly = true;
            txtName.ReadOnly = true;
            txtOverTimeFrom.ReadOnly = true;
            txtOverTimeHour.ReadOnly = true;
            txtShiftEnd.ReadOnly = true;
            txtShiftHours.ReadOnly = true;
            txtShiftStart.ReadOnly = true;
            comHalfDay.Enabled = false;
            comShiftType.Enabled = false;
            chkMarkAbsentWhenEarlyOut.Enabled = false;
            chkMarkAbsentWhenLateIn.Enabled = false;
            txtHalfDayHour.ReadOnly = true;
            txtEarlyOutGrace.ReadOnly = true;

        }

        public void GetShift(ref Shift shift)
        {

            shift.Code = txtCode.Text.Trim();
            shift.LateGrace = txtLateInGrace.Text.Trim();
            shift.LunchEnd = txtLunchEnd.Text.Trim();
            shift.LunchStart = txtLunchStart.Text.Trim();
            shift.MaxEarlyGrace = txtMaxEarlyOutGrace.Text.Trim();
            shift.MaxLateGrace = txtMaxLateInGrace.Text.Trim();
            shift.Name = txtName.Text.Trim();
            shift.OverTimeFrom = txtOverTimeFrom.Text.Trim();
            shift.OverTimeHour = txtOverTimeHour.Text.Trim();
            shift.ShiftEnd = txtShiftEnd.Text.Trim();
            shift.ShiftHours = txtShiftHours.Text.Trim();
            shift.ShiftStart = txtShiftStart.Text.Trim();
            shift.HalfDayOff = comHalfDay.SelectedText;
            shift.ShiftType = comShiftType.SelectedText;
            shift.MarkAbsentWhenEarlyOut = chkMarkAbsentWhenEarlyOut.Checked;
            shift.MarkAbsentWhenLateIn = chkMarkAbsentWhenLateIn.Checked;
            shift.HalfDayWorkingHour = txtHalfDayHour.Text.Trim();
            shift.EarlyGrace = txtEarlyOutGrace.Text.Trim();

            //shift.ShiftStartGrace = txtShiftStartGrace.Text.Trim();
            //shift.StartMonth = txtStartMonth.Text.Trim();
            //shift.StartGraceDays = txtStartGraceDays.Text.Trim();
            //shift.ShiftEndGrace = txtShiftEndGrace.Text.Trim();
            //shift.EndMonth = txtEndMonth.Text.Trim();
            //shift.EndGraceDays = txtEndGraceDays.Text.Trim();
            //shift.LateIn = txtMaxLateInGrace.Text.Trim();
            //shift.EarlyOut = txtOutGrace.Text.Trim();
            //shift.NoOfStaff =

        }

        private void SetShift(ref Shift shift)
        {
            txtCode.Text = shift.Code;
            txtLateInGrace.Text = shift.LateGrace;
            txtLunchEnd.Text = shift.LunchEnd;
            txtLunchStart.Text = shift.LunchStart;
            txtMaxEarlyOutGrace.Text = shift.MaxEarlyGrace;
            txtMaxLateInGrace.Text = shift.MaxLateGrace;
            txtName.Text = shift.Name;
            txtOverTimeFrom.Text = shift.OverTimeFrom;
            txtOverTimeHour.Text = shift.OverTimeHour;
            txtShiftEnd.Text = shift.ShiftEnd;
            txtShiftHours.Text = shift.ShiftHours;
            txtShiftStart.Text = shift.ShiftStart;
            comHalfDay.SelectedText = shift.HalfDayOff;
            comShiftType.SelectedText = shift.ShiftType;
            chkMarkAbsentWhenEarlyOut.Checked = shift.MarkAbsentWhenEarlyOut;
            chkMarkAbsentWhenLateIn.Checked = shift.MarkAbsentWhenLateIn;
            txtHalfDayHour.Text = shift.HalfDayWorkingHour;
            txtEarlyOutGrace.Text = shift.EarlyGrace;
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void btn_Create_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            iShiftHRM = new ShiftHRM();
            if (btn_Create.Text == "Create" && selectedEvent == "I")
            {
                Shift shift = new Shift();
                GetShift(ref shift);
                shift.CreateBy = 1;
                shift.CreateDate = System.DateTime.Now;
                shift.IsDeleted = false;
                shift.Event = 'I';
                shift.Id = 0;
                int a = iShiftHRM.Create(shift);
                if (a > 0)
                {
                    message = "Shift Created Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot create shift This Time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            else if (btn_Create.Text == "Save" && selectedEvent == "U")
            {
                Shift shift = null;
                shift = iShiftHRM.GetById(Convert.ToInt32(selectedId));
                GetShift(ref shift);
                shift.Event = 'U';
                shift.Id = Convert.ToInt32(selectedId);
                int a = iShiftHRM.Create(shift);
                if (a > 0)
                {
                    message = "Update Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot Update this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }
     
    }
}
