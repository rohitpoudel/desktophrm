﻿namespace HRM.Forms.SetUp
{
    partial class frmOfficeDays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblhorizentalline = new System.Windows.Forms.Label();
            this.cmbFicsalYear = new System.Windows.Forms.ComboBox();
            this.lblSalaryDeductionOnLate = new System.Windows.Forms.Label();
            this.lblLateInOrEarlyOut = new System.Windows.Forms.Label();
            this.cmbDaysInMonth = new System.Windows.Forms.ComboBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.lblheader = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(501, 192);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(113, 36);
            this.btn_Reset.TabIndex = 65;
            this.btn_Reset.Text = "Cancel";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(367, 192);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(113, 36);
            this.btnCreate.TabIndex = 66;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(-3, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1047, 1);
            this.label1.TabIndex = 64;
            // 
            // lblhorizentalline
            // 
            this.lblhorizentalline.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblhorizentalline.Location = new System.Drawing.Point(-6, 46);
            this.lblhorizentalline.Name = "lblhorizentalline";
            this.lblhorizentalline.Size = new System.Drawing.Size(1050, 1);
            this.lblhorizentalline.TabIndex = 63;
            // 
            // cmbFicsalYear
            // 
            this.cmbFicsalYear.FormattingEnabled = true;
            this.cmbFicsalYear.Location = new System.Drawing.Point(394, 81);
            this.cmbFicsalYear.Name = "cmbFicsalYear";
            this.cmbFicsalYear.Size = new System.Drawing.Size(247, 24);
            this.cmbFicsalYear.TabIndex = 60;
            // 
            // lblSalaryDeductionOnLate
            // 
            this.lblSalaryDeductionOnLate.AutoSize = true;
            this.lblSalaryDeductionOnLate.Location = new System.Drawing.Point(233, 127);
            this.lblSalaryDeductionOnLate.Name = "lblSalaryDeductionOnLate";
            this.lblSalaryDeductionOnLate.Size = new System.Drawing.Size(98, 17);
            this.lblSalaryDeductionOnLate.TabIndex = 58;
            this.lblSalaryDeductionOnLate.Text = "Days In Month";
            // 
            // lblLateInOrEarlyOut
            // 
            this.lblLateInOrEarlyOut.AutoSize = true;
            this.lblLateInOrEarlyOut.Location = new System.Drawing.Point(233, 88);
            this.lblLateInOrEarlyOut.Name = "lblLateInOrEarlyOut";
            this.lblLateInOrEarlyOut.Size = new System.Drawing.Size(78, 17);
            this.lblLateInOrEarlyOut.TabIndex = 59;
            this.lblLateInOrEarlyOut.Text = "Fiscal Year";
            // 
            // cmbDaysInMonth
            // 
            this.cmbDaysInMonth.FormattingEnabled = true;
            this.cmbDaysInMonth.Location = new System.Drawing.Point(394, 120);
            this.cmbDaysInMonth.Name = "cmbDaysInMonth";
            this.cmbDaysInMonth.Size = new System.Drawing.Size(247, 24);
            this.cmbDaysInMonth.TabIndex = 60;
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.Red;
            this.btn_close.Location = new System.Drawing.Point(999, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 32);
            this.btn_close.TabIndex = 46;
            this.btn_close.Text = "X";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lblheader
            // 
            this.lblheader.AutoSize = true;
            this.lblheader.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblheader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblheader.Location = new System.Drawing.Point(3, 5);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(226, 29);
            this.lblheader.TabIndex = 41;
            this.lblheader.Text = "Create Office Days";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.lblheader);
            this.panel1.Location = new System.Drawing.Point(4, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 38);
            this.panel1.TabIndex = 67;
            // 
            // frmOfficeDays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 353);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblhorizentalline);
            this.Controls.Add(this.cmbDaysInMonth);
            this.Controls.Add(this.cmbFicsalYear);
            this.Controls.Add(this.lblSalaryDeductionOnLate);
            this.Controls.Add(this.lblLateInOrEarlyOut);
            this.Name = "frmOfficeDays";
            this.Load += new System.EventHandler(this.frmOfficeDays_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblhorizentalline;
        private System.Windows.Forms.ComboBox cmbFicsalYear;
        private System.Windows.Forms.Label lblSalaryDeductionOnLate;
        private System.Windows.Forms.Label lblLateInOrEarlyOut;
        private System.Windows.Forms.ComboBox cmbDaysInMonth;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Panel panel1;
    }
}