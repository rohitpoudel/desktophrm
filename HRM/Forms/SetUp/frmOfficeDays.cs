﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class frmOfficeDays : Form
    {
        private string selectedEvent = "-1";
        private string selectedId = "-1";

        IOfficeDaysHRM iOfficeDaysHRM = null;

        public frmOfficeDays()
        {
            InitializeComponent();

            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            BindCmbFicsalYear();
        }

        public frmOfficeDays(string selectId, string selectEvent)
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            BindCmbFicsalYear();
            selectedId = selectId;
            selectedEvent = selectEvent;
        }

        private void BindCmbFicsalYear()
        {
            var data = Dropdown.BindYears();
            var dt = General.ToDataTable(data);
            cmbFicsalYear.DataSource = dt;
            cmbFicsalYear.DisplayMember = "Text";
            cmbFicsalYear.ValueMember = "Value";
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
     
        private void GetOfficeDays(ref OfficeDays officeDays)
        {
            officeDays.FiscalYearId = Convert.ToInt32(cmbFicsalYear.SelectedValue);
            officeDays.DaysInMonth = cmbDaysInMonth.SelectedValue.ToString();
        }
      
        private void ReadOnlyOfficeDays()
        {
            cmbFicsalYear.Enabled=false ;
            cmbDaysInMonth.Enabled = false;

        }

        private void SetOfficeDays(ref OfficeDays officeDays)
        {
            cmbFicsalYear.SelectedValue = officeDays.FiscalYearId;
            cmbDaysInMonth.SelectedValue = officeDays.DaysInMonth;
        }

        private void frmOfficeDays_Load(object sender, EventArgs e)
        {
            iOfficeDaysHRM = new OfficeDaysHRM();
            if (selectedEvent == "I")
            {
                btnCreate.Text = "Create";
                lblheader.Text = "Create Office Days";
            }
            else if (selectedEvent == "V" && selectedId != "-1")
            {
                lblheader.Text = "View Office Days";
                btnCreate.Visible = false;
                OfficeDays officeDays = null;
                officeDays = iOfficeDaysHRM.GetById(Convert.ToInt32(selectedId));
                if (officeDays != null)
                {
                    SetOfficeDays(ref officeDays);
                    ReadOnlyOfficeDays();
                }

            }
            else if (selectedEvent == "U" && selectedId != "-1")
            {
                lblheader.Text = "Modify Office Days";
                btnCreate.Text = "Save";
                OfficeDays officeDays = null;
                officeDays = iOfficeDaysHRM.GetById(Convert.ToInt32(selectedId));
                if (officeDays != null)
                {
                    SetOfficeDays(ref officeDays);
                }
            }

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            iOfficeDaysHRM = new OfficeDaysHRM();
            if (btnCreate.Text == "Create" && selectedEvent == "I")
            {
                OfficeDays officeDays = new OfficeDays();
                GetOfficeDays(ref officeDays);
                officeDays.Created_By = 1;
                officeDays.Created_Date = System.DateTime.Now;
                officeDays.Is_Active = true;
                officeDays.Event = 'I';
                officeDays.Id = 0;
                int a = iOfficeDaysHRM.Create(officeDays);
                if (a > 0)
                {
                    message = "Office Days Created Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot create Office Days This Time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            else if (btnCreate.Text == "Save" && selectedEvent == "U")
            {
                OfficeDays officeDays = null;
                officeDays = iOfficeDaysHRM.GetById(Convert.ToInt32(selectedId));
                GetOfficeDays(ref officeDays);
                officeDays.Updated_By = 1;
                officeDays.Updated_Date = DateTime.Now;
                officeDays.Event = 'U';
                officeDays.Id = Convert.ToInt32(selectedId);
                int a = iOfficeDaysHRM.Create(officeDays);
                if (a > 0)
                {
                    message = "Update Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot Update this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

            }
        }


    }
}
