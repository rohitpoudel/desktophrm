﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRM.Forms.SetUp
{
    public partial class frmOverTimeSetUp : Form
    {
        IOverTimeRuleHRM iOverTimeRuleHRM = null;
        private string selectedEvent = "-1";
        private string selectedId = "-1";
        
        public frmOverTimeSetUp()
        {
            InitializeComponent();
            BindCmbFicsalYear();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
        }

        public frmOverTimeSetUp(string selectId, string selectEvent)
        {
            InitializeComponent();
            int[] aa = General.GetColorSetUp1();
            this.BackColor = System.Drawing.Color.FromArgb(aa[0], aa[1], aa[2]);
            BindCmbFicsalYear();
            selectedId = selectId;
            selectedEvent = selectEvent;
        }

        private void BindCmbFicsalYear()
        {
            var data = Dropdown.BindYears();
            var dt = General.ToDataTable(data);
            cmbFicsalYear.DataSource = dt;
            cmbFicsalYear.DisplayMember = "Text";
            cmbFicsalYear.ValueMember = "Value";
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmOverTimeSetUp_Load(object sender, EventArgs e)
        {
            if (selectedEvent == "I")
            {
                btnCreate.Text = "Create";
                lblheader.Text = "Create Office Days";
            }
            else if (selectedId != "-1")
            {
                iOverTimeRuleHRM = new OverTimeRuleHRM();
                OverTimeRule overTimeRule = null;
                overTimeRule = iOverTimeRuleHRM.GetById(Convert.ToInt32(selectedId));
                if (overTimeRule != null)
                {
                    SetOverTimeRule(ref overTimeRule);
                    if (selectedEvent == "V")
                    {
                        lblheader.Text = "View Office Days";
                        btnCreate.Visible = false;
                        ReadOnlyOverTimeRule();
                    }
                    else if (selectedEvent == "U")
                    {
                        lblheader.Text = "Modify Office Days";
                        btnCreate.Text = "Save";
                    }
                }
            }
            
        }
        
        private void GetOverTimeRule(ref OverTimeRule overTimeRule)
        {
            overTimeRule.FiscalYearId = Convert.ToInt32(cmbFicsalYear.SelectedValue);
            overTimeRule.OverTimeAllowed = chkOverTimeAllowed.Checked;
        }
       
        private void ReadOnlyOverTimeRule()
        {
            cmbFicsalYear.Enabled = false;
            chkOverTimeAllowed.Enabled = false;
        }
       
        private void SetOverTimeRule(ref OverTimeRule overTimeRule)
        {
            cmbFicsalYear.SelectedValue = overTimeRule.FiscalYearId;
            chkOverTimeAllowed.Checked = overTimeRule.OverTimeAllowed;

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            iOverTimeRuleHRM = new OverTimeRuleHRM();
            if (btnCreate.Text == "Create" && selectedEvent == "I")
            {
                OverTimeRule overTimeRule = new OverTimeRule();
                GetOverTimeRule(ref overTimeRule);
                overTimeRule.Created_By = 1;
                overTimeRule.Created_Date = System.DateTime.Now;
                overTimeRule.Is_Active = true;
                overTimeRule.Event = 'I';
                overTimeRule.Id = 0;
                int a = iOverTimeRuleHRM.Create(overTimeRule);
                if (a > 0)
                {
                    message = "Over Time Created Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot create Over Time This Time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            else if (btnCreate.Text == "Save" && selectedEvent == "U")
            {
                OverTimeRule overTimeRule = null;
                overTimeRule = iOverTimeRuleHRM.GetById(Convert.ToInt32(selectedId));
                GetOverTimeRule(ref overTimeRule);
                overTimeRule.Updated_By = 1;
                overTimeRule.Updated_Date = DateTime.Now;
                overTimeRule.Event = 'U';
                overTimeRule.Id = Convert.ToInt32(selectedId);
                int a = iOverTimeRuleHRM.Create(overTimeRule);
                if (a > 0)
                {
                    message = "Update Successfully!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    message = "Cannot Update this time!";
                    MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }


    }
}
