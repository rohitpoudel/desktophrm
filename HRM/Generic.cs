﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Linq;
using System.Globalization;
using System.Security.Cryptography;
using System.IO;

namespace HRM
{
    public static class Generic
    {



        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "DigitalNepal";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())

            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "DigitalNepal";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public static DataTable ConvertToDataTable<T>(this IEnumerable<T> data)
        {
            List<T> list = data.Cast<T>().ToList();

            PropertyDescriptorCollection props = null;
            DataTable table = new DataTable();
            if (list != null && list.Count > 0)
            {
                props = TypeDescriptor.GetProperties(list[0]);
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            if (props != null)
            {
                object[] values = new object[props.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            return table;
        }
        public static string ConvertDateFormat(string Date)
        {
            string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
            if (Date.Length > 10)
            {
                if (Date.Contains("AM") || Date.Contains("PM"))
                {
                    Date = Date.Substring(0, Date.Length - 11);
                }
                else
                {
                    Date = Date.Substring(0, Date.Length - 8);
                }
            }
            string[] date = Date.Split('/').ToArray();


            if (sysFormat == "M/d/yyyy")
            {
                Date = date[2] + "-" + date[0] + "-" + date[1];
            }
            else
            {
                Date = date[2] + "-" + date[1] + "-" + date[0];
            }




            return Date;
        }
        public static string DifferenceInHourMins(string Time1, string Time2)

        {
            if (string.IsNullOrEmpty(Time1))
            {
                Time1 = "00:00";
            }
            if (string.IsNullOrEmpty(Time2))
            {
                Time2 = "00:00";
            }
            TimeSpan t1 = TimeSpan.Parse(Time1);
            TimeSpan t2 = TimeSpan.Parse(Time2);
            t1.Subtract(t2).ToString();

            return t1.Subtract(t2).ToString();
        }
  
       

        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item) ?? DBNull.Value;
                table.Rows.Add(values);
            }
            return table;
        }
        public static int GetWeekNumber(string date)
        {
            DateTime dtime = Convert.ToDateTime(date);
            int count = 0;
            if (dtime.Day <= 7)
            {
                count = 1;
            }
            else if (dtime.Day > 7 && dtime.Day <= 14)
            {
                count = 2;
            }
            else if (dtime.Day > 14 && dtime.Day <= 21)
            {
                count = 3;
            }
            else if (dtime.Day > 21 && dtime.Day <= 28)
            {
                count = 4;
            }
            else if (dtime.Day > 28)
            {
                count = 5;
            }
            return count;
        }
    }
}
