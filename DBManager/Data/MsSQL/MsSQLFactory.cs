using Microsoft.Extensions.Configuration;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace HRM
{
	/// <summary>
	/// 
	/// </summary>
	public class MsSQLFactory : IDatabaseFactory
    {
        public IDbConnection Db { get; set; }
        public Dialect Dialect => Dialect.SQLServer;

        public QueryBuilder QueryBuilder { get; }

        private readonly string _connectionString;
		public	MsSQLFactory()
		{ }
        //public IDbConnection GetConnectionByCompany(string cId)
        //{
        //    // var ss= new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
        //    //var tt = new SqlConnection(General.BuildDyanmicString("003").ToString());
        //    //ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        //    Db = new SqlConnection(General.BuildDyanmicStringForIndCompany((string)cId).ToString());

        //    //Db =new SqlConnection(General.BuildDyanmicString("003").ToString());

        //    return Db;
        //}
        public IDbConnection GetConnection()
        {
            var Db= new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            ////var tt = new SqlConnection(General.BuildDyanmicString("003").ToString());
            ////ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            //Db = new SqlConnection(General.BuildDyanmicString((string)HttpContext.Current.Session["CompanyCode"]).ToString());

            //Db =new SqlConnection(General.BuildDyanmicString("003").ToString());

            return Db;
        }

        public MsSQLFactory(IConfiguration configuration, IServiceProvider serviceProvider)

        {
            //IConfiguration configuration
            // connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
            _connectionString = configuration.GetConnectionString("DefaultConnection");// ConfigurationManager.ConnectionStrings[connectionStringName].ToString();
            Db = new SqlConnection(_connectionString);
            QueryBuilder = new MsSqlQueryBuilder(new MsSQLTemplate());
            //var hostingenv = serviceProvider.GetService<IHostingEnvironment>();
            //DbLogger = new DbLogger(hostingenv,new DefaultDbLoggerSetting());
        }

        public void Dispose()
        {
            if (Db.State == ConnectionState.Open)
                Db.Close();
            Db.Close();
            //db.Dispose();
        }

       // public ILogger DbLogger { get; set; }
    }


}