using System;

namespace HRM
{
    public interface ITableNameResolver
    {
        string ResolveTableName(Type type);
    }
}