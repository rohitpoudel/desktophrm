﻿using System;

namespace HRM
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreAllAttribute : System.Attribute
    {
    }
}