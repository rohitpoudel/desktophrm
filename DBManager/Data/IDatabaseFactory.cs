using System;
using System.Data;



namespace HRM
{
    public interface IDatabaseFactory : IDisposable
    {
        IDbConnection Db { get; }
        Dialect Dialect { get; }
        QueryBuilder QueryBuilder { get; }
       IDbConnection GetConnection();
       /// IDbConnection GetConnectionByCompany(string cId);
       // ILogger DbLogger { get; set; }
     }
}